float cuadrivelocidad[4], cuadriaceleracion[4], vectores[4][4], camara_w[3] = {0.f, 0.f, 1.f}, camara_angulo = 0.f;
float vectores_inv[4][4], *cam_matrix, *cam_matrix_inv;
float parametro = 0.f, camara_rotacion_angulo1 = 0.f, camara_rotacion_angulo2 = 0.f, camara_rotacion_angulo3 = 0.f, proper_time = 0.f;
int count, mx_anterior = 0, my_anterior = 0;


float producto_punto(float posicion[4], float vector1[4], float vector2[4]){
	float suma = 0.f;
	for(unsigned int i = 0; i < 4; i++)
		suma += vector1[i]*vector2[i]*metrica_metric(posicion[1], posicion[2], i);
	return suma;
}

float producto_punto3(float posicion[3], float vector1[3], float vector2[3]){
	float suma = 0.f;
	for(unsigned int i = 0; i < 3; i++)
		suma += vector1[i]*vector2[i]*metrica_metric(posicion[0], posicion[1], i + 1);
	return suma;
}


float camara_get_velocity(float posicion[4], float cuadrivelocidad[4]){
	float v, dT, dX, g0, g1, g2, g3, rho, theta;
	
	rho   = posicion[1];
	theta = posicion[2];
	g0 = metrica_metric(rho, theta, 0);
	g1 = metrica_metric(rho, theta, 1);
	g2 = metrica_metric(rho, theta, 2);
	g3 = metrica_metric(rho, theta, 3);
	
	dT = cuadrivelocidad[0]*sqrtf(-g0)/light_speed;
	
	dX = sqrtf(g1*powf(cuadrivelocidad[1], 2) + g2*powf(cuadrivelocidad[2], 2) + g3*powf(cuadrivelocidad[3], 2));
	v = dX/dT;
	return v/light_speed;
}


void gram_schimdt(float posicion[4]){
	int i;
	float modulo, modulo2, modulo3;
	modulo = sqrtf(-producto_punto(posicion, vectores[0], vectores[0]));
	for(i = 0;i < 4; i++) vectores[0][i] = vectores[0][i]/modulo;

	modulo = (producto_punto(posicion, vectores[0], vectores[1]));
	for(i = 0;i < 4; i++) vectores[1][i] = vectores[1][i] + modulo*vectores[0][i];
	modulo = sqrtf(producto_punto(posicion, vectores[1], vectores[1]));
	for(i = 0;i < 4; i++) vectores[1][i] = vectores[1][i]/modulo;

	modulo = (producto_punto(posicion, vectores[2], vectores[0]));
	modulo2 = (producto_punto(posicion, vectores[2], vectores[1]));
	for(i = 0;i < 4; i++) vectores[2][i] = vectores[2][i] + modulo*vectores[0][i] - modulo2*vectores[1][i];
	modulo = sqrtf(producto_punto(posicion, vectores[2], vectores[2]));
	for(i = 0;i < 4; i++) vectores[2][i] = vectores[2][i]/modulo;

	modulo = (producto_punto(posicion, vectores[3], vectores[0]));
	modulo2 = (producto_punto(posicion, vectores[3], vectores[1]));
	modulo3 = (producto_punto(posicion, vectores[3], vectores[2]));

	for(i = 0;i < 4; i++) vectores[3][i] = vectores[3][i] + modulo*vectores[0][i] - modulo2*vectores[1][i] - modulo3*vectores[2][i];
	modulo = sqrtf(producto_punto(posicion, vectores[3], vectores[3]));
	for(i = 0;i < 4; i++) vectores[3][i] = vectores[3][i]/modulo;
}

void camara_print_vectors(float vectores[4][4]){
	printf("Vectors\n");
	printf("(%f, %f, %f, %f)\n", vectores[0][0], vectores[0][1], vectores[0][2], vectores[0][3]);
	printf("(%f, %f, %f, %f)\n", vectores[1][0], vectores[1][1], vectores[1][2], vectores[1][3]);
	printf("(%f, %f, %f, %f)\n", vectores[2][0], vectores[2][1], vectores[2][2], vectores[2][3]);
	printf("(%f, %f, %f, %f)\n", vectores[3][0], vectores[3][1], vectores[3][2], vectores[3][3]);
}


float christoffel(float posicion[4], unsigned int gamma, unsigned int alpha, unsigned int beta){
	//Checked
	if(gamma == 0){
		if(((alpha == 0)&(beta == 1))||((alpha == 1)&(beta == 0)))
			return metrica_B_d(posicion[1])*0.5f/metrica_B(posicion[1]);
	else
		return 0.f;
	}
	else if(gamma == 1){
		if((alpha == 0)&(beta == 0))
			return powf(light_speed, 2)*metrica_B_d(posicion[1])*0.5f/metrica_A(posicion[1]);

		else if((alpha == 1)&(beta == 1))
			return metrica_A_d(posicion[1])/metrica_A(posicion[1]);
		
		else if((alpha == 2)&(beta == 2))
			return -metrica_r(posicion[1])*metrica_r_d(posicion[1])/metrica_A(posicion[1]);

		else if((alpha == 3)&(beta== 3))
			return -metrica_r(posicion[1])*metrica_r_d(posicion[1])*powf(sinf(posicion[2]), 2)/metrica_A(posicion[1]);

		else
			return 0.f;
	}
  
	else if(gamma == 2){
		if(((alpha == 2)&(beta == 1))||((alpha == 1)&(beta == 2)))
			return metrica_r_d(posicion[1])/metrica_r(posicion[1]);

		else if((alpha == 3)&(beta == 3))
			return -sinf(posicion[2])*cosf(posicion[2]);

		else
			return 0.f;
	}
	else{
		if(((alpha == 3)&(beta == 1))||((alpha == 1)&(beta == 3)))
			return metrica_r_d(posicion[1])/metrica_r(posicion[1]);

		else if(((alpha == 3)&(beta == 2))||((alpha == 2)&(beta == 3)))
			return cosf(posicion[2])/sinf(posicion[2]);

		else
			return 0.f;
	}
}








float camara_transpose(float *matriz, float *matriz_t){
	for(unsigned int i = 0; i < 3; i++){
		matriz_t[3*i + 0] = matriz[3*0 + i];
		matriz_t[3*i + 1] = matriz[3*1 + i];
		matriz_t[3*i + 2] = matriz[3*2 + i];
	}
}

float camara_matmul(float *matriz1, float *matriz2, float *matriz3){
	for(unsigned int i = 0; i < 3; i++){
		for(unsigned int j = 0; j < 3; j++){
			matriz3[3*i + j] = matriz1[3*i + 0]*matriz2[3*0 + j] + matriz1[3*i + 1]*matriz2[3*1 + j] + matriz1[3*i + 2]*matriz2[3*2 + j];
		}
	}
}

void matmul(float *matriz1, float *matriz2, float *matriz3){
	for(unsigned int i = 0; i < 4; i++){
		for(unsigned int j = 0; j < 4; j++){
			matriz3[4*i + j] = matriz1[4*i + 0]*matriz2[4*0 + j] 
			+ matriz1[4*i + 1]*matriz2[4*1 + j] 
			+ matriz1[4*i + 2]*matriz2[4*2 + j] 
			+ matriz1[4*i + 3]*matriz2[4*3 + j];
		}
	}
}

void camara_minkowski_inv(float *matriz){
	for(unsigned int i = 0; i < 16; i++)
		matriz[i] = 0.f;
	matriz[0] = -powf(light_speed, -2);
	matriz[1*4 + 1] = 1.f;
	matriz[2*4 + 2] = 1.f;
	matriz[3*4 + 3] = 1.f;
}

void camara_minkowski(float *matriz){
	for(unsigned int i = 0; i < 16; i++)
		matriz[i] = 0.f;
	matriz[0] = -powf(light_speed, 2);
	matriz[1*4 + 1] = 1.f;
	matriz[2*4 + 2] = 1.f;
	matriz[3*4 + 3] = 1.f;
}

void camara_metric(float posicion[4], float *matriz){
	for(unsigned int i = 0; i < 16; i++)
		matriz[i] = 0.f;
	matriz[0]       = metrica_metric(posicion[1], posicion[2], 0);
	matriz[1*4 + 1] = metrica_metric(posicion[1], posicion[2], 1);
	matriz[2*4 + 2] = metrica_metric(posicion[1], posicion[2], 2);
	matriz[3*4 + 3] = metrica_metric(posicion[1], posicion[2], 3);
}
void camara_metric_inv(float posicion[4], float *matriz){
	for(unsigned int i = 0; i < 16; i++)
		matriz[i] = 0.f;
	matriz[0]       = 1.f/metrica_metric(posicion[1], posicion[2], 0);
	matriz[1*4 + 1] = 1.f/metrica_metric(posicion[1], posicion[2], 1);
	matriz[2*4 + 2] = 1.f/metrica_metric(posicion[1], posicion[2], 2);
	matriz[3*4 + 3] = 1.f/metrica_metric(posicion[1], posicion[2], 3);
}


void transpose(float *matriz, float *matriz_t){
	for(unsigned int i = 0; i < 4; i++){
		matriz_t[4*i + 0] = matriz[4*0 + i];
		matriz_t[4*i + 1] = matriz[4*1 + i];
		matriz_t[4*i + 2] = matriz[4*2 + i];
		matriz_t[4*i + 3] = matriz[4*3 + i];
	}
}

void invert_vectors(float posicion[4]){
	float M[16], M_T[16], metrica[16], mink_inv[16], aux[16], result[16];
	for(unsigned int i = 0; i < 4; i++){
		for(unsigned int j = 0; j < 4; j++)
			M[4*i + j] = vectores[j][i];
	}
	transpose(&M[0], &M_T[0]);
	camara_minkowski_inv(&mink_inv[0]);
	camara_metric(posicion, &metrica[0]);
	
	matmul(&M_T[0], &metrica[0], &aux[0]);
	matmul(&mink_inv[0], &aux[0], &result[0]);
	
	for(unsigned int i = 0; i < 4; i++){
		for(unsigned int j = 0; j < 4; j++)
			vectores_inv[i][j] = result[4*i + j];
	}
}
void camara_rotacion(float w[3], float angulo){
    //w en la base vectores
    float *base2, *base2_t, *base_r_p, *R, *base_r, vectores2[4][4];

    base2     = (float *) malloc(9*sizeof(float));
    base2_t   = (float *) malloc(9*sizeof(float));
    base_r_p  = (float *) malloc(9*sizeof(float));
    R         = (float *) malloc(9*sizeof(float));
    base_r    = (float *) malloc(9*sizeof(float));
    
    //asumiendo que vectores[1] y w son perpendiculares, o sea w de rotacion perpendicular a la linea de la visual
    base2[3*0 + 0] = 1.f;  base2[3*0 + 2] = w[0];
    base2[3*1 + 0] = 0.f;  base2[3*1 + 2] = w[1];
    base2[3*2 + 0] = 0.f;  base2[3*2 + 2] = w[2];
    
    base2[3*0 + 1] = w[1]*base2[3*2 + 0] - w[2]*base2[3*1 + 0];
    base2[3*1 + 1] = w[2]*base2[3*0 + 0] - w[0]*base2[3*2 + 0];
    base2[3*2 + 1] = w[0]*base2[3*1 + 0] - w[1]*base2[3*0 + 0];

    camara_transpose(base2, base2_t);

	
    R[3*0 + 0] = cosf(angulo);  R[3*0 + 1] = -sinf(angulo);  R[3*0 + 2] = 0.f; 
    R[3*1 + 0] = sinf(angulo);  R[3*1 + 1] =  cosf(angulo);  R[3*1 + 2] = 0.f; 
    R[3*2 + 0] =          0.f;  R[3*2 + 1] =           0.f;  R[3*2 + 2] = 1.f; 

    camara_matmul(R, base2_t, base_r_p);

	camara_matmul(base2, base_r_p, base_r);
    
    vectores2[0][0] = vectores[0][0];
    vectores2[0][1] = vectores[0][1];
    vectores2[0][2] = vectores[0][2];
    vectores2[0][3] = vectores[0][3];
    
    for(unsigned int i = 1; i < 4; i++){
        vectores2[i][0]    = vectores[1][0]*base_r[3*0 + i - 1] + vectores[2][0]*base_r[3*1 + i - 1] + vectores[3][0]*base_r[3*2 + i - 1];
        vectores2[i][1]    = vectores[1][1]*base_r[3*0 + i - 1] + vectores[2][1]*base_r[3*1 + i - 1] + vectores[3][1]*base_r[3*2 + i - 1];
        vectores2[i][2]    = vectores[1][2]*base_r[3*0 + i - 1] + vectores[2][2]*base_r[3*1 + i - 1] + vectores[3][2]*base_r[3*2 + i - 1];
        vectores2[i][3]    = vectores[1][3]*base_r[3*0 + i - 1] + vectores[2][3]*base_r[3*1 + i - 1] + vectores[3][3]*base_r[3*2 + i - 1];
    }

    for(unsigned int i = 1; i < 4; i++){
        for(unsigned int j = 0; j < 4; j++){
            vectores[i][j] = vectores2[i][j];
        }
    }

    free(base2);
    free(base2_t);
    free(base_r_p);
    free(R);
    free(base_r);
}


void invert_transformation(float *posicion, float *cam_matrix, float *result){
	// Assuming the metric in this system is Minkowski
	
	float *M_T, *mink_inv, *metric, *aux;

	M_T      = (float *) malloc(4*4*sizeof(float));
	metric   = (float *) malloc(4*4*sizeof(float));
	mink_inv = (float *) malloc(4*4*sizeof(float));
	aux      = (float *) malloc(4*4*sizeof(float));


	transpose(cam_matrix, M_T);
	camara_metric(posicion, metric);
	camara_minkowski_inv(mink_inv);

	matmul(M_T, metric, aux);
	matmul(mink_inv, aux, result);

	free(M_T);
	free(metric);
	free(mink_inv);
	free(aux);
}

void show_matrix(float *matrix){
	printf("#######################################\n");
	printf("######  %f   %f   %f   %f##############\n", matrix[0*4 + 0], matrix[0*4 + 1], matrix[0*4 + 2], matrix[0*4 + 3]);
	printf("######  %f   %f   %f   %f##############\n", matrix[1*4 + 0], matrix[1*4 + 1], matrix[1*4 + 2], matrix[1*4 + 3]);
	printf("######  %f   %f   %f   %f##############\n", matrix[2*4 + 0], matrix[2*4 + 1], matrix[2*4 + 2], matrix[2*4 + 3]);
	printf("######  %f   %f   %f   %f##############\n", matrix[3*4 + 0], matrix[3*4 + 1], matrix[3*4 + 2], matrix[3*4 + 3]);
	printf("#######################################");
}
void update_camera_matrix(float *posicion){
	cam_matrix[4*0 + 0] = vectores[0][0]; cam_matrix[4*0 + 1] = vectores[1][0]; cam_matrix[4*0 + 2] = vectores[2][0]; cam_matrix[4*0 + 3] = vectores[3][0];  
	cam_matrix[4*1 + 0] = vectores[0][1]; cam_matrix[4*1 + 1] = vectores[1][1]; cam_matrix[4*1 + 2] = vectores[2][1]; cam_matrix[4*1 + 3] = vectores[3][1];  
	cam_matrix[4*2 + 0] = vectores[0][2]; cam_matrix[4*2 + 1] = vectores[1][2]; cam_matrix[4*2 + 2] = vectores[2][2]; cam_matrix[4*2 + 3] = vectores[3][2];  
	cam_matrix[4*3 + 0] = vectores[0][3]; cam_matrix[4*3 + 1] = vectores[1][3]; cam_matrix[4*3 + 2] = vectores[2][3]; cam_matrix[4*3 + 3] = vectores[3][3];  
	
	invert_transformation(posicion, cam_matrix, cam_matrix_inv);
	
	float *result;
	result = (float *) malloc(4*4*sizeof(float));
	
	
	
	matmul(cam_matrix, cam_matrix_inv, result);
	
	
	
	
	
// 	show_matrix(result);

	free(result);
}

void evolucionador(int *input, float *posicion, float *cuadrivelocidad, float w[3], float angulo){
	update_camera_matrix(posicion);
  // Calculo de geodesica o movimiento acelerado
  float a = 0.1f;
  float b = 3.f;
  float derivada_seg[4], suma, dtau, asdf[4][4], modulo;
  dtau = 0.01f;
  modulo = sqrtf(-producto_punto(posicion, cuadrivelocidad, cuadrivelocidad));
  for(int j = 0; j < 4; j++)
    vectores[0][j] = cuadrivelocidad[j]/modulo;

  gram_schimdt(posicion);

	proper_time += dtau;
  for(int gamma = 0; gamma <4; gamma++)
    cuadriaceleracion[gamma] = 0.f;
  
  // computando la aceleracion
  if(input[0] == 1){
    for(int j=0; j < 4; j++)
      cuadriaceleracion[j] = a*vectores[1][j];
  }
  else if(input[1] == 1){
    for(int j=0; j < 4; j++)
      cuadriaceleracion[j] = -a*vectores[1][j];
  }
  else if(input[2] == 1){
    for(int j=0; j < 4; j++)
      cuadriaceleracion[j] = a*vectores[2][j];
  }
  else if(input[3] == 1){
    for(int j=0; j < 4; j++)
      cuadriaceleracion[j] = -a*vectores[2][j];
  }
  //terminado la aceleracion

  for (int gamma = 0; gamma < 4; gamma++){
    
    derivada_seg[gamma] = 0.f;
    for(int j=0; j<4; j++){
      for(int k=0; k<4; k++)
        derivada_seg[gamma] += -christoffel(posicion, gamma, j, k)*cuadrivelocidad[j]*cuadrivelocidad[k] + cuadriaceleracion[gamma];
    }

  }
  
  // derivada covariante
  for (int i=0; i<4; i++){
    for (int gamma = 0; gamma < 4; gamma++){
      asdf[i][gamma] = 0.f;
    
      for(int j=0; j<4; j++){
        for(int k=0; k<4; k++)
          asdf[i][gamma] += -christoffel(posicion, gamma, j, k)*vectores[i][j]*cuadrivelocidad[k];
      }

    }
  }


    
  

  // avance en posiciones
  for(int j=0; j <4; j++)
    posicion[j] += cuadrivelocidad[j]*dtau;

  // avance en cuadrivelocidad
  for(int j=0; j <4; j++)
    cuadrivelocidad[j] += derivada_seg[j]*dtau;

  // avance en vectores
  for (int i = 0; i < 4; i++){
    for(int j=0; j<4; j++)
      vectores[i][j] += asdf[i][j]*dtau;
  }
  camara_rotacion(w, angulo);
  
  for(int i = 0; i < 8; i++)
	  input[i] = 0;
}





