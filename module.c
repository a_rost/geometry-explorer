#define N_PART_MAX 500
#define pi (3.141592653f)
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include "structs.h"

#include "geodesicas_test.h"

float model_acceleration(unsigned int j, float m[3], float n[3]){
}

void geodesic_calculator(float rho0, float phi0, float *rho, float *phi, float *angles, float *distances, float *solutions, int size){
// 	metrica_inicializador();
// 	geodesica_inicializador2();

	
	float posicion0[4] = {0., rho0, pi/2.f, phi0}, posicion1[3], *D, *alfas, *solution_type;
	
	D             = (float *) malloc(4*sizeof(float));
	alfas         = (float *) malloc(4*sizeof(float));
	solution_type = (float *) malloc(4*sizeof(float));
	for(unsigned int i = 0; i < size; i++){
		posicion1[0] = rho[i];
		posicion1[1] = pi/2.f;
		posicion1[2] = phi[i];
		geodesica_geodesica5(posicion0, posicion1, D, alfas, solution_type);
		angles[4*i + 0] = alfas[0];
		angles[4*i + 1] = alfas[1];
		angles[4*i + 2] = alfas[2];
		angles[4*i + 3] = alfas[3];

		distances[4*i + 0] = D[0];
		distances[4*i + 1] = D[1];
		distances[4*i + 2] = D[2];
		distances[4*i + 3] = D[3];

		solutions[4*i + 0] = solution_type[0];
		solutions[4*i + 1] = solution_type[1];
		solutions[4*i + 2] = solution_type[2];
		solutions[4*i + 3] = solution_type[3];
	}
	free(D);
	free(alfas);
	free(solution_type);
}


void geodesic_calculator2(float *rho0, float *rho, float *phi, float *angles, float *distances, float *solutions, int size){
// 	metrica_inicializador();
// 	geodesica_inicializador2();

	
	float posicion0[4], posicion1[3], *D, *alfas, *solution_type;
	

	D             = (float *) malloc(4*sizeof(float));
	alfas         = (float *) malloc(4*sizeof(float));
	solution_type = (float *) malloc(4*sizeof(float));


	posicion0[0] = 0.f;
	for(unsigned int i = 0; i < size; i++){
		
		posicion0[1] = rho0[i];
		posicion0[2] = pi/2.f;
		posicion0[3] = 0.f;

		posicion1[0] = rho[i];
		posicion1[1] = pi/2.f;
		posicion1[2] = phi[i];
		geodesica_geodesica5(posicion0, posicion1, D, alfas, solution_type);
		angles[4*i + 0] = alfas[0];
		angles[4*i + 1] = alfas[1];
		angles[4*i + 2] = alfas[2];
		angles[4*i + 3] = alfas[3];

		distances[4*i + 0] = D[0];
		distances[4*i + 1] = D[1];
		distances[4*i + 2] = D[2];
		distances[4*i + 3] = D[3];

		solutions[4*i + 0] = solution_type[0];
		solutions[4*i + 1] = solution_type[1];
		solutions[4*i + 2] = solution_type[2];
		solutions[4*i + 3] = solution_type[3];
	}
	free(D);
	free(alfas);
	free(solution_type);
}


void geodesic_F(float *rho, float *P, float *result, int size){
	for(unsigned int i = 0; i < size; i++)
		result[i] = geodesica_F(rho[i], P[i]);
}

void geodesic_T(float *rho, float *P, float *result, int size){
	for(unsigned int i = 0; i < size; i++)
		result[i] = geodesica_T(rho[i], P[i]);
}


void geodesica_approx_F(float *rho, float *P, float *result, int size){
 	for(unsigned int i = 0; i < size; i++)
 		result[i] = approx_F(rho[i], P[i]);
}

void geodesic_I(float *P, float *rho1, float *rho2, float *result, int size){
	for(unsigned int i = 0; i < size; i++)
		result[i] = geodesica_I(P[i], rho1[i], rho2[i]);
}

void geodesic_G(float *P, float *rho1, float *rho2, float *result, int size){
	for(unsigned int i = 0; i < size; i++)
		result[i] = geodesica_G(P[i], rho1[i], rho2[i]);
}

void geodesic_I_T(float *P, float *rho1, float *rho2, float *result, int size){
	for(unsigned int i = 0; i < size; i++)
		result[i] = geodesica_I_T(P[i], rho1[i], rho2[i]);
}

void geodesic_G_T(float *P, float *rho1, float *rho2, float *result, int size){
	for(unsigned int i = 0; i < size; i++)
		result[i] = geodesica_G_T(P[i], rho1[i], rho2[i]);
}


float f_phi(float rho, float sita, float phi, float M, float L){
	return L/(metrica_r(rho)*metrica_r(rho));
}

float f_sita(float rho, float sita, float phi, float M, float L){
	return 2*L*L*metrica_r_d(rho)*powf(metrica_r(rho), -3);
}

float f_rho(float rho, float sita, float phi, float M, float L){
	return sita;
}

void geodesic_euler(float rho0, float phi0, float alfa, float *rho_space, float *phi_space, int size){
	float dsigma = 0.01f, sita0, sita, rho, phi, A, B, C, L, M, c;
	c = 2.f;
	L = c*metrica_r(rho0)*sinf(alfa);
	M = c;
	sita0 = -c*cosf(alfa);

	rho  = rho0;
	sita = sita0;
	phi  = phi0;
	for(unsigned int i = 0; i < size; i++){
		rho_space[i] = rho;
		phi_space[i] = phi;
		
		A = f_rho(rho,  sita, phi, M, L);
		B = f_sita(rho, sita, phi, M, L);
		C = f_phi(rho,  sita, phi, M, L);
		rho  += A*dsigma;
		sita += B*dsigma;
		phi  += C*dsigma;
	}
}


//gcc -c -Ofast -fPIC module.c -o lib/module.o -lm
//gcc -shared lib/module.o lib/geodesicas_worm.o -o lib/module.so -lm -lGL -lGLEW -fopenmp
