import numpy as np
import matplotlib.pyplot as plt
plt.ion()
from mpl_toolkits.mplot3d import Axes3D
from scipy import integrate
import modulo

	
def axisEqual3D(ax):
	extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
	sz = extents[:,1] - extents[:,0]
	centers = np.mean(extents, axis=1)
	maxsize = max(abs(sz))
	r = maxsize/2
	for ctr, dim in zip(centers, 'xyz'):
		getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)




def raiz(P):
	if P < h:
		print("no hay solucion, P < h")
		return -1.
	elif P >= R:
		return P
	else:
		return np.sqrt((P - h)/b)



def integrando_phi(x, P):
	if 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/r(x)**2



def F(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_phi, rho_max, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_phi, 0., x, args=(P))[0]

def I(P, x1, x2):
	return abs(F(x1, P) - F(x2, P))

def G(P, x1, x2):
	return F(x1, P) + F(x2, P)


def load_volcado():
	init  = np.genfromtxt("volcado/result_vertex.dat", max_rows = 1)
	array = np.genfromtxt("volcado/result_vertex.dat", skip_header = 1)
	
	nvert = int(len(array)/4)
	index = np.arange(0, nvert*4, 4)
	
	rho_space   = array[:, 0][index]
	theta_space = array[:, 1][index]
	phi_space   = array[:, 2][index]
	u_space     = array[:, 3][index]
	v_space     = array[:, 4][index]
	
	alpha_space = array[:, 5]
	dist_space  = array[:, 6]
	type_space  = array[:, 7]
	
	return rho_space, theta_space, phi_space, u_space, v_space, alpha_space, dist_space, type_space, init


def basic(rho_space, theta_space, phi_space, u_space, v_space, alpha_space, dist_space, type_space, init):
	N = len(rho_space)
	rho1 = np.ones(N)*init[0]
	
	rho2 = rho_space
	
	versor0 = np.zeros([N, 3])
	versor1 = np.zeros([N, 3])
	
	versor0[:, 0] = np.sin(init[1])*np.cos(init[2])
	versor0[:, 1] = np.sin(init[1])*np.sin(init[2])
	versor0[:, 2] = np.cos(init[1])
	
	versor1[:, 0] = np.sin(theta_space)*np.cos(phi_space)
	versor1[:, 1] = np.sin(theta_space)*np.sin(phi_space)
	versor1[:, 2] = np.cos(theta_space)
	
	coseno = versor0[:, 0]*versor1[:, 0] + versor0[:, 1]*versor1[:, 1] + versor0[:, 2]*versor1[:, 2]
	
	phi = np.arccos(coseno)
	
	asdasd = modulo.geodesica(rho1, rho2, phi)
	return asdasd

	
def transform(result):
	XYZ = np.zeros([result.nvert, 3])
	
	XYZ[:, 0] = result.rho*np.sin(result.theta)*np.cos(result.phi)
	XYZ[:, 1] = result.rho*np.sin(result.theta)*np.sin(result.phi)
	XYZ[:, 2] = result.rho*np.cos(result.theta)

	return XYZ


class result:
	def __init__(self):
		self.rho, self.theta, self.phi, self.u, self.v, self.alpha, self.distance, self.sol_type, self.init = load_volcado()
		
		self.nvert = len(self.rho)
		self.geo1 = np.arange(0, self.nvert*4, 4) + 0
		self.geo2 = np.arange(0, self.nvert*4, 4) + 1
		self.geo3 = np.arange(0, self.nvert*4, 4) + 2
		self.geo4 = np.arange(0, self.nvert*4, 4) + 3
		self.XYZ  = transform(self)
		self.sol_type = np.round((self.sol_type - np.floor(self.sol_type))*3)
		
	def plot2D(self):
		fig, axs = plt.subplots(ncols = 3, nrows = 1)
		axs[0].plot(self.rho, self.theta, ".")
		axs[0].set_xlabel(r"$\rho$")
		axs[0].set_ylabel(r"$\theta$")

		axs[1].plot(self.rho, self.phi, ".")
		axs[1].set_xlabel(r"$\rho$")
		axs[1].set_ylabel(r"$\phi$")

		axs[2].plot(self.phi, self.theta, ".")
		axs[2].set_xlabel(r"$\phi$")
		axs[2].set_ylabel(r"$\theta$")


	def plot3D(self):
		fig = plt.figure()
		ax = fig.add_subplot(111, projection='3d')
		ax.scatter(self.XYZ[:, 0], self.XYZ[:, 1], self.XYZ[:, 2], s = 0.9)		
		axisEqual3D(ax)



T = 2.
b = 1./(2*T)
h = T - b*T**2
R = h + b*T**2

RHO_MAX, RHO_MIN = 5., -5.
condicion = 1

def r(x):
	if x >= T:
		return x
	elif x >= -T:
		return h + b*x**2
	else:
		return -x
r = np.vectorize(r)

objeto = result()




N = len(objeto.rho)
rho1 = np.ones(N)*objeto.init[0]
rho2 = objeto.rho
phi  = objeto.phi

mask = (np.isfinite(objeto.distance[objeto.geo1]) == False)

asdasd = modulo.geodesica(rho1[mask], rho2[mask], phi[mask])

N2 = len(asdasd[1])
mask2 = (~np.isfinite(asdasd[1][np.arange(0, N2, 4)]))
print("Errores: ", mask2.sum())
print(asdasd[1][np.arange(0, N2, 4)][mask2])



mask3 = (np.arange(N)[mask])[mask2]
asdasd = modulo.geodesica(rho1[mask3], rho2[mask3], phi[mask3])

modulo.toggle_debug()
asdasd = modulo.geodesica([rho1[mask3][0]], [rho2[mask3][0]], [phi[mask3][0]])


"""
plt.plot(objeto.rho, objeto.alpha[objeto.geo1], ".")
plt.plot(objeto.rho, objeto.alpha[objeto.geo2], ".")
plt.plot(objeto.rho, objeto.alpha[objeto.geo3], ".")



plt.hist(objeto.alpha[objeto.geo1], bins = 40, range = [0., 2*np.pi], alpha = 0.3)
plt.hist(objeto.alpha[objeto.geo2], bins = 40, range = [0., 2*np.pi], alpha = 0.3)
plt.hist(objeto.alpha[objeto.geo3], bins = 40, range = [0., 2*np.pi], alpha = 0.3)
plt.hist(objeto.alpha[objeto.geo4], bins = 40, range = [0., 2*np.pi], alpha = 0.3)

"""






"""
rho0, theta0, phi0 = 0.783218,  1.746714, 5.420547


P = r(rho0)*np.sin(objeto.alpha[objeto.geo1])


plt.plot(objeto.rho, P, ".")


pos0 = np.sin(theta0)*np.cos(phi0), np.sin(theta0)*np.sin(phi0), np.cos(theta0)

pos = np.zeros([objeto.nvert, 3])
pos[:, 0]  = np.sin(objeto.theta)*np.cos(objeto.phi)
pos[:, 1]  = np.sin(objeto.theta)*np.sin(objeto.phi)
pos[:, 2]  = np.cos(objeto.theta)

coseno = pos[:, 0]*pos0[0] + pos[:, 1]*pos0[1] + pos[:, 2]*pos0[2]
phi = np.arccos(coseno)
#cant = modulo.geodesica_F(objeto.rho, P) - modulo.geodesica_F(rho0*np.ones(objeto.nvert), P)

cant1 = modulo.geodesica_I(P, objeto.rho, rho0*np.ones(objeto.nvert))
cant2 = modulo.geodesica_G(P, objeto.rho, rho0*np.ones(objeto.nvert))

mask_I = objeto.sol_type[objeto.geo1] == 1
mask_G = ~mask_I

cant  = np.ones(objeto.nvert)
cant3 = np.ones(objeto.nvert)

cant[mask_I] = cant1[mask_I]
cant[mask_G] = cant2[mask_G]



for i in range(objeto.nvert):
	print(i, objeto.nvert)
	if mask_I[i]:
		cant3[i] = I(P[i], rho0, objeto.rho[i])
	else:
		cant3[i] = G(P[i], rho0, objeto.rho[i])

plt.plot(phi, cant/phi, ".")
plt.plot(phi, cant3/phi, ".")

error = abs(cant3/phi - 1.) >= 0.002
plt.plot(phi[error], (cant3/phi)[error], ".")

plt.plot(objeto.rho[mask_I], P[mask_I], ".")
plt.plot(objeto.rho[error*mask_I], P[error*mask_I], ".")
"""
"""
N = 10000
rho1_space = (np.random.random(N) - 0.5)*6.
rho2_space = (np.random.random(N) - 0.5)*6.

P_space = np.zeros(N)
I_space1 = np.zeros(N)
I_space2 = np.zeros(N)
for i in range(N):
	print(i)
	p = np.random.random()*min(r(rho1_space[i]), r(rho2_space[i]))
	P_space[i] = p
	I_space1[i] = I(p, rho1_space[i], rho2_space[i])

I_space2 = modulo.geodesica_I(P_space, rho1_space, rho2_space)

plt.plot(I_space1, I_space2/I_space1, ".")

mask = abs(I_space2/I_space1 - 1.) >= 0.01

fig, axs = plt.subplots(ncols = 2, nrows = 1)

axs[0].plot(rho1_space, P_space, ".")
axs[0].plot(rho1_space[mask], P_space[mask], ".")
axs[1].plot(rho1_space, rho2_space, ".")
axs[1].plot(rho1_space[mask], rho2_space[mask], ".")


	
rho1, rho2, N = 0.1, 1., 10000

P_space = np.linspace(0., min(r(rho1), r(rho2)), N)
I_space1 = np.zeros(N)
I_space2 = np.zeros(N)
I_space3 = np.zeros(N)

for i in range(N):
	print(i)
	I_space1[i] = I(P_space[i], rho1, rho2)
	I_space3[i] = abs(F4(rho1, P_space[i]) - F4(rho2, P_space[i]))
	
I_space2 = modulo.geodesica_I(P_space, rho1*np.ones(N), rho2*np.ones(N))

plt.plot(P_space, I_space1)
plt.plot(P_space, I_space2)
plt.plot(P_space, I_space3)


plt.plot(P_space, I_space2/I_space1)
plt.plot(P_space, I_space3/I_space1)
"""
