import numpy as np
import matplotlib.pyplot as plt
import metrica
plt.ion()


def integral(a, b, h):
	#calculates the integral of 1/sqrt(a*x + b*x**2) from 0 to h
	u1, u2 = 1., 1. + 2*h*b/a
	if b >= 0.:
		return (np.arccosh(u2) - np.arccosh(u1))/np.sqrt(b)
	else:
		return -(np.arcsin(u2) - np.arcsin(u1))/np.sqrt(-b)

def integral2(a, b, c, x):
	#calculates the integral of 1/sqrt(a + b*x + c*x**2) from 0 to h, assuming c > 0
	if c == 0.:
		return 2./b*np.sqrt(a + b*x)
	else:
		return np.log(2*np.sqrt(c)*np.sqrt(a + x*(b + c*x)) + b + 2*c*x)/np.sqrt(c)

def argumento(x, P):
	return metrica.r(x)**4/P**2 - metrica.r(x)**2

def argumento_p(x, P):
	return (4*metrica.r(x)**3/P**2 - 2*metrica.r(x))*metrica.r_p(x)

def argumento_p_p(x, P):
	return (4*metrica.r(x)**3/P**2 - 2*metrica.r(x))*metrica.r_p_p(x) + (12*metrica.r(x)**2/P**2 - 2)*metrica.r_p(x)**2


def aproximacion_final1(P, h):
	root = metrica.raiz(P)
	a = argumento_p(root, P)
	b = argumento_p_p(root, P)
	return integral(a, b, h)

def aproximacion_final2(P, rho0, h):
	a = argumento(rho0, P)
	b = argumento_p(rho0, P)
	c = argumento_p_p(rho0, P)/2.
	return integral2(a, b, c, rho0 + h) - integral2(a, b, c, rho0)


def integral_S(x, P):
	root = np.sqrt(x*x - P*P)
	return root + P*np.arctan(P/root)

def interpolador_lineal(P, array):
	dP = 2./999

	i = int(P/dP)
	
	if(i >= 1000):
		i = 999
	elif(i < 0):
		i = 0
	
	f1 = array[i]
	f2 = array[i + 1]
	return (f1 - f2)/dP*(P - i*dP) + f1

def S3(rho, P):
	if rho < 0.:
		return -S(-rho, P)

	elif rho >= 2.:
		if P >= h:
			rho_min = raiz(P)
			return integral_S(rho, P) - integral_S(rho_min, P)
		else:
			valor = interpolador_lineal(P, array_S2)
			return valor + integral_S(rho, P) - integral_S(2., P)
	else:
		if P >= h:
			rho_min = raiz(P)
			if rho - rho_min <= 0.05:
				a = -U_p(rho_min, P)
				H = abs(rho - rho_min)
				return np.sqrt(a)*2/3*np.power(H, 3./2)
			else:
				return metrica.interpolador(array_S, 2./399, 2./1599, 0., 0., rho, P)
		else:
			return metrica.interpolador(array_S, 2./399, 2./1599, 0., 0., rho, P)




def interpolador_lineal(P, array, debug = False):
	dP = 2./2000
	i = int(P/dP)
	
	if(i >= 2001):
		i = 2000
	elif(i < 0):
		i = 0
	f1 = array[i]
	f2 = array[i + 1]
	if debug:
		print("i = ", i, "f1 = ", f1, "f2 = ", f2)
	return (f2 - f1)/dP*(P - i*dP) + f1


def interpolador_lineal2(array, x1, x2, N, x):
	dx = (x2 - x1)/(N - 1)
	i = int((x - x1)/dx)
	
	if(i >= N):
		i = N - 1
	elif(i < 0):
		i = 0

	f1 = array[i]
	f2 = array[i + 1]
	return (f2 - f1)/dx*(x - i*dx - x1) + f1
	
	
def exact(x, P):
	v = -P/x
	return np.arcsin(v)


def calculador():
	P_res = 1000
	rho_res = 1000

	F_array= np.zeros([rho_res, P_res])
	for i in range(rho_res):
		x = 0.0 + i/(rho_res - 1.)*2.
		print(i)
		for j in range(P_res):
			P = 0.0 + j/(P_res - 1)*1
			if j == P_res - 1:
				P = 0.99999999
				#P = 1.
			F_array[i, j] = metrica.F2(2., x, P)

	array_exp1 = np.exp(F_array)


	array_edge1 = np.zeros(P_res)
	for i in range(P_res):
		P = 1./(P_res - 1)*i
		array_edge1[i] = metrica.F(2., P)
	array_edge1 = np.exp(-array_edge1)
	plt.plot(array_edge1)



	F_array= np.zeros([rho_res, P_res])
	for i in range(rho_res):
		x = 2./(rho_res - 1.)*i
		if i == 0:
			x = 0.000000001
		P1, P2 = 1., metrica.r(x)
		d_P4 = (P2 - P1)/(P_res - 1)
		print("x = ", x, "i = ", i)
		for j in range(P_res):
			P = d_P4*j + P1
			if j == P_res - 1:
				P = P2 - 0.0000000001
			elif j == 0:
				P = 1.0000000001
			F_array[i, j] = metrica.F2(2., x, P)
	array_exp2 = np.exp(F_array)
	plt.imshow(array_exp2)

	array_edge2 = np.zeros(P_res)
	for i in range(P_res):
		P = 1 + 1./(P_res - 1)*i
		array_edge2[i] = metrica.F(2., P)
	array_edge2 = np.exp(-array_edge2)
	plt.plot(array_edge2)




factor1, factor2, factor3 = 5., 5., 0.7

d_rho5 = 1.5/399
d_P5   = 1./399
array5 = np.zeros([400, 400])
for i in range(400):
	x = 0.5 + d_rho5*i
	print("rho = ", x, "i = ", i)
	for j in range(400):
		P = d_P5*j
		array5[i][j] = metrica.F2(0.5, x, P)
array_exp1 = np.exp(-np.float32(array5)/factor1)
plt.imshow(array_exp1)



d_rho4 = 2./399
array4 = np.zeros([400, 400])
prueba1 = np.zeros([400, 400])
prueba2 = np.zeros([400, 400])
for i in range(400):
	x = d_rho4*i
	if i == 0:
		x = 0.00000001
	P1, P2 = 1., metrica.r(x)
	d_P4 = (P2 - P1)/399.
	print("x = ", x, "i = ", i)
	for j in range(400):
		P = d_P4*j + P1
		if j == 399:
			P = P2 - 0.000000001
		elif j == 0:
			P = 1.000000001
		prueba1[i, j] = metrica.F(x, P)
		prueba2[i, j] = aproximacion_final1(P, x - metrica.raiz(P))
		#array4[i][j] = metrica.F2(2., x, P)
array_zone2 = prueba1/prueba2
plt.imshow(array_zone2)


array_edge = np.zeros(1000)

for i in range(1000):
	x = i*2./999.
	array_edge[i] = metrica.F2(2., x, 1.)

array_edge = np.exp(array_edge)


def guardador():
	archivo = open("../../data/worm/array_exp1.dat", "w")
	for i in range(P_res):
		for j in range(rho_res):
			archivo.write(str(array_exp1[i][j]) + "\n")
	archivo.close()

	archivo = open("../../data/worm/array_exp2.dat", "w")
	for i in range(P_res):
		for j in range(rho_res):
			archivo.write(str(array_exp2[i][j]) + "\n")
	archivo.close()

	archivo = open("../../data/worm/array_edge1.dat", "w")
	for i in range(P_res):
		archivo.write(str(array_edge1[i]) + "\n")
	archivo.close()

	archivo = open("../../data/worm/array_edge2.dat", "w")
	for i in range(P_res):
		archivo.write(str(array_edge2[i]) + "\n")
	archivo.close()

def calculador_S():
	N_rho = metrica.N_rho
	N_P   = metrica.N_P
	array_S  = np.zeros([N_rho, N_P])
	d_rho2 = 2./(N_rho - 1)
	d_P2   = 2./(N_P - 1)
	for j in range(N_P):
		P = d_P2*j
		print("P = ", P, "j = ", j)
		for i in range(N_rho):
			x = 0. + d_rho2*i
			array_S[i][j] = metrica.S(x, P)


	archivo = open("../../data/worm/S_array1.dat", "w")
	for i in range(N_rho):
		for j in range(N_P):
			archivo.write(str(array_S[i][j]) + "\n")
	archivo.close()
	plt.imshow(array_S)

	array_S2 = np.zeros(1000)
	x = metrica.T
	for j in range(1000):
		P = 2./999*j
		array_S2[j] = metrica.S(x, P)

	archivo = open("../../data/worm/S_array2.dat", "w")
	for i in range(1000):
		archivo.write(str(array_S2[i]) + "\n")
	archivo.close()


def F4(x, P, debug = False):
	root = metrica.raiz(P)

	if x < 0.:
		return -F4(-x, P)
	elif P == 0.:
		return 0.

	elif P == metrica.h:
		return 100.
		
	elif P == metrica.r(x):
		return 0.

	elif P <= metrica.h and x <= 2.:
		if debug:
			print("Zone 1")
		#Zone 1
		valor0  = interpolador_lineal2(array_edge1, 0., 1., 400, P)
		if x < 2.:
			valor  = metrica.interpolador(array_exp1, 2./399, 1./399, 0., 0., x, P)
			return -np.log(valor0) + np.log(valor)
		else:
			return -np.log(valor0)


	elif P > metrica.h and x <= 2.:
		if debug:
			print("Zone 2")
		P1, P2 = 1., metrica.r(x)
		u  = (P - P1)/(P2 - P1)
		valor0 = interpolador_lineal2(array_edge2, 1., 2., 400, P)
		if x < 2.:
			valor = metrica.interpolador(array_exp2, 2./399, 1./399, 0., 0., x, u)
			return -np.log(valor0) + np.log(valor)
		else:
			return -np.log(valor0)

	elif x > 2. and P < 2.:
		valor0 = F4(2., P)
		return valor0 + exact(x, P) - exact(2., P)

	elif P >= 2. and x >= 2.:
		if debug:
			print("Zone 4")
		return exact(x, P) - exact(root, P)
	else:
		print("error")



def F_comp(x, P):
	f1, f2, f3 = metrica.F(x, P), metrica.F3(x, P), F4(x, P)
	print("Vals:", f1, f2, f3)
	print("Diff:", abs(f2 - f1), abs(f3 - f1), "Factor: ", abs(f2 - f1)/abs(f3 - f1))


def test_F(rho1, rho2, N = 10000):
	if rho1*rho2 >= 0.:
		P1, P2 = 0., min(metrica.r(rho1), metrica.r(rho2))
	else:
		P1, P2 = 0., 1.

	P_space = np.linspace(P1, P2, N)
	diff1   = np.zeros(N)
	diff2   = np.zeros(N)
	diff3   = np.zeros(N)
	
	for i in range(N):
		diff1[i] = metrica.F2(rho1, rho2, P_space[i])
		diff2[i] = metrica.F3(rho2, P_space[i]) - metrica.F3(rho1, P_space[i])
		diff3[i] = F4(rho2, P_space[i]) - F4(rho1, P_space[i])
	plt.plot(np.arcsin(P_space/metrica.r(rho1)), diff1, linestyle = "dashed")
	plt.plot(np.arcsin(P_space/metrica.r(rho1)), diff2)
	plt.plot(np.arcsin(P_space/metrica.r(rho1)), diff3)
	plt.legend(["Real", "Aproximacion anterior", "Aproximacion mejorada"])

def F_comp2(x0, x1, P):
	f1, f2, f3 = metrica.F2(x0, x1, P), metrica.F3(x1, P) - metrica.F3(x0, P), F4(x1, P) - F4(x0, P)

	print("Vals:", f1, f2, f3)
	print("Diff:", abs(f2 - f1), abs(f3 - f1), "Factor: ", abs(f2 - f1)/abs(f3 - f1))
	

def comparacion(N):
	rho_space = np.random.random(N)*10. - 5.
	u_space   = np.random.random(N)
	P_space   = np.zeros(N)

	diff1 = np.zeros(N)
	diff2 = np.zeros(N)

	for i in range(N):
		P_max = metrica.r(rho_space[i])
		P = u_space[i]*P_max
		P_space[i] = P
		print(rho_space[i], P)
		diff1[i] = metrica.F3(rho_space[i], P) - metrica.F(rho_space[i], P)
		diff2[i] = F4(rho_space[i], P) - metrica.F(rho_space[i], P)
	return diff1, diff2, rho_space, P_space




def cociente(x, P):
	valor1 = F(x, P)
	#valor2 = aproximacion_final2(P, 0., x)
	valor3 = F3(x, P)
	valor2 = F4(x, P)
	
	return valor2/valor1, valor3/valor1

archivo = open("data/worm/array_exp2.dat", "w")
for i in range(400):
	for j in range(800):
		archivo.write(str(array_8[i][j]) + "\n")
archivo.close()


	fig, axs = plt.subplots(nrows = 1, ncols = 3)

	factor1, factor2, factor3 = 5., 5., 0.7

d_rho5 = 1.5/399
d_P5   = 1./799
array5 = np.zeros([400, 800])
array6 = np.zeros([400, 800])
for i in range(400):
	x = 0.5 + d_rho5*i
	print("rho = ", x, "i = ", i)
	for j in range(800):
		P = d_P5*j
		array5[i][j] = F2(0.5, x, P)

factor1 = 5.
array_exp1 = np.exp(-np.float32(array5)/np.float32(factor1))
recons = -np.float32(5.)*np.log(array_exp1)
plt.imshow(array_exp1, origin = "upper")
plt.imshow(recons)

archivo = open("../../data/worm/array_exp1.dat", "w")
for i in range(400):
	for j in range(800):
		archivo.write(str(array_exp1[i][j]) + "\n")
archivo.close()


arreglo1 = np.zeros(2000)
arreglo2 = np.zeros(2000)
for j in range(10):
	x = 0.3*j/10.
	for i in range(2000):
		P = 0.9 + i/1999.*0.1
		arreglo1[i] = F(x, P)
		arreglo2[i] = aproximacion_final2(P, 0., x)

	plt.plot(np.exp(-arreglo2/arreglo1))

		
		
"""
array_5 = np.zeros(2000)
for i in range(2000):
	P = 1. + 1./1999*i
	array_5[i] = F(2., P)
plt.plot(array_5)



compare1 = np.zeros(1000)
compare2 = np.zeros(1000)
for i in range(500):
	for j in range(500):
		compare1[i, j]



array_F = np.zeros([N_rho, N_P])
d_rho2 = 2./(N_rho - 1)
d_P2   = 1./(N_P - 1)
for j in range(N_P):
	P = d_P2*j
	print("P = ", P, "j = ", j)
	for i in range(N_rho):
		x = 0. + d_rho2*i
		array_F[i][j] = F(x, P)
plt.imshow(array_F)



array_T3 = np.zeros([N_rho3, N_P3])
for j in range(N_P3):
	P = h + 0.0020 + d_P3*j
	print("P = ", P, "j = ", j)
	for i in range(N_rho3):
		x = 0.2 + d_rho3*i
		array_T3[i][j] = t2(0.2, x, P)

archivo = open("worm/t_array3.dat", "w")
for i in range(N_rho3):
	for j in range(N_P3):
		archivo.write(str(array_T3[i][j]) + "\n")
archivo.close()
plt.imshow(array_T3)
"""

