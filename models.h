/*
	Header for 3d models common for all the different metrics, if a change is done, make sure it will be compatible with different metrics
*/
#define deg2rad (pi/180.f)
#define rad2deg (180.f/pi)

float producto_punto3(float [3], float [3], float [3]);

void model_intensidad(float posicion0[3], float posicion1[3], float *intensidades){
	float versor0[3], versor1[3], *versores1, *versores2, angulos[N_GEO], pos_aux2[3], pos_aux1[3], 
		dtau = 0.1f, versor_perp[3], *distancia_fuente, *alfas, *solution_type, *direccion, modulo;
	
	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2];
	float phi = acosf(coseno);
	versor_perp[0]  = versor1[0] - coseno*versor0[0];
	versor_perp[1]  = versor1[1] - coseno*versor0[1];
	versor_perp[2]  = versor1[2] - coseno*versor0[2];
	modulo          = sqrtf(versor_perp[0]*versor_perp[0] + versor_perp[1]*versor_perp[1] + versor_perp[2]*versor_perp[2]);
	versor_perp[0] *= 1.f/modulo;
	versor_perp[1] *= 1.f/modulo;
	versor_perp[2] *= 1.f/modulo;

	distancia_fuente = (float *) malloc(N_GEO*sizeof(float));
	alfas            = (float *) malloc(N_GEO*sizeof(float));
	solution_type    = (float *) malloc(N_GEO*sizeof(float));
	versores1         = (float *) malloc(N_GEO*3*sizeof(float));
	versores2         = (float *) malloc(N_GEO*3*sizeof(float));
	direccion         = (float *) malloc(N_GEO*3*sizeof(float));

	geodesica_geodesica4(posicion1, posicion0, distancia_fuente, alfas, solution_type, versores1);
	
	versor0[0] = 1.f;
	versor0[1] = 0.f;
	versor0[2] = 0.f;
	
	for(unsigned int i = 0; i < N_GEO; i++){
		coseno = versor0[0]*versores1[3*i + 0] + versor0[1]*versores1[3*i + 1] + versor0[2]*versores1[3*i + 2];
// 		if(coseno != 0.f){
			direccion[3*i + 0] = versor0[0] - coseno*versores1[3*i + 0];
			direccion[3*i + 1] = versor0[1] - coseno*versores1[3*i + 1];
			direccion[3*i + 2] = versor0[2] - coseno*versores1[3*i + 2];
// 		}
// 		else{
// 			coseno = versor_perp[0]*versores1[3*i + 0] + versor_perp[1]*versores1[3*i + 1] + versor_perp[2]*versores1[3*i + 2];
// 			direccion[3*i + 0] = versor_perp[0] - coseno*versores1[3*i + 0];
// 			direccion[3*i + 1] = versor_perp[1] - coseno*versores1[3*i + 1];
// 			direccion[3*i + 2] = versor_perp[2] - coseno*versores1[3*i + 2];
// 		}
		modulo = sqrtf(direccion[3*i + 0]*direccion[3*i + 0] + direccion[3*i + 1]*direccion[3*i + 1] + direccion[3*i + 2]*direccion[3*i + 2]);
		direccion[3*i + 0] *= 1.f/modulo;
		direccion[3*i + 1] *= 1.f/modulo;
		direccion[3*i + 2] *= 1.f/modulo;
	}
	
	for(unsigned int i = 0; i < N_GEO; i++){
		pos_aux1[0] = posicion1[0] + dtau*direccion[3*i + 0]/sqrtf(metrica_metric(posicion1[0], posicion1[1], 1));
		pos_aux1[1] = posicion1[1] + dtau*direccion[3*i + 1]/sqrtf(metrica_metric(posicion1[0], posicion1[1], 2));
		pos_aux1[2] = posicion1[2] + dtau*direccion[3*i + 2]/sqrtf(metrica_metric(posicion1[0], posicion1[1], 3));
		pos_aux2[0] = posicion1[0] - dtau*direccion[3*i + 0]/sqrtf(metrica_metric(posicion1[0], posicion1[1], 1));
		pos_aux2[1] = posicion1[1] - dtau*direccion[3*i + 1]/sqrtf(metrica_metric(posicion1[0], posicion1[1], 2));
		pos_aux2[2] = posicion1[2] - dtau*direccion[3*i + 2]/sqrtf(metrica_metric(posicion1[0], posicion1[1], 3));

		
		geodesica_geodesica4(posicion0, pos_aux1, distancia_fuente, alfas, solution_type, versores1);
		geodesica_geodesica4(posicion0, pos_aux2, distancia_fuente, alfas, solution_type, versores2);
		coseno = versores1[3*i + 0]*versores2[3*i + 0] +  versores1[3*i + 1]*versores2[3*i + 1] +  versores1[3*i + 2]*versores2[3*i + 2];
		angulos[i] = acosf(coseno);
		intensidades[i] = angulos[i]*(dtau/metrica_r(posicion1[0]))/sinf(phi);
	}
	printf("intensidades  = (%f, %f, %f, %f)\n", angulos[0], angulos[1], angulos[2], angulos[3]);
}
	


static inline void geodesica_xyz2esf(float x, float y, float z, float *theta, float *phi){  
  const float r = sqrtf((powf(x,2) + powf(y,2) + powf(z,2)));
  *theta  = acosf(z/r)*180.f/pi;
  *phi    = fmodf(atan2f(y, x)*180.f/pi+360.f, 360.f);
}


float christoffel(float[4], unsigned int, unsigned int, unsigned int);
struct object_data model_lighting_processing(struct object_data, float[3]);

float model_acceleration(unsigned int i, float X[3], float V[3]){
	float suma = 0.f, pos[4];
	pos[0] = 0.f;
	pos[1] = X[0];
	pos[2] = X[1];
	pos[3] = X[2];

	for(unsigned int j = 1; j < 4; j++){
		for(unsigned int k = 1; k < 4; k++)
			suma += christoffel(pos, i + 1, j, k)*V[j - 1]*V[k - 1];
	}
	return -suma;
}

void model_evolucionador(float vertice[3], float derivada[3], float distancia, float *result){
	float T = 0.f, dp = 0.01f, X[3], V[3], A[3];
	X[0] = vertice[0];
	X[1] = vertice[1];
	X[2] = vertice[2];
	V[0] = derivada[0];
	V[1] = derivada[1];
	V[2] = derivada[2];
	while (T < distancia){
		A[0] = model_acceleration(0, X, V);
		A[1] = model_acceleration(1, X, V);
		A[2] = model_acceleration(2, X, V);

		X[0] += dp*V[0];
		X[1] += dp*V[1];
		X[2] += dp*V[2];

		V[0] += dp*A[0];
		V[1] += dp*A[1];
		V[2] += dp*A[2];

		T += dp*sqrtf(producto_punto3(X, V, V));
	}
	
	//##########################
	// Last step:
	//##########################
	dp = (distancia - T)/sqrtf(producto_punto3(X, V, V));
	X[0] += dp*V[0];
	X[1] += dp*V[1];
	X[2] += dp*V[2];

	T += dp*sqrtf(producto_punto3(X, V, V));
	
	
	//##########################
	//##########################
	result[0] = X[0];
	result[1] = X[1];
	result[2] = X[2];
}

float model_surface(struct object_data objeto){
	float vertice1[3], vertice2[3], vertice3[3], vector1[3], vector2[3], vector3[3], suma = 0.f;
	unsigned int T[3];

	for(unsigned int i = 0; i < objeto.n_triang; i++){
		T[0] = objeto.triangulos[3*i + 0];
		T[1] = objeto.triangulos[3*i + 1];
		T[2] = objeto.triangulos[3*i + 2];

		vertice1[0] = objeto.vertices[3*T[0] + 0];
		vertice1[1] = objeto.vertices[3*T[0] + 1];
		vertice1[2] = objeto.vertices[3*T[0] + 2];

		vertice2[0] = objeto.vertices[3*T[1] + 0];
		vertice2[1] = objeto.vertices[3*T[1] + 1];
		vertice2[2] = objeto.vertices[3*T[1] + 2];

		vertice3[0] = objeto.vertices[3*T[2] + 0];
		vertice3[1] = objeto.vertices[3*T[2] + 1];
		vertice3[2] = objeto.vertices[3*T[2] + 2];

		vector1[0] = vertice2[0] - vertice1[0];
		vector1[1] = vertice2[1] - vertice1[1];
		vector1[2] = vertice2[2] - vertice1[2];

		vector2[0] = vertice3[0] - vertice1[0];
		vector2[1] = vertice3[1] - vertice1[1];
		vector2[2] = vertice3[2] - vertice1[2];

		vector1[0] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 1));
		vector1[1] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 2));
		vector1[2] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 3));

		vector2[0] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 1));
		vector2[1] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 2));
		vector2[2] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 3));

		vector3[0] = vector1[1]*vector2[2] - vector1[2]*vector2[1];
		vector3[1] = vector1[0]*vector2[2] - vector1[2]*vector2[0];
		vector3[2] = vector1[0]*vector2[1] - vector1[1]*vector2[0];
		suma += sqrtf(vector3[0]*vector3[0] + vector3[1]*vector3[1] + vector3[2]*vector3[2])*0.5f;
	}
// 	printf("Superficie = %f\n", suma);
	return suma;
}

struct object_data model_normal_creator(struct object_data objeto_inicial){
	struct object_data objeto;
	objeto = objeto_inicial;
	float vertice1[3], vertice2[3], vertice3[3], vector1[3], vector2[3], vector3[3], area = 0.f, *normalizaciones;
	unsigned int T[3];

	objeto.normales = (float *) malloc(3*objeto.n_vert*sizeof(float));
	normalizaciones = (float *) malloc(objeto.n_vert*sizeof(float));
	memset(normalizaciones, 0, objeto.n_vert*sizeof(float));
	memset(objeto.normales, 0, objeto.n_vert*sizeof(float)*3);
	
	for(unsigned int i = 0; i < objeto.n_triang; i++){
		T[0] = objeto.triangulos[3*i + 0];
		T[1] = objeto.triangulos[3*i + 1];
		T[2] = objeto.triangulos[3*i + 2];

		vertice1[0] = objeto.vertices[3*T[0] + 0];
		vertice1[1] = objeto.vertices[3*T[0] + 1];
		vertice1[2] = objeto.vertices[3*T[0] + 2];

		vertice2[0] = objeto.vertices[3*T[1] + 0];
		vertice2[1] = objeto.vertices[3*T[1] + 1];
		vertice2[2] = objeto.vertices[3*T[1] + 2];

		vertice3[0] = objeto.vertices[3*T[2] + 0];
		vertice3[1] = objeto.vertices[3*T[2] + 1];
		vertice3[2] = objeto.vertices[3*T[2] + 2];

		//vertice1
		vector1[0] = vertice2[0] - vertice1[0];
		vector1[1] = vertice2[1] - vertice1[1];
		vector1[2] = vertice2[2] - vertice1[2];

		vector2[0] = vertice3[0] - vertice1[0];
		vector2[1] = vertice3[1] - vertice1[1];
		vector2[2] = vertice3[2] - vertice1[2];

		vector1[0] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 1));
		vector1[1] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 2));
		vector1[2] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 3));

		vector2[0] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 1));
		vector2[1] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 2));
		vector2[2] *= sqrtf(metrica_metric(vertice1[0], vertice1[1], 3));

		vector3[0] = vector1[1]*vector2[2] - vector1[2]*vector2[1];
		vector3[1] = vector1[2]*vector2[0] - vector1[0]*vector2[2];
		vector3[2] = vector1[0]*vector2[1] - vector1[1]*vector2[0];
		area = sqrtf(vector3[0]*vector3[0] + vector3[1]*vector3[1] + vector3[2]*vector3[2])*0.5f;
		
		normalizaciones[T[0]] += area;
		float aux[3], aux2;
		aux[0] = objeto.normales[T[0]*3 + 0];
		aux[1] = objeto.normales[T[0]*3 + 1];
		aux[2] = objeto.normales[T[0]*3 + 2];
		aux2 = sqrtf(aux[0]*aux[0] + aux[1]*aux[1] + aux[2]*aux[2]);
// 		printf("vector1 = (%f, %f, %f), vector2 = (%f, %f, %f)\n", aux[0]/aux2, aux[1]/aux2, aux[2]/aux2, vector3[0]/(2*area), vector3[1]/(2*area), vector3[2]/(2*area));

		objeto.normales[T[0]*3 + 0] += vector3[0];
		objeto.normales[T[0]*3 + 1] += vector3[1];
		objeto.normales[T[0]*3 + 2] += vector3[2];

		

		//vertice2
		vector1[0] = vertice3[0] - vertice2[0];
		vector1[1] = vertice3[1] - vertice2[1];
		vector1[2] = vertice3[2] - vertice2[2];

		vector2[0] = vertice1[0] - vertice2[0];
		vector2[1] = vertice1[1] - vertice2[1];
		vector2[2] = vertice1[2] - vertice2[2];

		vector1[0] *= sqrtf(metrica_metric(vertice2[0], vertice2[1], 1));
		vector1[1] *= sqrtf(metrica_metric(vertice2[0], vertice2[1], 2));
		vector1[2] *= sqrtf(metrica_metric(vertice2[0], vertice2[1], 3));

		vector2[0] *= sqrtf(metrica_metric(vertice2[0], vertice2[1], 1));
		vector2[1] *= sqrtf(metrica_metric(vertice2[0], vertice2[1], 2));
		vector2[2] *= sqrtf(metrica_metric(vertice2[0], vertice2[1], 3));

		vector3[0] = vector1[1]*vector2[2] - vector1[2]*vector2[1];
		vector3[1] = vector1[2]*vector2[0] - vector1[0]*vector2[2];
		vector3[2] = vector1[0]*vector2[1] - vector1[1]*vector2[0];
		area = sqrtf(vector3[0]*vector3[0] + vector3[1]*vector3[1] + vector3[2]*vector3[2])*0.5f;
		
		normalizaciones[T[1]] += area;
		objeto.normales[T[1]*3 + 0] += vector3[0];
		objeto.normales[T[1]*3 + 1] += vector3[1];
		objeto.normales[T[1]*3 + 2] += vector3[2];

		//vertice3
		vector1[0] = vertice1[0] - vertice3[0];
		vector1[1] = vertice1[1] - vertice3[1];
		vector1[2] = vertice1[2] - vertice3[2];

		vector2[0] = vertice2[0] - vertice3[0];
		vector2[1] = vertice2[1] - vertice3[1];
		vector2[2] = vertice2[2] - vertice3[2];

		vector1[0] *= sqrtf(metrica_metric(vertice3[0], vertice3[1], 1));
		vector1[1] *= sqrtf(metrica_metric(vertice3[0], vertice3[1], 2));
		vector1[2] *= sqrtf(metrica_metric(vertice3[0], vertice3[1], 3));

		vector2[0] *= sqrtf(metrica_metric(vertice3[0], vertice3[1], 1));
		vector2[1] *= sqrtf(metrica_metric(vertice3[0], vertice3[1], 2));
		vector2[2] *= sqrtf(metrica_metric(vertice3[0], vertice3[1], 3));

		vector3[0] = vector1[1]*vector2[2] - vector1[2]*vector2[1];
		vector3[1] = vector1[2]*vector2[0] - vector1[0]*vector2[2];
		vector3[2] = vector1[0]*vector2[1] - vector1[1]*vector2[0];
		area = sqrtf(vector3[0]*vector3[0] + vector3[1]*vector3[1] + vector3[2]*vector3[2])*0.5f;
		
		normalizaciones[T[2]] += area;
		objeto.normales[T[2]*3 + 0] += vector3[0];
		objeto.normales[T[2]*3 + 1] += vector3[1];
		objeto.normales[T[2]*3 + 2] += vector3[2];
	}

	for(unsigned int i = 0; i < objeto.n_vert; i++){
		area = sqrtf(powf(objeto.normales[3*i + 0], 2) + powf(objeto.normales[3*i + 1], 2) + powf(objeto.normales[3*i + 2], 2));

		objeto.normales[3*i + 0] *= 1.f/area;
		objeto.normales[3*i + 1] *= 1.f/area;
		objeto.normales[3*i + 2] *= 1.f/area;
	}
	free(normalizaciones);
	printf("Normals calculated\n");
	return objeto;
}

struct object_data geodesica_attach(struct object_data *objetos, unsigned int n_objetos, float light_position[3], float d0, float theta0, float phi0){
    struct object_data objeto;
    float vectores2[4][4];
    objeto.n_vert   = 0;
    objeto.n_triang = 0;
    for(unsigned int i = 0; i < n_objetos; i++){
        objeto.n_vert   += objetos[i].n_vert;
        objeto.n_triang += objetos[i].n_triang;
    }
    printf("n vertices = %d, n triangulos = %d\n", objeto.n_vert, objeto.n_triang);
    
    vectores2[0][0] = 1.f; vectores2[0][1] = 0.f; vectores2[0][2] = 0.f; vectores2[0][3] = 0.f; 
	vectores2[1][0] = 0.f; vectores2[1][1] = 1.f; vectores2[1][2] = 0.f; vectores2[1][3] = 0.f; 
	vectores2[2][0] = 0.f; vectores2[2][1] = 0.f; vectores2[2][2] = 1.f; vectores2[2][3] = 0.f; 
	vectores2[3][0] = 0.f; vectores2[3][1] = 0.f; vectores2[3][2] = 0.f; vectores2[3][3] = 1.f; 

    
    objeto.vertices 	 = (float *) malloc(3*objeto.n_vert*sizeof(float));
	objeto.xyz_coord	 = (float *) malloc(3*objeto.n_vert*sizeof(float));

	objeto.normales      = (float *) malloc(3*objeto.n_vert*sizeof(float));

	objeto.u             = (float *) malloc(1*objeto.n_vert*sizeof(float));
	objeto.v             = (float *) malloc(1*objeto.n_vert*sizeof(float));
    objeto.triangulos 	 = (unsigned int *) malloc(3*objeto.n_triang*sizeof(unsigned int));

    printf("memoria alocatada\n");
    
    objeto.u = (float *) malloc(objeto.n_vert*sizeof(float));
    objeto.v = (float *) malloc(objeto.n_vert*sizeof(float));
    
	objeto.light_position[0] = light_position[0];
	objeto.light_position[1] = light_position[1];
	objeto.light_position[2] = light_position[2];

    
    //por adjuntar todas las coordenadas xyz de todos los objetos
    unsigned int start = 0;
    for(unsigned int i = 0; i < n_objetos; i++){
        for(unsigned int j = 0; j < objetos[i].n_vert; j++){
            objeto.xyz_coord[3*(j + start) + 0] = objetos[i].xyz_coord[3*j + 0];
            objeto.xyz_coord[3*(j + start) + 1] = objetos[i].xyz_coord[3*j + 1];
            objeto.xyz_coord[3*(j + start) + 2] = objetos[i].xyz_coord[3*j + 2];
                        
            objeto.u[j + start] = objetos[i].u[j];
            objeto.v[j + start] = objetos[i].v[j];
        }
        start += objetos[i].n_vert;
    }
    start = 0;
    unsigned int start_idx = 0;
    for(unsigned int i = 0; i < n_objetos; i++){
        for(unsigned int j = 0; j < objetos[i].n_triang; j++){        
            objeto.triangulos[3*(j + start) + 0] = objetos[i].triangulos[3*j + 0] + start_idx;
            objeto.triangulos[3*(j + start) + 1] = objetos[i].triangulos[3*j + 1] + start_idx;
            objeto.triangulos[3*(j + start) + 2] = objetos[i].triangulos[3*j + 2] + start_idx;
        }
        start += objetos[i].n_triang;
        start_idx += objetos[i].n_vert;
    }

    
    printf("por crear parte curvilinea\n");

	float posicion[4], *versor_u, distancia_fuente, vertice[3], *cuadrivector_U, normalizacion;
	unsigned char visible;
	versor_u 		= (float *) malloc(3*sizeof(float));
	cuadrivector_U  = (float *) malloc(4*sizeof(float));


	float derivada[3], distancia, *result;
	vertice[0] = d0;
	vertice[1] = theta0;
	vertice[2] = phi0;
	result = (float *) malloc(3*sizeof(float));

	for(unsigned int k = 0; k < objeto.n_vert; k++){
			derivada[0] = objeto.xyz_coord[3*k + 0]/sqrtf(metrica_metric(d0, theta0, 1));
			derivada[1] = - objeto.xyz_coord[3*k + 2]/sqrtf(metrica_metric(d0, theta0, 2));
			derivada[2] = objeto.xyz_coord[3*k + 1]/sqrtf(metrica_metric(d0, theta0, 3));
			distancia = sqrtf(powf(objeto.xyz_coord[3*k+0], 2) + powf(objeto.xyz_coord[3*k+1], 2) + powf(objeto.xyz_coord[3*k+2], 2));
			model_evolucionador(vertice, derivada, distancia, result);
			objeto.vertices[3*k + 0] = result[0];
			objeto.vertices[3*k + 1] = result[1];
			objeto.vertices[3*k + 2] = result[2];
	}

	free(objeto.xyz_coord);
	for(unsigned int i = 0; i < n_objetos; i++){
		free(objetos[i].xyz_coord);
		free(objetos[i].triangulos);
		free(objetos[i].u);
		free(objetos[i].v);
	}

	objeto = model_normal_creator(objeto);
	objeto = model_lighting_processing(objeto, objeto.light_position);
	return objeto;
}

void geodesica_matmul(float *matriz1, float *matriz2, float *matriz3){
    for(unsigned int i = 0; i < 3; i++){
        for(unsigned int j = 0; j < 3; j++){
            matriz3[3*i + j] =  matriz1[3*i + 0]*matriz2[3*0 + j] + 
                                matriz1[3*i + 1]*matriz2[3*1 + j] + 
                                matriz1[3*i + 2]*matriz2[3*2 + j];
        }
    }
}

void geodesica_object_rotate(float angulo1, float angulo2, float angulo3, struct object_data* objeto){
    float *matriz1, *matriz2, *matriz3, *matriz, *aux, x, y, z;
    
    matriz1 = (float *) malloc(9*sizeof(float));
    matriz2 = (float *) malloc(9*sizeof(float));
    matriz3 = (float *) malloc(9*sizeof(float));
    matriz  = (float *) malloc(9*sizeof(float));
    aux     = (float *) malloc(9*sizeof(float));
    
    matriz1[3*0 + 0] = cosf(angulo1);  matriz1[3*0 + 1] = sinf(angulo1);  matriz1[3*0 + 2] =           0.f;  
    matriz1[3*1 + 0] =-sinf(angulo1);  matriz1[3*1 + 1] = cosf(angulo1);  matriz1[3*1 + 2] =           0.f;  
    matriz1[3*2 + 0] =           0.f;  matriz1[3*2 + 1] =           0.f;  matriz1[3*2 + 2] =           1.f;  

    matriz2[3*0 + 0] =           1.f;  matriz2[3*0 + 1] =           0.f;  matriz2[3*0 + 2] =           0.f;  
    matriz2[3*1 + 0] =           0.f;  matriz2[3*1 + 1] = cosf(angulo2);  matriz2[3*1 + 2] = sinf(angulo2);  
    matriz2[3*2 + 0] =           0.f;  matriz2[3*2 + 1] =-sinf(angulo2);  matriz2[3*2 + 2] = cosf(angulo2);  
    
    matriz3[3*0 + 0] = cosf(angulo3);  matriz3[3*0 + 1] = sinf(angulo3);  matriz3[3*0 + 2] =           0.f;  
    matriz3[3*1 + 0] =-sinf(angulo3);  matriz3[3*1 + 1] = cosf(angulo3);  matriz3[3*1 + 2] =           0.f;  
    matriz3[3*2 + 0] =           0.f;  matriz3[3*2 + 1] =           0.f;  matriz3[3*2 + 2] =           1.f;  

    geodesica_matmul(matriz1, matriz2,    aux);
    geodesica_matmul(aux,     matriz3, matriz);
    
    for(unsigned int i = 0; i < objeto->n_vert; i++){
        x = objeto->xyz_coord[3*i + 0];
        y = objeto->xyz_coord[3*i + 1];
        z = objeto->xyz_coord[3*i + 2];
        
        objeto->xyz_coord[3*i + 0] = matriz[3*0 + 0]*x + matriz[3*0 + 1]*y + matriz[3*0 + 2]*z;
        objeto->xyz_coord[3*i + 1] = matriz[3*1 + 0]*x + matriz[3*1 + 1]*y + matriz[3*1 + 2]*z;
        objeto->xyz_coord[3*i + 2] = matriz[3*2 + 0]*x + matriz[3*2 + 1]*y + matriz[3*2 + 2]*z;

//         x = objeto->xyz_normales[3*i + 0];
//         y = objeto->xyz_normales[3*i + 1];
//         z = objeto->xyz_normales[3*i + 2];

//         objeto->xyz_normales[3*i + 0] = matriz[3*0 + 0]*x + matriz[3*0 + 1]*y + matriz[3*0 + 2]*z;
//         objeto->xyz_normales[3*i + 1] = matriz[3*1 + 0]*x + matriz[3*1 + 1]*y + matriz[3*1 + 2]*z;
//         objeto->xyz_normales[3*i + 2] = matriz[3*2 + 0]*x + matriz[3*2 + 1]*y + matriz[3*2 + 2]*z;
    }
    
    free(matriz1);
    free(matriz2);
    free(matriz3);
    free(matriz);
    free(aux);
}

struct object_data geodesica_rectangulo(unsigned int n, unsigned int m, float dir1[3], float dir2[3], float x0[3], int signo){
    // esto solo crea las coordenadas 3d y arma los triangulos
    printf("armando rectangulo\n");
    struct object_data objeto;
    unsigned int N = n*m, i, j;
    
    float u, v, offset, x, y, z;
    offset = sqrtf(powf(x0[0], 2) + powf(x0[1], 2) + powf(x0[2], 2));
    
    objeto.n_vert = N;
    objeto.xyz_coord	 = (float *) malloc(3*objeto.n_vert*sizeof(float));
// 	objeto.xyz_normales  = (float *) malloc(3*objeto.n_vert*sizeof(float));
	objeto.u             = (float *) malloc(1*objeto.n_vert*sizeof(float));
	objeto.v             = (float *) malloc(1*objeto.n_vert*sizeof(float));

    
    for(i = 0; i < n; i++){
        u = -0.5f + i*1.f/(n - 1);
        for(j = 0; j < m; j++){
            v = -0.5f + j*1.f/(m - 1);
            
            x = x0[0] + dir1[0]*u + dir2[0]*v;
            y = x0[1] + dir1[1]*u + dir2[1]*v;
            z = x0[2] + dir1[2]*u + dir2[2]*v;
            
            objeto.xyz_coord[3*(i*m + j) + 0] = x;
            objeto.xyz_coord[3*(i*m + j) + 1] = y;
            objeto.xyz_coord[3*(i*m + j) + 2] = z;
            
            objeto.u[i*m + j] = u + 0.5f;
            objeto.v[i*m + j] = v + 0.5f;
        }
    }
    
    //armado de triangulos
    objeto.n_triang = 2*(n - 1)*(m - 1);
    objeto.triangulos 	 = (unsigned int *) malloc(3*objeto.n_triang*sizeof(unsigned int));


	if(signo == 1){
		for(i = 0; i < n - 1; i++){
		    //triangulos de arriba
		    for(j = 0; j < m - 1; j++){
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 0) + 0] = i*m + j;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 0) + 1] = (i + 1)*m + j + 1;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 0) + 2] = i*m + j + 1;
		    }
		    //triangulos de abajo
		    for(j = 0; j < m - 1; j++){
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 1) + 0] = i*m + j;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 1) + 1] = (i + 1)*m + j + 0;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 1) + 2] = (i + 1)*m + j + 1;
		    }
		}
	}
	else{
		for(i = 0; i < n - 1; i++){
		    //triangulos de arriba
		    for(j = 0; j < m - 1; j++){
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 0) + 0] = i*m + j;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 0) + 2] = (i + 1)*m + j + 1;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 0) + 1] = i*m + j + 1;
		    }
		    //triangulos de abajo
		    for(j = 0; j < m - 1; j++){
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 1) + 0] = i*m + j;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 1) + 2] = (i + 1)*m + j + 0;
		        objeto.triangulos[3*((i*(m - 1) + j)*2 + 1) + 1] = (i + 1)*m + j + 1;
		    }
		}
	}

    return objeto;
}


struct object_data geodesica_cubo(unsigned int n, unsigned int m, unsigned int p, float light_position[3], 
                                  float d0,      float theta0,  float phi0, 
                                  float a,       float b,       float c, 
                                  float angulo1, float angulo2, float angulo3){
    struct object_data *objetos;
    objetos = (struct object_data*) malloc(6*sizeof(struct object_data));
    float dir1[3] = {1.f, 0.f, 0.f}, dir2[3] = {0.f, 1.f, 0.f}, x0[3] = {0.f, 0.f, 1.f};
    
    //cara 1 y opuesta
    dir1[0] =   a; dir1[1] = 0.f; dir1[2] = 0.f;
    dir2[0] = 0.f; dir2[1] =   b; dir2[2] = 0.f;
    x0[0]   = 0.f; x0[1]   = 0.f; x0[2]   = c*0.5f;
    objetos[0] = geodesica_rectangulo(n, m, dir1, dir2,  x0,  1);
    x0[0]   = 0.f; x0[1]   = 0.f; x0[2]   = -c*0.5f;
    objetos[1] = geodesica_rectangulo(n, m, dir1, dir2,  x0, -1);

    //cara 2 y opuesta
    dir1[0] =   a; dir1[1] = 0.f; dir1[2] = 0.f;
    dir2[0] = 0.f; dir2[1] = 0.f; dir2[2] =   c;
    x0[0]   = 0.f; x0[1]   =  b*0.5f; x0[2]   = 0.f;
    objetos[2] = geodesica_rectangulo(n, p, dir1, dir2,  x0, -1);
    x0[0]   = 0.f; x0[1]   = -b*0.5f; x0[2]   = 0.f;
    objetos[3] = geodesica_rectangulo(n, p, dir1, dir2,  x0,  1);

    //cara 3 y opuesta
    dir1[0] = 0.f; dir1[1] =   b; dir1[2] = 0.f;
    dir2[0] = 0.f; dir2[1] = 0.f; dir2[2] =   c;
    x0[0]   = 0.5f*a; x0[1]   = 0.f; x0[2]   = 0.f;
    objetos[4] = geodesica_rectangulo(m, p, dir1, dir2,  x0,  1);
    x0[0]   = -0.5f*a; x0[1]   = 0.f; x0[2]   = 0.f;
    objetos[5] = geodesica_rectangulo(m, p, dir1, dir2,  x0, -1);

//    geodesica_object_rotate(0., 0., 0.,  &objetos[0]);
    geodesica_object_rotate(angulo1, angulo2, angulo3, &objetos[0]);
    geodesica_object_rotate(angulo1, angulo2, angulo3, &objetos[1]);
    geodesica_object_rotate(angulo1, angulo2, angulo3, &objetos[2]);
    geodesica_object_rotate(angulo1, angulo2, angulo3, &objetos[3]);
    geodesica_object_rotate(angulo1, angulo2, angulo3, &objetos[4]);
    geodesica_object_rotate(angulo1, angulo2, angulo3, &objetos[5]);
    struct object_data objeto = geodesica_attach(objetos, 6, light_position, d0, theta0, phi0);
    free(objetos);

	printf("Lados = (%f, %f, %f), Superficie euclideana = %f, Superficie = %f, ratio = %f\n", a, b, c, 2*(a*b + a*c + c*b), model_surface(objeto), model_surface(objeto)/(2*(a*b + a*c + c*b)));
    return objeto;
}


struct object_data geodesica_inicializador_esfera1(unsigned int n, unsigned int m, float light_position[3], float d0, float theta0, 
													float phi0, float radio){
	struct object_data objeto;
    unsigned int N;
    N = 2 + (n - 2)*m;
    float r, theta, phi, x, y, z, vectores2[4][4];
    
// 	objeto.vertices 	 = (float *) malloc(3*N*sizeof(float));
	objeto.xyz_coord	 = (float *) malloc(3*N*sizeof(float));

    objeto.u = (float *) malloc(N*sizeof(float));
    objeto.v = (float *) malloc(N*sizeof(float));
    
	objeto.n_vert = N;
	objeto.light_position[0] = light_position[0];
	objeto.light_position[1] = light_position[1];
	objeto.light_position[2] = light_position[2];



	vectores2[0][0] = 1.f; vectores2[0][1] = 0.f; vectores2[0][2] = 0.f; vectores2[0][3] = 0.f; 
	vectores2[1][0] = 0.f; vectores2[1][1] = 1.f; vectores2[1][2] = 0.f; vectores2[1][3] = 0.f; 
	vectores2[2][0] = 0.f; vectores2[2][1] = 0.f; vectores2[2][2] = 1.f; vectores2[2][3] = 0.f; 
	vectores2[3][0] = 0.f; vectores2[3][1] = 0.f; vectores2[3][2] = 0.f; vectores2[3][3] = 1.f; 


	//##############################################################################################################################
	//############################ parte euclidiana, definiciones de vertices y normales como si fuera todo euclidiano
	//##############################################################################################################################
    printf("por crear parte euclidiana\n");
	//vertice del polo norte
    r     = radio;
    theta = 0.f;
    phi   = 0.f;
    x = radio*sinf(theta)*cosf(phi); y = radio*sinf(theta)*sinf(phi); z = radio*cosf(theta);
    
	objeto.xyz_coord[0] = x;
	objeto.xyz_coord[1] = y;
	objeto.xyz_coord[2] = z;

    objeto.u[0] = 0.f;
    objeto.v[0] = 0.f;

	//vertices de latitudes no polares
    for(unsigned int i = 0; i < n - 2; i++){
        for(unsigned int j = 0; j < m ; j++){
            r     = radio;
            theta = (pi*(i + 1))/(n - 1);
            phi   = 2.f*pi*j/(m - 1);
            x = radio*sinf(theta)*cosf(phi); y = radio*sinf(theta)*sinf(phi); z = radio*cosf(theta);
    
            objeto.u[i*m + 1 + j] = phi*0.5f/pi;
            objeto.v[i*m + 1 + j] = theta/pi;

			objeto.xyz_coord[3*(i*m + 1 + j) + 0] = x;
			objeto.xyz_coord[3*(i*m + 1 + j) + 1] = y;
			objeto.xyz_coord[3*(i*m + 1 + j) + 2] = z;
		}
	}

	//vertice del polo sur
    r     = radio;
    theta = pi;
    phi   = 0.f;
    x = radio*sinf(theta)*cosf(phi); y = radio*sinf(theta)*sinf(phi); z = radio*cosf(theta);
    
	objeto.xyz_coord[3*(N - 1) + 0] = x;
	objeto.xyz_coord[3*(N - 1) + 1] = y;
	objeto.xyz_coord[3*(N - 1) + 2] = z;

    objeto.u[N - 1] = 0.f;
    objeto.v[N - 1] = 1.f;

	//##############################################################################################################################
    //armado de triangulos
	//##############################################################################################################################
	printf("armando triangulos\n");
    objeto.n_triang   = 2*(m - 1)*(n - 2);
    objeto.triangulos = (unsigned int*) malloc(3*objeto.n_triang*sizeof(unsigned int));
    
    unsigned int control = 0;
    for(unsigned int j = 0; j < m - 1; j++){
        objeto.triangulos[3*(j) + 0] = 0;
        objeto.triangulos[3*(j) + 1] = j + 1;
        objeto.triangulos[3*(j) + 2] = j + 2;
        control += 1;
    }
    control = 0;
    for(unsigned int i = 0; i < (n - 3); i += 1){
        for(unsigned int j = 0; j < m - 1; j++){
            objeto.triangulos[3*(2*i*(m - 1) + m - 1 + j) + 0] = i*m + 1 + j;
            objeto.triangulos[3*(2*i*(m - 1) + m - 1 + j) + 1] = (i + 1)*m + 1 + j + 1;
            objeto.triangulos[3*(2*i*(m - 1) + m - 1 + j) + 2] = i*m + 1 + j + 1;
            control += 1;
        }

        for(unsigned int j = 0; j < m - 1; j++){
            objeto.triangulos[3*(2*i*(m - 1) + 2*(m - 1) + j) + 0] = i*m + 1 + j;
            objeto.triangulos[3*(2*i*(m - 1) + 2*(m - 1) + j) + 1] = (i + 1)*m + 1 + j + 0;
            objeto.triangulos[3*(2*i*(m - 1) + 2*(m - 1) + j) + 2] = (i + 1)*m + 1 + j + 1;
            control += 1;
        }
    }
    control = 0;
    for(unsigned int i = objeto.n_triang - m + 1; i < objeto.n_triang; i++){
        objeto.triangulos[3*(i) + 0] = N - 1;
        objeto.triangulos[3*(i) + 1] = N - 1 + i - (objeto.n_triang - m + 1) - m;
        objeto.triangulos[3*(i) + 2] = N - 0 + i - (objeto.n_triang - m + 1) - m;
        control += 1;
    }
    printf("armado terminado\n");

	struct object_data objeto_final = geodesica_attach(&objeto, 1, light_position, d0, theta0, phi0);

	printf("Radio = %f, Superficie euclideana = %f, Superficie = %f\n", radio, 4*pi*radio*radio, model_surface(objeto_final));
// 	model_normal_creator(objeto_final);
	return objeto_final;
}

struct object_data geodesica_inicializador_esfera2(unsigned int n, unsigned int m, float light_position[3], float d0, float theta0, 
													float phi0, float radio){
	struct object_data objeto;
    unsigned int N;
    N = n*m;
    float r, theta, phi, x, y, z, vectores2[4][4];
    
// 	objeto.vertices 	 = (float *) malloc(3*N*sizeof(float));
	objeto.xyz_coord	 = (float *) malloc(3*N*sizeof(float));

    objeto.u = (float *) malloc(N*sizeof(float));
    objeto.v = (float *) malloc(N*sizeof(float));
    
	objeto.n_vert = N;
	objeto.light_position[0] = light_position[0];
	objeto.light_position[1] = light_position[1];
	objeto.light_position[2] = light_position[2];



	vectores2[0][0] = 1.f; vectores2[0][1] = 0.f; vectores2[0][2] = 0.f; vectores2[0][3] = 0.f; 
	vectores2[1][0] = 0.f; vectores2[1][1] = 1.f; vectores2[1][2] = 0.f; vectores2[1][3] = 0.f; 
	vectores2[2][0] = 0.f; vectores2[2][1] = 0.f; vectores2[2][2] = 1.f; vectores2[2][3] = 0.f; 
	vectores2[3][0] = 0.f; vectores2[3][1] = 0.f; vectores2[3][2] = 0.f; vectores2[3][3] = 1.f; 


	//##############################################################################################################################
	//############################ parte euclidiana, definiciones de vertices y normales como si fuera todo euclidiano
	//##############################################################################################################################
    printf("por crear parte euclidiana\n");

    for(unsigned int i = 0; i < n; i++){
        for(unsigned int j = 0; j < m ; j++){
            r     = radio;
            theta = i*pi/(n - 1);
            phi   = 2.f*j*pi/(m - 1);
            x = radio*sinf(theta)*cosf(phi); y = radio*sinf(theta)*sinf(phi); z = radio*cosf(theta);
    
            objeto.u[i*m + j] = phi*0.5f/pi;
            objeto.v[i*m + j] = theta/pi;

			objeto.xyz_coord[3*(i*m + j) + 0] = x;
			objeto.xyz_coord[3*(i*m + j) + 1] = y;
			objeto.xyz_coord[3*(i*m + j) + 2] = z;
		}
	}
	objeto.n_vert = N;
	//##############################################################################################################################
    //armado de triangulos
	//##############################################################################################################################
	printf("armando triangulos\n");
    objeto.n_triang   = 2*(m - 1)*(n - 2);
    objeto.triangulos = (unsigned int*) malloc(3*objeto.n_triang*sizeof(unsigned int));
    
    unsigned int control = 0;
    for(unsigned int j = 0; j < m - 1; j++){
        objeto.triangulos[3*j + 0] = j;
        objeto.triangulos[3*j + 1] = j + m;
        objeto.triangulos[3*j + 2] = j + m + 1;
        control += 1;
    }
    control = 0;
    for(unsigned int i = 1; i < (n - 2); i += 1){
        for(unsigned int j = 0; j < m - 1; j++){
			printf("triangulo %d, de %d, vertice %d, de %d\n", (((m - 1)*(i - 1) + j)*2 + m - 1), objeto.n_triang, (i + 1)*(m - 1) + j + 1, objeto.n_vert);

            objeto.triangulos[3*(((m - 1)*(i - 1) + j)*2 + m - 1) + 0] = i*m + j;
            objeto.triangulos[3*(((m - 1)*(i - 1) + j)*2 + m - 1) + 1] = (i + 1)*m + j;
            objeto.triangulos[3*(((m - 1)*(i - 1) + j)*2 + m - 1) + 2] = (i + 1)*m + j + 1;
            control += 1;
        }

        for(unsigned int j = 0; j < m - 1; j++){
			printf("triangulo %d, de %d, vertice %d, de %d\n", (((m - 1)*(i - 1) + j)*2 + m), objeto.n_triang, (i + 1)*(m - 1) + j + 1, objeto.n_vert);
            objeto.triangulos[3*(((m - 1)*(i - 1) + j)*2 + m) + 0] = i*m + j;
            objeto.triangulos[3*(((m - 1)*(i - 1) + j)*2 + m) + 1] = (i + 1)*m + j + 1;
            objeto.triangulos[3*(((m - 1)*(i - 1) + j)*2 + m) + 2] = i*m + j + 1;
            control += 1;
        }
    }
    control = 0;
    for(unsigned int j = 0; j < m - 1; j++){
        objeto.triangulos[3*(objeto.n_triang - m + 1 + j) + 0] = j + N + 1 - m;
        objeto.triangulos[3*(objeto.n_triang - m + 1 + j) + 1] = j + N + 1 - m - m + 1;
        objeto.triangulos[3*(objeto.n_triang - m + 1 + j) + 2] = j + N + 1 - m - m;
        control += 1;
    }
    printf("armado terminado\n");

	struct object_data objeto_final = geodesica_attach(&objeto, 1, light_position, d0, theta0, phi0);

	printf("Radio = %f, Superficie euclideana = %f, Superficie = %f\n", radio, 4*pi*radio*radio, model_surface(objeto_final));
// 	model_normal_creator(objeto_final);
	return objeto_final;
}

struct object_data geodesica_inicializador_background1(unsigned int n, unsigned int m, float light_position[3], float rho){
	struct object_data objeto;
    unsigned int N;
    N = n*m;
    float theta, phi, x, y, z, vectores2[4][4];
    
	objeto.vertices 	 = (float *) malloc(3*N*sizeof(float));
	objeto.xyz_coord	 = (float *) malloc(3*N*sizeof(float));

    objeto.u = (float *) malloc(N*sizeof(float));
    objeto.v = (float *) malloc(N*sizeof(float));
    
	objeto.n_vert = N;
	objeto.light_position[0] = light_position[0];
	objeto.light_position[1] = light_position[1];
	objeto.light_position[2] = light_position[2];



	vectores2[0][0] = 1.f; vectores2[0][1] = 0.f; vectores2[0][2] = 0.f; vectores2[0][3] = 0.f; 
	vectores2[1][0] = 0.f; vectores2[1][1] = 1.f; vectores2[1][2] = 0.f; vectores2[1][3] = 0.f; 
	vectores2[2][0] = 0.f; vectores2[2][1] = 0.f; vectores2[2][2] = 1.f; vectores2[2][3] = 0.f; 
	vectores2[3][0] = 0.f; vectores2[3][1] = 0.f; vectores2[3][2] = 0.f; vectores2[3][3] = 1.f; 


	//##############################################################################################################################
	//############################ parte euclidiana, definiciones de vertices y normales como si fuera todo euclidiano
	//##############################################################################################################################

	//vertices de latitudes no polares
    for(unsigned int i = 0; i < n; i++){
        for(unsigned int j = 0; j < m ; j++){
            theta = (pi*i)/(n - 1);
            phi   = 2.f*pi*j/(m - 1);
    
            objeto.u[i*m + j] = phi/(2.f*pi);
            objeto.v[i*m + j] = theta/pi;

			objeto.vertices[3*(i*m + j) + 0] = rho;
			objeto.vertices[3*(i*m + j) + 1] = theta;
			objeto.vertices[3*(i*m + j) + 2] = phi;
		}
	}

	//##############################################################################################################################
    //armado de triangulos
	//##############################################################################################################################
	printf("armando triangulos\n");
    objeto.n_triang   = 2*(m - 1)*(n - 1);
    objeto.triangulos = (unsigned int*) malloc(3*objeto.n_triang*sizeof(unsigned int));
    
    for(unsigned int i = 0; i < n - 1; i++){
		for(unsigned int j = 0; j < m - 1; j++){
			objeto.triangulos[6*((m - 1)*i + j) + 0] = i*m + j;
			objeto.triangulos[6*((m - 1)*i + j) + 1] = (i + 1)*m + j;
			objeto.triangulos[6*((m - 1)*i + j) + 2] = (i + 1)*m + j + 1;

			objeto.triangulos[6*((m - 1)*i + j) + 3] = i*m + j;
			objeto.triangulos[6*((m - 1)*i + j) + 4] = (i + 1)*m + j + 1;
			objeto.triangulos[6*((m - 1)*i + j) + 5] = i*m + j + 1; 
			
		}
    }
    printf("armado terminado\n");

// 	struct object_data objeto_final = geodesica_attach(&objeto, 1, light_position, d0, theta0, phi0);

// 	printf("Radio = %f, Superficie euclideana = %f, Superficie = %f\n", radio, 4*pi*radio*radio, model_surface(objeto_final));
//  	struct object_data objeto_final = model_normal_creator(objeto);
	return objeto;
}

float geodesica_cilindro(float theta, float L, float R, char *tipo){
    float ang_crit;
    ang_crit = atanf(2*R/L);

    if(theta < ang_crit){
		*tipo = 1;
        return 0.5f*L/cosf(theta);
    }
    else if(theta  < pi - ang_crit){
		*tipo = 0;
        return R/cosf(pi*0.5f - theta);
    }    
    else{
		*tipo = -1;
        return 0.5f*L/cosf(pi - theta);
    }
}


struct object_data geodesica_inicializador_cilindro(unsigned int n, unsigned int m, float light_position[3], float d0, float theta0, float phi0, float radio, float h){
	struct object_data objeto;
    unsigned int N;
    N = 2 + (n - 2)*m;
    float r, theta, phi, x, y, z, vectores2[4][4];
    
	objeto.vertices 	 = (float *) malloc(3*N*sizeof(float));
	objeto.xyz_coord	 = (float *) malloc(3*N*sizeof(float));

    objeto.u = (float *) malloc(N*sizeof(float));
    objeto.v = (float *) malloc(N*sizeof(float));
    
	objeto.n_vert = N;
	objeto.light_position[1] = light_position[0];
	objeto.light_position[1] = light_position[1];
	objeto.light_position[2] = light_position[2];
// 	objeto.light_position[3] = light_position[2];



	vectores2[0][0] = 1.f; vectores2[0][1] = 0.f; vectores2[0][2] = 0.f; vectores2[0][3] = 0.f; 
	vectores2[1][0] = 0.f; vectores2[1][1] = 1.f; vectores2[1][2] = 0.f; vectores2[1][3] = 0.f; 
	vectores2[2][0] = 0.f; vectores2[2][1] = 0.f; vectores2[2][2] = 1.f; vectores2[2][3] = 0.f; 
	vectores2[3][0] = 0.f; vectores2[3][1] = 0.f; vectores2[3][2] = 0.f; vectores2[3][3] = 1.f; 


	//##############################################################################################################################
	//############################ parte euclidiana, definiciones de vertices y normales como si fuera todo euclidiano
	//##############################################################################################################################
    printf("por crear parte euclidiana\n");
	//vertice del polo norte
    r     = radio;
    theta = 0.f;
    phi   = 0.f;
    x = radio*sinf(theta)*cosf(phi); y = radio*sinf(theta)*sinf(phi); z = radio*cosf(theta);
    
	objeto.xyz_coord[0] = 0.f;
	objeto.xyz_coord[1] = 0.f;
	objeto.xyz_coord[2] = 0.5f*h;

    objeto.u[0] = 0.f;
    objeto.v[0] = 0.f;

	char tipo;
	//vertices de latitudes no polares
    for(unsigned int i = 0; i < n - 2; i++){
        for(unsigned int j = 0; j < m ; j++){
            theta = (pi*(i + 1))/(n - 1);
            phi   = 2.f*pi*j/(m - 1);
    		r     = geodesica_cilindro(theta, h, radio, &tipo);

            x = r*sinf(theta)*cosf(phi); y = r*sinf(theta)*sinf(phi); z = r*cosf(theta);
            objeto.u[i*m + 1 + j] = phi*0.5f/pi;
            objeto.v[i*m + 1 + j] = theta/pi;

			objeto.xyz_coord[3*(i*m + 1 + j) + 0] = x;
			objeto.xyz_coord[3*(i*m + 1 + j) + 1] = y;
			objeto.xyz_coord[3*(i*m + 1 + j) + 2] = z;
		}
	}

	//vertice del polo sur
    r     = radio;
    theta = pi;
    phi   = 0.f;
    x = radio*sinf(theta)*cosf(phi); y = radio*sinf(theta)*sinf(phi); z = radio*cosf(theta);
    
	objeto.xyz_coord[3*(N - 1) + 0] = 0.f;
	objeto.xyz_coord[3*(N - 1) + 1] = 0.f;
	objeto.xyz_coord[3*(N - 1) + 2] = -0.5f*h;

    objeto.u[N - 1] = 0.f;
    objeto.v[N - 1] = 1.f;
    

	//##############################################################################################################################
    //armado de triangulos
	//##############################################################################################################################
	printf("armando triangulos\n");
    objeto.n_triang   = 2*(m - 1)*(n - 2);
    objeto.triangulos = (unsigned int*) malloc(3*objeto.n_triang*sizeof(unsigned int));
    
    unsigned int control = 0;
    for(unsigned int j = 0; j < m - 1; j++){
        objeto.triangulos[3*(j) + 0] = 0;
        objeto.triangulos[3*(j) + 1] = j + 1;
        objeto.triangulos[3*(j) + 2] = j + 2;
        control += 1;
    }
    control = 0;
    for(unsigned int i = 0; i < (n - 3); i += 1){
        for(unsigned int j = 0; j < m - 1; j++){
            objeto.triangulos[3*(2*i*(m - 1) + m - 1 + j) + 0] = i*m + 1 + j;
            objeto.triangulos[3*(2*i*(m - 1) + m - 1 + j) + 1] = (i + 1)*m + 1 + j + 1;
            objeto.triangulos[3*(2*i*(m - 1) + m - 1 + j) + 2] = i*m + 1 + j + 1;
            control += 1;
        }

        for(unsigned int j = 0; j < m - 1; j++){
            objeto.triangulos[3*(2*i*(m - 1) + 2*(m - 1) + j) + 0] = i*m + 1 + j;
            objeto.triangulos[3*(2*i*(m - 1) + 2*(m - 1) + j) + 1] = (i + 1)*m + 1 + j + 0;
            objeto.triangulos[3*(2*i*(m - 1) + 2*(m - 1) + j) + 2] = (i + 1)*m + 1 + j + 1;
            control += 1;
        }
    }
    control = 0;
    for(unsigned int i = objeto.n_triang - m + 1; i < objeto.n_triang; i++){
        objeto.triangulos[3*(i) + 0] = N - 1;
        objeto.triangulos[3*(i) + 1] = N - 1 + i - (objeto.n_triang - m + 1) - m;
        objeto.triangulos[3*(i) + 2] = N - 0 + i - (objeto.n_triang - m + 1) - m;
        control += 1;
    }

	struct object_data objeto_final = geodesica_attach(&objeto, 1, light_position, d0, theta0, phi0);
	
	printf("Radio = %f, altura = %f, Superficie euclideana = %f, Superficie = %f\n", radio, h, pi*2*radio*h + 2*pi*radio*radio, model_surface(objeto_final));
	return objeto_final;
}
struct object_data model_uv_mapping(struct object_data modelo){
	float vertice[3], theta, phi;
	struct object_data objeto = modelo;
	
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertice[0] = objeto.xyz_coord[3*i + 0];
		vertice[1] = objeto.xyz_coord[3*i + 1];
		vertice[2] = objeto.xyz_coord[3*i + 2];
		
		geodesica_xyz2esf(vertice[0], vertice[1], vertice[2], &theta, &phi);
		objeto.u[i] = phi/(2*pi)*deg2rad;
		objeto.v[i] = theta/pi*deg2rad;
	}
	return objeto;
}
struct object_data geodesica_load_obj2(char* file_name, char with, float light_position[3], float d0, float theta0, float phi0, float scale){
    struct object_data objeto;
    FILE *pfout;
    pfout = fopen(file_name, "r");
    if(pfout == NULL ){
        printf("Impossible to open the file !\n");
    }
    
    objeto.n_vert   = 0;
    objeto.n_triang = 0;
    
    while( 1 ){
        char lineHeader[128];
        // Lee la primera palabra de la línea
        int res = fscanf(pfout, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File, es decir, el final del archivo. Se finaliza el ciclo.

        char tipo[2];
        tipo[0] = lineHeader[0];
        tipo[1] = lineHeader[1];
        
        if (strcmp(tipo, "v" ) == 0 ){
            objeto.n_vert += 1;
            fscanf(pfout, "\n");
        }
        else if (strcmp(tipo, "vt" ) == 0 ){
            fscanf(pfout, "\n");
        }
        else if (strcmp(tipo, "f" ) == 0 ){
            objeto.n_triang += 1;
            fscanf(pfout, "\n");
        }
        printf("leyendo %d, %d\r", objeto.n_vert, objeto.n_triang);
    }
	printf("lectura completada!, vertices %d, triangulos %d\n", objeto.n_vert, objeto.n_triang);
	float *normales, *u, *v;

    objeto.xyz_coord    = (float *) malloc(3*objeto.n_vert*sizeof(float));
    objeto.xyz_normales = (float *) malloc(3*objeto.n_vert*sizeof(float));
    normales			= (float *) malloc(3*objeto.n_vert*sizeof(float));
    u          		    = (float *) malloc(1*objeto.n_vert*sizeof(float));
    v            		= (float *) malloc(1*objeto.n_vert*sizeof(float));
    objeto.u            = (float *) malloc(1*objeto.n_vert*sizeof(float));
    objeto.v            = (float *) malloc(1*objeto.n_vert*sizeof(float));
    objeto.triangulos   = (unsigned int *) malloc(3*objeto.n_triang*sizeof(unsigned int));
    fclose(pfout);
    pfout = fopen(file_name, "r");
    float vertice[3], normal[3], vt[2];
    

    memset(u, 0, objeto.n_vert*sizeof(float));
    memset(objeto.u, 0, objeto.n_vert*sizeof(float));
    memset(v, 0, objeto.n_vert*sizeof(float));
    memset(objeto.v, 0, objeto.n_vert*sizeof(float));
    memset(normales, 0, 3*objeto.n_vert*sizeof(float));
    
    unsigned int asdf;
    if(with == 1)
        asdf = 9;
    else
        asdf = 6;
    unsigned int boludez[asdf];
    unsigned int vert_idx = 0, triang_idx = 0, normal_idx = 0, tv_idx = 0;
    printf("por leer de nuevo\n");
    while( 1 ){
		printf("avanzando, %d, %d, %d, %d\n", vert_idx, tv_idx, normal_idx, triang_idx);
        char lineHeader[128];
        // Lee la primera palabra de la línea
        int res = fscanf(pfout, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File, es decir, el final del archivo. Se finaliza el ciclo.

        char tipo[2];
        tipo[0] = lineHeader[0];
        tipo[1] = lineHeader[1];
        
        if (strcmp(tipo, "v" ) == 0 ){
            fscanf(pfout, "%f %f %f\n", &vertice[0], &vertice[1], &vertice[2]);
            objeto.xyz_coord[3*vert_idx + 0] = vertice[0]*scale;
            objeto.xyz_coord[3*vert_idx + 1] = vertice[1]*scale;
            objeto.xyz_coord[3*vert_idx + 2] = vertice[2]*scale;
            vert_idx += 1;
        }
        else if (strcmp(tipo, "vt" ) == 0 ){
            fscanf(pfout, "%f %f\n", &vt[0], &vt[1]);
            u[tv_idx] = vt[0];
            v[tv_idx] = vt[1];
            tv_idx  += 1;
        }
        else if (strcmp(tipo, "vn" ) == 0 ){
            fscanf(pfout, "%f %f %f\n", &normal[0], &normal[1], &normal[2]);
            normales[3*normal_idx + 0] = normal[0];
            normales[3*normal_idx + 1] = normal[1];
            normales[3*normal_idx + 2] = normal[2];
            normal_idx += 1;
        }
        else if (strcmp(tipo, "f" ) == 0 ){
            if(with == 1){
                fscanf(pfout, "%d//%d %d//%d %d//%d %d//%d\n", &boludez[0], &boludez[2], &boludez[3], &boludez[5], &boludez[6], &boludez[8]);
                objeto.triangulos[3*triang_idx + 0] = boludez[0] - 1;
                objeto.triangulos[3*triang_idx + 1] = boludez[3] - 1;
                objeto.triangulos[3*triang_idx + 2] = boludez[6] - 1;

// 				objeto.xyz_normales[3*(boludez[0] - 1) + 0] = normales[3*(boludez[2] - 1) + 0];
// 				objeto.xyz_normales[3*(boludez[0] - 1) + 1] = normales[3*(boludez[2] - 1) + 1];
// 				objeto.xyz_normales[3*(boludez[0] - 1) + 2] = normales[3*(boludez[2] - 1) + 2];

// 				objeto.xyz_normales[3*(boludez[3] - 1) + 0] = normales[3*(boludez[5] - 1) + 0];
// 				objeto.xyz_normales[3*(boludez[3] - 1) + 1] = normales[3*(boludez[5] - 1) + 1];
// 				objeto.xyz_normales[3*(boludez[3] - 1) + 2] = normales[3*(boludez[5] - 1) + 2];

// 				objeto.xyz_normales[3*(boludez[6] - 1) + 0] = normales[3*(boludez[8] - 1) + 0];
// 				objeto.xyz_normales[3*(boludez[6] - 1) + 1] = normales[3*(boludez[8] - 1) + 1];
// 				objeto.xyz_normales[3*(boludez[6] - 1) + 2] = normales[3*(boludez[8] - 1) + 2];

// 				objeto.u[boludez[0] - 1] = u[boludez[1] - 1];
// 				objeto.u[boludez[3] - 1] = u[boludez[4] - 1];
// 				objeto.u[boludez[6] - 1] = u[boludez[7] - 1];
// 				objeto.v[boludez[0] - 1] = v[boludez[1] - 1];
// 				objeto.v[boludez[3] - 1] = v[boludez[4] - 1];
// 				objeto.v[boludez[6] - 1] = v[boludez[7] - 1];

            }
            else{
                fscanf(pfout, "%d %d %d\n", &boludez[0], &boludez[1], &boludez[2]);
                objeto.triangulos[3*triang_idx + 0] = boludez[0] - 1;
                objeto.triangulos[3*triang_idx + 1] = boludez[1] - 1;
                objeto.triangulos[3*triang_idx + 2] = boludez[2] - 1;
            }
            triang_idx += 1;
        }
    }
    free(u);
    free(v);
    free(normales);
    printf("datos cargados, por transformar, numero de vertices = %d, numero de triangulos = %d\n", objeto.n_vert, objeto.n_triang);
	objeto = model_uv_mapping(objeto);
    struct object_data planeta;
    planeta = geodesica_attach(&objeto, 1, light_position, d0, theta0, phi0);
    printf("planeta creado, por retornar\n");
    return planeta;
}

struct object_data model_lighting_processing(struct object_data objeto_inicial, float light_position[3]){
	struct object_data objeto;
	objeto = objeto_inicial;
	float vert_position[3], *distancia_fuente, *versores, *solution_type, *alfas, normal[3], vec_perp[3], vec_norm[3], coseno, *intensidades;

	distancia_fuente = (float *) malloc(N_GEO*sizeof(float));
	alfas            = (float *) malloc(N_GEO*sizeof(float));
	solution_type    = (float *) malloc(N_GEO*sizeof(float));
	versores         = (float *) malloc(N_GEO*3*sizeof(float));
	objeto.lights_info    = (float *) malloc(objeto.n_vert*N_GEO*4*sizeof(float));
	intensidades    = (float *) malloc(N_GEO*sizeof(float));


	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vert_position[0] = objeto.vertices[3*i + 0];
		vert_position[1] = objeto.vertices[3*i + 1];
		vert_position[2] = objeto.vertices[3*i + 2];

		normal[0] = objeto.normales[3*i + 0];
		normal[1] = objeto.normales[3*i + 1];
		normal[2] = objeto.normales[3*i + 2];
		
		geodesica_geodesica4(vert_position, light_position, distancia_fuente, alfas, solution_type, versores);
		model_intensidad(light_position, vert_position, intensidades);
		for(unsigned int j = 0; j < N_GEO; j++){
			coseno =  versores[3*j + 0]*normal[0] + versores[3*j + 1]*normal[1] + versores[3*j + 2]*normal[2];

// 			printf("Modulo normal = %f, modulo xyz = %f\n", normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2], versores[3*j + 0]*versores[3*j + 0]  + versores[3*j + 1]*versores[3*j + 1] + versores[3*j + 2]*versores[3*j + 2]);
			
			vec_norm[0] = normal[0]*coseno;
			vec_norm[1] = normal[1]*coseno;
			vec_norm[2] = normal[2]*coseno;
		
			vec_perp[0] = versores[3*j + 0] - vec_norm[0];
			vec_perp[1] = versores[3*j + 1] - vec_norm[1];
			vec_perp[2] = versores[3*j + 2] - vec_norm[2];
		
			objeto.lights_info[i*N_GEO*4 + 4*j + 0] = (vec_norm[0] - vec_perp[0]);
			objeto.lights_info[i*N_GEO*4 + 4*j + 1] = (vec_norm[1] - vec_perp[1]);
			objeto.lights_info[i*N_GEO*4 + 4*j + 2] = (vec_norm[2] - vec_perp[2]);

			if(coseno < 0.f)
				coseno = 0.f;
			if(isnan(intensidades[j]) == 1)
				intensidades[j] = 0.f;
			objeto.lights_info[i*N_GEO*4 + 4*j + 3] = coseno*intensidades[j]*1000.f;

			
			printf("Exacto = %f, aproximacion = %f\n", powf(metrica_r(distancia_fuente[j]), -2), intensidades[j]);
			
		}
		
	}
	free(distancia_fuente);
	free(alfas);
	free(solution_type);
	free(versores);
	free(intensidades);
	printf("Lighting calculated\n");
	return objeto;
}


struct object_data *model_escenario0(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 40; //70

	*number = 2;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));
	

// 	planeta[1] = geodesica_inicializador_esfera1(size, size, light_position, 2.8f, pi*0.5f,  180.f*deg2rad, 0.4f);               //cilindro
// 	planeta[1].texture = 5;
	planeta[0] = geodesica_cubo(15, 15, 15, light_position, 4.2f, pi*0.6f,  40.f*deg2rad, 1.4f, 1.4f, 1.4f, 0.f, 0.f, 0.f);
	planeta[0].texture = 6;

 	planeta[1] = geodesica_inicializador_background1(size, size, light_position, 15.f);
 	planeta[1].texture = 0;
// 	planeta[0] = geodesica_inicializador_cilindro(20, 20, light_position, 1.f, pi*0.6f, 0.83798f, 0.3f, 1.f);               //cilindro
// 	planeta[0].texture = 5;
	printf("Por retornar\n");
	return planeta;
}

struct object_data *model_escenario1(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 30; //70

	*number = 6;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));
	
	planeta[0] = geodesica_inicializador_cilindro(20, 20, light_position, 2.f, pi*0.5f, +0.83798f, 0.3f, 1.f);               //cilindro
	planeta[0].texture = 5;

	planeta[1] = geodesica_inicializador_esfera1(size, size, light_position, -3.f, pi*0.5f,  140.f*deg2rad, 1.4f);               //cilindro
	planeta[1].texture = 3;


	planeta[2] = geodesica_inicializador_esfera1(size, size, light_position, 4.5f, pi*0.3f,  90.f*deg2rad, 0.49f);               //cilindro
	planeta[2].texture = 1;

 	planeta[3] = geodesica_cubo(12, 12, 12, light_position, 0.5f, pi*0.5f,  70.f*deg2rad, 1.f, 1.f, 1.f, 0.f, 0.f, 0.f);
 	planeta[3].texture = 0;

	planeta[4] = geodesica_inicializador_esfera1(size, size, light_position, 2.3f, pi*0.7f,  120.f*deg2rad, 0.4f);               //cilindro
	planeta[4].texture = 2;

	planeta[5] = geodesica_inicializador_esfera2(size, size, light_position, -2.f, pi*0.2f,  90.f*deg2rad, 0.2f);               //cilindro
	planeta[5].texture = 4;
	
	printf("Por retornar\n");
	return planeta;
}


struct object_data *model_escenario2(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 30; //70

	*number = 5;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));
	
	planeta[1] = geodesica_cubo(12, 12, 12, light_position, 5.f, pi*0.5f,  90.f*deg2rad, 0.55f, 0.55f, 0.43f, 0.f, 0.f, 0.f);
	planeta[1].texture = 0;

	planeta[0] = geodesica_cubo(20, 20, 20, light_position, 0.5f, 1.746714f,  5.420547f, 2.0f, 2.0f, 2.0f, 0.f, 0.f, 0.f);
	planeta[0].texture = 2;
	
	planeta[2] = geodesica_cubo(10, 10, 10, light_position, -1.6f, pi*0.7f,  0.f*deg2rad, 0.3f, 0.1f, 0.2f, 0.f, 0.f, 0.f);
	planeta[2].texture = 2;

 	planeta[3] = geodesica_cubo(12, 12, 12, light_position, 0.5f, pi*0.5f,  165.f*deg2rad, 0.5f, 2.5f, 0.4f, 0.f, 0.f, 0.f);
 	planeta[3].texture = 4;

 	planeta[4] = geodesica_cubo(10, 10, 10, light_position, 2.5f, pi*0.5f,  -20.f*deg2rad, 1.5f, 0.5f, 0.9f, 0.f, 0.f, 0.f);
 	planeta[4].texture = 3;

	*number = 1;
	printf("Por retornar\n");
	return planeta;
}



struct object_data *model_escenario3(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
// 	float light_position[3] = {4.f, pi*0.5f, 0.f};

	*number = 12;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));

	unsigned int resx = 40, resy = 7;
	float length = 5.f, width = 0.3f;
	planeta[0] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.75f,  70.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[0].texture = 3;
	planeta[1] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.25f,  70.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[1].texture = 3;
	planeta[2] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.50f,  70.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[2].texture = 3;
	planeta[3] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.75f,  250.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[3].texture = 3;
	planeta[4] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.25f,  250.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[4].texture = 3;
	planeta[5] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.50f,  250.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[5].texture = 3;

	planeta[6] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.75f,  160.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[6].texture = 3;
	planeta[7] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.25f,  160.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[7].texture = 3;
	planeta[8] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.50f,  160.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[8].texture = 3;
	planeta[9] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.75f,  340.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[9].texture = 3;
	planeta[10] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.25f,  340.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[10].texture = 3;
	planeta[11] = geodesica_cubo(resx, resy, resy, light_position, 0.0f, pi*0.50f,  340.f*deg2rad, length, width, width, 0.f, 0.f, 0.f);
	planeta[11].texture = 3;

	printf("Por retornar\n");
	return planeta;
}

struct object_data *model_escenario4(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 30; //70

	size = 20;
	*number = 6;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));
	
	planeta[0] = geodesica_inicializador_esfera1(size, size, light_position, 3.f, pi*0.5f,  150.f*deg2rad, 0.4f);               //cilindro
	planeta[0].texture = 0;
	
	planeta[1] = geodesica_inicializador_esfera1(size, size, light_position, 4.f, pi*0.5f,  40.f*deg2rad, 0.4f);               //cilindro
	planeta[1].texture = 1;

	planeta[2] = geodesica_inicializador_esfera1(size, size, light_position, 0.5f, pi*0.5f,  90.f*deg2rad, 0.49f);               //cilindro
	planeta[2].texture = 2;

	planeta[3] = geodesica_inicializador_esfera1(size, size, light_position, 2.f, pi*0.5f,  270.f*deg2rad, 0.4f);               //cilindro
	planeta[3].texture = 3;

	planeta[4] = geodesica_inicializador_esfera1(size, size, light_position, 0.3f, pi*0.5f,  190.f*deg2rad, 0.2f);               //cilindro
	planeta[4].texture = 4;

	planeta[5] = geodesica_inicializador_esfera1(size, size, light_position, 1.f, pi*0.2f,  90.f*deg2rad, 0.2f);               //cilindro
	planeta[5].texture = 5;
	printf("planeta 6 creado \n");

// 	planeta[7] = geodesica_inicializador_background1(size, size, light_position, -15.f);
// 	planeta[7].texture = 7;
// 	*number = /*1*/;
	printf("Por retornar\n");
	return planeta;
}

struct object_data *model_video1(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 30; //70

	size = 20;
	*number = 2;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));
	
	planeta[0] = geodesica_inicializador_esfera1(size, size, light_position, 3.f, pi*0.5f,  150.f*deg2rad, 1.0f);               //cilindro
	planeta[0].texture = 3;
	
	planeta[1] = geodesica_inicializador_esfera1(size, size, light_position, 3.f, pi*0.5f,  200.f*deg2rad, 0.25f);               //cilindro
	planeta[1].texture = 6;

	printf("Por retornar\n");
	return planeta;
}

void integrador(float rho_min, float rho_max, unsigned int steps, float *integral){
	float suma = 0.0f, dx = (rho_max - rho_min)/(steps - 1), rho;
	
	for(unsigned int i = 0; i < steps; i++){
		rho = rho_min + dx*i;
		if(rho > rho_max)
			rho = rho_max;
		
		suma += dx*metrica_metric(rho, 0.f, 2);
		integral[i] = suma;
	}
	for(unsigned int i = 0; i < steps; i++)
		integral[i] *= 1.f/suma;
}

float acumulada_inversa(float x, float rho_min, float rho_max, unsigned int steps, float *integral){
	if(x == 0.f)
		return rho_min;
	else if(x == 1.f)
		return rho_max;
	else{
		unsigned int i = 0;
		float x1, x2, rho1, rho2, drho = (rho_max - rho_min)/(steps - 1);
		for(i = 0; i < steps - 1; i++){
			rho1 = drho*i       + rho_min;
			rho2 = drho*(i + 1) + rho_min;
			
			x1 = integral[i];
			x2 = integral[i + 1];
			if((x >= x1)&&(x < x2))
				break;
		}
		return (rho2 - rho1)/(x2 - x1)*(x - x1) + rho1;
	}
}
struct particula_data model_points(unsigned int number){
	struct particula_data objeto;
	float r, coseno, phi, *integral;
	objeto.moving = 0;
	objeto.n_vert = number;
	objeto.vertices = (float *) malloc(3*number*sizeof(float));
	objeto.velocity = (float *) malloc(3*number*sizeof(float));
	objeto.color    = (float *) malloc(3*number*sizeof(float));
	
	float rho_min = 2.5f, rho_max = 5.f, X;
	unsigned int steps = 2000;
	
	integral        = (float *) malloc(steps*sizeof(float));
	integrador(rho_min, rho_max, steps, integral);
	
	for(unsigned int i = 0; i < number; i++){
		X      = (float)rand()/RAND_MAX;
		r      =  acumulada_inversa(X, rho_min, rho_max, steps, integral);
		coseno =  ((float)rand()/RAND_MAX*2 - 1);
		phi    =  (float)rand()/RAND_MAX*2*pi;
		
		objeto.vertices[3*i + 0] = r;
		objeto.vertices[3*i + 1] = acosf(coseno);
		objeto.vertices[3*i + 2] = phi;

		objeto.color[3*i + 0] = 0.7f + (float)rand()/RAND_MAX*0.3f;
		objeto.color[3*i + 1] = 0.7f + (float)rand()/RAND_MAX*0.3f;
		objeto.color[3*i + 2] = 0.7f + (float)rand()/RAND_MAX*0.3f;
	
		objeto.velocity[3*i + 0] = (float)rand()/RAND_MAX;
		objeto.velocity[3*i + 1] = (float)rand()/RAND_MAX;
		objeto.velocity[3*i + 2] = (float)rand()/RAND_MAX;
	}
	free(integral);
	return objeto;
}

struct particula_data model_inicializador(){
	struct particula_data particulas;

	particulas.n_vert   = 0;
	particulas.moving   = 1;
	particulas.vertices = (float *) malloc(3*500*sizeof(float));
	particulas.velocity = (float *) malloc(3*500*sizeof(float));
	particulas.color    = (float *) malloc(3*500*sizeof(float));

	return particulas;
}

struct particula_data disparo(float posicion[4], float cuadrivelocidad[4], float vectores[4][4], struct particula_data particulas, unsigned int tipo){
	struct particula_data objeto = particulas;
	float velocidad;
	unsigned int stride;
	stride = particulas.n_vert;
	printf("stride = %d\n", stride);
	if(tipo == 0){
		velocidad = 1.f;
		objeto.n_vert += 1;
		objeto.vertices[3*stride + 0] = posicion[1];
		objeto.vertices[3*stride + 1] = posicion[2];
		objeto.vertices[3*stride + 2] = posicion[3];
		
		objeto.velocity[3*stride + 0] = vectores[1][1]*velocidad + cuadrivelocidad[1];
		objeto.velocity[3*stride + 1] = vectores[1][2]*velocidad + cuadrivelocidad[2];
		objeto.velocity[3*stride + 2] = vectores[1][3]*velocidad + cuadrivelocidad[3];
		
		objeto.color[3*stride + 0] = 0.f;
		objeto.color[3*stride + 1] = 0.5f;
		objeto.color[3*stride + 2] = 1.f;
	}
	else if(tipo == 1){
		velocidad = 5.f;
		objeto.n_vert += 1;
		objeto.vertices[3*stride + 0] = posicion[1];
		objeto.vertices[3*stride + 1] = posicion[2];
		objeto.vertices[3*stride + 2] = posicion[3];
		
		objeto.velocity[3*stride + 0] = vectores[1][1]*velocidad + cuadrivelocidad[1];
		objeto.velocity[3*stride + 1] = vectores[1][2]*velocidad + cuadrivelocidad[2];
		objeto.velocity[3*stride + 2] = vectores[1][3]*velocidad + cuadrivelocidad[3];
		
		objeto.color[3*stride + 0] = 0.f;
		objeto.color[3*stride + 1] = 0.8f;
		objeto.color[3*stride + 2] = 0.4f;
	}
	else if(tipo == 2){
		velocidad = 3.f;
		float random1, random2, vector[3], angulo = 15.f, modulo;
		for(unsigned int i = 0; i < 20; i++){
			stride = objeto.n_vert;
			objeto.n_vert += 1;
			objeto.vertices[3*stride + 0] = posicion[1];
			objeto.vertices[3*stride + 1] = posicion[2];
			objeto.vertices[3*stride + 2] = posicion[3];
			
			random1 = ((float)rand()/RAND_MAX - 0.5f)*tanf(angulo*deg2rad);
			random2 = ((float)rand()/RAND_MAX - 0.5f)*tanf(angulo*deg2rad);
			
			vector[0] = vectores[1][1]*1.f + random1*vectores[2][1] + random2*vectores[3][1];
			vector[1] = vectores[1][2]*1.f + random1*vectores[2][2] + random2*vectores[3][2];
			vector[2] = vectores[1][3]*1.f + random1*vectores[2][3] + random2*vectores[3][3];
			modulo = sqrtf(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
			vector[0] *= velocidad/modulo;
			vector[1] *= velocidad/modulo;
			vector[2] *= velocidad/modulo;
			
			objeto.velocity[3*stride + 0] = vector[0] + cuadrivelocidad[1];
			objeto.velocity[3*stride + 1] = vector[1] + cuadrivelocidad[2];
			objeto.velocity[3*stride + 2] = vector[2] + cuadrivelocidad[3];
			
			objeto.color[3*stride + 0] = 0.8f;
			objeto.color[3*stride + 1] = 0.3f;
			objeto.color[3*stride + 2] = 0.1f;
		}
	}

	return objeto;
}

struct object_data model_tunnel(struct object_data tunnel, float posicion[4], float vectores[4][4], float radio, unsigned int resolution, unsigned char init){
	struct object_data objeto;
	unsigned int pisos = 200;
	unsigned int max_vert = pisos*resolution, stride1, stride2;
	if(init == 1){
		objeto.vertices   = (float *) malloc(max_vert*3*sizeof(float));
		objeto.triangulos = (unsigned int *) malloc(2*(resolution)*(pisos - 1)*3*sizeof(unsigned int));
		objeto.u          = (float *) malloc(max_vert*sizeof(float));
		objeto.v          = (float *) malloc(max_vert*sizeof(float));
		objeto.n_triang   = 0;
		objeto.texture    = 1;
		stride1 = (resolution)*2*3;
		for(unsigned int i = 0; i < pisos - 1; i++){
			for(unsigned int j = 0; j < resolution; j++){
				objeto.triangulos[stride1*i + 6*j + 0] = (resolution)*i       + j + 0;
				objeto.triangulos[stride1*i + 6*j + 1] = (resolution)*i       + j + 1;
				objeto.triangulos[stride1*i + 6*j + 2] = (resolution)*(i + 1) + j + 0;
				objeto.triangulos[stride1*i + 6*j + 3] = (resolution)*i       + j + 1;
				objeto.triangulos[stride1*i + 6*j + 4] = (resolution)*(i + 1) + j + 1;
				objeto.triangulos[stride1*i + 6*j + 5] = (resolution)*(i + 1) + j + 0;
			}
			objeto.triangulos[stride1*i + 6*(resolution - 1) + 1] = (resolution)*i + 0;
			objeto.triangulos[stride1*i + 6*(resolution - 1) + 3] = (resolution)*i + 0;
			objeto.triangulos[stride1*i + 6*(resolution - 1) + 4] = (resolution)*(i + 1) + 0;
		}
		float u, v, *result, vertice[3], derivada[3], theta;
		for(unsigned int i = 0; i < pisos; i++){
			u = (powf(-1, i + 1) + 1)*0.5f;
			for(unsigned int j = 0; j < resolution; j++){
				v = j*1.f/(resolution - 1);
				objeto.u[resolution*i + j] = u;
				objeto.v[resolution*i + j] = v;
			}
		}
		
		result = (float *) malloc(3*sizeof(float));
		
		vertice[0] = posicion[1];
		vertice[1] = posicion[2];
		vertice[2] = posicion[3];
		
		for(unsigned int i = 0; i < resolution; i++){
			theta       = i*2*pi/(resolution - 1);
			derivada[0] = vectores[2][1]*cosf(theta) + vectores[3][1]*sinf(theta);
			derivada[1] = vectores[2][2]*cosf(theta) + vectores[3][2]*sinf(theta);
			derivada[2] = vectores[2][3]*cosf(theta) + vectores[3][3]*sinf(theta);
		
			model_evolucionador(vertice, derivada, radio, result);
			objeto.vertices[3*i + 0] = result[0];
			objeto.vertices[3*i + 1] = result[1];
			objeto.vertices[3*i + 2] = result[2];
		}
		objeto.n_vert     = resolution;

		free(result);
		return objeto;
	}
	else{
		float *result, vertice[3], derivada[3], theta;
		objeto = tunnel;

		printf("numero de vertices antes = %d\n", objeto.n_vert);

		result = (float *) malloc(3*sizeof(float));
		
		vertice[0] = posicion[1];
		vertice[1] = posicion[2];
		vertice[2] = posicion[3];

		for(unsigned int i = 0; i < resolution; i++){
			theta       = i*2*pi/(resolution - 1);
			derivada[0] = vectores[2][1]*cosf(theta) + vectores[3][1]*sinf(theta);
			derivada[1] = vectores[2][2]*cosf(theta) + vectores[3][2]*sinf(theta);
			derivada[2] = vectores[2][3]*cosf(theta) + vectores[3][3]*sinf(theta);

			model_evolucionador(vertice, derivada, radio, result);
			objeto.vertices[3*objeto.n_vert + 3*i + 0] = result[0];
			objeto.vertices[3*objeto.n_vert + 3*i + 1] = result[1];
			objeto.vertices[3*objeto.n_vert + 3*i + 2] = result[2];
		}
		objeto.n_vert   += resolution;
		objeto.n_triang += 2*(resolution - 1);
		free(result);
		return objeto;
	}
}



struct object_data model_add_tunnel(float posicion2[4], float vectores2[4][4], float radio, float length, unsigned int resolution){
	float posicion[4], vectores[4][4];
	
	for(unsigned int i = 0; i < 4; i++){
		posicion[i] = posicion2[i];
		vectores[i][0] = vectores2[i][0];
		vectores[i][1] = vectores2[i][1];
		vectores[i][2] = vectores2[i][2];
		vectores[i][3] = vectores2[i][3];
		
	}
	struct object_data objeto;

	unsigned int pisos    = 200;
	unsigned int max_vert = pisos*resolution, stride1, stride2;

	objeto.vertices   = (float *) malloc(max_vert*3*sizeof(float));
	objeto.triangulos = (unsigned int *) malloc(2*(resolution)*(pisos - 1)*3*sizeof(unsigned int));
	objeto.u          = (float *) malloc(max_vert*sizeof(float));
	objeto.v          = (float *) malloc(max_vert*sizeof(float));
	objeto.n_triang   = 2*(resolution)*(pisos - 1);
	objeto.texture    = 1;
	stride1 = (resolution)*2*3;

	for(unsigned int i = 0; i < pisos - 1; i++){
		for(unsigned int j = 0; j < resolution; j++){
			objeto.triangulos[stride1*i + 6*j + 0] = (resolution)*i       + j + 0;
			objeto.triangulos[stride1*i + 6*j + 1] = (resolution)*i       + j + 1;
			objeto.triangulos[stride1*i + 6*j + 2] = (resolution)*(i + 1) + j + 0;
			objeto.triangulos[stride1*i + 6*j + 3] = (resolution)*i       + j + 1;
			objeto.triangulos[stride1*i + 6*j + 4] = (resolution)*(i + 1) + j + 1;
			objeto.triangulos[stride1*i + 6*j + 5] = (resolution)*(i + 1) + j + 0;
			if(stride1*i + 6*j + 5 >= 2*(resolution)*(pisos - 1)*3)
				printf("se van de rango los triangulos, numero = %u de %u\n", stride1*i + 6*j + 5, 2*(resolution)*(pisos - 1)*3);
			if((resolution)*(i + 1) + j + 1 >= max_vert*3)
				printf("se van de rango los vertices, numero = %u de %u\n", (resolution)*(i + 1) + j + 1, max_vert*3);
		}
		objeto.triangulos[stride1*i + 6*(resolution - 1) + 1] = (resolution)*i + 0;
		objeto.triangulos[stride1*i + 6*(resolution - 1) + 3] = (resolution)*i + 0;
		objeto.triangulos[stride1*i + 6*(resolution - 1) + 4] = (resolution)*(i + 1) + 0;

		if(stride1*i + 6*(resolution - 1) + 4 >= 2*(resolution)*(pisos - 1)*3)
			printf("se van de rango los triangulos, numero = %u de %u\n", stride1*i + 6*(resolution - 1) + 4, 2*(resolution)*(pisos - 1)*3);
		if((resolution)*(i + 1) + 0 >= max_vert*3)
			printf("se van de rango los vertices, numero = %u de %u\n", (resolution)*(i + 1) + 0, max_vert*3);
		
	}
	
	float u, v, *result, vertice[3], derivada[3], theta;
	for(unsigned int i = 0; i < pisos; i++){
		u = (powf(-1, i + 1) + 1)*0.5f;
		for(unsigned int j = 0; j < resolution; j++){
			v = j*1.f/(resolution - 1);
			objeto.u[resolution*i + j] = u;
			objeto.v[resolution*i + j] = v;
		}
	}
	
	result = (float *) malloc(3*sizeof(float));
	unsigned int steps = 1000;
	float L = 0.f, velocidad[3], aceleracion[3], asdf[3][3], dtau = length/pisos/steps;

	vertice[0] = posicion[1];
	vertice[1] = posicion[2];
	vertice[2] = posicion[3];

	
	velocidad[0] = vectores[1][1];
	velocidad[1] = vectores[1][2];
	velocidad[2] = vectores[1][3];
	
	objeto.n_vert = 0;
	for(unsigned int k = 0; k < pisos*steps; k++){
		if((isnan(vertice[0])==1)||(isnan(vertice[1])==1)||(isnan(vertice[2])==1))
			printf("aca empezo el problema, k = %u, vertices = (%f, %f, %f), velocidad = (%f, %f, %f), aceleracion = (%f, %f, %f)\n", k, vertice[0], vertice[1], 
																																		vertice[2], velocidad[0],
																																		velocidad[1], velocidad[2],
																												aceleracion[0], aceleracion[1], aceleracion[2]);
		if(k%steps == 0){
			for(unsigned int i = 0; i < resolution; i++){
				theta       = i*2*pi/(resolution - 1);
				derivada[0] = vectores[2][1]*cosf(theta) + vectores[3][1]*sinf(theta);
				derivada[1] = vectores[2][2]*cosf(theta) + vectores[3][2]*sinf(theta);
				derivada[2] = vectores[2][3]*cosf(theta) + vectores[3][3]*sinf(theta);

				model_evolucionador(vertice, derivada, radio, result);
				objeto.vertices[3*k*resolution/steps + 3*i + 0] = result[0];
				objeto.vertices[3*k*resolution/steps + 3*i + 1] = result[1];
				objeto.vertices[3*k*resolution/steps + 3*i + 2] = result[2];
			}
		}
		posicion[0] = 0.f;
		posicion[1] = vertice[0];
		posicion[2] = vertice[1];
		posicion[3] = vertice[2];
		for(unsigned int i = 0; i < 3; i++){
			for(unsigned int gamma = 0; gamma < 3; gamma++){
				asdf[i][gamma] = 0.f;
				for(unsigned int n = 0; n < 3; n++){
					asdf[i][gamma] += -christoffel(posicion, gamma + 1, n + 1 , 1)*vectores[i + 1][n + 1]*velocidad[0];
					asdf[i][gamma] += -christoffel(posicion, gamma + 1, n + 1 , 2)*vectores[i + 1][n + 1]*velocidad[1];
					asdf[i][gamma] += -christoffel(posicion, gamma + 1, n + 1 , 3)*vectores[i + 1][n + 1]*velocidad[2];
				}
			}
		}
		
		aceleracion[0] = model_acceleration(0, vertice, velocidad);
		aceleracion[1] = model_acceleration(1, vertice, velocidad);
		aceleracion[2] = model_acceleration(2, vertice, velocidad);
		
		vertice[0] += dtau*velocidad[0];
		vertice[1] += dtau*velocidad[1];
		vertice[2] += dtau*velocidad[2];

		velocidad[0] += dtau*aceleracion[0];
		velocidad[1] += dtau*aceleracion[1];
		velocidad[2] += dtau*aceleracion[2];
		
		for(unsigned int i = 0; i < 3; i++){
			vectores[i + 1][1] += dtau*asdf[i][0];
			vectores[i + 1][2] += dtau*asdf[i][1];
			vectores[i + 1][3] += dtau*asdf[i][2];
		}
	}

	objeto.n_triang = 2*(resolution)*(pisos - 1);
	objeto.n_vert   = max_vert;
	free(result);
// 	return objeto;
	float light_position[3] = {4.f, pi*0.5f, 0.f};
	
	return model_lighting_processing(model_normal_creator(objeto), light_position);
	
}



struct object_data *model_escenario5(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 30; //70
// 	float light_position[3] = {4.f, pi*0.5f, 0.f};

	*number = 6;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));

	float posicion[4], vectores[4][4];
	posicion[0] = 0.f;
	posicion[1] = 1.f;
	posicion[2] = pi*0.5f;
	posicion[3] = 0.f;
	
	vectores[0][0] = 1.f; vectores[0][1] = 0.f; vectores[0][2] = 0.f;                        vectores[0][3] = 0.f;
	vectores[1][0] = 0.f; vectores[1][1] = 1.f; vectores[1][2] = 0.f;                        vectores[1][3] = 0.f;
	vectores[2][0] = 0.f; vectores[2][1] = 0.f; vectores[2][2] = 1.f/metrica_r(posicion[1]); vectores[2][3] = 0.f;
	vectores[3][0] = 0.f; vectores[3][1] = 0.f; vectores[3][2] = 0.f;                        vectores[3][3] = 1.f/metrica_r(posicion[1])/sinf(posicion[2]);
	
	planeta[0] = model_add_tunnel(posicion, vectores, 0.1f, 15.f, 10);
	planeta[0].texture = 1;

	planeta[1] = geodesica_inicializador_cilindro(20, 20, light_position, 5.f, pi*0.75f, 90.f*deg2rad, 1.3f, 5.f);               //cilindro
	planeta[1].texture = 5;

	planeta[2] = geodesica_inicializador_esfera1(size, size, light_position, 1.5f, pi*0.5f,  90.f*deg2rad, 1.49f);               //cilindro
	planeta[2].texture = 1;

 	planeta[3] = geodesica_cubo(20, 20, 20, light_position, -3.5f, pi*0.5f,  3.f*deg2rad, 3.f, 3.f, 3.f, 0.f, 0.f, 0.f);
 	planeta[3].texture = 0;

	planeta[4] = geodesica_inicializador_esfera1(size, size, light_position, 2.3f, pi*0.5f,  190.f*deg2rad, 2.2f);               //cilindro
	planeta[4].texture = 2;

	planeta[5] = geodesica_inicializador_esfera1(size, size, light_position, 1.f, pi*0.2f,  90.f*deg2rad, 2.2f);               //cilindro
	planeta[5].texture = 4;
	
	printf("Por retornar\n");
	return planeta;
}


struct object_data *model_escenario6(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 30; //70

	*number = 3;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));

	float posicion[4], vectores[4][4];
	posicion[0] = 0.f;
	posicion[1] = -5.f;
	posicion[2] = pi*0.6f;
	posicion[3] = 0.1f;
	
	vectores[0][0] = 1.f; vectores[0][1] = 0.f; vectores[0][2] = 0.f;                        vectores[0][3] = 0.f;
	vectores[1][0] = 0.f; vectores[1][1] = 1.f; vectores[1][2] = 0.f;                        vectores[1][3] = 0.f;
	vectores[2][0] = 0.f; vectores[2][1] = 0.f; vectores[2][2] = 1.f/metrica_r(posicion[1]); vectores[2][3] = 0.f;
	vectores[3][0] = 0.f; vectores[3][1] = 0.f; vectores[3][2] = 0.f;                        vectores[3][3] = 1.f/metrica_r(posicion[1])/sinf(posicion[2]);
	
	planeta[0] = model_add_tunnel(posicion, vectores, .5f, 15.f, 20);
	planeta[0].texture = 1;

// 	planeta[0] = geodesica_load_obj2("models/asteroide.obj", 1, light_position, 0.1f, pi*0.1f, 1.f, 0.2f);
// 	planeta[0].texture = 6;

	planeta[1] = geodesica_inicializador_esfera1(size, size, light_position, 1.f, pi*0.5f,  40.f*deg2rad, 1.4f);               //cilindro
	planeta[1].texture = 3;

	planeta[2] = geodesica_cubo(12, 12, 12, light_position, 3.f, pi*0.9f,  40.f*deg2rad, 0.4f, 0.4f, 0.4f, 0.f, 0.f, 0.f);
	planeta[2].texture = 0;
	*number = 1;
	printf("Por retornar\n");
	return planeta;
}
struct object_data *model_scene_worm(unsigned int *number, float light_position[3]){
	struct object_data *planeta;
	unsigned int size = 15; //70

	*number = 4;
	planeta = (struct object_data*) malloc((*number)*sizeof(struct object_data));

	float posicion[4], vectores[4][4];
	posicion[0] = 0.f;
	posicion[1] = 1.f;
	posicion[2] = pi*0.5f;
	posicion[3] = 0.f;
	
	vectores[0][0] = 1.f; vectores[0][1] = 0.f; vectores[0][2] = 0.f;                        vectores[0][3] = 0.f;
	vectores[1][0] = 0.f; vectores[1][1] = 1.f; vectores[1][2] = 0.f;                        vectores[1][3] = 0.f;
	vectores[2][0] = 0.f; vectores[2][1] = 0.f; vectores[2][2] = 1.f/metrica_r(posicion[1]); vectores[2][3] = 0.f;
	vectores[3][0] = 0.f; vectores[3][1] = 0.f; vectores[3][2] = 0.f;                        vectores[3][3] = 1.f/metrica_r(posicion[1])/sinf(posicion[2]);
	
	planeta[0] = geodesica_load_obj2("models/asteroide2.obj", 1, light_position, 0.f, pi/2.f, 1.f, 0.3f);
	planeta[0].texture = 6;

	planeta[1] = model_add_tunnel(posicion, vectores, 0.1f, 15.f, 10);
 	planeta[1].texture = 1;


	planeta[2] = geodesica_inicializador_esfera1(size, size, light_position, -2.f, pi*0.5f,  40.f*deg2rad, 1.4f);               //cilindro
	planeta[2].texture = 3;

	planeta[3] = geodesica_cubo(20, 20, 2, light_position, -1.f, pi*0.6f,  80.f*deg2rad, 1.4f, 1.4f, 0.03f, 0.f, 0.f, 0.f);
	planeta[3].texture = 0;

	printf("Por retornar\n");
	return planeta;
}


void model_load_shaderinfo(unsigned int ssbo, struct object_data planetas[50], unsigned int numero){
	unsigned int stride = 0;
	for(unsigned int i = 0; i < numero; i++){
		glBufferSubData(GL_SHADER_STORAGE_BUFFER, stride*N_GEO*4*sizeof(float), N_GEO*4*planetas[i].n_vert*sizeof(float), planetas[i].lights_info);
		stride += planetas[i].n_vert;
	}
}


void model_volcado(struct object_data objeto, float dtau){
	FILE *pfout1;
	pfout1 = fopen("volcado.dat", "w");
	float vertice[3], normal[3], dx[3];
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertice[0] = objeto.vertices[3*i + 0];
		vertice[1] = objeto.vertices[3*i + 1];
		vertice[2] = objeto.vertices[3*i + 2];
		normal[0]  = objeto.normales[3*i + 0];
		normal[1]  = objeto.normales[3*i + 1];
		normal[2]  = objeto.normales[3*i + 2];
		dx[0] = dtau*normal[0]/sqrtf(metrica_metric(vertice[0], vertice[1], 1));
		dx[1] = dtau*normal[1]/sqrtf(metrica_metric(vertice[0], vertice[1], 2));
		dx[2] = dtau*normal[2]/sqrtf(metrica_metric(vertice[0], vertice[1], 3));

		fprintf(pfout1, "%f %f %f %f %f %f\n", vertice[0], vertice[1], vertice[2], vertice[0] + dx[0], vertice[1] + dx[1], vertice[2] + dx[2]);
	}
	fclose(pfout1);
	
}

void model_volcado2(struct object_data objeto, float posicion[4]){
	float vertice[3], *alfas, *solution_type, *versores, *distancia_fuente, posicion0[3], vector[3];
	FILE *pfout;
	

	distancia_fuente = (float *) malloc(N_GEO*sizeof(float));
	alfas            = (float *) malloc(N_GEO*sizeof(float));
	solution_type    = (float *) malloc(N_GEO*sizeof(float));
	versores         = (float *) malloc(N_GEO*3*sizeof(float));

	posicion0[0] = posicion[1];
	posicion0[1] = posicion[2];
	posicion0[2] = posicion[3];
	
	pfout = fopen("volcado.dat", "w");
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertice[0] = objeto.vertices[3*i + 0];
		vertice[1] = objeto.vertices[3*i + 1];
		vertice[2] = objeto.vertices[3*i + 2];

		geodesica_geodesica4(posicion0, vertice, distancia_fuente, alfas, solution_type, versores);
		for(unsigned int j = 0; j < N_GEO; j++){
			vector[0] = versores[3*j + 0]*distancia_fuente[j];
			vector[1] = versores[3*j + 1]*distancia_fuente[j];
			vector[2] = versores[3*j + 2]*distancia_fuente[j];
			
			fprintf(pfout, "%f %f %f\n", vector[0], vector[1], vector[2]);
		}
	}
	fclose(pfout);
}


void model_volcado3(struct object_data objeto, float posicion[4]){
	float vertice[3], *alfas, *solution_type, *versores, *distancia_fuente, posicion0[3], vector[3];
	FILE *pfout;
	

	distancia_fuente = (float *) malloc(N_GEO*sizeof(float));
	alfas            = (float *) malloc(N_GEO*sizeof(float));
	solution_type    = (float *) malloc(N_GEO*sizeof(float));
	versores         = (float *) malloc(N_GEO*3*sizeof(float));

	posicion0[0] = posicion[1];
	posicion0[1] = posicion[2];
	posicion0[2] = posicion[3];
	
	pfout = fopen("volcado.dat", "a");
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertice[0] = objeto.vertices[3*i + 0];
		vertice[1] = objeto.vertices[3*i + 1];
		vertice[2] = objeto.vertices[3*i + 2];

		geodesica_geodesica4(posicion0, vertice, distancia_fuente, alfas, solution_type, versores);
// 		for(unsigned int j = 0; j < N_GEO; j++)
			fprintf(pfout, "%f ", distancia_fuente[0]);
		
	}
	fprintf(pfout, "\n");
	fclose(pfout);
	free(distancia_fuente);
	free(alfas);
	free(solution_type);
	free(versores);
}
void delete_objects(struct object_data *objetos, unsigned int numero){
	for(unsigned int i = 0; i < numero; i++){
		free(objetos[i].vertices);
		free(objetos[i].triangulos);
		free(objetos[i].normales);
		free(objetos[i].u);
		free(objetos[i].v);
		free(objetos[i].lights_info);
	}
}

void model_reload_lights(float light_position[3], struct object_data *planetas, unsigned int numero){
	struct object_data aux, *pobjeto;
	for(unsigned int i = 0; i < numero; i++){
		free(planetas[i].lights_info);
		aux = model_lighting_processing(planetas[i], light_position);
		pobjeto = &planetas[i];
		pobjeto->lights_info = aux.lights_info;
	}
}
