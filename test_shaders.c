#define N_PART_MAX 500
#define pi (3.141592653f)
#include <GL/glew.h>
//#include <GL/gl.h>
#include <GL/freeglut.h>   // freeglut.h might be a better alternative, if available.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "stb.h"
#include "structs.h"
#include "geodesicas.h"
// #define metrica_h (1.f)



float christoffel(float posicion[4], unsigned int gamma, unsigned int alpha, unsigned int beta){
	if(gamma == 0){
		if(((alpha == 0)&(beta == 1))||((alpha == 1)&(beta == 0)))
			return metrica_B_d(posicion[1])*0.5f/metrica_B(posicion[1]);
	else
		return 0.f;
	}
	else if(gamma == 1){
	if((alpha == 0)&(beta == 0))
		return powf(light_speed, 2)*metrica_B_d(posicion[1])*0.5f/metrica_A(posicion[1]);

	else if((alpha == 1)&(beta == 1))
		return metrica_A_d(posicion[1])/2.f/metrica_A(posicion[1]);
	
	else if((alpha == 2)&(beta == 2))
		return -metrica_r(posicion[1])*metrica_r_d(posicion[1])/metrica_A(posicion[1]);

	else if((alpha == 3)&(beta== 3))
		return -metrica_r(posicion[1])*metrica_r_d(posicion[1])*powf(sinf(posicion[2]), 2)/metrica_A(posicion[1]);

	else
		return 0.f;
	}
  
	else if(gamma == 2){
		if(((alpha == 2)&(beta == 1))||((alpha == 1)&(beta == 2)))
			return metrica_r_d(posicion[1])/metrica_r(posicion[1]);

	else if((alpha == 3)&(beta == 3))
		return -sinf(posicion[2])*cosf(posicion[2]);

	else
		return 0.f;
	}
	else{
		if(((alpha == 3)&(beta == 1))||((alpha == 1)&(beta == 3)))
			return metrica_r_d(posicion[1])/metrica_r(posicion[1]);

	else if(((alpha == 3)&(beta == 2))||((alpha == 2)&(beta == 3)))
		return cosf(posicion[2])/sinf(posicion[2]);

	else
		return 0.f;
	}
}

float model_acceleration(unsigned int i, float X[3], float V[3]){
	float suma = 0.f, pos[4];
	pos[0] = 0.f;
	pos[1] = X[0];
	pos[2] = X[1];
	pos[3] = X[2];

	for(unsigned int j = 1; j < 4; j++){
		for(unsigned int k = 1; k < 4; k++)
			suma += christoffel(pos, i + 1, j, k)*V[j - 1]*V[k - 1];
	}
	return -suma;
}

// int ind_mode = 0, triangulo = 48;
int texture[10], n_triang;
const int resolution_x = 1300, resolution_y = 768;
float posicion[4], vectores[4][4], vectores_inv[4][4], *triangulos, aspect_ratio;
unsigned int shaderProgram1, shaderProgram2, shaderProgram3;
GLuint *VBO;
/*
void draw_vertex(unsigned int shaderProgram2, unsigned int texture, GLuint VBO, unsigned int numero_triang, float *triangulos){
	float  worm_max_angle;

	worm_max_angle = 0.497561f;
	float worm_position[4] = {0.000000f, -0.000000f, 0.041667f, 0.497561f};

	float base[9] = {-0.827994, -0.355216, -0.433874, 0.398730, 0.171058, -0.900973, 0.394258, -0.919000, 0.000000};

	float aspect_ratio = 1.692708f, posicion[4] = {2.049999, 2.095191, 2.019585, 3.546853};
	
	float vectores[4][4] = {{0.500000, 0.f, 0.f, 0.f},  
							{0.f, 1.f, 0.f, 0.f}, 
							{0.f, 0.f, 0.477283, 0.f}, 
							{0.f, 0.f, 0.f, 0.529742}};

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*8*numero_triang*sizeof(float), triangulos);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);      // coordenadas
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)12);     // alpha
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)16);     // distance
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)20);     // type
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)24);     // uv coordinates
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glUseProgram(shaderProgram2);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	
	GLuint i = glGetUniformLocation(shaderProgram2, "worm_position");
	glUniform4f(i, worm_position[0], worm_position[1], worm_position[2], worm_max_angle);

	i = glGetUniformLocation(shaderProgram2, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram2, "vectores");
	glUniformMatrix4fv(i, 1, GL_FALSE, &vectores[0][0]);

	i = glGetUniformLocation(shaderProgram2, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	i = glGetUniformLocation(shaderProgram2, "base");
	glUniformMatrix3fv(i, 1, GL_FALSE, &base[0]);

	if(ind_mode == 1)
		glDrawArrays(GL_TRIANGLES, 3*triangulo, 3);
	else
		glDrawArrays(GL_TRIANGLES, 0, 3*numero_triang);

}
*/
void texture_loader(unsigned int idx, const char *file_name){
	int width, height, nrChannels;
	unsigned char *data = stbi_load(file_name, &width, &height, &nrChannels, 0); 
	glGenTextures(1, &texture[idx]);  
	printf("Textura numero %d\n", texture[idx]);
	glBindTexture(GL_TEXTURE_2D, texture[idx]);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);
}


void keyboard(unsigned char key, int x, int y){
	switch(key) {
		case 'd':
			triangulos += 1;
			break;
		case 'a':
			triangulos += -1;
			break;
		case 'x':
// 			ind_mode *= -1;
			break;
		case 'q':
			exit(0);
			break;
		default:
			break;
	}
}

static void RenderSceneCB() {
	glutKeyboardFunc(keyboard);
	glClearColor(0.f, 0.f, 0.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	render_background(posicion,  vectores, shaderProgram2, resolution_x, resolution_y, aspect_ratio);
	draw_vertex(posicion, vectores_inv, aspect_ratio, shaderProgram1, texture[0], VBO[0], n_triang, triangulos, triangulos);
	glutSwapBuffers();
}




int main(int argc, char *argv[]){
	unsigned int i;

	aspect_ratio = resolution_x*1.f/resolution_y;
	//Loading snapshot data
	//#################################################
	//#################################################
	FILE *pfin;
	pfin = fopen("volcado/vertex_volcado.txt", "r");
	
	fscanf(pfin, "%d\n", &n_triang);
	printf("Loaded model, %d triangles, n_atrib %d\n", n_triang, n_atrib);

// 	n_triang *= N_GEO;
// 	n_triang = n_triang/(N_GEO*3*n_atrib);
	
	triangulos = (float *) malloc(N_GEO*3*n_atrib*n_triang*sizeof(float));
	
	fscanf(pfin, "%f %f %f %f\n", &posicion[0], &posicion[1], &posicion[2], &posicion[3]);
	for(i = 0; i < 4; i++)
		fscanf(pfin, "%f %f %f %f\n", &vectores[i][0], &vectores[i][1], &vectores[i][2], &vectores[i][3]);

	for(i = 0; i < 4; i++)
		fscanf(pfin, "%f %f %f %f\n", &vectores_inv[i][0], &vectores_inv[i][1], &vectores_inv[i][2], &vectores_inv[i][3]);
	
	for(unsigned int i = 0; i < n_triang*n_atrib*3*N_GEO; i++)
		fscanf(pfin, "%f\n", &triangulos[i]);
	
	//#################################################
	//#################################################
	
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA); 
    glutInitWindowSize(resolution_x, resolution_y);
    glutInitWindowPosition(100, 100);

	glutCreateWindow("Geometry Explorer"); 
    
    GLenum res = glewInit();
    if (res != GLEW_OK){
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return 1;
    }
	if (!GLEW_VERSION_2_1){
		printf("%s\n", glGetString(GL_VERSION));
	//exit(1);
	}

    
    // carga de texturas

	texture_loader(0, "textures/f.jpg");
	texture_loader(1, "textures/planet8.jpg");
	texture_loader(2, "textures/planet3.jpg");
	texture_loader(3, "textures/earth2.jpg");
	texture_loader(4, "textures/planet9.jpg");


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_POINT_SPRITE);
	glutSetCursor(GLUT_CURSOR_NONE);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	printf("d = %f, theta = %f, phi 6 = %f\n", posicion[1], posicion[2], posicion[3]);

	
	
	load_and_compile(&shaderProgram1, &shaderProgram2, &shaderProgram3);

    VBO = (GLuint *) malloc(2*sizeof(GLuint));
    glGenBuffers(1, &VBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	
	glBufferData(GL_ARRAY_BUFFER, N_GEO*8*3*n_triang*sizeof(float), triangulos, GL_DYNAMIC_DRAW);
	
	load_background_arrays();
    //iniciando animacion
    glutDisplayFunc(RenderSceneCB); 
    glutIdleFunc(RenderSceneCB); 
    
    glutMainLoop(); 
    glClear(GL_COLOR_BUFFER_BIT);
    glutSwapBuffers(); 
	
}
