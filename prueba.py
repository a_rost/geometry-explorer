import numpy as np
import matplotlib.pyplot as plt
plt.ion()

t_space = np.linspace(0., 1., 10)


#######################triangulos mejor
colores = ["b", "r", "g", "k", "y", "m"]
dtheta = 15*np.pi/180.

def r(t, r0, da):
	return r0 + t*da
"""
plt.plot([A[0], B[0]], [A[1], B[1]], "k")
plt.plot([A[0], C[0]], [A[1], C[1]], "k")
plt.plot([C[0], B[0]], [C[1], B[1]], "k")
plt.plot([0.], [0.], marker = "s", color = "k")

for i in range(N):
	plt.plot(t_space*np.cos(i*dtheta), t_space*np.sin(i*dtheta))
"""


##definitivo

def plot(a, b, c, i):
	color1, color2 = colores[np.mod(2*i, len(colores))], colores[np.mod(2*i + 1, len(colores))]
	plt.plot([a[0], b[0]], [a[1], b[1]], color1, linestyle = "--")
	plt.plot([a[0], c[0]], [a[1], c[1]], color1, linestyle = "--")
	plt.plot([c[0], b[0]], [c[1], b[1]], color1, linestyle = "--")

def dot(a):
	plt.plot([a[0]], [a[1]], ".", "k")



def angulo(vector, tipo = 0):
	if tipo == 0:
		numero = np.arctan2(vector[1], vector[0])
		if numero >= 0.:
			return numero
		else:
			return 2*np.pi + numero
	else:
		return np.pi + np.arctan2(vector[1], vector[0])

def T(n, r0, da, tipo = 0):
	if tipo == 0:
		theta = n*dtheta
	else:
		theta = n*dtheta - np.pi
	tan = np.tan(theta)
	t = (r0[0]*tan - r0[1])/(da[1] - da[0]*tan)
	if(abs(np.cos(theta)) >= 0.1):
		return t, (da[0]*t + r0[0])/np.cos(theta)
	else:
		return t, (da[1]*t + r0[1])/np.sin(theta)
		

def interseccion(A, B, C, i, dtheta, tipo = 0):
	global da, r0
	soluciones = 0
	da, r0 = B - A, A
	t, sigma = T(i, r0, da, tipo)
	print("t = ", t, "sigma = ", sigma)
	if(t >= -0.0001 and t <= 1.0001)and sigma >= 0.:
		soluciones += 1
		punto1 = r(t, r0, da)
		print("solucion encontrada", punto1, soluciones)
	da, r0 = C - B, B
	t, sigma = T(i, r0, da, tipo)
	print("t = ", t, "sigma = ", sigma)
	if(t >= -0.0001 and t <= 1.0001)and sigma >= 0.:
		soluciones += 1
		if soluciones == 2: 
			punto2 = r(t, r0, da)
			print("solucion encontrada", punto2, soluciones)
		else: 
			punto1 = r(t, r0, da)
			print("solucion encontrada", punto1, soluciones)
	da, r0 = A - C, C
	t, sigma = T(i, r0, da, tipo)
	print("t = ", t, "sigma = ", sigma)
	if(t >= -0.0001 and t <= 1.0001)and sigma >= 0. and soluciones < 2:
		soluciones += 1
		punto2 = r(t, r0, da)
		print("solucion encontrada", punto2, soluciones)
	if soluciones == 2:
		r1, r2 = sum(punto1*punto1), sum(punto2*punto2)
		if(r1 < r2):
			return punto1, punto2
		else:
			return punto2, punto1
	else:
		print("error")
		return [0,0], [0, 0]

def interseccion2(A, B, C, phi_medio, tipo = 0):
	i = phi_medio/dtheta
	if(angulo(A, tipo) == phi_medio):
		da, r0 = C - B, B
		t, sigma = T(i, r0, da, tipo)
		punto1 = r(t, r0, da)
		punto2 = np.copy(A)
	elif(angulo(B, tipo) == phi_medio):
		da, r0 = C - A, C
		t, sigma = T(i, r0, da, tipo)
		punto1 = r(t, r0, da)
		punto2 = np.copy(B)
	else:
		da, r0 = A - B, B
		t, sigma = T(i, r0, da, tipo)
		punto1 = r(t, r0, da)
		punto2 = np.copy(C)
	r1, r2 = sum(punto1*punto1), sum(punto2*punto2)
	if(r1 < r2):
		return punto1, punto2
	else:
		return punto2, punto1

def interseccion3(A, B, C, phi_medio, tipo):
	if(angulo(A, tipo) == phi_medio):
		punto2 = np.copy(A)
	elif(angulo(B, tipo) == phi_medio):
		punto2 = np.copy(B)
	else:
		punto2 = np.copy(C)
	return punto2

A, B, C = np.random.random(2) - 0.5, np.random.random(2) - 0.5, np.random.random(2) - 0.5


tipo = 0
#tipo 
da, r0 = B - A, A
t, sigma =  T(0, r0, da)
if(t >= 0. and t <= 1.)and sigma >= 0.:
	tipo = 1

da, r0 = C - B, B
t, sigma =  T(0, r0, da)
if(t >= 0. and t <= 1.)and sigma >= 0.:
	tipo = 1

da, r0 = A - C, C
t, sigma =  T(0, r0, da)
if(t >= 0. and t <= 1.)and sigma >= 0.:
	tipo = 1


phi_a, phi_b, phi_c = angulo(A, tipo), angulo(B, tipo), angulo(C, tipo)
phi_minimo, phi_maximo = min(phi_a, min(phi_b, phi_c)), max(phi_a, max(phi_b, phi_c))
if phi_a != phi_minimo and phi_a != phi_maximo: 
	phi_medio = phi_a
	medio = A
elif phi_b != phi_minimo and phi_b != phi_maximo: 
	phi_medio = phi_b
	medio = B
else: 
	phi_medio = phi_c
	medio = C

n1, n2, n3 = int(phi_minimo/dtheta) + 1 , int(phi_medio/dtheta) + 1, int(phi_maximo/dtheta) + 1

"""
if phi_minimo == phi_a: 
	vec_min = np.copy(A)
elif phi_minimo == phi_b: 
	vec_min = np.copy(B)
else: 
	vec_min = np.copy(C)

if phi_maximo == phi_a:
	vec_max = np.copy(A)
elif phi_maximo == phi_b:
	vec_max = np.copy(B)
else:
	vec_max = np.copy(C)
a = np.copy(vec_min)
b = np.copy(vec_min)

for i in range(n1, n3 + 1):
	if(i == n2):
		c, d = interseccion2(A, B, C, phi_medio, tipo)
		plot(a, b, c, 2*i)
		plot(b, c, d, 2*i + 1)
		a, b = np.copy(c), np.copy(d)
	if(i == n3):
		c, d = np.copy(vec_max), np.copy(vec_max)
		plot(a, b, c, 2*i)
		plot(b, c, d, 2*i + 1)
	else:
		c, d = interseccion(A, B, C, i, dtheta, tipo)
		plot(a, b, c, 2*i)
		plot(b, c, d, 2*i + 1)
		a, b = np.copy(c), np.copy(d)

"""


if phi_minimo == phi_a: 
	vec_min = np.copy(A)
elif phi_minimo == phi_b: 
	vec_min = np.copy(B)
else: 
	vec_min = np.copy(C)

if phi_maximo == phi_a:
	vec_max = np.copy(A)
elif phi_maximo == phi_b:
	vec_max = np.copy(B)
else:
	vec_max = np.copy(C)
a = np.copy(vec_min)
b = np.copy(vec_min)


for i in range(n1, n3 + 1):
	if(i == n2):
		nuevo = interseccion3(A, B, C, phi_medio, tipo)
		c, d  = interseccion2(A, B, C, phi_medio, tipo)
		if(sum(d - nuevo) == 0.):
			print("primero")
			d = np.copy(nuevo)
			c = np.copy(a)
			plot(a, b, d, 2*i)
		else:
			print("segundo")
			d = np.copy(b)
			c = np.copy(nuevo)
			plot(a, b, c, 2*i)
#		plot(b, c, d, 2*i + 1)
		a, b = np.copy(c), np.copy(d)
	if(i == n3):
		c, d = np.copy(vec_max), np.copy(vec_max)
		plot(a, b, c, 2*i)
		plot(b, c, d, 2*i + 1)
	else:
		c, d = interseccion(A, B, C, i, dtheta, tipo)
		plot(a, b, c, 2*i)
		plot(b, c, d, 2*i + 1)
		a, b = np.copy(c), np.copy(d)



####triangulo con interior en (0.,0.)


def interseccion5(A, B, C, i):
	da, r0 = B - A, A
	t, sigma = T(i, r0, da, 0)
	if(t >= 0. and t <= 1.)and sigma >= 0.:
		return r(t, r0, da)
	else:
		da, r0 = C - B, B
		t, sigma = T(i, r0, da, 0)
		if(t >= 0. and t <= 1.)and sigma >= 0.:
			return r(t, r0, da)
		else:
			da, r0 = A - C, C
			t, sigma = T(i, r0, da, 0)
			if(t >= 0. and t <= 1.)and sigma >= 0.:
				return r(t, r0, da)

A, B, C = np.random.random(2) - 0.5, np.random.random(2) - 0.5, np.random.random(2) - 0.5


phi_a, phi_b, phi_c = angulo(A, 0), angulo(B, 0), angulo(C, 0)
phi_minimo, phi_maximo = min(phi_a, min(phi_b, phi_c)), max(phi_a, max(phi_b, phi_c))

if phi_minimo == phi_a: 
	vec_min = np.copy(A)
elif phi_minimo == phi_b: 
	vec_min = np.copy(B)
else: 
	vec_min = np.copy(C)

if phi_maximo == phi_a:
	vec_max = np.copy(A)
elif phi_maximo == phi_b:
	vec_max = np.copy(B)
else:
	vec_max = np.copy(C)
	
if phi_a != phi_minimo and phi_a != phi_maximo: 
	phi_medio = phi_a
	medio = A
elif phi_b != phi_minimo and phi_b != phi_maximo: 
	phi_medio = phi_b
	medio = B
else: 
	phi_medio = phi_c
	medio = C

n1, n2, n3 = int(phi_minimo/dtheta) + 1 , int(phi_medio/dtheta) + 1, int(phi_maximo/dtheta) + 1
centro = np.array([0., 0.])
b = interseccion5(A, B, C, 0)
a = centro + (b - centro)*0.1
for i in range(int(2*np.pi/dtheta) + 1):
	if(i == n1):
		d = np.copy(vec_min)
		#c = centro + (d - centro)*0.1
		#plot(a, b, d, 2*i)
		#plot(c, d, a, 2*i)
		#a = np.copy(c)
		#b = np.copy(d)
	if(i == n2):
		d = np.copy(medio)
		#c = centro + (d - centro)*0.1
		#plot(a, b, d, 2*i)
		#plot(c, d, a, 2*i)
		#a = np.copy(c)
		#b = np.copy(d)
	if(i == n3):
		d = np.copy(vec_max)
		#c = centro + (d - centro)*0.1
		#plot(a, b, d, 2*i)
		#plot(c, d, a, 2*i)
		#a = np.copy(c)
		#b = np.copy(d)
	#else:
	c = centro + (d - centro)*0.1
	plot(a, b, d, 2*i)
	plot(c, d, a, 2*i)
	a = np.copy(c)
	b = np.copy(d)

	d = interseccion5(A, B, C, i)
	c = centro + (d - centro)*0.1
	plot(a, b, d, 2*i)
	plot(c, d, a, 2*i)
	a = np.copy(c)
	b = np.copy(d)
