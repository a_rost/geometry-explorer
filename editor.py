import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mayavi import mlab
import metrica
import vtk
import solve_ode as solve
plt.ion()


def plot(vertex, size):
	mlab.points3d(vertex[:,0], vertex[:,1], vertex[:,2], scale_factor = size)

def plot_triang(vertex, triangles):
	surf = mlab.triangular_mesh(vertex[:,0], vertex[:,1], vertex[:,2], triangles)
	
def esf2xyz(vertex):
	XYZ = np.zeros([len(vertex), 3])
	XYZ[:,0] = vertex[:,1]*np.cos(vertex[:,3])*np.sin(vertex[:,2])
	XYZ[:,1] = vertex[:,1]*np.sin(vertex[:,3])*np.sin(vertex[:,2])
	XYZ[:,2] = vertex[:,1]*np.cos(vertex[:,2])
	return XYZ

	
def det_derivada(vertex, position):
	d0     = position[0]
	theta0 = position[1]
	derivada   = np.zeros([len(vertex), 4])
	posiciones = np.zeros([len(vertex), 4])
	
	derivada[:,1] =  vertex[:, 0]/np.sqrt(metrica.metrica(d0, theta0, 1))
	derivada[:,2] = -vertex[:, 2]/np.sqrt(metrica.metrica(d0, theta0, 2))
	derivada[:,3] =  vertex[:, 1]/np.sqrt(metrica.metrica(d0, theta0, 3))
	
	posiciones[:, 1] = position[0]
	posiciones[:, 2] = position[1]
	posiciones[:, 3] = position[2]
	
	distancia = np.linalg.norm(vertex, axis = 1)
	return posiciones, derivada, distancia

class objeto():
	def plot(self, size = 1.):
		plot(self.vertex_euclid, size)
	
	def create_triangles(self):
		self.triangles = [ np.array(self.faces[item][0]) - 1 for item in range(len(self.faces)) ]

	def __init__(self, tipo, position = np.array([0., np.pi/2., 0.])):
		self.tipo      = tipo
		self.position  = position
	
	def place_vertex(self, scale = 1.):
		posiciones, derivada, distances = det_derivada(self.vertex_euclid, self.position)
		self.vertex = solve.solve_ode(posiciones, derivada, distances*scale, dsigma = 0.01)
		
	def plot_triang(self):
		plot_triang(self.vertex_euclid, self.triangles)
	vertex_euclid = []
	vertex        = []
	normals       = []
	uv            = []

def load_obj(filename, name):
	"""Loads a Wavefront OBJ file. """
	
	result = objeto(name)

	vertices = []
	normals = []
	texcoords = []
	faces = []

	material = None
	for line in open(filename, "r"):
		if line.startswith('#'): continue
		values = line.split()
		if not values: continue

		if values[0] == 'v':
			v = list(map(float, values[1:4]))
			v = v[0], v[2], v[1]
			vertices.append(v)

		elif values[0] == 'vn':
			v = list(map(float, values[1:4]))
			v = v[0], v[2], v[1]
			normals.append(v)

		elif values[0] == 'vt':
			texcoords.append(list(map(float, values[1:3])))

		elif values[0] == 'f':
			face      = []
			texcoords = []
			norms     = []
			for v in values[1:]:
				w = v.split('/')
				face.append(int(w[0]))
				if len(w) >= 2 and len(w[1]) > 0:
					texcoords.append(int(w[1]))
				else:
					texcoords.append(0)
				if len(w) >= 3 and len(w[2]) > 0:
					norms.append(int(w[2]))
				else:
					norms.append(0)
			faces.append((face, norms, texcoords, material))

	#for face in faces:
		#vertices, normals, texture_coords, material = face
	
	result.vertex_euclid = np.array(vertices)
	result.normals       = np.array(normals)
	result.uv            = np.array(texcoords)
	result.faces         = np.array(faces)
	
	result.create_triangles()
	return result



asteroide1 = load_obj("models/asteroide.obj", "Asteroide 1")
asteroide2 = load_obj("models/asteroide2.obj", "Asteroide 2")
asteroide3 = load_obj("models/asteroide3.obj", "Asteroide 3")

model2     = load_obj("models/models2.obj", "Monkey")
model1     = load_obj("models/exports.obj", "Monkey")
