import numpy as np
import modulo
import time
from scipy import integrate

import matplotlib.pyplot as plt
plt.ion()
DEBUG = False

T = 2.
b = 1./(2*T)
h = T - b*T**2
R = h + b*T**2

RHO_MAX, RHO_MIN = 5., -5.
condicion = 1

def r(x):
	if x >= T:
		return x
	elif x >= -T:
		return h + b*x**2
	else:
		return -x

def r_p(x):
	if x >= T:
		return 1.
	elif x >= -T:
		return 2*b*x
	else:
		return -1.

def r_p_p(x):
	if x >= T:
		return 0.
	elif x >= -T:
		return 2*b
	else:
		return 0.

def U(x, P):
	return -1./P**2 + 1./r(x)**2

def U_p(x, P):
	return -2*r_p(x)*r(x)**(-3)

def U_p_p(x, P):
	return 6*r(x)**(-4)*r_p(x)**2 - 2*r(x)**(-3)*r_p_p(x)

def raiz(P):
	if P < h:
		#print("no hay solucion, P < h")
		return 0.
	elif P >= R:
		return P
	else:
		return np.sqrt((P - h)/b)



def metrica(x, theta, indice):
	if indice == 0:
		return -c**2
	elif indice == 1:
		return 1.
	elif indice == 2:
		return r(x)**2
	elif indice == 3:
		return (r(x)*np.sin(theta))**2





def integrando_phi(x, P):
	if 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/r(x)**2



def F(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_phi, rho_max, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_phi, 0., x, args=(P))[0]


def F2(x0, x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_phi, x0, x, args=(P))[0]
		else:
			return integrate.quad(integrando_phi, x0, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_phi, x0, x, args=(P))[0]

def F6(x, P):
	return modulo.geodesica_F([x], [P])[0]

def T6(x, P):
	return modulo.geodesica_T([x], [P])[0]



def integrando_T(x, P):
	if 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/P



def t(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_T, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_T, rho_max, x, args=(P))[0]
	elif P == 0.:
		return x
	else:
		return integrate.quad(integrando_T, 0., x, args=(P))[0]

def t2(x0, x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_T, x0, x, args=(P))[0]
		else:
			return integrate.quad(integrando_T, x0, x, args=(P))[0]
	elif P == 0.:
		return x - x0
	else:
		return integrate.quad(integrando_T, x0, x, args=(P))[0]








def juan(x):
	#integral of 1/sqrt(x**2 - 1)
	return np.log(abs(np.sqrt(abs(x**2 - 1.)) + x))

def integral(a, b, c, x):
	#integral of 1/sqrt(a + b*x + c*x**2)
	if c == 0.:
		return 2/b*np.sqrt(a + b*x) - 2*np.sqrt(a)/b
	if b == 0.:
		return np.arcsinh(x*np.sqrt(c/a))/np.sqrt(c)
	if c < 0.:
		c = abs(c)
		sita = -b/2./np.sqrt(c)
		eta = np.sqrt(a + sita**2)
		arg = np.sqrt(c)*x/eta + sita/eta
		return (np.arcsin(arg) - np.arcsin(sita/eta))/np.sqrt(c)
	else:
		if a - b**2/(4*c) >= 0.:
			eta = np.sqrt(a - b**2/(4*c))
			return (np.arcsinh(np.sqrt(c)*x/eta + b/2/np.sqrt(c)/eta) - np.arcsinh(b/2/np.sqrt(c)/eta))/np.sqrt(c)
		else:
			eta = np.sqrt(-a + b**2/(4*c))
			#print("Eta: ", eta)
			return (juan(np.sqrt(c)*x/eta + b/2/np.sqrt(c)/eta) - juan(b/2/np.sqrt(c)/eta))/np.sqrt(c)

def coefficients(rho0, P):
	A = r(rho0)**4/P**2 - r(rho0)**2
	B = (4*r(rho0)**3/P**2 - 2*r(rho0))*r_p(rho0)
	C = ((4*r(rho0)**3/P**2 - 2*r(rho0))*r_p_p(rho0) + r_p(rho0)**2*(12*r(rho0)**2/P**2 - 2.))/2.
	return A, B, C

def coefficients_t(rho0, P):
	A = 1. - P**2/r(rho0)**2
	B = 2*P**2*r_p(rho0)*r(rho0)**(-3)
	C = (-6*P**2*r(rho0)**-4*r_p(rho0)**2 + 2*P**2*r(rho0)**-3*r_p_p(rho0))/2.
	#if C < 0.:
		#C = 0.
	return A, B, C

def approx_F(x, P):
	#print(A, B, C)
	#print("Det: ", A - B**2/(4*C))
	if P >= 1.:
		if x > 0.:
			A, B, C = coefficients(rho0 = raiz(P), P = P)
			if DEBUG:
				print("A = " + str(A) + " B = " + str(B) + " C = " + str(C))
			return integral(0., B, C, abs(x - raiz(P)))  
		else:
			return 0.
	else:
		A, B, C = coefficients(rho0 = 0., P = P)
		return integral(A, B, C, abs(x)) - integral(A, B, C, 0.)


def approx_t(x, P):
	if P >= 1.:
		if x > 0.:
			A, B, C = coefficients_t(rho0 = raiz(P), P = P)
			return integral(0., B, C, abs(x - raiz(P)))  
		else:
			return 0.
	else:
		A, B, C = coefficients_t(rho0 = 0., P = P)
		return integral(A, B, C, abs(x)) - integral(A, B, C, 0.)
		

def approx_F2(rho1, rho2, P):
	A, B, C = coefficients(rho0 = rho1, P = P)

	return integral(A, B, C, abs(rho2 - rho1))  

def approx_T2(rho1, rho2, P):
	A, B, C = coefficients_t(rho0 = rho1, P = P)
	#if C < 0.:
		#C = 0.
	if DEBUG:
		print("A = " + str(A) + " B = " + str(B) + " C = " + str(C))

	return integral(A, B, C, abs(rho2 - rho1))  



def fix(array):
	array[0] = array[1]
	array[-1] = array[-2]
	
	array[:, 0] = array[:, 1]
	array[:, -1] = array[:, -2]
	return array







def interpolador(array, d_rho, d_P, rho0, P0, x, P):
	i = int(np.floor((x - rho0)/d_rho))
	j = int(np.floor((P - P0)/d_P))
	
	valores = np.zeros(4)
	valores[0] = array[i + 0][j + 0]
	valores[1] = array[i + 0][j + 1]
	valores[2] = array[i + 1][j + 0]
	valores[3] = array[i + 1][j + 1]

	#if j < 0:
	#		valores[0] = np.pi*0.5
	#		valores[2] = np.pi*0.5
	#

	#
	valores2 = np.zeros(2)
	valores2 = (valores[2:4] - valores[0:2])*(x - i*d_rho - rho0)/d_rho + valores[0:2]

	valor = (valores2[1] - valores2[0])*(P - j*d_P - P0)/d_P + valores2[0]

	if DEBUG:
		print("i = ", i, "j = ", j)
		print(valores2)
		print(valores)
		print(valor)
		print("drho: ", (x - i*d_rho - rho0)/d_rho)
		print("dP:   ", (P - j*d_P   - P0)/d_P)
	return valor


def interpolador_1d(array, dx, x0, x):
	i = int((x - x0)/dx)
	if i + 1 < len(array):
		x1 = array[i]
		x2 = array[i + 1]
		s = x - (x0 + i*dx)
		
		if DEBUG:
			print("i = ", i)
			print(x1, x2, x1 + (x2 - x1)*s/dx)
		return x1 + (x2 - x1)*s/dx


def F4(x, P):
	P_max = r(x)

	if P == 0.:
		return 0.

	elif P == P_max:
		return 0.

	elif P > P_max:
		print("Error")

	elif x == 0.:
		return 0.

	elif x < 0.:
		return -F4(-x, P)
	
	elif x < 2.:
		if P <= 1.:
			u = 1. - np.sqrt(1 - P)
			M1 = interpolador(array1, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, u)
			M2 = interpolador_1d(array2, 1./(N_P - 1), 0., u)

		else:
			u = raiz(P)/x
			M1 = interpolador(array3_bis, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, u)
			
			u = raiz(P)/2.
			M2 = interpolador_1d(array4_bis, 1./(N_P - 1), 0., u)
		
		if DEBUG:
			print(M1, M2, approx_F(2., P), approx_F(2., P) - approx_F(x, P))
		return M2*approx_F(2., P) - (approx_F(2., P) - approx_F(x, P))*M1

	elif x == 2.:
		if P <= 1.:
			u  = 1. - np.sqrt(1 - P)
			M2 = interpolador_1d(array2, 1./(N_P - 1), 0., u)
		else:
			u  = raiz(P)/2.
			M2 = interpolador_1d(array4_bis, 1./(N_P - 1), 0., u)
		return M2*approx_F(2., P)

	elif x > 2.:
		u2 = P/x
		if P <= 2.:
			u1 = P/2.
			M1 = F4(2., P)

		else:
			u1 = 1.
			M1 = 0.

		return -np.arcsin(u2) + np.arcsin(u1) + M1



def F_offset(x, P):
	P_max = r(x)

	if P == 0.:
		return 0.

	elif P == P_max:
		return 0.

	elif P > P_max:
		print("Error")

	elif x == 0.:
		return 0.

	elif x < 0.:
		return -F4(-x, P)
	
	elif x < 2.:
		if P <= 1.:
			u = 1. - np.sqrt(1 - P)
			M1 = interpolador(array1, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, u)

		else:
			u = raiz(P)/x
			M1 = interpolador(array3_bis, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, u)
			

		return  -(approx_F(2., P) - approx_F(x, P))*M1

	elif x == 2.:
		return 0.

	elif x > 2.:
		u2 = P/x
		if P <= 2.:
			u1 = P/2.

		else:
			u1 = 1.

		return -np.arcsin(u2) + np.arcsin(u1)



def integrando_S(rho, P):
	return 2.*F_offset(rho, P)*r(rho)*r_p(rho)


def S(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_S, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_S, rho_max, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_S, 0., x, args=(P))[0]
	
	
	
def calculate_arrays_region1(N_P, N_rho, F, F2, approx_F2 = approx_F2):
	array = np.zeros([N_rho, N_P])

	P_space   = (1. - np.linspace(0., 1., N_P)**2.)
	P_space.sort()

	P_space = np.linspace(0., 1., N_P)
	rho_space = np.linspace(0., 2., N_rho)
	drho = rho_space[1] - rho_space[0]
	rho_space[0] = drho/100.
	rho_space[-1] = 2. - drho/100.
	
	dP = P_space[-1] - P_space[-2]
	P_space[-1] = 1 - dP/100.
	
	dP = P_space[1] - P_space[0]
	P_space[0] = dP/100.
	for i in range(N_P):
		P = P_space[i]
		for j in range(N_rho):
			rho = rho_space[j]
			try:
				#array[j, i] = -F2(2., rho, P)/(approx_F(2., P) - approx_F(rho, P))
				#array[j, i] = F2(rho, 2., P)/approx_F2(rho, 2., P)
				array[j, i] = (F6(2., P) - F(rho,  P))/approx_F2(rho, 2., P)
			except:
				pass

	#array[0] = array[1]
	#array[:, 0] = array[:, 1]

	rho = rho_space[0]
	array2 = np.zeros(N_P)
	for i in range(N_P):
		#array2[i] = F(rho, P_space[i])/approx_F(rho, P_space[i])
		array2[i] = F2(rho, 2., P_space[i])/approx_F2(rho, 2., P_space[i])
	array[0] = array2
	
	#array2[0] = array2[1]
	return np.float32(array), np.float32(array2)


def calculate_arrays_region2(N_P, N_rho, F, F2, approx_F2 = approx_F2):
	array3 = np.zeros([N_rho, N_P])

	
	rho_space = np.linspace(0., 2., N_rho)
	drho = rho_space[1] - rho_space[0]
	rho_space[0] = drho/100.
	rho_space[-1] = 2. - drho/100.
	
	r_vec = np.vectorize(r)
	for j in range(N_rho):
		w_space  = np.linspace(0., rho_space[j], N_P)
		
		dw = w_space[1] - w_space[0]
		
		w_space[0] = dw/100.
		w_space[-1] = rho_space[j] - dw/100.
		
		P_space = r_vec(w_space)

		for i in range(N_P):
			rho = rho_space[j]
			P = P_space[i]
			try:
				#array3[j, i] = -F2(2., rho, P)/(approx_F(2., P) - approx_F(rho, P))
				array3[j, i] = F2(rho, 2., P)/approx_F2(rho, 2., P)
			except:
				pass

	#array3 = fix(array3)

	rho = rho_space[-1]
	w_space  = np.linspace(0., rho, N_P)
	dw = w_space[1] - w_space[0]
	w_space[0] = dw/100.
	w_space[-1] = 2. - dw/100.
	
	P_space = r_vec(w_space)
	array4 = np.zeros(N_P)
	for i in range(N_P):
		#array4[i] = F(rho, P_space[i])/approx_F(rho, P_space[i])
		rho = raiz(P_space[i])
		
		array4[i] = F2(rho, 2., P_space[i])/approx_F2(rho, 2., P_space[i])
	array4[0] = 1.
	#array4[0] = array4[1]
	#array4[-1] = array4[-2]
	#array4[-1] = 1.
	array3.T[-1] = array4
	#array3[0] = 1.
	return array3, array4


def show_array():
	plt.imshow(array3_bis, extent = [0., 1., -1., 0.], origin = "upper")
	
	n = 50
	P_space = np.linspace(1., 2., n)
	rho_space = np.linspace(0., 2., n)
	raiz_vec = np.vectorize(raiz)
	ones = np.ones(n)
	for i in range(n):
		P = P_space[i]
		u = rho_space/2.
		w = raiz_vec(P)/rho_space
		plt.plot(u, -w, "k")
	plt.xlim(0., 1.)
	plt.ylim(-1, 0.)




def test_3(rho1, phi):
	N = 50000
	rho2 = np.linspace(-3., 3., N)

	ones = np.ones(N)
	t_est= np.zeros(N)
	phi_est1= np.zeros(N)
	phi_est2= np.zeros(N)
	angles0, distances0, solutions0 = modulo.geodesica(rho1*ones, rho2, phi*ones)
	
	geo1 = np.arange(0, N*4, 4) + 0
	geo2 = np.arange(0, N*4, 4) + 1
	geo3 = np.arange(0, N*4, 4) + 2
	geo4 = np.arange(0, N*4, 4) + 3
	
	alpha = angles0[geo1]
	dists = distances0[geo1]
	P_sol = np.zeros(N)

	dists_real = np.zeros(N)
	for i in range(N):
		P_sol[i] = r(rho1)*abs(np.sin(alpha[i]))
		#t_est[i] = I(P_sol[i], rho1, rho2[i], t)
		#phi_est1[i] = I(P_sol[i], rho1, rho2[i], F)
		#phi_est2[i] = I(P_sol[i], rho1, rho2[i], F6)
		#dists_real[i] = abs(t2(rho1, rho2[i], P_sol[i]))
	archivo = open("continuity_test2.txt", "a")
	archivo.write(str(rho1) + " " + str(rho2) + " " + str(N_P*N_rho*2) + " ")
	for i in range(N):
		archivo.write(str(alpha[i]) + " ")
	archivo.write("\n")

	archivo.write(str(rho1) + " " + str(rho2) + " " + str(N_P*N_rho*2) + " ")
	for i in range(N):
		archivo.write(str(dists[i]) + " ")
	archivo.write("\n")

	archivo.close()
	
	r_vec = np.vectorize(r)
	dist_approx = r_vec((rho1*ones + rho2)/2.)*phi
	fig, axs = plt.subplots(ncols = 4, nrows = 1, sharex = "all")

	axs[0].plot(rho2, np.mod(alpha, np.pi))
	axs[0].plot(rho2, np.mod(angles0[geo2], np.pi))
	axs[0].plot(rho2, np.mod(angles0[geo3], np.pi))
	axs[0].plot(rho2, np.mod(angles0[geo4], np.pi))

	axs[0].fill_between(rho2, alpha - 0.03*np.pi/180, alpha + 0.03*np.pi/180, alpha = 0.3, color = "k")
	axs[0].plot(rho2, np.pi/2. + np.arcsin(P_sol/r_vec(rho2)))
	axs[0].plot(rho2, ones*np.pi/2.)

	axs[1].plot(rho2, dists)
	axs[1].plot(rho2, distances0[geo2])
	axs[1].plot(rho2, distances0[geo3])
	axs[1].plot(rho2, distances0[geo4])
	
	#axs[1].plot(rho2, t_est)
	axs[1].plot(rho2, modulo.geodesica_T(2.*ones, P_sol))
	
	#axs[1].plot(rho2, dists_real)
	axs[2].plot(rho2, P_sol)
	axs[2].plot(rho2, r_vec(rho1*ones)*abs(np.sin(angles0[geo2])))
	axs[2].plot(rho2, r_vec(rho1*ones)*abs(np.sin(angles0[geo3])))
	axs[2].plot(rho2, r_vec(rho1*ones)*abs(np.sin(angles0[geo4])))

	axs[3].plot(rho2, phi*ones)
	#axs[3].plot(rho2, phi_est1)
	#axs[3].plot(rho2, phi_est2)
	
	
	return {"rho1": rho1*ones, "rho2": rho2, "phi": phi*ones, "P": P_sol, "P_found": P_sol, "alpha_found": alpha}

def I(P, rho1, rho2, F):
	return abs(F(rho1, P) - F(rho2, P))

def G(P, rho1, rho2, F):
	return abs(F(rho1, P) + F(rho2, P))

def save_arrays():
	np.savetxt("data/worm/matrix_array1", array1)
	np.savetxt("data/worm/vector_array2", array2)

	np.savetxt("data/worm/matriz_array_bis", array3)
	np.savetxt("data/worm/vector_array_bis", array4)
	
	np.savetxt("data/worm/matrix_array5", array1_t)
	np.savetxt("data/worm/vector_array6", array2_t)
	np.savetxt("data/worm/matrix_array7", array3_t)
	np.savetxt("data/worm/vector_array8", array4_t)

def load_arrays():
	array1 = np.loadtxt("data/worm/matrix_array1")
	array2 = np.loadtxt("data/worm/vector_array2")

	array3_bis = np.loadtxt("data/worm/matriz_array_bis")
	array4_bis = np.loadtxt("data/worm/vector_array_bis")

	array1_t = np.loadtxt("data/worm/matrix_array5")
	array2_t = np.loadtxt("data/worm/vector_array6")
	array3_t = np.loadtxt("data/worm/matrix_array7")
	array4_t = np.loadtxt("data/worm/vector_array8")
	
	return array1, array2, array3_bis, array4_bis, array1_t, array2_t, array3_t, array4_t


def ratio(P):
	rho_min = raiz(P)
	return F2(rho_min, 2., P)/approx_F2(rho_min, 2., P)

array1, array2, array3_bis, array4_bis, array1_t, array2_t, array3_t, array4_t = load_arrays()

rho = 0.1015
P = 1.000079
N_rho, N_P = 200, 200

if True:
	array1,     array2     = calculate_arrays_region1(N_P, N_rho, F, F2, approx_F2)
	array3_bis, array4_bis = calculate_arrays_region2(N_P, N_rho, F, F2, approx_F2)

	array1_t, array2_t = calculate_arrays_region1(N_P, N_rho, t, t2, approx_F2 = approx_T2)
	array3_t, array4_t = calculate_arrays_region2(N_P, N_rho, t, t2, approx_F2 = approx_T2)

else:
	array1,     array2     = calculate_arrays1_orig(N_P, N_rho, F, F2)
	array3_bis, array4_bis = calculate_arrays2_orig(N_P, N_rho, F, F2)
	
	array1_t, array2_t = calculate_arrays(N_P, N_rho, t, t2)
	array3_t, array4_t = calculate_arrays2(N_P, N_rho, t, t2)



array3_bis[0] = array4_bis[0]
array1.T[-1] = array3_bis.T[0]
array1_t.T[-1] = array3_t.T[0]

array2[-1] = array4_bis[0]
array2_t[-1] = array4_t[0]

save_arrays()











#def F5(x, P):
	#P_max = r(x)

	#if P == 0.:
		#return 0.

	#elif P == P_max:
		#return 0.

	#elif P > P_max:
		#print("Error")

	#elif x == 0.:
		#return 0.

	#elif x < 0.:
		#return -F4(-x, P)
	
	#elif x < 2.:
		#if P <= 1.:
			#M1 = interpolador(array1, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, P)
			#M2 = interpolador_1d(array2, 1./(N_P - 1), 0., P)

		#else:
			#P_max = r(x)
			#u = (P - 1.)/(P_max - 1.)
			#M1 = interpolador(array3_bis, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, u)
			#M2 = interpolador_1d(array4_bis, 1./(N_P - 1), 1., P)
		#return -np.log(M2) + np.log(M1)

	#elif x == 2.:
		#if P <= 1.:
			#M2 = interpolador_1d(array2, 1./(N_P - 1), 0., P)
		#else:
			#M2 = interpolador_1d(array4, 1./(N_P - 1), 1., P)
		#return -np.log(M2)

	#elif x > 2.:
		#u2 = P/x
		#if P <= 2.:
			#u1 = P/2.
			#M1 = F4(2., P)

		#else:
			#u1 = 1.
			#M1 = 0.

		#return -np.arcsin(u2) + np.arcsin(u1) + M1


#def det_coefficients(diff):
	#if diff >= 0.01:
		#return 1., 0.
	#else:
		#return diff/0.01, 1. - diff/0.01
	

#def F5(x, P):
	#P_max = r(x)

	#if P == 0.:
		#return 0.

	#elif P == P_max:
		#return 0.

	#elif P > P_max:
		#print("Error")

	#elif x == 0.:
		#return 0.

	#elif x < 0.:
		#return -F5(-x, P)
	
	#elif x < 2.:
		#if P <= 1.:
			#M1 = interpolador(array1, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, P)
			#M2 = interpolador_1d(array2, 1./(N_P - 1), 0., P)
			#return -np.log(M2) + np.log(M1)

		#else:
			#rho_min = raiz(P)
			#u = (x - rho_min)/(2. - rho_min)
			#M1 = interpolador(array5, 1./(N_rho - 1), 1./(N_P - 1), 0., 1., u, P)
			#M2 = interpolador_1d(array6, 1./(N_P - 1), 1., P)

			#M1 *= -(approx_F2(2., P) - approx_F2(x, P))
			#M2 *= approx_F2(2., P)
			#return M1 + M2

	#elif x == 2.:
		#if P <= 1.:
			#M2 = interpolador_1d(array2, 1./(N_P - 1), 0., P)
			#return -np.log(M2)
		#else:
			#M2 = interpolador_1d(array6, 1./(N_P - 1), 1., P)
			#return M2*approx_F2(2., P)
	#elif x > 2.:
		#u2 = P/x
		#if P <= 2.:
			#u1 = P/2.
			#M1 = F5(2., P)

		#else:
			#u1 = 1.
			#M1 = 0.

		#return -np.arcsin(u2) + np.arcsin(u1) + M1



#def juancho(x, P):
	#return x*np.sqrt(1. - P*P/(x*x))



#def t4(x, P):
	#P_max = r(x)

	#if P == 0.:
		#return x

	#elif P == P_max:
		#return 0.

	#elif P > P_max:
		#print("Error")

	#elif x == 0.:
		#return 0.

	#elif x < 0.:
		#return -T4(-x, P)
	
	#elif x < 2.:
		#if P <= 1.:
			#M1 = interpolador(array1_t, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, P)
			#M2 = interpolador_1d(array2_t, 1./(N_P - 1), 0., P)

		#else:
			#rho_min = raiz(P)
			#u = (x - rho_min)/(2. - rho_min)
			#M1 = interpolador(array3_t, 1./(N_rho - 1), 1./(N_P - 1), 0., 1., u, P)
			#M2 = interpolador_1d(array4_t, 1./(N_P - 1), 1., P)

		#return -np.log(M2) + np.log(M1)

	#elif x == 2.:
		#if P <= 1.:
			#M2 = interpolador_1d(array2_t, 1./(N_P - 1), 0., P)
		#else:
			#M2 = interpolador_1d(array4_t, 1./(N_P - 1), 1., P)
		#return -np.log(M2)

	#elif x > 2.:
		#u2 = x
		#if P <= 2.:
			#u1 = 2.
			#M1 = t4(2., P)

		#else:
			#u1 = P
			#M1 = 0.

		#return juancho(u2, P) - juancho(u1, P) + M1




#def test(F, M, array = False):
	#test_array = np.zeros([M, M])
	
	#rho_space = np.linspace(0., 3., M)
	#for i in range(M):
		#rho = rho_space[i]
		#P_space = np.linspace(0., r(rho), M)
		#for j in range(M):
			#P = P_space[j]
			#if array:
				#test_array[i, j] = F([rho], [P])
			#else:
				#test_array[i, j] = F(rho, P)
	#return test_array

#def test_2(F, M, array = False):
	#test_array = np.zeros([M, M])
	
	#rho_space = np.linspace(0., 2., M)
	#for i in range(M):
		#rho = rho_space[i]
		#P_space = np.linspace(1., r(rho), M)
		#for j in range(M):
			#P = P_space[j]
			#if array:
				#test_array[i, j] = F([rho], [P])
			#else:
				#test_array[i, j] = F(rho, P)
	#return test_array



#test1 = test_2(F,  257)
#test2 = test_2(F4, 257)
#test3 = test_2(F5, 257)
#test4 = test_2(modulo.geodesica_F, 257, array = True)

#diff1 = abs(test2 - test1)
#diff2 = abs(test3 - test1)
#diff3 = abs(test4 - test1)

#frac1 = abs(test2/test1)
#frac2 = abs(test3/test1)

#err1 = diff1/test1
#err2 = diff2/test1




"""
def F2_3(rho1, rho2, P):
	return F3(rho2, P) - F3(rho1, P)


mejora_porc = (err1/err2)
mejora_porc = mejora_porc.flatten()

plt.hist(np.log10(mejora_porc), 50, range = [-2., 4.])


P_space = np.linspace(1., 2., 2595)
random = np.random.random(2595)
random.sort()
P_space = random + 1. 

asdf1 = np.zeros(len(P_space))
asdf2 = np.zeros(len(P_space))
asdf3 = np.zeros(len(P_space))
for i in range(len(P_space)):
	asdf1[i] = F(2., P_space[i])
	asdf2[i] = F4(2., P_space[i])
	asdf3[i] = F5(2., P_space[i])


plt.plot(np.log10(abs(asdf2 - asdf1)))
plt.plot(np.log10(abs(asdf3 - asdf1)))

diff1 = abs(asdf2 - asdf1)
diff2 = abs(asdf3 - asdf1)


plt.plot(np.log10(diff1/diff2))

plt.plot(np.log10(abs(asdf2/asdf1 - 1.)))
plt.plot(np.log10(abs(asdf3/asdf1 - 1.)))




random = np.random.random(2595)
random.sort()
rho_space = random + 0.875623133 
rho_min = np.min(rho_space)
P = r(rho_min)
asdf1 = np.zeros(len(rho_space))
asdf2 = np.zeros(len(rho_space))
asdf3 = np.zeros(len(rho_space))
for i in range(len(rho_space)):
	asdf1[i] = F(rho_space[i], P)
	asdf2[i] = F4(rho_space[i], P)
	asdf3[i] = F5(rho_space[i], P)

diff1 = abs(asdf2 - asdf1)
diff2 = abs(asdf3 - asdf1)

#####################CHECKED UNTIL HERE


def integrando_S(x, P):
	return F4(x, P)*r(x)*r_p(x)



#def t(x, P):
	#if P > h:
		#rho_min = raiz(P)
		#rho_max = -rho_min
		#if (x < rho_min) and (x > rho_max):
			#print("Error, fuera de rango")
			#return 0.
		#if x >= 0.:
			#return integrate.quad(integrando_t, rho_min, x, args=(P))[0]
		#else:
			#return integrate.quad(integrando_t, rho_max, x, args=(P))[0]
	#elif P == 0.:
		#return x
	#else:
		#return integrate.quad(integrando_t, 0., x, args=(P))[0]
def S(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_S, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_S, rho_max, x, args=(P))[0]
	elif P == 0.:
		return x
	else:
		return integrate.quad(integrando_S, 0., x, args=(P))[0]


S_array = np.zeros([100, 100])
rho_space = np.linspace(0., 3., 100)
for i in range(len(S_array)):
	print(i)
	rho = rho_space[i]
	P_max = r(rho)
	P_space = np.linspace(0., 1., 100)
	for j in range(100):
		P = P_space[j]
		S_array[i, j] = S(rho, P)
		
plt.imshow(np.log10(S_array))

"""
	




#matriz1 = np.zeros([100, 100])
#matriz2 = np.zeros([100, 100])
#matriz3 = np.zeros([100, 100])

#rho_space = np.linspace(0., 4., 100)**0.5
#rho_space[0] = 0.000001
#for i in range(100):
	#print(i)
	#P_space = np.linspace(1.000, r(rho_space[i]), 100)
	#for j in range(100):
		#rho_min = raiz(P_space[j])
		#matriz1[i, j] = F4(rho_space[i], P_space[j])
		#matriz2[i, j] = F(rho_space[i], P_space[j])
		#matriz3[i, j] = np.sqrt(abs(rho_space[i] - rho_min))


#Nrho, NP = 400, 400
#matriz1 = np.zeros([Nrho, NP])
#matriz2 = np.zeros([Nrho, NP])
#matriz3 = np.zeros([Nrho, NP])
#matriz4 = np.zeros([Nrho, NP])
#matriz5 = np.zeros([Nrho, NP])

#rho_space = np.linspace(0., 4., Nrho)**0.5
#rho_space = np.linspace(0., 2., Nrho)
#rho_space[0] = 0.00000001
#for i in range(Nrho):
	#print(i)
	#P_space = np.linspace(1, r(rho_space[i]), NP)
	##P_space[0] = 1.0000001
	#for j in range(NP):
		#rho_min = raiz(P_space[j])
		#matriz1[i, j] = F(rho_space[i], P_space[j])
		#matriz2[i, j] = approx_F2(rho_space[i], P_space[j])
		#matriz3[i, j] = -F2(2., rho_space[i], P_space[j])
		#matriz4[i, j] = (approx_F2(2., P_space[j]) - approx_F2(rho_space[i], P_space[j]))
		#matriz5[i, j] = (approx_F(2., P_space[j]) - approx_F(rho_space[i], P_space[j]))
##array_exp = np.zeros(NP)
##P_space = np.linspace(1., 2., NP)
##for i in range(NP):
	##array_exp[i] = F(2., P_space[i])/approx_F2(2., P_space[i])


#fig, axs = plt.subplots(ncols = 5, nrows = 1)
#axs[0].imshow(np.log(matriz1 + 1))
#axs[1].imshow(np.log(matriz2 + 1))
#axs[2].imshow(np.log(matriz3 + 1))
#axs[3].imshow(np.log(matriz4 + 1))
#axs[4].imshow(np.log(matriz5 + 1))

#fig, axs = plt.subplots(ncols = 3, nrows = 1)
#axs[0].imshow((matriz1/matriz2))
#axs[1].imshow((matriz3/matriz4))
#axs[2].imshow((matriz3/matriz5))



#plt.imshow(matriz2)




#def F_exp(x, P):
	#P_max = r(x)

	#if P == 0.:
		#return 0.

	#elif P == P_max:
		#return 0.

	#elif P > P_max:
		#print("Error")

	#elif x == 0.:
		#return 0.

	#elif x < 0.:
		#return -F4(-x, P)
	
	#elif x < 2.:
		#if P <= 1.:
			#M1 = interpolador(array1, 2./(N_rho - 1), 1./(N_P - 1), 0., 0., x, P)
			#M2 = interpolador_1d(array2, 1./(N_P - 1), 0., P)

		#else:
			#P_max = r(x)
			#eta = x**2
			#u   = (P - 1.)/(P_max - 1.)

			#M1 = interpolador(matriz2, 2.**2/(Nrho - 1), 1./(NP - 1), 0., 0., eta, u)
			#M2 = interpolador_1d(array_exp, 1./(N_P - 1), 1., P)
			
			##return M1*(approx_F2(2., P) - approx_F2(x, P)) + M2*approx_F2(2., P)

			#return M1*approx_F2(x, P)
		
	#elif x == 2.:
		#if P <= 1.:
			#M2 = interpolador_1d(array2, 1./(N_P - 1), 0., P)
		#else:
			#M2 = interpolador_1d(array4, 1./(N_P - 1), 1., P)
		#return -np.log(M2)

	#elif x > 2.:
		#u2 = P/x
		#if P <= 2.:
			#u1 = P/2.
			#M1 = F4(2., P)

		#else:
			#u1 = 1.
			#M1 = 0.

		#return -np.arcsin(u2) + np.arcsin(u1) + M1



#P_space = np.linspace(1., 2., 100)
#for i in range(100):
	#print(i)
	#rho_min = raiz(P_space[i])
	#rho_space = np.linspace(rho_min, 2., 100)
	#for j in range(100):
		#matriz1[i, j] = F4(rho_space[j], P_space[i])
		#matriz2[i, j] = F(rho_space[j], P_space[i])


#rho_space   = np.linspace(0., 2., 100)
#for i in range(100):
	#print(i)
	#rho = rho_space[i]
	#alpha_min = np.arcsin(1./r(rho))
	#alpha_space = np.linspace(alpha_min, np.pi/2., 100)
	#for j in range(100):
		#alpha = alpha_space[j]
		#P = np.sin(alpha)*r(rho)
		##matriz1[i, j] = F4(rho, P)
		#matriz2[i, j] = F(rho, P)
		#matriz3[i, j] = F2(2., rho, P)


#fraction = matriz1/matriz2

#plt.imshow(fraction, vmin = 0.9, vmax = 1.1)





#rho1, rho2, N = .01, 1.1, 10000

#P_space = np.linspace(0.0, min(r(rho1), r(rho2)), N)
#I_space1 = np.zeros(N)
#I_space2 = np.zeros(N)
#I_space3 = np.zeros(N)

#for i in range(N):
	#print(i)
	#I_space1[i] = I(P_space[i], rho1, rho2, F)
	#I_space2[i] = I(P_space[i], rho1, rho2, F6)
	#I_space3[i] = I(P_space[i], rho1, rho2, F5)
	
#fig, axs = plt.subplots(ncols = 2, nrows = 1, sharex = "all")
#axs[0].plot(P_space, I_space1)
#axs[0].plot(P_space, I_space2)
#axs[0].plot(P_space, I_space3)
#axs[0].legend(["F", "F6", "F5"])

#axs[1].plot(P_space, np.log10(abs(I_space2 - I_space1)))
#axs[1].plot(P_space, np.log10(abs(I_space3 - I_space1)))
#axs[1].legend(["F6", "F5"])

#def test_I(N, F_test, upper = True):
	#rho_space1 = np.random.random(N)*0.1
	#rho_space2 = np.random.random(N)*0.1
	#I_space0     = np.zeros(N)
	#I_space_test = np.zeros(N)
	#P_space      = np.zeros(N)
	#for i in range(N):
		#if np.mod(i, 100) == 0:
			#print(i)
		#rho1, rho2 = rho_space1[i], rho_space2[i]
		#if upper:
			#P = 1. + np.random.random()*(min(r(rho1), r(rho2)) - 1.)
		#else:
			#P = np.random.random()
		#P_space[i] = P
		##print(rho1, rho2, P)
		#I_space0[i] = I(P, rho1, rho2, F)
		#I_space_test[i] = I(P, rho1, rho2, F_test)
		
	#return rho_space1, rho_space2, P_space, I_space0, I_space_test

#rho_space1, rho_space2, P_space, I_space0, I_space_test = test_I(10000, F5, upper = True)


#def test_I2(N):
	#rho_space1 = np.random.random(N)*2.
	#rho_space2 = np.random.random(N)*2.
	#P_space    = np.zeros(N)
	#I_space0   = np.zeros(N)
	#I_space1   = np.zeros(N)
	#for i in range(N):
		#if N%100 == 0: print(i, "of ", N)
		#rho1, rho2 = rho_space1[i], rho_space2[i]
		#P = np.random.random()*min(r(rho1), r(rho2))
		#P_space[i] = P
		
		#I_space0[i] = I(P, rho1, rho2, F)
		#I_space1[i] = I(P, rho1, rho2, F6)
		
	#angles0, distances0, solutions0 = modulo.geodesica(rho_space1, rho_space2, I_space0)
	#angles1, distances1, solutions1 = modulo.geodesica(rho_space1, rho_space2, I_space1)
	
	#geo1 = np.arange(0, N*4, 4) + 0
	
	#alpha0 = angles0[geo1]
	#alpha1 = angles1[geo1]
	
	#P_sol0 = np.zeros(N)
	#P_sol1 = np.zeros(N)

	#for i in range(N):
		#P_sol0[i] = r(rho_space1[i])*np.sin(alpha0[i])
		#P_sol1[i] = r(rho_space1[i])*np.sin(alpha1[i])

	#result = {"rho1": rho_space1, "rho2": rho_space2, "P": P_space, "P0": P_sol0, "P1": P_sol1, "phi0": I_space0, "phi1": I_space1}
	#return result



def generate_solutions():
	N = 1000000
	np.random.seed(1)
	rho_space1 = (np.random.random(N) - 0.)*3.
	rho_space2 = (np.random.random(N) - 0.)*3.
	alpha_space= np.zeros(N)
	P_space    = np.zeros(N)
	phi_space  = np.zeros(N)

	for i in range(N):
		if i%100 == 0: print(i, "of ", N)
		rho1, rho2 = rho_space1[i], rho_space2[i]

		P_max = min(r(rho1), r(rho2))
		if rho1*rho2 < 0.:
			P_max = 1.
		
		alpha_max = np.arcsin(P_max/r(rho1))
		if rho2 <= rho1: 
			alpha_space[i] = np.random.random()*alpha_max
		else:
			alpha_space[i] = np.pi/2. + np.random.random()*np.pi/2.
		P = np.sin(alpha_space[i])*r(rho1)
		P_space[i] = P
		
		phi_space[i] = I(P, rho1, rho2, F)

	result = {"rho1": rho_space1, "rho2": rho_space2, "P": P_space, "phi": phi_space, "alpha": alpha_space}
	np.save("Exact_solutions", result)

def test_alpha():
	data = np.load("Exact_solutions.npy", allow_pickle = True).item()

	rho1, rho2, P, phi, alpha = data["rho1"], data["rho2"], data["P"], data["phi"], data["alpha"]

	N = len(rho1)
	F1 = modulo.geodesica_F(rho1, P)
	F2 = modulo.geodesica_F(rho2, P)
	phi_approx = abs(F1 - F2)

	tiempo0 = time.time()
	angles0, distances0, solutions0 = modulo.geodesica(rho1, rho2, phi)
	tiempo1 = time.time()
	delta   = tiempo1 - tiempo0
	
	angles1, distances1, solutions1 = modulo.geodesica(rho1, rho2, phi_approx)
	
	geo1 = np.arange(0, N*4, 4) + 0
	
	alpha0 = angles0[geo1]
	alpha1 = angles1[geo1]
	
	P_sol0 = np.zeros(N)
	P_sol1 = np.zeros(N)

	for i in range(N):
		P_sol0[i] = r(rho1[i])*np.sin(alpha0[i])
		P_sol1[i] = r(rho1[i])*np.sin(alpha1[i])

	error = abs(alpha - alpha0)*180./np.pi
	result = {"rho1": rho1, "rho2": rho2, "P": P, "P_found": P_sol0, "P_found2": P_sol1, "phi": phi, "phi_approx": phi_approx, "alpha": alpha, "alpha_found": alpha0, "alpha_found2": alpha1, "error": error}
	
	mask_pi = phi <= np.pi
	region1 = P <  1.
	region2 = P >= 1.
	archivo = open("log_errors.txt", "a")
	
	mask      = (error >= 0.03)*(phi <= np.pi)*(phi >= 0.01)*mask_pi
	mask_crit = (error >=  0.1)*(phi <= np.pi)*(phi >= 0.01)*mask_pi
	archivo.write(str(error[mask_pi].mean()) + " " + str(10**(np.log10(error[mask_pi]).mean())) + " " + str(10**(np.log10(error[mask_pi*region1]).mean())) + " " + str(10**(np.log10(error[mask_pi*region2]).mean())) + " " + str(np.sum(mask*region1)) + " " + str(np.sum(mask*region2)) + " " + str(np.sum(mask_crit*region1)) + " " + str(np.sum(mask_crit*region2)) + " " + str(1000000.*delta/len(rho1)) + " " + str(N_P*N_rho*2) + "\n")
	archivo.close()
	return result

result = test_alpha()







def test_2(rho1, rho2):
	N = 50000
	phi = np.linspace(0., np.pi, N)

	ones = np.ones(N)
	angles0, distances0, solutions0 = modulo.geodesica(rho1*ones, rho2*ones, phi)
	
	geo1 = np.arange(0, N*4, 4) + 0
	
	alpha = angles0[geo1]
	dists = distances0[geo1]
	P_sol = np.zeros(N)

	for i in range(N):
		P_sol[i] = r(rho1)*np.sin(alpha[i])

	archivo = open("continuity_test.txt", "a")
	archivo.write(str(rho1) + " " + str(rho2) + " " + str(N_P*N_rho*2) + " ")
	for i in range(N):
		archivo.write(str(alpha[i]) + " ")
	archivo.write("\n")

	archivo.write(str(rho1) + " " + str(rho2) + " " + str(N_P*N_rho*2) + " ")
	for i in range(N):
		archivo.write(str(dists[i]) + " ")
	archivo.write("\n")

	archivo.close()
	fig, axs = plt.subplots(ncols = 2, nrows = 1)
	axs[0].plot(phi, alpha)
	axs[1].plot(phi, dists)




def plot_IG(rho1, rho2, T_value = False):
	P_max = min(r(rho1), r(rho2))
	N = 100000
	P_space1 = np.linspace(1., P_max, N)
	P_space2 = np.linspace(1., P_max, N)
	ones     = np.ones(N)

	if T_value:
		I_array = modulo.geodesica_I_T(P_space1, rho1*ones, rho2*ones)
		G_array = modulo.geodesica_G_T(P_space2, rho1*ones, rho2*ones)
	else:
		I_array = modulo.geodesica_I(P_space1, rho1*ones, rho2*ones)
		G_array = modulo.geodesica_G(P_space2, rho1*ones, rho2*ones)

	
	plt.plot(P_space1, I_array)
	plt.plot(P_space2, G_array)


def det_distance(rho1, rho2, phi):
	result = modulo.geodesica([rho1], [rho2], [phi])
	P = r(rho1)*np.sin(result[0][0])
	print("P = %f", P)
	if result[2][0] == 1:
		print("I type")
		dist_approx = modulo.geodesica_I_T([P], [rho1], [rho2])
	if result[2][0] == 2:
		print("G type")
		dist_approx = modulo.geodesica_G_T([P], [rho1], [rho2])
	return result[1][0], dist_approx[0], abs(result[1][0] - dist_approx[0])




def test_F(N, F, F4):
	rho_space = np.linspace(0., 0.2, N)
	drho = rho_space[1] - rho_space[0]
	
	rho_space[0] = drho/10.

	rango = [0.0, .05]
	w_space = np.linspace(rango[0], rango[1], N)
	dw = w_space[1] - w_space[0]
	w_space[0]  += dw/10.
	w_space[-1] += -dw/10.
	
	array_test = np.zeros([N, N])
	rho_points = np.linspace(0., 4., N_rho)
	w_points   = np.linspace(0., 1., N_P)
	
	for i in range(N):
		rho = rho_space[i]
		P_max = r(rho)
		P_space = 1. + w_space*(P_max - 1.)
		for j in range(N):
			P = P_space[j]
			array_test[i, j] = F(rho, P)/F4(rho, P)

	plt.imshow(array_test, vmin = 0.99, vmax = 1.01)
	return array_test

def test_F2(N, F4):
	rho_space = np.linspace(0., 1., N)
	P_space   = np.linspace(0., 1., N)

	
	array_test = np.zeros([N, N])
	
	for i in range(N):
		rho = rho_space[i]

		for j in range(N):
			P = P_space[j]
			array_test[i, j] = F(rho, P)/F4(rho, P)

	plt.imshow(array_test, vmin = 0.999, vmax = 1.001)
	return array_test


def plot_diff(P_space, P_sol0, P_sol1):
	diff0 = abs(P_sol0 - P_space)
	diff1 = abs(P_sol1 - P_space)
	plt.plot(P_space, np.log10(diff0), ".")
	plt.plot(P_space, np.log10(diff1), ".")


def det_mask(result, criteria = 0.03):
	diff = abs(result["alpha"] - result["alpha_found"])*180./np.pi
	mask = (diff >= criteria)*(result["phi"] <= np.pi)*(result["phi"] >= 0.01)*(result["rho1"] <= 2.0)*(result["rho2"] <= 2.0)
	return mask
	
mask = det_mask(result)
mask_crit = det_mask(result, 0.1)*(result["P"] >= 1.)
plt.plot(result["P"][mask], -result["rho1"][mask], ".")
plt.plot(result["P"][mask_crit], -result["rho1"][mask_crit], ".")


mask_pi = (result["phi"] <= np.pi)*(result["phi"] >= 0.01)

diff = np.log10(abs(result["alpha"] - result["alpha_found"])*180./np.pi)



def zoom(result, mask, i):
	N = 10000
	rho1 = result["rho1"][mask][i]
	rho2 = result["rho2"][mask][i]
	P    = result["P"][mask][i]
	phi  = result["phi"][mask][i]
	P_found = result["P_found"][mask][i]
	ones = np.ones(N)
	#print(P, rho1, rho2, " Error: ", abs(result["alpha"][mask][i] - result["alpha_found"][mask][i])*180./np.pi, " Criteria: ", 0.03)
	P_max = min(r(rho1), r(rho2))
	
	P_space1 = np.linspace(0., P_max, N)
	P_space2 = np.linspace(0., 1.,    N)
	P_space3 = np.linspace(1., P_max, N)
	I_space1 = np.zeros(N)
	I_space2 = np.zeros(N)
	I_space3 = np.zeros(N)
	I_space4 = np.zeros(N)
	I_space5 = np.zeros(N)
	for i in range(N):
		I_space1[i] = I(P_space1[i], rho1, rho2, F6)
		I_space2[i] = I(P_space2[i], rho1, rho2, F6)
		I_space3[i] = I(P_space3[i], rho1, rho2, F6)
		I_space4[i] = I(P_space1[i], rho1, rho2, F)
		I_space5[i] = I(P_space1[i], rho1, rho2, F4)
	
	fig, axs = plt.subplots(ncols = 3, nrows = 2)
	axs[0, 0].plot(P_space1, I_space1)
	axs[0, 1].plot(P_space2, I_space2)
	axs[0, 2].plot(P_space3, I_space3)
	axs[0, 0].plot(P_space1, I_space4, linestyle = "dashed")
	axs[0, 0].plot(P_space1, I_space5, linestyle = "dashed")
	
	ymin1 = I_space1.min()
	ymin2 = I_space2.min()
	ymin3 = I_space3.min()
	
	ymax1 = I_space1.max()
	ymax2 = I_space1.max()
	ymax3 = I_space1.max()
	
	axs[0, 0].plot(P_space1, phi*ones, linestyle = "dashed", color = "k")
	axs[0, 1].plot(P_space2, phi*ones, linestyle = "dashed", color = "k")
	axs[0, 2].plot(P_space3, phi*ones, linestyle = "dashed", color = "k")
	
	axs[0, 0].vlines(x = P, ymin = ymin1, ymax = ymax1, linestyle = "dashed", color = "k")
	axs[0, 1].vlines(x = P, ymin = ymin2, ymax = ymax2, linestyle = "dashed", color = "k")
	axs[0, 2].vlines(x = P, ymin = ymin3, ymax = ymax3, linestyle = "dashed", color = "k")

	axs[0, 0].vlines(x = P_found, ymin = ymin1, ymax = ymax1, linestyle = "dashed", color = "r")
	axs[0, 1].vlines(x = P_found, ymin = ymin2, ymax = ymax2, linestyle = "dashed", color = "r")
	axs[0, 2].vlines(x = P_found, ymin = ymin3, ymax = ymax3, linestyle = "dashed", color = "r")

	axs[1, 0].plot(P_space1, np.exp(-phi)*ones, linestyle = "dashed", color = "k")
	axs[1, 1].plot(P_space2, np.exp(-phi)*ones, linestyle = "dashed", color = "k")
	axs[1, 2].plot(P_space3, np.exp(-phi)*ones, linestyle = "dashed", color = "k")
	
	ymax1 = np.exp(-ymin1)
	ymax2 = np.exp(-ymin2)
	ymax3 = np.exp(-ymin3)
	
	ymin1 = 0.
	ymin2 = 0.
	ymin3 = 0.
	axs[1, 0].vlines(x = P, ymin = ymin1, ymax = ymax1, linestyle = "dashed", color = "k")
	axs[1, 1].vlines(x = P, ymin = ymin2, ymax = ymax2, linestyle = "dashed", color = "k")
	axs[1, 2].vlines(x = P, ymin = ymin3, ymax = ymax3, linestyle = "dashed", color = "k")

	axs[1, 0].vlines(x = P_found, ymin = ymin1, ymax = ymax1, linestyle = "dashed", color = "r")
	axs[1, 1].vlines(x = P_found, ymin = ymin2, ymax = ymax2, linestyle = "dashed", color = "r")
	axs[1, 2].vlines(x = P_found, ymin = ymin3, ymax = ymax3, linestyle = "dashed", color = "r")

	axs[1, 0].plot(P_space1, np.exp(-I_space1))
	axs[1, 1].plot(P_space2, np.exp(-I_space2))
	axs[1, 2].plot(P_space3, np.exp(-I_space3))
	axs[1, 0].plot(P_space1, np.exp(-I_space4), linestyle = "dashed")


def show_parameters(result, mask, i):
	print("rho1 = %f, rho2 = %f, phi = %f, alpha0 = %f, alpha1 = %f", result["rho1"][mask][i], result["rho2"][mask][i], result["phi"][mask][i], result["alpha"][mask][i], result["alpha_found"][mask][i])

modulo.toggle_debug()


idx = 0

mask0 = mask1
zoom(result, mask0, idx)


rho1, rho2, phi, P = result["rho1"][mask0][idx], result["rho2"][mask0][idx], result["phi"][mask0][idx], result["P"][mask0][idx]
#show_parameters(result, mask0, idx)

asdasd = modulo.geodesica([rho1], [rho2], [phi])

delta_rho = abs(result["rho1"] - result["rho2"])
phi = result["phi0"]


#diff = abs(I_space0 - I_space_test)
#diff_rel = abs(I_space0 - I_space_test)/abs(I_space0)

#def plot_hist(diff):
	#plt.hist(diff, 100, range = [np.percentile(diff, 5.), np.percentile(diff, 95.)])

#plot_hist(diff_rel)

#def plot_mask(diff):
	#mask = diff >= np.percentile(diff, 90.)

	#plt.plot(P_space, -rho_space1, ".")
	#plt.plot(P_space[mask], -rho_space1[mask], ".")

#plot_mask(diff_rel)
