
extern void geodesica_dibujador_objeto3(float[4], float[4][4], struct object_data, float *, unsigned char); 

extern void geodesica_triangulador(struct object_data, float *, float *);

extern void geodesica_evolucionador(struct particula_data);

extern void geodesica_inicializador2();

extern void geodesica_exit();

extern const unsigned int N_GEO;

extern float light_speed;

extern float metrica_r(float);

extern float metrica_r_d(float);

extern float metrica_B_d(float);

extern float metrica_A(float);

extern float metrica_A_d(float);

extern float metrica_B(float);

extern float metrica_metric(float, float, int);

extern unsigned int n_atrib, v_info, v_info2;

extern void geodesica_geodesica4(float[3], float[3], float *, float *, float *, float *);

extern void metrica_inicializador();



// extern void draw_vertex(float [4], float *, float *, float, float, unsigned int, unsigned int, float, unsigned int, unsigned int *, unsigned int, GLuint, unsigned int, float *, float *);

extern void draw_vertex(float [4], float *, float *, float, float, unsigned int, unsigned int, unsigned int, unsigned int *, unsigned int, GLuint, unsigned int, float *, float *);
//             draw_vertex(float [4], float *, float *, float, unsigned int, unsigned int, float, unsigned int, unsigned int *, unsigned int, GLuint, unsigned int, float *, float *)
extern void load_and_compile(unsigned int *, unsigned int *, unsigned int *);

extern void volcado_test(int);

extern void geodesica_benchmark_F(unsigned int);

extern void load_background_arrays();

extern void render_background(float [4],  float *, float *, unsigned int, unsigned int, unsigned int, float, float, unsigned int *, unsigned int, unsigned int);
// extern void geodesica_volcado(struct object_data, float[4]);
