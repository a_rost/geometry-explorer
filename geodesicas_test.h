
extern void geodesica_dibujador_objeto3(float[4], float[4][4], struct object_data, float *, unsigned char); 

extern void geodesica_triangulador(struct object_data, float *, float *);

extern void geodesica_triangulador_shaders(struct object_data, float *, float *);

extern void geodesica_evolucionador(struct particula_data);

extern void geodesica_dibujador_puntos(float[4], float[4][4], struct particula_data, float *);

//extern void geodesica_cambio_base(float[4], float[3], float[4], unsigned int);

//extern void geodesica_ray_tracer(float[4], float[4], unsigned int, float[4][4], float[4], unsigned char*);

extern void geodesica_inicializador2();

extern void geodesica_exit();
//extern float geodesica_F(float, float);
extern const unsigned int N_GEO;

extern float light_speed;

extern float metrica_r(float);

extern float metrica_r_d(float);

extern float metrica_B_d(float);

extern float metrica_A(float);

extern float metrica_A_d(float);

extern float metrica_B(float);

extern float geodesica_F(float, float);

extern float geodesica_T(float, float);

extern float geodesica_I(float, float, float);

extern float geodesica_I_T(float, float, float);

extern float geodesica_G_T(float, float, float);

extern float approx_F(float, float);

extern float geodesica_G(float, float, float);

extern void toggle_debug();

extern float metrica_metric(float, float, int);

extern unsigned int n_atrib, n_atrib_shading, v_info, v_info2;

extern void geodesica_geodesica4(float[3], float[3], float *, float *, float *, float *);

extern void geodesica_geodesica5(float[4], float[3], float *, float *, float *);

extern void metrica_inicializador();

