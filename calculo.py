import numpy as np
import matplotlib.pyplot as plt
import metrica
plt.ion()


def integral(a, b, h):
	#calculates the integral of 1/sqrt(a*x + b*x**2) from 0 to h
	u1, u2 = 1., 1. + 2*h*b/a
	if b >= 0.:
		return (np.arccosh(u2) - np.arccosh(u1))/np.sqrt(b)
	else:
		return -(np.arcsin(u2) - np.arcsin(u1))/np.sqrt(-b)

def integral2(a, b, c, x):
	#calculates the integral of 1/sqrt(a + b*x + c*x**2) from 0 to h, assuming c > 0
	if c == 0.:
		return 2./b*np.sqrt(a + b*x)
	else:
		return np.log(2*np.sqrt(c)*np.sqrt(a + x*(b + c*x)) + b + 2*c*x)/np.sqrt(c)

def argumento(x, P):
	return metrica.r(x)**4/P**2 - metrica.r(x)**2

def argumento_p(x, P):
	return (4*metrica.r(x)**3/P**2 - 2*metrica.r(x))*metrica.r_p(x)

def argumento_p_p(x, P):
	return (4*metrica.r(x)**3/P**2 - 2*metrica.r(x))*metrica.r_p_p(x) + (12*metrica.r(x)**2/P**2 - 2)*metrica.r_p(x)**2


def aproximacion_final1(P, h):
	root = metrica.raiz(P)
	a = argumento_p(root, P)
	b = argumento_p_p(root, P)
	return integral(a, b, h)

def aproximacion_final2(P, rho0, h):
	a = argumento(rho0, P)
	b = argumento_p(rho0, P)
	c = argumento_p_p(rho0, P)/2.
	return integral2(a, b, c, rho0 + h) - integral2(a, b, c, rho0)


def integral_S(x, P):
	root = np.sqrt(x*x - P*P)
	return root + P*np.arctan(P/root)


def S3(rho, P):
	if rho < 0.:
		return -S(-rho, P)

	elif rho >= 2.:
		if P >= h:
			rho_min = raiz(P)
			return integral_S(rho, P) - integral_S(rho_min, P)
		else:
			valor = interpolador_lineal(P, array_S2)
			return valor + integral_S(rho, P) - integral_S(2., P)
	else:
		if P >= h:
			rho_min = raiz(P)
			if rho - rho_min <= 0.05:
				a = -U_p(rho_min, P)
				H = abs(rho - rho_min)
				return np.sqrt(a)*2/3*np.power(H, 3./2)
			else:
				return metrica.interpolador(array_S, 2./399, 2./1599, 0., 0., rho, P)
		else:
			return metrica.interpolador(array_S, 2./399, 2./1599, 0., 0., rho, P)




def interpolador_lineal(P, array, debug = False):
	dP = 2./2000
	i = int(P/dP)
	
	if(i >= 2001):
		i = 2000
	elif(i < 0):
		i = 0
	f1 = array[i]
	f2 = array[i + 1]
	if debug:
		print("i = ", i, "f1 = ", f1, "f2 = ", f2)
	return (f2 - f1)/dP*(P - i*dP) + f1


def interpolador_lineal2(array, x1, x2, N, x):
	dx = (x2 - x1)/(N - 1)
	i = int((x - x1)/dx)
	
	if(i >= N):
		i = N - 1
	elif(i < 0):
		i = 0

	f1 = array[i]
	f2 = array[i + 1]
	return (f2 - f1)/dx*(x - i*dx - x1) + f1
	
	
def exact(x, P):
	v = -P/x
	return np.arcsin(v)


P_res   = 1000
rho_res = 1000

def calculador():
	global array_exp1, array_exp2, array_edge1, array_edge2
	F_array= np.zeros([rho_res, P_res])
	for i in range(rho_res):
		x = 0.0 + i/(rho_res - 1.)*2.
		print(i)
		for j in range(P_res):
			P = 0.0 + j/(P_res - 1)*1
			if j == P_res - 1:
				P = 0.99999999
				#P = 1.
			F_array[i, j] = metrica.F2(2., x, P)

	array_exp1 = np.exp(F_array)


	array_edge1 = np.zeros(P_res)
	for i in range(P_res):
		P = 1./(P_res - 1)*i
		array_edge1[i] = metrica.F(2., P)
	array_edge1 = np.exp(-array_edge1)
	plt.plot(array_edge1)



	F_array= np.zeros([rho_res, P_res])
	for i in range(rho_res):
		x = 2./(rho_res - 1.)*i
		if i == 0:
			x = 0.000000001
		P1, P2 = 1., metrica.r(x)
		d_P4 = (P2 - P1)/(P_res - 1)
		print("x = ", x, "i = ", i)
		for j in range(P_res):
			P = d_P4*j + P1
			if j == P_res - 1:
				P = P2 - 0.0000000001
			elif j == 0:
				P = 1.0000000001
			F_array[i, j] = metrica.F2(2., x, P)

	#F_array= np.zeros([rho_res, P_res])
	#P1, P2 = 1., 2.
	#for j in range(P_res):
		#print("j = ", j)
		#P = P1 + (P2 - P1)*j/(P_res - 1)
		#rho_min = metrica.raiz(P)
		
		#rho1, rho2 = rho_min, 2.
		#for i in range(rho_res):
			#x = rho1 + (rho2 - rho1)/(rho_res - 1.)*i
			#F_array[i, j] = metrica.F2(2., x, P)

	array_exp2 = np.exp(F_array)
	plt.imshow(array_exp2)

	array_edge2 = np.zeros(P_res)
	for i in range(P_res):
		P = 1 + 1./(P_res - 1)*i
		array_edge2[i] = metrica.F(2., P)
	array_edge2 = np.exp(-array_edge2)
	plt.plot(array_edge2)




def guardador():
	archivo = open("data/worm/array_exp1.dat", "w")
	for i in range(P_res):
		for j in range(rho_res):
			archivo.write(str(array_exp1[i][j]) + "\n")
	archivo.close()

	archivo = open("data/worm/array_exp2.dat", "w")
	for i in range(P_res):
		for j in range(rho_res):
			archivo.write(str(array_exp2[i][j]) + "\n")
	archivo.close()

	archivo = open("data/worm/array_edge1.dat", "w")
	for i in range(P_res):
		archivo.write(str(array_edge1[i]) + "\n")
	archivo.close()

	archivo = open("data/worm/array_edge2.dat", "w")
	for i in range(P_res):
		archivo.write(str(array_edge2[i]) + "\n")
	archivo.close()

def calculador_S():
	global array_S, array_S2
	N_rho = metrica.N_rho
	N_P   = metrica.N_P
	array_S  = np.zeros([N_rho, N_P])
	d_rho2 = 2./(N_rho - 1)
	d_P2   = 2./(N_P - 1)
	for j in range(N_P):
		P = d_P2*j
		print("P = ", P, "j = ", j)
		for i in range(N_rho):
			x = 0. + d_rho2*i
			array_S[i][j] = metrica.S(x, P)


	archivo = open("data/worm/S_array1.dat", "w")
	for i in range(N_rho):
		for j in range(N_P):
			archivo.write(str(array_S[i][j]) + "\n")
	archivo.close()
	plt.imshow(array_S)

	array_S2 = np.zeros(1000)
	x = metrica.T
	for j in range(1000):
		P = 2./999*j
		array_S2[j] = metrica.S(x, P)

	archivo = open("data/worm/S_array2.dat", "w")
	for i in range(1000):
		archivo.write(str(array_S2[i]) + "\n")
	archivo.close()


def F4(x, P, debug = False):
	root = metrica.raiz(P)

	if x < 0.:
		return -F4(-x, P)
	elif P == 0.:
		return 0.

	elif P == metrica.h:
		return 100.
		
	elif P == metrica.r(x):
		return 0.

	elif P <= metrica.h and x <= 2.:
		if debug:
			print("Zone 1")
		#Zone 1
		valor0  = interpolador_lineal2(array_edge1, 0., 1., P_res, P)
		if x < 2.:
			valor  = metrica.interpolador(array_exp1, 2./(rho_res - 1), 1./(P_res - 1), 0., 0., x, P)
			return -np.log(valor0) + np.log(valor)
		else:
			return -np.log(valor0)

	elif P > metrica.h and abs(x - root) <= 0.01:
		return aproximacion_final1(P, abs(x - root))
	elif P > metrica.h and x <= 2.:
		if debug:
			print("Zone 2")
		P1, P2 = 1., metrica.r(x)
		u  = (P - P1)/(P2 - P1)
		valor0 = interpolador_lineal2(array_edge2, 1., 2., P_res, P)
		if x < 2.:
			valor = metrica.interpolador(array_exp2, 2./(rho_res - 1), 1./(P_res - 1), 0., 0., x, u)
			print(valor)
			return -np.log(valor0) + np.log(valor)
		else:
			return -np.log(valor0)

	elif x > 2. and P < 2.:
		valor0 = F4(2., P)
		return valor0 + exact(x, P) - exact(2., P)

	elif P >= 2. and x >= 2.:
		if debug:
			print("Zone 4")
		return exact(x, P) - exact(root, P)
	else:
		print("error")



array_exp1, array_exp2, array_edge1, array_edge2 = 0., 0., 0., 0.

def cargador():
	global array_exp1, array_exp2, array_edge1, array_edge2
	
	array = np.genfromtxt("data/worm/array_exp1.dat")
	array_exp1 = np.reshape(array, [rho_res, P_res])

	array = np.genfromtxt("data/worm/array_exp2.dat")
	array_exp2 = np.reshape(array, [rho_res, P_res])

	array_edge1 = np.genfromtxt("data/worm/array_edge1.dat")
	array_edge2 = np.genfromtxt("data/worm/array_edge2.dat")
	
	
	

def test_F(Nrho = 100, NP  = 200, F = F4):
	rho_space = np.linspace(0., .5, Nrho)
	u_space   = np.linspace(0., 1., NP)

	result = np.zeros([Nrho, NP])
	for i in range(Nrho):
		for j in range(NP):
			P_max = r(rho_space[i])
			#print(rho_space[i], P_space[j])
			result[i, j] = F(rho_space[i], 1. + u_space[j]*(P_max - 1.))
	return result

def test_F2(rho1, rho2, N = 1000, F0 = metrica.F, F2 = F4, printdiff = True):
	P1, P2 = 0., min(r(rho1), r(rho2))
	
	P_space = np.linspace(P1, P2, N)
	array1 = np.zeros(N)
	array2 = np.zeros(N)
	for i in range(N):
		P = P_space[i]
		array1[i] = F0(rho2, P) - F0(rho1, P)
		array2[i] = F2(rho2, P) - F2(rho1, P)
	if printdiff:
		plt.plot(P_space, (array2 - array1)/array1)
	else:
		plt.plot(P_space, array1)
		plt.plot(P_space, array2)



def compare(F0, F1, F2, rho, P):
	f0 = F0(rho, P)
	f1 = F1(rho, P)
	f2 = F2(rho, P)
	print("|F1 - F0| = ", abs(f1 - f0)[0])
	print("|F2 - F0| = ", abs(f2 - f0))
	print("abs err1 = ", abs(f1 - f0)[0]/f0)
	print("abs err2 = ", abs(f2 - f0)/f0)


#cargador()

calculador()
guardador()
calculador_S()
