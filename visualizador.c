#include "stb.h"
#define pi (3.141592653f)
#include <GL/glew.h>
#include <GL/freeglut.h>   // freeglut.h might be a better alternative, if available.
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

#include "structs.h"
#include "geodesicas.h"
#include "models.h"
#include "camara.h"


#define resolution_x (1500)
#define resolution_y (1000)

float posicion[4], light_position[3], **vertices_tot, **triangulos, aspect_ratio, field_of_view = 130.f, pix_size;

struct object_data planeta[50];

double delta_time = 0.;
unsigned int shaderProgram1, shaderProgram2, shaderProgram3, n_planetas, texture[10], num_vert_tot = 0, num_triang_tot = 0, VAO[2];
GLuint *VBO, ssbo, *input;





void visualizador(float *restrict posicion, float vectores[4][4], unsigned char init){
	invert_vectors(posicion);

	for(unsigned int k = 0; k < n_planetas; k++){
		geodesica_dibujador_objeto3(posicion, vectores, planeta[k], vertices_tot[k], init);
		geodesica_triangulador(planeta[k], vertices_tot[k], triangulos[k]);
	}
}








void mouseMove(int mx, int my) {
	int mx_diff = mx - mx_anterior;
	int my_diff = my - my_anterior;
    
	float modulo = sqrtf(powf(mx_diff, 2) + powf(my_diff, 2));
	camara_w[0] = 0.f;
	camara_w[1] = my_diff/modulo;
	camara_w[2] = -mx_diff/modulo;
    
	if(modulo == 0.f){
		camara_w[1] = 0.f;
		camara_w[2] = 1.f;
	}
    
	camara_angulo = powf(modulo, 1.3)*0.01f;
	mx_anterior = mx;
	my_anterior = my;

	if(mx < 30)
		glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) - 30, my);
	else if(mx > glutGet(GLUT_WINDOW_WIDTH) - 30)
		glutWarpPointer(30, my);

	if(my < 30)
		glutWarpPointer(mx, glutGet(GLUT_WINDOW_HEIGHT) - 30);
	else if(my > glutGet(GLUT_WINDOW_HEIGHT) - 30)
		glutWarpPointer(mx, 30);

	mx_anterior = mx;
	my_anterior = my;
}


void display_performance(){
	printf("P. time per vertex = %lf us, fps = %lf\n", delta_time*1000000.f/(num_vert_tot), 1.f/delta_time);
}

void volcado_debug(float *vertex){
	FILE *pfout;
	float vertices[3], uv[2];
	pfout = fopen("volcado/result_vertex.dat", "w");
	
	struct object_data objeto = planeta[0];
	fprintf(pfout, "%f %f %f\n", posicion[1], posicion[2], posicion[3]);
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertices[0] = objeto.vertices[3*i + 0];
		vertices[1] = objeto.vertices[3*i + 1];
		vertices[2] = objeto.vertices[3*i + 2];
		
		uv[0]       = objeto.u[i];
		uv[1]       = objeto.v[i];
		
		for(unsigned int k = 0; k < N_GEO; k++){
			fprintf(pfout, "%f %f %f %f %f ", vertices[0], vertices[1], vertices[2], uv[0], uv[1]);
			for(unsigned int j = 0; j < v_info; j++)
				fprintf(pfout, "%f ", vertex[v_info*(4*i + k) + j]);
			fprintf(pfout, "\n");
		}
	}
	fclose(pfout);
	
}


void save_object(){
	int i;
	FILE *pfout;
	pfout = fopen("volcado/vertex_volcado.txt", "w");
	
	fprintf(pfout, "%d\n", planeta[0].n_triang);
	
	fprintf(pfout, "%f %f %f %f\n", posicion[0], posicion[1], posicion[2], posicion[3]);
	for(i = 0; i < 4; i++)
		fprintf(pfout, "%f %f %f %f\n", vectores[i][0], vectores[i][1], vectores[i][2], vectores[i][3]);
	
	for(i = 0; i < 4; i++)
		fprintf(pfout, "%f %f %f %f\n", vectores_inv[i][0], vectores_inv[i][1], vectores_inv[i][2], vectores_inv[i][3]);
	
	for(unsigned int i = 0; i < planeta[0].n_triang*n_atrib*3*N_GEO; i++)
		fprintf(pfout, "%f\n", triangulos[0][i]);
	fclose(pfout);
}

void keyboard(unsigned char key, int x, int y){
// 	extern int input[8];

	FILE *pfout;
	switch(key) {
		case 'w':
			//accelerates towards x+ 
			input[0] = 1;
			break;

		case 's':
			//accelerates towards x- 
			input[1] = 1;
			break;

		case 'd':
			//accelerates towards y-
			input[3] = 1;
			break;

		case 'a':
			//accelerates towards y+
			input[2] = 1;
			break;

		case 't':
			//prints current coordinates of the camera
			printf("coordenadas (d, theta, phi) = (%f, %f, %f), vector = (%f, %f, %f), Speed = %f c\n", posicion[1], posicion[2], posicion[3], vectores[1][1], vectores[1][2],
			vectores[1][3], camara_get_velocity(posicion, cuadrivelocidad));
			break;

		case 'f':
			//prints performance test
			display_performance();
			break;

		case 'x':
			save_object();
			break;

		case 'g':
			volcado_debug(vertices_tot[0]);
			break;

		case 'q':
			//to exit the program
			delete_objects(&planeta[0], n_planetas);
			geodesica_exit();
			free(input);
			exit(0);

		default:
			break;
	}
}

static void RenderSceneCB() {
	double time1, time2;
	time1 = omp_get_wtime();
    
	glutKeyboardFunc(keyboard);
	glutPassiveMotionFunc(mouseMove);
  
	evolucionador(input, posicion, cuadrivelocidad, camara_w, camara_angulo);

	camara_angulo = 0.f;
    
	visualizador(posicion, vectores, 0);
    
	time2 = omp_get_wtime();

	delta_time = time2 - time1;
    
	glClearColor(0.f, 0.f, 0.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	unsigned int stride = 0;
	for(unsigned int i = 0; i < n_planetas; i++){
		if(planeta[i].n_triang > 0){
			render_background(posicion,  cam_matrix, cam_matrix_inv, shaderProgram2, resolution_x, resolution_y, aspect_ratio, pix_size, &texture[0], 3, 0);
// 			draw_vertex(posicion, cam_matrix, cam_matrix_inv, aspect_ratio, pix_size, resolution_x, resolution_y, shaderProgram1, &texture[0], planeta[i].texture, VBO[0], planeta[i].n_triang, vertices_tot[i], triangulos[i]);

			draw_vertex(posicion, cam_matrix, cam_matrix_inv, aspect_ratio, pix_size, resolution_x, resolution_y, shaderProgram1, &texture[0], planeta[i].texture, VBO[0], planeta[i].n_triang, vertices_tot[i], triangulos[i]);
			stride += planeta[i].n_vert;
		}
	}
	glutSwapBuffers();
}

void texture_loader(unsigned int idx, const char *file_name){
	int width, height, nrChannels;
	unsigned char *data = stbi_load(file_name, &width, &height, &nrChannels, 0); 
	glGenTextures(1, &texture[idx]);  
	printf("Textura numero %d\n", texture[idx]);
	glBindTexture(GL_TEXTURE_2D, texture[idx]);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);
}


void initialize_coords(float d0, float theta0, float phi0){
	input = (unsigned int *) malloc(8*sizeof(unsigned int));

	posicion[0] = 0.0f;
	posicion[1] = d0;
	posicion[2] = theta0;
	posicion[3] = phi0;

	float dt = 2.f;
	float dphi = dt*sqrtf(-christoffel(posicion, 1, 0, 0)/christoffel(posicion, 1, 3, 3));

	cuadrivelocidad[0] = dt;
	cuadrivelocidad[1] = 0.0f;
	cuadrivelocidad[2] = 0.f;
	cuadrivelocidad[3] = dphi;

	for(unsigned int i = 0; i < 4; i++){
		for(unsigned int j = 0; j < 4; j++)
			vectores[i][j] = 0.f;
	}

	vectores[1][1] = 1.f;
	vectores[2][2] = 1.f;
	vectores[3][3] = 1.f;

	cam_matrix     = (float *) malloc(4*4*sizeof(float));
	cam_matrix_inv = (float *) malloc(4*4*sizeof(float));
// 	normalizador(posicion, &cuadrivelocidad[0], vectores);
	float aux = sqrtf(-producto_punto(posicion, cuadrivelocidad, cuadrivelocidad));

	for(unsigned int i = 0; i < 4; i++)
		vectores[0][i] = cuadrivelocidad[i]*light_speed/aux; 

	for(unsigned int i = 0; i < 4; i++)
		cuadrivelocidad[i] = vectores[0][i]; 

	gram_schimdt(posicion);
}


int main(int argc, char *argv[]){
	unsigned int i, j;
	float modulo;
	FILE *archivo, *pfin;
	metrica_inicializador();
	geodesica_inicializador2();

	initialize_coords(0.783218f, 1.746714f, 5.420547f);
	
	

	unsigned int number;
	struct object_data *objetos;
	light_position[0] = posicion[1];
	light_position[1] = posicion[2];
	light_position[2] = posicion[3];
	
	objetos = model_escenario6(&number, light_position);
	printf("Escenario creado\n");

	for(unsigned int i = 0; i < number; i++)
		planeta[i] = objetos[i];

	n_planetas = number;
	unsigned int n_atrib_max = n_atrib;

	printf("planetas creados\n");
	vertices_tot = (float **) malloc(20*sizeof(float *));
	triangulos   = (float **) malloc(20*sizeof(float *));
	for(unsigned int i = 0; i < n_planetas; i++){
		vertices_tot[i] = (float *) malloc(v_info*N_GEO*planeta[i].n_vert*sizeof(float));
		triangulos[i]   = (float *) malloc(N_GEO*3*n_atrib_max*planeta[i].n_triang*sizeof(float));
	}

	for(unsigned int i = 0; i < n_planetas; i++){
		printf("planeta n = %d, n vert = %d, n triang = %d\n", i, planeta[i].n_vert, planeta[i].n_triang);
		num_triang_tot += planeta[i].n_triang;
		num_vert_tot   += planeta[i].n_vert;
	}
	printf("NUMERO DE TRIANGULOS = %d X2, NUMERO DE VERTICES = %d X2\n", num_triang_tot, num_vert_tot);

	//Preparing background########################################################################
	float *background_alphas, *background_distances;
	background_alphas    = (float *)malloc(500*500*sizeof(float));
	background_distances = (float *)malloc(500*500*sizeof(float));
	
	FILE *pfout = fopen("volcado/array_alphas.txt", "w");
	for(unsigned int i = 0; i < 500; i++){
		for(unsigned int j = 0; j < 500; j++)
			fprintf(pfout, "%f ", background_alphas[i*500 + j]);
		fprintf(pfout, "\n");
	}
	fclose(pfout);
	//########################################################################
	camara_print_vectors(vectores);
	camara_get_velocity(posicion, cuadrivelocidad);

	if(argc > 10){
	}
	else{
	visualizador(posicion, vectores, 1);

	printf("Primer mensaje, por llamar a glutInit, esperando segundo mensaje\n");
	glutInit(&argc, argv);
	printf("Segundo mensaje, glutInit llamado, esperando tercer mensaje\n");
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA); 
    glutInitWindowSize(resolution_x, resolution_y);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Geometry Explorer"); 

	aspect_ratio = resolution_x*1.f/resolution_y;
	pix_size = 2.f/sqrtf(powf(aspect_ratio, 2) + 1.f)*tan(field_of_view*pi/180.f/2.f)/(1.f*resolution_y);

	
	GLenum res = glewInit();
    if (res != GLEW_OK){
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return 1;
    }
	if (!GLEW_VERSION_2_1){
		printf("%s\n", glGetString(GL_VERSION));
	}
    printf("d = %f, theta = %f, phi 4 = %f\n", posicion[1], posicion[2], posicion[3]);

  	load_background_arrays();
  
    // carga de texturas

	texture_loader(0, "textures/f.jpg");
	texture_loader(1, "textures/planet8.jpg");
	texture_loader(2, "textures/planet3.jpg");
	texture_loader(3, "textures/a.jpg");
	texture_loader(4, "textures/planet9.jpg");
	texture_loader(5, "textures/planet7.jpg");
	texture_loader(6, "textures/moon.jpg");
	texture_loader(7, "textures/a.jpg");

	printf("TEXTURAS CARGADAS\n");
    
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_POINT_SPRITE);
	glutSetCursor(GLUT_CURSOR_NONE);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	load_and_compile(&shaderProgram1, &shaderProgram2, &shaderProgram3);
	
	
	VBO = (GLuint *) malloc(2*sizeof(GLuint));
    
    unsigned int maximo = 0, i_maximo = 0;
    for(unsigned int i = 0; i < number; i++){
        if(planeta[i].n_triang > maximo){
            maximo = planeta[i].n_triang;
            i_maximo = i;
        }
    }
    printf("maximo determinado = %d, cantidad total de triangulos = %d,  max geometry vertex = %d\n", maximo, planeta[0].n_triang, GL_MAX_GEOMETRY_OUTPUT_VERTICES); 
    
	glGenBuffers(1, &VBO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	unsigned int maximo2;
	maximo2 = N_GEO*maximo*3*n_atrib_max*sizeof(float);
	glBufferData(GL_ARRAY_BUFFER, maximo2, triangulos[i_maximo], GL_DYNAMIC_DRAW);

	
	glGenBuffers(1, &ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
	float *aux;
	aux = (float *) malloc(N_GEO*4*num_vert_tot*sizeof(float));
	glBufferData(GL_SHADER_STORAGE_BUFFER, N_GEO*4*num_vert_tot*sizeof(float), aux, GL_STATIC_DRAW);
	printf("Allocated %f mb on the video card\n", N_GEO*4*num_vert_tot*sizeof(float)/1000000.f);
	free(aux);
// 	model_load_shaderinfo(ssbo, planeta, number);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, ssbo);
//	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0); // unbind

    //armando buffers de vertices

    //iniciando animacion
    glutDisplayFunc(RenderSceneCB); 
    glutIdleFunc(RenderSceneCB); 
    
    glutMainLoop(); 
    glClear(GL_COLOR_BUFFER_BIT);
    glutSwapBuffers(); 


      
}
  return 0;

}
