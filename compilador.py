import os
import sys
#os.system("gcc visualizador_worm.c lib/stb_image.o lib/geodesicas_closed.o -Ofast -march=native -o programa.x -lm -lgomp -lGL -lglut -lGLEW -D_GLIBCXX_PARALLEL -unroll-loops -fgnu89-inline -std=c99 -g")

if "worm" in sys.argv:
	libreria = "lib/geodesicas_worm.o"
elif "closed" in sys.argv:
	libreria = "lib/geodesicas_closed.o"
elif "hyperb" in sys.argv:
	libreria = "lib/geodesicas_hyperb.o"
elif "bh" in sys.argv:
	libreria = "lib/geodesicas_schwarzschild.o"
elif "test" in sys.argv:
    os.system("gcc test_shaders.c lib/stb_image.o lib/geodesicas_worm.o -o test.x -lm -lgomp -lGL -lglut -lGLEW -g")
    exit()
else:
	libreria = "lib/geodesicas_closed.o"

os.system("gcc visualizador.c lib/stb_image.o " + libreria + " -o programa.x -lm -lgomp -lGL -lglut -lGLEW -g")

#MESA_GL_VERSION_OVERRIDE=3.3
#MESA_GLSL_VERSION_OVERRIDE=330

#MESA_GL_VERSION_OVERRIDE=3.3 MESA_GLSL_VERSION_OVERRIDE=330 ./programa.x
#os.system("gcc-6 `pkg-config gtk+-2.0 --cflags` visualizador_float3.c -Ofast -march=native -o programa.x `pkg-config gtk+-2.0 --libs` -lm -fopenmp -lncurses -D_GLIBCXX_PARALLEL -unroll-loops -fgnu89-inline -std=c99")
