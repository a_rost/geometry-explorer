
import numpy as np
import numpy.ctypeslib as npct
from ctypes import c_int, c_float
import time
#from _ctypes import dlclose




def comp():
	import os
	os.system("gcc -c -Ofast -fPIC module.c -o lib/module.o -lm")
	os.system("gcc -shared lib/module.o lib/geodesicas_worm.o -o lib/module.so -lm -lGL -lGLEW -fopenmp")
	
comp()
# input type for the cos_doubles function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float32, ndim=1, flags='CONTIGUOUS')

# load the library, using numpy mechanisms
libcd = npct.load_library("lib/module.so", ".")


# setup the return types and argument types
libcd.geodesic_calculator2.restype = None
libcd.geodesic_calculator2.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int]

#void geodesic_calculator2(float *rho0, float *rho, float *phi, float *angles, float *distances, float *solutions, int size)
libcd.geodesic_F.restype = None
libcd.geodesic_F.argtypes = [array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.geodesica_approx_F.restype = None
libcd.geodesica_approx_F.argtypes = [array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.geodesic_T.restype = None
libcd.geodesic_T.argtypes = [array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.geodesic_I.restype = None
libcd.geodesic_I.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.geodesic_G.restype = None
libcd.geodesic_G.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.geodesic_G_T.restype = None
libcd.geodesic_G_T.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.geodesic_I_T.restype = None
libcd.geodesic_I_T.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int]

libcd.metrica_inicializador.restype = None
libcd.metrica_inicializador.argtypes = []

libcd.geodesica_inicializador2.restype = None
libcd.geodesica_inicializador2.argtypes = []

libcd.toggle_debug.restype = None
libcd.toggle_debug.argtypes = []

libcd.metrica_inicializador()
libcd.geodesica_inicializador2()

def reload():
	libcd.metrica_inicializador()
	libcd.geodesica_inicializador2()
	
def toggle_debug():
	libcd.toggle_debug()
	
def geodesica(rho0, rho, phi):
	angles    = np.zeros(4*len(rho), dtype = np.float32)
	distances = np.zeros(4*len(rho), dtype = np.float32)
	solutions = np.zeros(4*len(rho), dtype = np.float32)
	
	tiempo0 = time.time()
	libcd.geodesic_calculator2(np.float32(rho0), np.float32(rho), np.float32(phi), angles, distances, solutions, len(rho))
	tiempo1 = time.time()
	delta = tiempo1 - tiempo0
	print("Performance: ", delta/len(rho)*1000000., " us per vertex. Total time: ", delta, " sec.") 
	return angles, distances, solutions

def geodesica_F(rho, P):
	result = np.zeros(len(rho), dtype = np.float32)
	libcd.geodesic_F(np.float32(rho), np.float32(P), result, len(rho))
	return result

def geodesica_T(rho, P):
	result = np.zeros(len(rho), dtype = np.float32)
	libcd.geodesic_T(np.float32(rho), np.float32(P), result, len(rho))
	return result

def approx_F(rho, P):
	result = np.zeros(len(rho), dtype = np.float32)
	libcd.geodesica_approx_F(np.float32(rho), np.float32(P), result, len(rho))
	return result

def geodesica_I(P, rho1, rho2):
	result = np.zeros(len(rho1), dtype = np.float32)
	libcd.geodesic_I(np.float32(P), np.float32(rho1), np.float32(rho2), result, len(rho1))
	return result

def geodesica_G(P, rho1, rho2):
	result = np.zeros(len(rho1), dtype = np.float32)
	libcd.geodesic_G(np.float32(P), np.float32(rho1), np.float32(rho2), result, len(rho1))
	return result

def geodesica_I_T(P, rho1, rho2):
	result = np.zeros(len(rho1), dtype = np.float32)
	libcd.geodesic_I_T(np.float32(P), np.float32(rho1), np.float32(rho2), result, len(rho1))
	return result

def geodesica_G_T(P, rho1, rho2):
	result = np.zeros(len(rho1), dtype = np.float32)
	libcd.geodesic_G_T(np.float32(P), np.float32(rho1), np.float32(rho2), result, len(rho1))
	return result

