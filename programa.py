import numpy as np
import modulo 
import matplotlib.pyplot as plt
import time
from importlib import reload
import metrica
#from matplotlib.widgets import Slider, Button, RadioButtons
plt.ion()


def lineal4(rho0):
	phi0 = 0.
	#rho0, phi0 = 0.3, 0.

	rho = np.linspace(3.,   -3., 100000, dtype = np.float32)
	phi = np.linspace(0., np.pi, 100000, dtype = np.float32)

	alfas, dist, solutions = modulo.geodesica(rho0, phi0, rho, phi)


	plt.plot(rho, alfas[np.arange(len(rho))*4 + 0]*180/np.pi, ".")
	plt.plot(rho, alfas[np.arange(len(rho))*4 + 1]*180/np.pi, ".")
	plt.plot(rho, alfas[np.arange(len(rho))*4 + 2]*180/np.pi, ".")
	plt.plot(rho, alfas[np.arange(len(rho))*4 + 3]*180/np.pi, ".")
	return rho, phi, alfas, dist, solutions


def analysis(rho0, rho, phi, alfas, mask, i, exp = False):
	arreglo = np.arange(0, len(alfas), 4)
	

	P_sol = abs(np.sin(alfas)*r(rho0))
	alfas1, alfas2, alfas3, alfas4 = alfas[arreglo + 0], alfas[arreglo + 1], alfas[arreglo + 2], alfas[arreglo + 3]
	P1, P2, P3, P4 = P_sol[arreglo + 0], P_sol[arreglo + 1], P_sol[arreglo + 2], P_sol[arreglo + 3]
	
	graficos(rho0, rho[mask][i], exp = exp)

	y1 = 0*np.pi + phi[mask][i]
	y2 = 2*np.pi - phi[mask][i]
	y3 = 2*np.pi + phi[mask][i]
	y4 = 4*np.pi - phi[mask][i]
	if exp:
		y1 = np.exp(-y1)
		y2 = np.exp(-y2)
		y3 = np.exp(-y3)
		y4 = np.exp(-y4)
		
	plt.plot(P1[mask][i], y1, "o")
	plt.plot(P2[mask][i], y2, "o")
	plt.plot(P3[mask][i], y3, "o")
	plt.plot(P4[mask][i], y4, "o")
	plt.axhline(y = y1, linewidth=1, color='k', linestyle = "dashed")
	plt.axhline(y = y2, linewidth=1, color='k', linestyle = "dashed")
	plt.axhline(y = y3, linewidth=1, color='k', linestyle = "dashed")
	plt.axhline(y = y4, linewidth=1, color='k', linestyle = "dashed")
	
def mascareador(rho, alfas, indice, rho1, rho2, alfa1, alfa2):
	alfa = alfas[np.arange(0, len(alfas), 4) + indice]
	mask = (rho >= rho1)*(rho < rho2)
	mask *= (alfa >= alfa1)*(alfa < alfa2)
	return mask

T = 2.
b = 1./(2*T)
h = T - b*T**2
R = h + b*T**2

RHO_MAX, RHO_MIN = 5., -5.
condicion = 1

def r(x):
	if x >= T:
		return x
	elif x >= -T:
		return h + b*x**2
	else:
		return -x

def r_d(x):
	if x >= T:
		return 1.
	elif x >= -T:
		return 2*b*x
	else:
		return -1.
L = 0.
def f_rho(rho, sita, phi):
	return sita

def f_phi(rho, sita, phi):
	return L/r(rho)**2

def f_sita(rho, sita, phi):
	return L**2*r_d(rho)/r(rho)**3



def evolucionador2(rho0, phi0, alfa, T, dsigma = 0.01):
	global L
	L = r(rho0)*np.sin(alfa)

	rho, phi, sita = rho0, phi0, -np.cos(alfa)
	tiempo = time.time()
	
	nsteps = int(T/dsigma)
	d_ult  = T - nsteps*dsigma
	rho_space, phi_space = np.zeros(nsteps + 1), np.zeros(nsteps + 1)
	print(nsteps, d_ult)
	for i in range(nsteps + 1):
		if i == nsteps:
			dsigma = d_ult
		
		k_rho0  = dsigma*f_rho( rho, sita, phi)
		k_sita0 = dsigma*f_sita(rho, sita, phi)
		k_phi0  = dsigma*f_phi( rho, sita, phi)

		k_rho1  = dsigma*f_rho( rho + 0.5*k_rho0, sita + 0.5*k_sita0, phi + 0.5*k_phi0)
		k_sita1 = dsigma*f_sita(rho + 0.5*k_rho0, sita + 0.5*k_sita0, phi + 0.5*k_phi0)
		k_phi1  = dsigma*f_phi( rho + 0.5*k_rho0, sita + 0.5*k_sita0, phi + 0.5*k_phi0)
		
		k_rho2  = dsigma*f_rho( rho + 0.5*k_rho1, sita + 0.5*k_sita1, phi + 0.5*k_phi1)
		k_sita2 = dsigma*f_sita(rho + 0.5*k_rho1, sita + 0.5*k_sita1, phi + 0.5*k_phi1)
		k_phi2  = dsigma*f_phi( rho + 0.5*k_rho1, sita + 0.5*k_sita1, phi + 0.5*k_phi1)
		
		k_rho3  = dsigma*f_rho( rho + k_rho2, sita + k_sita2, phi + k_phi2)
		k_sita3 = dsigma*f_sita(rho + k_rho2, sita + k_sita2, phi + k_phi2)
		k_phi3  = dsigma*f_phi( rho + k_rho2, sita + k_sita2, phi + k_phi2)

		rho  += (k_rho0  + 2*k_rho1 + 2*k_rho2 + k_rho3)/6.
		sita += (k_sita0 + 2*k_sita1 + 2*k_sita2 + k_sita3)/6.
		phi  += (k_phi0  + 2*k_phi1 + 2*k_phi2 + k_phi3)/6.
		rho_space[i] = rho
		phi_space[i] = phi
	#print("time required: ", (time.time() - tiempo)*1000, " ms")
	return rho_space, phi_space

def plot(rho0, phi0, alfa0):
	rho, phi = geodesica2(rho0, phi0, alfa0, 1000)
	x, y = rho*np.cos(phi), rho*np.sin(phi)
	plt.plot(x, y)

#rho0, phi0, rho, phi = 1., 0., 2., 0.3
#alfas, dist = modulo.geodesica(rho0, phi0, np.array([rho], dtype = np.float32), np.array([phi], dtype = np.float32))



def plot(rho0, rho, phi):
	phi0 = 0.
	alfas, dist, __asd__ = modulo.geodesica(rho0, phi0, np.array([rho], dtype = np.float32), np.array([phi], dtype = np.float32))
	print("Angulos: ", alfas*180/np.pi)
	print("Distancias: ", dist)
	colors = ["#458b74", "#ffa500", "#66cd00", "#9a32cd"]
	leyenda = [ "1st geodesic", "2nd geodesic", "3rd geodesic", "4th geodesic"]
	nsteps = 10000
	plt.polar([phi0, phi], [rho0, rho], "o", label = "Start and end")
	for i in range(4):
		nsteps = int(dist[i]/0.01)
		rho_space, phi_space = evolucionador2(rho0, phi0, alfas[i], dist[i]*2)
		plt.polar(phi_space, rho_space, color = colors[i], linewidth = 1., linestyle = "dashed")
		rho_space, phi_space = evolucionador2(rho0, phi0, alfas[i], dist[i])
		plt.polar(phi_space, rho_space, color = colors[i], linewidth = 2., label = leyenda[i])
		
		
	plt.ylim([min(rho0, rho, 0.) - 1., max(rho0, rho, 0.) + 1])
	plt.polar(np.linspace(0., 2*np.pi, 100), np.zeros(100), linestyle = "dashed", c = "k", label = "Zero border")
	plt.legend()


def compare(rho0, alfa, dist):
	rho_space, phi_space = evolucionador2(rho0, 0., alfa, nsteps = int(dist/0.01))
	rho, phi = rho_space[-1], phi_space[-1]
	alfas2, dist2 = modulo.geodesica(rho0, 0., np.array([rho], dtype = np.float32), np.array([phi], dtype = np.float32))
	print("Alfas = (", alfa, alfas2)
	print("Dist  = (", dist, dist2)
	print("Error = ", abs(alfa - alfas2[0])*180/np.pi)
	
def precision(rho0, rho, phi):
	phi0 = 0.
	alfas, dist, solutions = modulo.geodesica(rho0, phi0, np.array([rho], dtype = np.float32), np.array([phi], dtype = np.float32))
	if np.isnan(dist).sum() != 0:
		print("Nan: ", rho0, rho, phi)
	dist[np.isnan(dist)] = 0.
	distancias = np.zeros(4)
	for i in range(4):
		rho_space, phi_space = evolucionador2(rho0, phi0, alfas[i], dist[i], dsigma = 0.01)
		rho1, phi1 = rho_space[-1], np.mod(phi_space[-1], 2*np.pi)
		#print(rho, phi, rho1, phi1)
		dx = np.sqrt((rho1 - rho)**2 + (phi1 - phi)**2*r(rho)**2)
		distancias[i] = dx
	return distancias


def lineal(rho0, N = 10000, fname = ""):
	np.random.seed(0)
	rho = np.linspace(5., -5., N)
	phi = np.linspace(0.1, np.pi, N)
	rho = np.random.random(N)*10. - 5.
	phi = np.random.random(N)*np.pi
	precs = np.zeros([N, 4])
	for i in range(N):
		print(rho[i], phi[i])
		precs[i] = precision(rho0, rho[i], phi[i])
	fig, axs = plt.subplots(nrows = 1, ncols = 3)
	axs[0].plot(rho, precs[:,0], ".")
	axs[0].plot(rho, precs[:,1], ".")
	axs[0].plot(rho, precs[:,2], ".")
	axs[0].plot(rho, precs[:,3], ".")
	axs[0].legend(["1st", "2nd", "3rd", "4th"])

	axs[1].plot(phi, precs[:,0], ".")
	axs[1].plot(phi, precs[:,1], ".")
	axs[1].plot(phi, precs[:,2], ".")
	axs[1].plot(phi, precs[:,3], ".")
	axs[1].legend(["1st", "2nd", "3rd", "4th"])

	axs[2].hist(precs[:,0], bins = 100, range = [-0.1, 0.2], alpha = 0.3)
	axs[2].hist(precs[:,1], bins = 100, range = [-0.1, 0.2], alpha = 0.3)
	axs[2].hist(precs[:,2], bins = 100, range = [-0.1, 0.2], alpha = 0.3)
	axs[2].hist(precs[:,3], bins = 100, range = [-0.1, 0.2], alpha = 0.3)
	axs[2].legend(["1st", "2nd", "3rd", "4th"])
	
	errors_physc = {"rho0": np.ones(N)*rho0, "phi": phi, "rho": rho, "precs": precs}
	if len(fname) > 0:
		np.save("errors/physical_errors_rho0_" + str(rho0) + "_" + fname,  errors_physc)
	return errors_physc

def lineal2(rho0, alfa, dist_max):
	rho_space, phi_space = evolucionador2(rho0, 0., alfa, dist_max, dsigma = 0.001)
	alfas, dist, solutions = modulo.geodesica(rho0, 0., np.array(rho_space, dtype = np.float32), np.array(phi_space, dtype = np.float32))
	fig, axs = plt.subplots(nrows = 1, ncols = 2)
	axs[0].plot((alfas[np.arange(0, len(alfas), 4)] - alfa)*180./np.pi*3600/49.6, ".")
	axs[1].plot(dist[np.arange(0, len(alfas), 4)] - np.linspace(0., dist_max, len(rho_space)), ".")
	axs[0].set_ylim([-5., 5.])
	axs[0].set_ylabel("Difference [units of angular size of Jupiter (50'')]")
	axs[1].set_ylim([-5., 5.])
	axs[1].set_ylabel("Difference")

def lineal3(rho0, N = 10000, fname = ""):
	np.random.seed(0)
	alfa0 = np.random.random(N)*np.pi
	dist0 = np.random.random(N)*3.
	#dist0 = np.ones(N)*3.
	alfa1 = np.zeros(N)
	dist1 = np.zeros(N)
	index = np.zeros(N, dtype = np.int)
	rho = np.zeros(N)
	phi = np.zeros(N)
	for i in range(N):
		rho_space, phi_space = evolucionador2(rho0, 0., alfa0[i], dist0[i])
		rho1, phi1 = rho_space[-1], phi_space[-1]
		rho[i] = rho1
		phi[i] = phi1
		alfas, dist, solutions = modulo.geodesica(rho0, 0., np.array([rho1], dtype = np.float32), np.array([phi1], dtype = np.float32))
		alfas = np.mod(alfas, 2*np.pi)
		#alfas, dist = modulo.geodesica(rho0, 0., np.array(rho_space, dtype = np.float32), np.array(phi_space, dtype = np.float32))
		indice   = np.argmin(abs(alfas - alfa0[i]))
		index[i] = indice
		alfa1[i] = alfas[indice]
		dist1[i] = dist[indice]

	errors_alfa = {"alfa0": alfa0, "alfa1": alfa1, "dist0": dist0, "dist1": dist1, "rho0": np.ones(N)*rho0, "rho": rho, "phi": phi}

	if len(fname) > 0:
		np.save("errors/alfas_error_" + str(rho0) + "_" + fname, errors_alfa)

	lim = np.percentile(abs(alfa1 - alfa0), 90.)
	mask = abs(alfa1 - alfa0) <= lim
	plt.hist((alfa1 - alfa0)[mask]*180./np.pi*3600/50., bins = 50)
	return errors_alfa


def carga_errors():
	rho0 = np.load("errors/physical_error_rho0.npy")
	phi  = np.load("errors/physical_error_phi.npy")
	rho  = np.load("errors/physical_error_rho.npy")
	errors=np.load("errors/physical_error_error.npy")
	return rho0, rho, phi, errors

def carga_alfas(rho0, fname):
	errores = np.load("errors/alfas_error_" + str(rho0) + "_" + fname + ".npy", allow_pickle = True).item()
	return errores

def analize_errors(lim = 90., lim_universal = False):
	rho0, rho, phi, errors = carga_errors()
	
	if lim_universal:
		lim1 = np.percentile(errors[:,0], lim)
		lim2 = np.percentile(errors[:,1], lim)
		lim3 = np.percentile(errors[:,2], lim)
		lim4 = np.percentile(errors[:,3], lim)

	else:
		lim1 = lim2 = lim3 = lim4 = lim_universal
	mask1 = errors[:, 0] >= lim1
	mask2 = errors[:, 0] >= lim2
	mask3 = errors[:, 0] >= lim3
	mask4 = errors[:, 0] >= lim4

	plt.polar(phi[mask1], rho[mask1], ".")
	plt.polar(phi[mask2], rho[mask2], ".")
	plt.polar(phi[mask3], rho[mask3], ".")
	plt.polar(phi[mask4], rho[mask4], ".")
	plt.legend(["1st", "2nd", "3rd", "4th"])

def F(x, P):
	return modulo.geodesica_F(np.array([x]), np.array([P]))

def S(x, P):
	return modulo.geodesica_S(np.array([x]), np.array([P]))

def test_F(N, M):
	matriz = np.zeros([N, M], dtype = np.float32)
	rho_space = np.linspace(-5., 5., N)
	u_space   = np.linspace(0., 1., M)
	for i in range(N):
		P_max = r(rho_space[i])
		P_space = u_space*P_max
		result = modulo.geodesica_F(np.ones(M)*rho_space[i], P_space)
		matriz[i] = result
	plt.imshow(matriz)
	return matriz

def test_S(N, M):
	matriz = np.zeros([N, M], dtype = np.float32)
	rho_space = np.linspace(-5., 5., N)
	u_space   = np.linspace(0., 1., M)
	for i in range(N):
		P_max = r(rho_space[i])
		P_space = u_space*P_max
		result = modulo.geodesica_S(np.ones(M)*rho_space[i], P_space)
		matriz[i] = result
	plt.imshow(matriz)
	return matriz

r_vec = np.vectorize(r)

def test_I_G(rho1, N):
	rho2 = np.random.random(N)*10. - 5.
	phi  = np.random.random(N)*np.pi
	alfas, dist, solutions = modulo.geodesica(rho1, 0., np.float32(rho2), np.float32(phi))
	P = np.sin(alfas[np.arange(0, len(alfas), 4)])*r_vec(rho1)
	I_array = modulo.geodesica_I(P, rho1*np.ones(N), rho2)
	G_array = modulo.geodesica_G(P, rho1*np.ones(N), rho2)
	
	diff = np.zeros(len(P))
	mask_I = solutions[np.arange(0, len(alfas), 4)] == 1.
	print(len(mask_I), len(P), len(phi), mask_I)
	diff[mask_I]  = I_array[mask_I]  - phi[mask_I]
	diff[~mask_I] = G_array[~mask_I] - phi[~mask_I]
	
	return I_array, G_array, rho2, phi, diff

def graficos(rho1, rho2, exp = True, alfa = False, N = 10000):
	P1 = r(rho1)
	P2 = r(rho2)
	
	if rho1*rho2 < 0.:
		P_min = 0.
		P_max = h
		P_space = np.linspace(P_min, P_max, N)
		
		I_array = modulo.geodesica_I(P_space, rho1*np.ones(N), rho2*np.ones(N))
		if alfa:
			P_space = np.arcsin(P_space/r(rho1))
		if exp:
			plt.plot(P_space, np.exp(-I_array))
		else:
			plt.plot(P_space, I_array)
		
	else:
		P_min = 0.
		P_max = min(P1, P2)
		P_space1 = np.linspace(P_min, P_max, N)
		P_space2 = np.linspace(h,     P_max, N)
		
		I_array = modulo.geodesica_I(P_space1, rho1*np.ones(N), rho2*np.ones(N))
		G_array = modulo.geodesica_G(P_space2, rho1*np.ones(N), rho2*np.ones(N))

		if alfa:
			P_space1 = np.arcsin(P_space1/r(rho1))
			P_space2 = np.arcsin(P_space2/r(rho1))
		if exp:
			plt.plot(P_space1, np.exp(-I_array))
			plt.plot(P_space2, np.exp(-G_array))
		else:
			plt.plot(P_space1, I_array)
			plt.plot(P_space2, G_array)


def test_F2(N, rho_max = 0.5):
	rho_space = np.random.random(N)*2*rho_max - rho_max
	P_space   = np.zeros(N)
	F0_space  = np.zeros(N)
	for i in range(N):
		P_max = r(rho_space[i])
		P     = np.random.random()*P_max
		P_space[i] = P
		F0_space[i] = metrica.F(rho_space[i], P)
	F1_space = modulo.geodesica_F(rho_space, P_space)
	return rho_space, P_space, F0_space, F1_space

def test_F3(N = 100, M = 200):
	rho_space = np.linspace(0., 3., N)
	u_space   = np.linspace(0., 1., M)

	F0_space   = np.zeros([N, M])
	F1_space   = np.zeros([N, M])

	P_min = 1.
	for i in range(N):
		rho   = rho_space[i]
		P_max = r(rho)
		
		for j in range(M):
			P = P_min + (P_max - P_min)*u_space[j]
			
			F0_space[i, j] = metrica.F(rho, P)
			F1_space[i, j] = F(rho, P)
			
	return F0_space, F1_space

def solution(rho1, rho2, P, phi, exp = True):
	graficos(rho1, rho2, N = 1000000, exp = exp)
	P_min, P_max = 0., min(r(rho1), r(rho2))
	
	if exp == False:
		plt.vlines(x = P, ymin = 0., ymax = phi*2.)
		plt.hlines(y = phi, xmin = P_min, xmax = P_max)
	else:
		plt.vlines(x = P, ymin = np.exp(0.), ymax = np.exp(-phi*2.))
		plt.hlines(y = np.exp(-phi), xmin = P_min, xmax = P_max)
		
#mask = abs(F1_space/F0_space - 1.) >= 0.01
#plt.plot(rho, P, ",")
#plt.plot(rho[mask], P[mask], ",")

rho0 = 0.4
rho, phi, alfas, dist, solutions =lineal4(rho0)


N = int(len(alfas)/4)

alfa1 = alfas[np.arange(0, 4*N, 4) + 0]*180./np.pi
alfa2 = alfas[np.arange(0, 4*N, 4) + 1]*180./np.pi
alfa3 = alfas[np.arange(0, 4*N, 4) + 2]*180./np.pi
alfa4 = alfas[np.arange(0, 4*N, 4) + 3]*180./np.pi

P1 = r(rho0)*np.sin(alfa1*np.pi/180.)
P2 = r(rho0)*np.sin(alfa2*np.pi/180.)
P3 = r(rho0)*np.sin(alfa3*np.pi/180.)
P4 = r(rho0)*np.sin(alfa4*np.pi/180.)


mask = abs(alfa1 - 90.) <= 0.25

plt.plot(rho, abs(P1))
plt.plot(rho, abs(P2))
plt.plot(rho, abs(P3))
plt.plot(rho, abs(P4))
plt.hlines(xmin = rho.min(), xmax = rho.max(), y = r(rho0))
plt.hlines(xmin = rho.min(), xmax = rho.max(), y = 1., linestyle = "dashed")

def geo(rho0, rho1, phi):
	rho0_arr = np.float32(rho0)
	rho1_arr = np.float32(rho1)
	phi1_arr = np.float32(phi)

	return modulo.geodesica(rho0_arr, rho1_arr, phi1_arr)


modulo.toggle_debug()
geo(rho0, 0.58647585, 1.2637182)




N, M = 200, 200

rho0_space = np.linspace(-5., 5., N)
rho1_space = np.linspace(-5., 5., N)
phi_space  = np.linspace(0., np.pi, M)

cube_rho1, cube_rho2, cube_phi = np.meshgrid(rho0_space, rho1_space, phi_space)


rho1, rho2, phi = np.reshape(cube_rho1, [N**2*M]), np.reshape(cube_rho2, [N**2*M]), np.reshape(cube_phi, [N**2*M])

result = geo(rho1, rho2, phi)[0]

group1 = result[np.arange(0, len(result), 4) + 0]
group2 = result[np.arange(0, len(result), 4) + 1]
group3 = result[np.arange(0, len(result), 4) + 2]
group4 = result[np.arange(0, len(result), 4) + 3]


angles1 = np.reshape(group1, [N, N, M])
angles2 = np.reshape(group2, [N, N, M])
angles3 = np.reshape(group3, [N, N, M])
angles4 = np.reshape(group4, [N, N, M])
