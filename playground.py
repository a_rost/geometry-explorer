 
	
def region1(N_P, N_rho, F, F2, approx_F2 = approx_F2):
	array_F  = np.zeros([N_rho, N_P])
	array_F2 = np.zeros([N_rho, N_P])
	array_A1 = np.zeros([N_rho, N_P])
	array_A2 = np.zeros([N_rho, N_P])

	P_space   = np.linspace(0., 1., N_P)
	rho_space = np.linspace(0., 2., N_rho)

	drho = rho_space[1] - rho_space[0]
	rho_space[0] = drho/10.
	rho_space[-1] = 2. - drho/10.
	
	dP = P_space[-1] - P_space[-2]
	P_space[-1] = 1 - dP/10.
	
	dP = P_space[1] - P_space[0]
	P_space[0] = dP/10.

	for i in range(N_P):
		P = P_space[i]
		for j in range(N_rho):
			rho = rho_space[j]
			try:
				array_F[j, i]  = F(rho, P)
				array_F2[j, i] = F(2., P)
				array_A1[j, i] = approx_F2(0., 2., P)
				array_A2[j, i] = approx_F2(rho, 2., P)
				
			except:
				pass

	array1 = (array_F2 - array_F) - array_A2
	array2 = (array_F2 - array_A1)[0]
	return np.float32(array1), np.float32(array2)


def region2(N_P, N_rho, F, F2, approx_F2 = approx_F2):
	array_F  = np.zeros([N_rho, N_P])
	array_F2 = np.zeros([N_rho, N_P])
	array_A1 = np.zeros([N_rho, N_P])
	array_A2 = np.zeros([N_rho, N_P])
	
	rho_space = np.linspace(0., 2., N_rho)
	drho      = rho_space[1] - rho_space[0]
	rho_space[0]  = drho/10.
	rho_space[-1] = 2. - drho/10.
	
	r_vec = np.vectorize(r)
	for j in range(N_rho):
		w_space  = np.linspace(0., rho_space[j], N_P)
		
		dw = w_space[1] - w_space[0]
		
		w_space[0] = dw/10.
		w_space[-1] = rho_space[j] - dw/10.
		
		P_space = r_vec(w_space)

		for i in range(N_P):
			rho = rho_space[j]
			P = P_space[i]
			rho_min = raiz(P)
			try:
				array_F[j, i]  = F(rho, P)
				array_F2[j, i] = F(2., P)
				array_A1[j, i] = approx_F2(rho_min, 2., P)
				array_A2[j, i] = approx_F2(rho, 2., P)
			except:
				pass

	array3 = (array_F2 - array_F) - array_A2
	array4 = (array_F2 - array_A1)[-1]
	return np.float32(array3), np.float32(array4)


array1,   array2   = region1(N_P, N_rho, F, F2, approx_F2 = approx_F2)
array1_t, array2_t = region1(N_P, N_rho, t, t2, approx_F2 = approx_T2)

array3,   array4   = region2(N_P, N_rho, F, F2, approx_F2 = approx_F2)
array3_t, array4_t = region2(N_P, N_rho, t, t2, approx_F2 = approx_T2)


array3_t.T[0] = array3_t.T[1]
array3.T[0] = array3.T[1]

array3[0] = array4[0]
array1.T[-1] = array3.T[0]

array3_t[0] = array4_t[0]
array1_t.T[-1] = array3_t.T[0]

array2[-1] = array4[0]
array2_t[-1] = array4_t[0]

save_arrays()
