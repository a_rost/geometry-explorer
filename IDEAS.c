Programar geodesicas para esferas concentricas de manera eficiente, así se puede utilizar fondos (backgrounds) en wormhole:
Como es de simetria esferica se podría setear una distancia en el infinito, entonces no hace falta una busqueda de soluciones en tiempo
real, con un arreglo predeterminado en 2d es suficiente, esto permitiria tener una resolucion elevada de estas esferas sin perder fps.

Programar método eficiente de búsqueda de raíces: ya que frame por frame las soluciones de las ecuaciones de geodesicas son parecidas, se 
pueden utilizar las soluciones del frame anterior como punto de partida en el metodo de la secante/bisección y se podría acelerar la 
búsqueda de soluciones.

Determinar soluciones de geodesicas cercanas a la coordenada rho = 0 de manera teórica para resolver glitches.

Construir un editor, y generar un escenario de "ciudad" o "casas" en el cual esté presente un agujero de gusano y que te lleve a otro 
"pueblo" o "ciudad" por edificaciones distorsionadas.

Considerar el aumento de resolucion de las integrales precalculadas como posibles soluciones a imprecisiones.

Minimizar la allocatación y liberación de memoria en las funciones geodesicas_raiz_I y G, podría incrementarse el rendimiento. Se 
allocatarían una sola vez y serían globales.

* resolver glitches cerca de rho = 0 debido a mala determinacion de "alpha".

* resolver glitches en el shader de geometría. 

* mejorar el criterio que decide cuando usar y cuando no la amplificación de triángulos.


