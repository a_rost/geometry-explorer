#version 330 core

in vec3 Color;
in float distancia;
out vec4 FragColor;

float tau = 0.1;
void main(){
	gl_FragDepth = distancia;
	float intensidad = exp(-distancia/tau);
	FragColor.xyz = Color*intensidad;
	FragColor.w = 1.;
	vec2 coord = gl_PointCoord - vec2(0.5);  //from [0,1] to [-0.5,0.5]
	FragColor.xyz = Color*exp(-gl_PointCoord.s);
	if(length(coord) > 0.5)                  //outside of circle radius?
		discard;

}
