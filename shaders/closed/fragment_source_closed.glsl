#version 330 core

in vec2 tex_coord;
in float distancia;
out vec4 FragColor;
//out float gl_FragDepth;

uniform sampler2D ourTexture;

float tau = 0.1;
void main(){
	gl_FragDepth = distancia;
	float intensidad = exp(-distancia/tau);
	FragColor = texture(ourTexture, tex_coord)*intensidad;
	FragColor.w = 1.;
}
