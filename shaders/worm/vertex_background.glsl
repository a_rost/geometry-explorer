#version 130

in vec2 pos;

void main(){
	gl_Position.xy = pos;
	gl_Position.zw = vec2(1., 1.);
}
