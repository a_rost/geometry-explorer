#version 440 core

#define res_x 200
#define res_y 200
#define pi (3.1415926535)

layout(std430, binding = 3) buffer ssbo
{
    float data_ssbo[4*res_x*res_y];
};

out vec4 FragColor;

layout(binding=0) uniform sampler2D texture1;
layout(binding=1) uniform sampler2D texture2;
uniform float aspect_ratio, light_speed, pix_size;
uniform mat4 cam_matrix, cam_matrix_inv;
uniform mat3 base;
uniform vec4 posicion;
uniform vec2 resolution;

vec3 xyz2esf(vec3 vector){
	vec3 result;
	float rho, theta, phi;
	
	rho   = length(vector);
	theta = acos(vector.z/rho);
	phi   = pi + atan(vector.y, vector.x);
	
	result.x = rho;
	result.y = theta;
	result.z = phi;
	return result;
}


float metrica_r(float x){
	if(x >= 2.)
		return x;
	else if(x >= -2.)
		return 1. + 0.25*x*x;
	else
		return -x;
}

vec4 transform_cam2esf(vec4 vector, int inverted){
	if(inverted == 0)
		return cam_matrix*vector;
	else if(inverted == 1)
		return cam_matrix_inv*vector;
}

vec4 transform_esf2esfn(vec4 vector, int inverted){
	vec4 result;
	if(inverted == 0){
		result.x = vector.x;
		result.y = vector.y;
		result.z = vector.z*metrica_r(posicion[1]);
		result.w = vector.w*metrica_r(posicion[1])*sin(posicion[2]);
	}
	if(inverted == 1){
		result.x = vector.x;
		result.y = vector.y;
		result.z = vector.z/metrica_r(posicion[1]);
		result.w = vector.w/metrica_r(posicion[1])/sin(posicion[2]);
	}
	return result;
}

vec4 transform_esfn2cart(vec4 vector, int inverted){
	vec4 result;
	if(inverted == 0)
		result.yzw = base*(vector.yzw);
	else if(inverted == 1)
		result.yzw = (vector.yzw)*base;
	result.x = vector.x;
	return result;
}



vec2 rotate(vec4 pix_norm, float angle){
	vec3 vec_rho, vec_perp, vec_parall, rotated, result;
	
	vec_rho.x = sin(posicion[2])*cos(posicion[3]);
	vec_rho.y = sin(posicion[2])*sin(posicion[3]);
	vec_rho.z = cos(posicion[2]);

	vec_perp = cross(vec_rho, transform_esfn2cart(pix_norm, 0).yzw);
	vec_perp = normalize(vec_perp);

	vec_parall = cross(vec_perp, vec_rho);

	rotated = cos(angle)*vec_rho + sin(angle)*vec_parall;
	result  = xyz2esf(rotated);
	result.y *= 1./pi;
	result.z *= 1./(2*pi);
	return result.zy;
}



float interpolate_array(int array_id, float rho, float w){
	int shift, i, j;
	float rho_max, drho, dw, x1, x2, y1, y2;

	drho = 10./(res_x - 1);
	dw   = 1./(res_y - 1);
	
	i = int(rho/drho);
	j = int(w/dw);
	
	if(i >= res_x)
		i = res_x - 2;
	else if(i < 0)
		i = 0;
	if(j >= res_y)
		j = res_y - 2;
	else if(j < 0)
		j = 0;
	shift = array_id*res_x*res_y;

	
	x1 = data_ssbo[shift + i*res_y + j];
	x2 = data_ssbo[shift + (i + 1)*res_y + j];
	
	y1 = x1 + (x2 - x1)*(rho - drho*i)/drho;

	x1 = data_ssbo[shift + i*res_y + j + 1];
	x2 = data_ssbo[shift + (i + 1)*res_y + j + 1];
	
	y2 = x1 + (x2 - x1)*(rho - drho*i)/drho;
	
	return y1 + (y2 - y1)*(w - dw*j)/dw;
}

void main(){
	vec4 vec_cam, vec_esf, vec_esfn;
	vec3 vec_ogl;
	vec2 texcoord;
	float norma, alpha, P, alpha_lim, w, rho, phi, L1, L2, dist;
	
	L1 = pix_size*resolution.x;
	L2 = pix_size*resolution.y;


	vec_ogl.x = gl_FragCoord.x*pix_size - (L1 - pix_size)/2.;
	vec_ogl.y = gl_FragCoord.y*pix_size - (L2 - pix_size)/2.;
	vec_ogl.z = -1.;
	
	
	vec_cam.y = -vec_ogl.z;
	vec_cam.z = -vec_ogl.x;
	vec_cam.w =  vec_ogl.y;
	
	norma = length(vec_cam.yzw);
	
	vec_cam.yzw *= 1./norma;
	vec_cam.x = -1./light_speed;

	vec_esf  = transform_cam2esf(vec_cam,  0);
	vec_esfn = transform_esf2esfn(vec_esf, 0);

	alpha = acos(-vec_esfn.y/length(vec_esfn.yzw));
	P     = sin(alpha)*metrica_r(posicion[1]);

	
	alpha_lim = asin(1./metrica_r(posicion[1]));

	rho = posicion[1];
	gl_FragDepth = 0.99;

	if(rho >= 0.){
		if(alpha <= alpha_lim){
			w = alpha/alpha_lim;
			phi = -interpolate_array(0, abs(rho), w);
			dist = interpolate_array(2, abs(rho), w);
		}
		else{
			w = (alpha - alpha_lim)/(pi - alpha_lim);
			phi = interpolate_array(1, abs(rho), w);
			dist = interpolate_array(3, abs(rho), w);
		}
		texcoord  = rotate(vec_esfn, phi);

		if(alpha <= alpha_lim)
			FragColor = texture(texture1, texcoord);
		else
			FragColor = texture(texture2, texcoord);
	}
	else{
		alpha = pi - alpha;
		if(alpha <= alpha_lim){
			w = alpha/alpha_lim;
			phi = -interpolate_array(0, abs(rho), w);
			dist = interpolate_array(2, abs(rho), w);
		}
		else{
			w = (alpha - alpha_lim)/(pi - alpha_lim);
			phi = interpolate_array(1, abs(rho), w);
			dist = interpolate_array(3, abs(rho), w);
		}
		texcoord  = rotate(vec_esfn, phi);
		if(alpha <= alpha_lim)
			FragColor = texture(texture2, texcoord);
		else
			FragColor = texture(texture1, texcoord);
	}
	FragColor.xyz *= 0.5;
}
