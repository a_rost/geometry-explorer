#version 440 core

#define pi (3.1415926535)

layout(triangles) in;
layout(triangle_strip, max_vertices = 100) out;


in vec3 cons_pos[];
in float alfas[];
in float T[];
in float tipo2[];
in vec2 TexCoord[];

out vec3 color;
out vec2 texcoord;
out float dist;
uniform vec4 posicion;
uniform mat4 cam_matrix, cam_matrix_inv;
uniform float modo, aspect_ratio, pix_size, indice, cantidad, light_speed;
uniform mat3  base;
uniform vec2 resolution;

vec3 versor0;
const float dtheta = 5*pi/180.;
int tipo, outside;
int n1, n2, n3, i, idx_min, idx_med, idx_max, counter = 0;
float phi[3];
int adentro;                                                   //0 if radial direction lies inside current triangle, 1 otherwise 

void det_phi_values(vec3 vert, vec3 pos, int idx){
	vec3 vers;
	float phi_raw;
	vers = normalize(vert);
	
	phi_raw = acos(dot(vers, pos));
	if(tipo2[idx] == 1.)
		phi[idx] = phi_raw;
	else if(tipo2[idx] == 2.)
		phi[idx] = 2*pi - phi_raw;
	else if(tipo2[idx] == 3.)
		phi[idx] = 2*pi + phi_raw;
	else
		phi[idx] = 4*pi - phi_raw;
}

vec3 esf2xyz(vec3 vertice){
	//Be careful, this returns a vector of lenght 1.
	vec3 retorno;
	retorno = vec3(sin(vertice.y)*cos(vertice.z), sin(vertice.y)*sin(vertice.z), cos(vertice.y));
	return retorno;
}

float metrica_r(float x){
	if(x >= 2.)
		return x;
	else if(x >= -2.)
		return 1. + 0.25*x*x;
	else
		return -x;
}




vec4 transform_cam2esf(vec4 vector, int inverted){
	if(inverted == 0)
		return cam_matrix*vector;
	else if(inverted == 1)
		return cam_matrix_inv*vector;
}

vec4 transform_esf2esfn(vec4 vector, int inverted){
	vec4 result;
	if(inverted == 0){
		result.x = vector.x;
		result.y = vector.y;
		result.z = vector.z*metrica_r(posicion[1]);
		result.w = vector.w*metrica_r(posicion[1])*sin(posicion[2]);
	}
	if(inverted == 1){
		result.x = vector.x;
		result.y = vector.y;
		result.z = vector.z/metrica_r(posicion[1]);
		result.w = vector.w/metrica_r(posicion[1])/sin(posicion[2]);
	}
	return result;
}

vec4 transform_esfn2cart(vec4 vector, int inverted){
	vec4 result;
	if(inverted == 0)
		result.yzw = base*(vector.yzw);
	else if(inverted == 1)
		result.yzw = (vector.yzw)*base;
	result.x = vector.x;
	return result;
}




vec4 geodesica4(vec3 pos, float alfa){
	vec3 versor1, versor_perp;
	vec4 vector_esfn, versor_cart;
	float modulo, coseno;
	int k;

	versor1 = esf2xyz(pos);
	coseno  = dot(versor0, versor1);
	
	versor_perp  = normalize(versor1 - coseno*versor0);

	versor_cart.yzw = -cos(alfa)*versor0 + sin(alfa)*versor_perp;
	versor_cart.x = -1./light_speed;

	vector_esfn = transform_esfn2cart(versor_cart, 1);
	return vector_esfn;
}

vec4 geodesica2(float angulo, float alfa){
	vec3 versor1, versor_perp, versor_angular;
	vec4 versor_cart, vector_esfn;
	float coseno;
	
	versor1 = vec3(0., 0., 1.);
	coseno = dot(versor0, versor1);
	versor1 = normalize(versor1 - coseno*versor0);
	versor_perp  = versor1 - coseno*versor0;

	versor_perp = normalize(cross(versor1, versor0));

	
	versor_angular = cos(angulo)*versor_perp + sin(angulo)*versor1;
	
	versor_cart.yzw = normalize(-cos(alfa)*versor0 + sin(alfa)*versor_angular);
	
	
	versor_cart.x = -1./light_speed;

	vector_esfn = transform_esfn2cart(versor_cart, 1);
	return vector_esfn;
}

vec3 transform_cam2opengl(vec4 vector_original){
	vec3 vector_opengl, vector_3D;
	float L1, L2;
	
// 	vector_original *= 1./vector_original.z;
	
	L1 = resolution.x*pix_size;
	L2 = resolution.y*pix_size;

	vector_3D = vector_original.yzw;
	
	vector_opengl.x = -vector_3D.y;
	vector_opengl.y = vector_3D.z;
	vector_opengl.z = -vector_3D.x;

	vector_opengl.x *= 2./L1;
	vector_opengl.y *= 2./L2;
	return vector_opengl;
}


vec3 transformacion(vec3 pos, float alfa, float distancia){
	vec4 vec_esfn, vec_cam, vec_esf;

	vec_esfn  = geodesica4(pos, alfa);
	vec_esf   = transform_esf2esfn(vec_esfn, 1);
	vec_cam   = transform_cam2esf(vec_esf, 1);

	return transform_cam2opengl(vec_cam);
}

vec3 transformacion2(float angulo, float alfa, float distancia){
	vec4 vec_esf, vec_esfn, vec_cam;

	if(tipo == 1)
		angulo += pi;

	vec_esfn  = geodesica2(angulo, alfa);
	vec_esf   = transform_esf2esfn(vec_esfn, 1);
	vec_cam   = transform_cam2esf(vec_esf, 1);

	return transform_cam2opengl(vec_cam);
}

float angulo(vec2 vector, int tipo){
	// Determines the angle of the 2d vector with the horizontal that starts at the centre towards the right. Basically "phi", in spherical coordinates
	// Tipo refers whether the horizontal is left or right.
	float numero;
	if(tipo == 0){
		numero = atan(vector.y, vector.x);
		if(numero >= 0.)
			return numero;
		else
			return 2*pi + numero;
	}
	else
		return pi + atan(vector.y, vector.x);
}

int det_tipo(vec2 A, vec2 B, vec2 C){
	// Determines whether the horizontal mentioned above crosses the triangle or not (if the centre is not inside the triangle)
	float theta1, theta2, theta3;
	
	theta1 = angulo(A, 0);
	theta2 = angulo(B, 0);
	theta3 = angulo(C, 0);
	if( (abs(theta1 - theta2) <= pi)&&(abs(theta1 - theta3) <= pi)&&(abs(theta1 - theta2) <= pi) )
		return 0;
	else
		return 1;
}

float signo (vec2 p1, vec2 p2, vec2 p3){
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

int in_triangle (vec2 pt, vec2 v1, vec2 v2, vec2 v3){
	// Determines whether the centre (direction to the wormhole) is inside the triangle

    int b1, b2, b3;
	if(signo(pt, v1, v2) <= 0.)
	    b1 = 1;
	else
		b1 = 0;
	if(signo(pt, v2, v3) <= 0.)
	    b2 = 1;
	else
		b2 = 0;
	if(signo(pt, v3, v1) <= 0.)
	    b3 = 1;
	else
		b3 = 0;

	if((b1 == b2) && (b2 == b3))
	    return 1;
	else
		return 0;
}
float det_subtended_angle(vec2 A, vec2 B){
	// Angle between 2d vectors
	vec2 a, b;
	a = normalize(A);
	b = normalize(B);
	return abs(acos(dot(a, b)));
}

void three_2_two(in vec3 pos1, in vec3 pos2, in vec3 pos3, inout vec2 A, inout vec2 B, inout vec2 C){
	// Transforms from 3D vectors to 2D in the "screen space"
	vec3 pos3D, vector_base1, vector_base2, var_pos1, var_pos2, var_pos3;
	pos3D = esf2xyz(posicion.yzw);

	vector_base1 = vec3(0., 0., 1.);
	float coseno = dot(vector_base1, pos3D);
// 	if(abs(coseno - 1.) <= 0.01){
// 		vector_base1 = vec3(1., 0., 0.);
// 		coseno = dot(vector_base1, pos3D);
// 	}
	vector_base1 = normalize(vector_base1 - coseno*pos3D);

	vector_base2 = cross(vector_base1, pos3D);

	var_pos1 = esf2xyz(pos1);
	var_pos2 = esf2xyz(pos2);
	var_pos3 = esf2xyz(pos3);

	A = vec2(dot(var_pos1, vector_base2), dot(var_pos1, vector_base1));
	B = vec2(dot(var_pos2, vector_base2), dot(var_pos2, vector_base1));
	C = vec2(dot(var_pos3, vector_base2), dot(var_pos3, vector_base1));
}






void ordenador(in float angulo1, in float angulo2, in float angulo3, inout int idx_min, inout int idx_med, inout int idx_max){
	float ang_min, ang_med, ang_max;
	ang_min = min(angulo1, min(angulo2, angulo3));
	ang_max = max(angulo1, max(angulo2, angulo3));

	if(ang_min == angulo1){
		idx_min = 0;
		if(ang_max == angulo2){
			idx_max = 1;
			idx_med = 2;
		}
		else{
			idx_max = 2;
			idx_med = 1;
		}
	}
	else if(ang_min == angulo2){
		idx_min = 1;
		if(ang_max == angulo1){
			idx_max = 0;
			idx_med = 2;
		}
		else{
			idx_max = 2;
			idx_med = 0;
		}
	}
	else{
		idx_min = 2;
		if(ang_max == angulo1){
			idx_max = 0;
			idx_med = 1;
		}
		else{
			idx_max = 1;
			idx_med = 0;
		}
	}
}

void interpolador1d(float ang_min, float ang_med, float ang_max, float cant_min, float cant_med, float cant_max, float angulo, inout float result1, inout float result2){
	if(angulo <= ang_med){
		result1 = cant_min + (angulo - ang_min)*(cant_med - cant_min)/(ang_med - ang_min);
		result2 = cant_min + (angulo - ang_min)*(cant_max - cant_min)/(ang_max - ang_min);
	}
	else{
		result1 = cant_med + (angulo - ang_med)*(cant_max - cant_med)/(ang_max - ang_med);
		result2 = cant_min + (angulo - ang_min)*(cant_max - cant_min)/(ang_max - ang_min);
	}
	float aux;
	if(outside == 1){
		aux = result1;
		result1 = result2;
		result2 = aux;
	}
}

void interpolador3d(float ang_min, float ang_med, float ang_max, vec3 cant_min, vec3 cant_med, vec3 cant_max, float angulo, inout vec3 result1, inout vec3 result2){
	if(angulo <= ang_med){
		result1 = cant_min + (angulo - ang_min)*(cant_med - cant_min)/(ang_med - ang_min);
		result2 = cant_min + (angulo - ang_min)*(cant_max - cant_min)/(ang_max - ang_min);
	}
	else{
		result1 = cant_med + (angulo - ang_med)*(cant_max - cant_med)/(ang_max - ang_med);
		result2 = cant_min + (angulo - ang_min)*(cant_max - cant_min)/(ang_max - ang_min);
	}
	vec3 aux;
	if(outside == 1){
		aux = result1;
		result1 = result2;
		result2 = aux;
	}
}

void interpolador2d(float ang_min, float ang_med, float ang_max, vec2 cant_min, vec2 cant_med, vec2 cant_max, float angulo, inout vec2 result1, inout vec2 result2){
	if(angulo <= ang_med){
		result1 = cant_min + (angulo - ang_min)*(cant_med - cant_min)/(ang_med - ang_min);
		result2 = cant_min + (angulo - ang_min)*(cant_max - cant_min)/(ang_max - ang_min);
	}
	else{
		result1 = cant_med + (angulo - ang_med)*(cant_max - cant_med)/(ang_max - ang_med);
		result2 = cant_min + (angulo - ang_min)*(cant_max - cant_min)/(ang_max - ang_min);
	}
	vec2 aux;
	if(outside == 1){
		aux = result1;
		result1 = result2;
		result2 = aux;
	}
}

int determine_side(vec2 A, vec2 B, vec2 C){
	//determines whether the triangle is facing in or out. 
	vec2 versor1, versor2, pos_min, pos_med, pos_max, posiciones[3];
	vec2 A_p, B_p, C_p;
	A_p = normalize(A)*alfas[0];
	B_p = normalize(B)*alfas[1];
	C_p = normalize(C)*alfas[2];

	posiciones[0] = A_p;
	posiciones[1] = B_p;
	posiciones[2] = C_p;
	pos_min = posiciones[idx_min];
	pos_med = posiciones[idx_med];
	pos_max = posiciones[idx_max];
	
	versor1 = normalize(pos_max - pos_min);
	versor2.x = -versor1.y;
	versor2.y =  versor1.x;
	float coord1 = dot(pos_med      - pos_min, versor2);
	float coord2 = dot(vec2(0., 0.) - pos_min, versor2);
	if(coord1*coord2 <= 0.)
		return 0;
	else
		return 1;
}

vec3 determine_criteria(vec2 A, vec2 B, vec2 C){
	vec3 criteria;
	float angle, cutoff = 5*pi/180., fraction = 3*pi/4.;
	bool is_I_type, is_acute;

	// Triangle A-B
	is_I_type = (tipo2[0] == 1.)&&(tipo2[1] == 1.);
	is_acute  = (phi[0] <= fraction)&&(phi[1] <= fraction);
	if(is_I_type&&is_acute)
		criteria.x = 0.;
	else{
		angle = det_subtended_angle(A, B);
		if(angle > cutoff)
			criteria.x = 1.;
		else
			criteria.x = 0.;
	}
	// Triangle B-C
	is_I_type = (tipo2[1] == 1.)&&(tipo2[2] == 1.);
	is_acute  = (phi[1] <= fraction)&&(phi[2] <= fraction);
	if(is_I_type&&is_acute)
		criteria.y = 0.;
	else{
		angle = det_subtended_angle(B, C);
		if(angle > cutoff)
			criteria.y = 1.;
		else
			criteria.y = 0.;
	}
	// Triangle C-A
	is_I_type = (tipo2[2] == 1.)&&(tipo2[0] == 1.);
	is_acute  = (phi[2] <= fraction)&&(phi[0] <= fraction);
	if(is_I_type&&is_acute)
		criteria.z = 0.;
	else{
		angle = det_subtended_angle(C, A);
		if(angle > cutoff)
			criteria.z = 1.;
		else
			criteria.z = 0.;
	}
	return criteria;
}
void emisor(vec3 vert1, vec3 vert2, vec3 vert3, vec2 tex1, vec2 tex2, vec2 tex3, float T1, float T2, float T3){
	// Emits a triangle
//   	color = vec4(angulo/6.28, angulo/6.28, angulo/6.28, 1.);
	
	if(adentro == 1)
		color = vec3(1., 0., 0.);
	else 
		color = vec3(0.5, 0.5, 0.7);
	
 	texcoord = tex1;
	dist     = T1;
	gl_Position    = vec4(vert1, -vert1.z);
// 	gl_Position.z *= vert1.z;
	EmitVertex();

	texcoord = tex2;
	dist     = T2;
	gl_Position    = vec4(vert2, -vert2.z);
// 	gl_Position.z *= vert2.z;
	EmitVertex();

	texcoord = tex3;
	dist     = T3;
	gl_Position    = vec4(vert3, -vert3.z);
// 	gl_Position.z *= vert3.z;
	EmitVertex();
	EndPrimitive();
}

float simple_interpolation_1D(float ang1, float ang2, float quan1, float quan2, float ang){
	if(ang2 - ang1 == 0.)
		return quan1;
	else
		return quan1 + (ang - ang1)*(quan2 - quan1)/(ang2 - ang1);
}
vec2 simple_interpolation_2D(float ang1, float ang2, vec2 quan1, vec2 quan2, float ang){
	if(ang2 - ang1 == 0.)
		return quan1;
	else
		return quan1 + (ang - ang1)*(quan2 - quan1)/(ang2 - ang1);
}
vec3 simple_interpolation_3D(float ang1, float ang2, vec3 quan1, vec3 quan2, float ang){
	if(ang2 - ang1 == 0.)
		return quan1;
	else
		return quan1 + (ang - ang1)*(quan2 - quan1)/(ang2 - ang1);
}


void render_centred_triangle(vec2 A, vec2 B, vec2 C){
	int N, idx_min, idx_med, idx_max, idx_alphamin;
	float alpha_min, angulos[3], angulo1, angulo2, angulo3, ang_min, ang_med, ang_max, T_a, T_b, T_c, T_d, ang_a, ang_b, ang_c, ang_d;
	vec3 pos_min, pos_med, pos_max, a, b, c, d;
	vec2 tex_a, tex_b, tex_c, tex_d;

	N = int(2*pi/dtheta);
	alpha_min = min(alfas[0], min(alfas[1], alfas[2]));

	if(alfas[0] == alpha_min)
		idx_alphamin = 0;
	else if(alfas[1] == alpha_min)
		idx_alphamin = 1;
	else
		idx_alphamin = 2;

	angulo1 = angulo(A, 0);
	angulo2 = angulo(B, 0);
	angulo3 = angulo(C, 0);
	
	angulos[0] = angulo1;
	angulos[1] = angulo2;
	angulos[2] = angulo3;

	ordenador(angulo1, angulo2, angulo3, idx_min, idx_med, idx_max);

	pos_min = cons_pos[idx_min];
	pos_med = cons_pos[idx_med];
	pos_max = cons_pos[idx_max];
	ang_min = angulos[idx_min];
	ang_med = angulos[idx_med];
	ang_max = angulos[idx_max];


	ang_a = alpha_min;
	ang_b = simple_interpolation_1D(ang_min + 2*pi, ang_max, alfas[idx_min],    alfas[idx_max], 0.);

	T_a = T[idx_alphamin];
	T_b   = simple_interpolation_1D(ang_min + 2*pi, ang_max, T[idx_min],    T[idx_max], 0.);
	
	tex_a = TexCoord[idx_alphamin];
	tex_b = simple_interpolation_2D(ang_min + 2*pi, ang_max, TexCoord[idx_min],    TexCoord[idx_max], 0.);

	a = transformacion2(i*dtheta, ang_a, T_a/100.);
	b = transformacion2(i*dtheta, ang_b, T_b/100.);

	for(i = 1; i < N; i++){
		if(i*dtheta <= ang_min){
			ang_d = simple_interpolation_1D(ang_min + 2*pi, ang_max,    alfas[idx_min],       alfas[idx_max], i*dtheta);
			T_d   = simple_interpolation_1D(ang_min + 2*pi, ang_max,        T[idx_min],           T[idx_max], i*dtheta);
			tex_d = simple_interpolation_2D(ang_min + 2*pi, ang_max, TexCoord[idx_min],    TexCoord[idx_max], i*dtheta);
		}
		else if(i*dtheta <= ang_med){
			ang_d = simple_interpolation_1D(ang_min, ang_med,    alfas[idx_min],       alfas[idx_med], i*dtheta);
			T_d   = simple_interpolation_1D(ang_min, ang_med,        T[idx_min],           T[idx_med], i*dtheta);
			tex_d = simple_interpolation_2D(ang_min, ang_med, TexCoord[idx_min],    TexCoord[idx_med], i*dtheta);
		}
		else{
			ang_d = simple_interpolation_1D(ang_med, ang_max,    alfas[idx_med],       alfas[idx_max], i*dtheta);
			T_d   = simple_interpolation_1D(ang_med, ang_max,        T[idx_med],           T[idx_max], i*dtheta);
			tex_d = simple_interpolation_2D(ang_med, ang_max, TexCoord[idx_med],    TexCoord[idx_max], i*dtheta);
		}
		
		c = transformacion2(i*dtheta, ang_c, T_c/100.);
		d = transformacion2(i*dtheta, ang_d, T_d/100.);
		emisor(a, b, c, tex_a, tex_b, tex_c, T_a, T_b, T_c);
		emisor(b, c, d, tex_b, tex_c, tex_d, T_b, T_c, T_d);

		a = c;
		b = d;
		tex_a = tex_c;
		tex_b = tex_d;
		T_a   = T_c;
		T_b   = T_d;
	}
}



void main(){
	vec3 vert1, vert2, vert3, criteria;
	vec2 A, B, C;

	float angulo1, angulo2, angulo3;
	
	versor0 = esf2xyz(posicion.yzw);

	vert1 = transformacion(cons_pos[0], alfas[0], T[0]/100.);
	vert2 = transformacion(cons_pos[1], alfas[1], T[1]/100.);
	vert3 = transformacion(cons_pos[2], alfas[2], T[2]/100.);

	det_phi_values(esf2xyz(cons_pos[0]), versor0, 0);
	det_phi_values(esf2xyz(cons_pos[1]), versor0, 1);
	det_phi_values(esf2xyz(cons_pos[2]), versor0, 2);

	three_2_two(cons_pos[0], cons_pos[1], cons_pos[2], A, B, C);
	adentro  = in_triangle(vec2(0., 0.), A, B, C);
	criteria = determine_criteria(A, B, C);

	tipo    = det_tipo(A, B, C);
	angulo1 = angulo(A, tipo);
	angulo2 = angulo(B, tipo);
	angulo3 = angulo(C, tipo);
	
	float ang_min, ang_med, ang_max, angulos[3];
	angulos[0] = angulo1;
	angulos[1] = angulo2;
	angulos[2] = angulo3;
	
	vec3 pos_min, pos_med, pos_max, a, b, c, d;
	ordenador(angulo1, angulo2, angulo3, idx_min, idx_med, idx_max);
	
	pos_min = cons_pos[idx_min];
	pos_med = cons_pos[idx_med];
	pos_max = cons_pos[idx_max];
	ang_min = angulos[idx_min];
	ang_med = angulos[idx_med];
	ang_max = angulos[idx_max];

	n1 = int(ang_min/dtheta);
	n2 = int(ang_med/dtheta);
	n3 = int(ang_max/dtheta);
	
	a = transformacion(pos_min, alfas[idx_min], T[idx_min]/100.);
	b = a;
	outside = determine_side(A, B, C);

	float ang_c, ang_d, T_c, T_d, T_a, T_b;
	vec2  tex_a, tex_b, tex_c, tex_d;

	//Determine criteria to decide whether to amplify geometry or not
	int criterio = int(criteria.x + criteria.y + criteria.z);
	if(criterio >= 1)
		criterio = 1;

	if((adentro != 1)&&(criterio == 1)){
		tex_a = TexCoord[idx_min];
		tex_b = tex_a;
		T_a   = T[idx_min];
		T_b   = T_a;
		for(i = n1 + 1; i <= n3 + 1; i++){
			counter++;
			if(i == n2 + 1){
				c = transformacion(pos_med, alfas[idx_med], T[idx_med]/100.);
				d = c;
				tex_c = TexCoord[idx_med];
				T_c   = T[idx_med];
				tex_d = tex_c;
				T_d   = T_c;
				emisor(a, b, c, tex_a, tex_b, tex_c, T_a, T_b, T_c);

				if(outside == 0){
					a = c;
					tex_a = tex_c;
					T_a   = T_c;
				}
				else{
					b = c;
					tex_b = tex_c;
					T_b   = T_c;
				}
			}
			if(i == n3 + 1){
				c = transformacion(pos_max, alfas[idx_max], T[idx_max]/100.);
				d = c;
				tex_c = TexCoord[idx_max];
				tex_d = tex_c;
				T_c   = T[idx_max];
				T_d   = T_c;
				emisor(a, b, c, tex_a, tex_b, tex_c, T_a, T_b, T_c);
				emisor(b, c, d, tex_b, tex_c, tex_d, T_b, T_c, T_d);
				a = c;
				b = d;
				tex_a = tex_c;
				tex_b = tex_d;
				T_a   = T_c;
				T_b   = T_d;
			}
			else{
				interpolador1d(ang_min, ang_med, ang_max,    alfas[idx_min],    alfas[idx_med],    alfas[idx_max], i*dtheta, ang_c, ang_d);
				interpolador1d(ang_min, ang_med, ang_max,        T[idx_min],        T[idx_med],        T[idx_max], i*dtheta,   T_c,   T_d);
				interpolador2d(ang_min, ang_med, ang_max, TexCoord[idx_min], TexCoord[idx_med], TexCoord[idx_max], i*dtheta, tex_c, tex_d);

				c = transformacion2(i*dtheta, ang_c, T_c/100.);
				d = transformacion2(i*dtheta, ang_d, T_d/100.);
				emisor(a, b, c, tex_a, tex_b, tex_c, T_a, T_b, T_c);
				emisor(b, c, d, tex_b, tex_c, tex_d, T_b, T_c, T_d);
				a = c;
				b = d;
				tex_a = tex_c;
				tex_b = tex_d;
				T_a   = T_c;
				T_b   = T_d;
			}
		}
	}
	else if(criterio == 0)
		emisor(vert1, vert2, vert3, TexCoord[0], TexCoord[1], TexCoord[2], T[0], T[1], T[2]);
	if(adentro == 1)
		render_centred_triangle(A, B, C);
}
