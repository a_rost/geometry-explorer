#version 440 core

in vec3 color;
in vec2 texcoord;
in float dist;
out vec4 FragColor;

layout(binding=0) uniform sampler2D ourTexture;

void main(){
	
	FragColor = texture(ourTexture, texcoord); 
// 	FragColor.xyz = color;
// 	FragColor.x = 1.;
// 	FragColor.xyz = vec3(dist, dist, dist)/10.;
	gl_FragDepth = dist/100.;
}
