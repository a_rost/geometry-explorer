#version 330 core

in vec2 tex_coord;
in float distancia;
// in float intensidad;
in vec3 reflex1;
in vec3 reflex2;
in vec3 incidencia;
in float coseno1;
in float coseno2;

out vec4 FragColor;
//out float gl_FragDepth;

uniform sampler2D ourTexture;
uniform float contrast;

float tau = 0.1;

float phong(vec3 reflex, vec3 obs, float potencia){
	float coseno = dot(reflex, obs);
	if(coseno < 0.)
		coseno = 0.;
	return 0.3 + pow(coseno, potencia);
}

void main(){
	gl_FragDepth = distancia;
	float intensidad2 = exp(-distancia/tau)*(phong(normalize(reflex1), normalize(incidencia), 20.)*1. + phong(normalize(reflex2), normalize(incidencia),20.)*1.)*contrast*0.;
	
 	intensidad2 += exp(-distancia/tau)*(coseno1 + coseno2)*contrast;
	FragColor = texture(ourTexture, tex_coord)*intensidad2;
//   	FragColor.xyz = vec3(1.)*intensidad2;
	FragColor.w = 1.;
}
