#version 440 core

layout(std430, binding = 3) buffer layoutName
{
	float data_SSBO[];
};
 
in vec3  cons_pos;
in float alfa;
in float T2;
in float indice_float;
in vec2  TexCoord;

out vec2  tex_coord;
out float distancia;
// out float intensidad;
out vec3 reflex1;
out vec3 reflex2;
out vec3 incidencia;
out float coseno1;
out float coseno2;

uniform int stride;
uniform vec4 posicion;
uniform mat4 vectores;
uniform mat3 base;
uniform float aspect_ratio;

const float pi = 3.1415926535;
vec3 versor0;
mat4 vectores2;
// int stride = 0;

float metrica_r(float x){
	return 5.*sin(x/5.);
}


vec3 geodesica4(vec3 pos, float alfa){
	vec3 versor1, versor_perp, vector_esf, versor_xyz;
	float modulo, coseno;
	int k;

	versor1[0] = sin(pos[1])*cos(pos[2]);
	versor1[1] = sin(pos[1])*sin(pos[2]);
	versor1[2] = cos(pos[1]);

	coseno = dot(versor0, versor1);
	
	versor_perp  = versor1 - coseno*versor0;
	
	modulo = sqrt(dot(versor_perp, versor_perp));

	versor_perp *= 1./modulo;

	versor_xyz = -cos(alfa)*versor0 + sin(alfa)*versor_perp;
	
	vector_esf = versor_xyz*base;
	return vector_esf;
}

vec3 geodesica4_bis(vec3 pos, vec3 pos2, float distancia){
	vec3 versor1, versor_perp, vector_esf, versor_xyz;
	float modulo, coseno, D, alfas;
	int k;

	mat3 base2;
	base2[0][0] =  sin(pos[1])*cos(pos[2]);
	base2[0][1] =  sin(pos[1])*sin(pos[2]);
	base2[0][2] =  cos(pos[1]);

	base2[1][0] =  cos(pos[1])*cos(pos[2]);
	base2[1][1] =  cos(pos[1])*sin(pos[2]);
	base2[1][2] = -sin(pos[1]);

	base2[2][0] = -sin(pos[2]);
	base2[2][1] =  cos(pos[2]);
	base2[2][2] =  0.;

	versor1[0] = sin(pos[1])*cos(pos[2]);
	versor1[1] = sin(pos[1])*sin(pos[2]);
	versor1[2] = cos(pos[1]);

	coseno = dot(versor0, versor1);
	
	versor_perp  = versor0 - coseno*versor1;
	
	modulo = sqrt(dot(versor_perp, versor_perp));

	versor_perp *= 1./modulo;

	D     = acos(cos(pos[0]/5.)*cos(pos2[0]/5.) + sin(pos[0]/5.)*sin(pos2[0]/5.)*coseno)*5.;
	alfas = acos((cos(pos2[0]/5.) - cos(pos[0]/5.)*cos(D/5.))/(sin(pos[0]/5.)*sin(D/5.)));

	if(D != distancia){
		D = 2*pi*5. - D;
		alfas += pi;
	}
	versor_xyz = -cos(alfas)*versor1 + sin(alfas)*versor_perp;
	vector_esf = versor_xyz*base2;
	return normalize(vector_esf);
}

vec4 raytracer(vec3 versor_u){
	vec4 cuadrivector_u, cuadrivector_U;
	int k;

	cuadrivector_u[0] = -1.;
	cuadrivector_u[1] = versor_u[0];
	cuadrivector_u[2] = versor_u[1];
	cuadrivector_u[3] = versor_u[2];

	cuadrivector_U = cuadrivector_u*vectores2;
	return cuadrivector_U;
}



vec3 transformacion(vec3 pos, float alfa, float distancia){
	vec3 vector_esf, vector;
	vec4 cuadrivector;
	float norma;

	vector_esf = geodesica4(pos, alfa);
	cuadrivector = raytracer(vector_esf);

	norma = sqrt(dot(cuadrivector.yzw, cuadrivector.yzw));
	
	vector.xyz = -distancia*cuadrivector.zwy/norma;
	return vector;
}

vec3 vert1;
float phong(vec3 reflex, vec3 obs, float potencia){
	float coseno = dot(reflex, obs);
	if(coseno < 0.)
		coseno = 0.;
	return 0.3 + pow(coseno, potencia);
}

void main(){
	int k, indice;
	indice = int(indice_float + 0.1);
//  	float coseno1, coseno2;
 	coseno1 = data_SSBO[4*2*stride + 2*4*indice + 3];
 	coseno2 = data_SSBO[4*2*stride + 2*4*indice + 7];
	
 	if(coseno1 <= 0.)
 		coseno1 = 0.;
 	if(coseno2 <= 0.)
 		coseno2 = 0.;
//  	intensidad = (coseno1 + coseno2)/2.*0.;
	//Cantidades para el procesamiento	
	tex_coord = TexCoord;
	versor0[0] = sin(posicion[2])*cos(posicion[3]);
	versor0[1] = sin(posicion[2])*sin(posicion[3]);
	versor0[2] = cos(posicion[2]);
	
	for(k = 0; k < 4; k++){
		vectores2[k][0]   = -vectores[k][0];
		vectores2[k][1]   =  vectores[k][1];
		vectores2[k][2]   =  vectores[k][2]*metrica_r(posicion[1]);
		vectores2[k][3]   =  vectores[k][3]*metrica_r(posicion[1])*sin(posicion[2]);
	}
	for(k = 0; k < 4; k++) 
		vectores2[0][k] *= -1.;
// 	vec3 reflex1, reflex2;
	reflex1.x = data_SSBO[4*2*stride + 2*4*indice + 0];
	reflex1.y = data_SSBO[4*2*stride + 2*4*indice + 1];
	reflex1.z = data_SSBO[4*2*stride + 2*4*indice + 2];
	reflex2.x = data_SSBO[4*2*stride + 2*4*indice + 4];
	reflex2.y = data_SSBO[4*2*stride + 2*4*indice + 5];
	reflex2.z = data_SSBO[4*2*stride + 2*4*indice + 6];
	
	incidencia = geodesica4_bis(cons_pos, posicion.yzw, T2);
	vert1 = transformacion(cons_pos, alfa, T2/100.);
//  	intensidad += (phong(reflex1, incidencia, 20.)*coseno1 + phong(reflex2, incidencia, 20.)*coseno2)/2.;
	vec3 aPos3 = vert1;
	float w = -vert1.z;
	distancia = sqrt(dot(aPos3, aPos3));
	gl_Position    = vec4(vert1.xyz, w);
	gl_Position.z *= -w;
 	gl_Position.y *= aspect_ratio;
}
