#version 330 core

in vec2 tex_coord;
in float P_min;
in vec3 aPos3;
in float eliminar;
in vec4 color;
in float render;
out vec4 FragColor;

uniform sampler2D ourTexture;
uniform vec4 worm_position;
uniform vec4 posicion;
float tau = 0.7;
float metrica_r(float x){
	if(x >= 2.)
		return x;
	else if(x >= -2.)
		return 1. + 0.25*x*x;
	else
		return -x;
}

void main(){
	float coseno, angulo, modulo, P, pi = 3.14159265;
	vec3 aPos;
//  	if(render == 1.)
		FragColor = texture(ourTexture, tex_coord)*exp(-sqrt(dot(aPos3, aPos3))/tau)+color*0.;
// 	else
// 		FragColor = texture(ourTexture, tex_coord)*0.+color*1.;
	
	FragColor.w = 1.;
	modulo = sqrt(dot(aPos3, aPos3));
	aPos = aPos3/modulo;

	modulo = sqrt(dot(worm_position.xyz, worm_position.xyz));

// 	aPos.x *= 1./modulo;
// 	aPos.y *= 1./modulo;
// 	aPos.z *= 1./modulo;
//	FragColor = vec4(0.7, P_min/5., 0.9, 1.);

	coseno = dot(worm_position.xyz/modulo, aPos);
// 	coseno = worm_position[0]*aPos[0] + worm_position[1]*aPos[1] + worm_position[2]*aPos[2];
	angulo = acos(coseno);
	P = abs(sin(angulo))*metrica_r(posicion[1]);
//  	FragColor.xyz = color;
 	if((P < P_min - 0.005)&&(eliminar == 1.))
 		discard;
	if(eliminar == 2.)
		discard;
}
