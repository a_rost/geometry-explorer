#version 330 core
in vec3  color;
in float alfa;
in vec3  cons_pos;
in float T2;

out vec3  Color;
out float distancia;
// out float radio;

uniform vec4 posicion;
uniform mat4 vectores;
uniform mat3 base;
uniform float aspect_ratio;

vec3 versor0;
mat4 vectores2;

float metrica_r(float x){
	return 5.*sinh(x/5.);
}


vec3 geodesica4(vec3 pos, float alfa){
	vec3 versor1, versor_perp, versor_xyz, vector_esf;
	float modulo, coseno;
	int k;

	versor1[0] = sin(pos[1])*cos(pos[2]);
	versor1[1] = sin(pos[1])*sin(pos[2]);
	versor1[2] = cos(pos[1]);

	coseno = dot(versor0, versor1);
	
	versor_perp  = versor1 - coseno*versor0;
	
	modulo = sqrt(dot(versor_perp, versor_perp));

	versor_perp *= 1./modulo;

	versor_xyz = -cos(alfa)*versor0 + sin(alfa)*versor_perp;
	
	vector_esf = versor_xyz*base;
	return vector_esf;
}

vec4 raytracer(vec3 versor_u){
	vec4 cuadrivector_u, cuadrivector_U;
	int k;

	cuadrivector_u[0] = -1.;
	cuadrivector_u[1] = versor_u[0];
	cuadrivector_u[2] = versor_u[1];
	cuadrivector_u[3] = versor_u[2];

	cuadrivector_U = cuadrivector_u*vectores2;
	return cuadrivector_U;
}



vec3 transformacion(vec3 pos, float alfa, float distancia){
	vec3 vector_esf, vector;
	vec4 cuadrivector;
	float norma;

	vector_esf = geodesica4(pos, alfa);
	cuadrivector = raytracer(vector_esf);

	norma = sqrt(dot(cuadrivector.yzw, cuadrivector.yzw));
	
	vector.xyz = -distancia*cuadrivector.zwy/norma;
	return vector;
}

vec3 vert1;

void main(){
	int k;
	Color = color;
	//Cantidades para el procesamiento	
// 	tex_coord = TexCoord;
	versor0[0] = sin(posicion[2])*cos(posicion[3]);
	versor0[1] = sin(posicion[2])*sin(posicion[3]);
	versor0[2] = cos(posicion[2]);
	
	for(k = 0; k < 4; k++){
		vectores2[k][0]   = -vectores[k][0];
		vectores2[k][1]   =  vectores[k][1];
		vectores2[k][2]   =  vectores[k][2]*metrica_r(posicion[1]);
		vectores2[k][3]   =  vectores[k][3]*metrica_r(posicion[1])*sin(posicion[2]);
	}
	for(k = 0; k < 4; k++) 
		vectores2[0][k] *= -1.;

	vert1 = transformacion(cons_pos, alfa, T2/100.);

	vec3 aPos3 = vert1;
	float w = -vert1.z;
	distancia = sqrt(dot(aPos3, aPos3));
// 	radio = 0.003/(metrica_r(distancia)*metrica_r(distancia));
	gl_PointSize = 0.003/(metrica_r(distancia)*metrica_r(distancia));

	gl_Position    = vec4(vert1.xyz, w);
	gl_Position.z *= -w;
 	gl_Position.y *= aspect_ratio;
}
