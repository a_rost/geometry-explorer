struct object_data{
	unsigned int n_vert;
	float *vertices;
	unsigned int n_triang;
	unsigned int *triangulos;
	float *xyz_coord;
	float *xyz_normales;
	float *normales;
	float *u;
	float *v;
	float *light_reflex;
	float *light_intensity;

	//posicion de luz usada para el objeto
	float light_position[4];
    unsigned int texture;
};

struct particula_data {
	unsigned char moving;
	unsigned int n_vert;
	float *color;
	float *velocity;
	float *vertices;
};

struct particula_movil{
	unsigned int n_vert;
	float *color;
	float *velocity;
	float *vertices;
};

