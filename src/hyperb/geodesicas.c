#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <GL/glew.h>
#include "parametros.h"
#include "metrica.h"
#include "elemental.h"
#include "structs.h"

unsigned int fracasos;


static inline void geodesica_xyz2esf(float x, float y, float z, float *theta, float *phi){  
  const float r = sqrtf((powf(x,2) + powf(y,2) + powf(z,2)));
  *theta  = acosf(z/r)*180.f/pi;
  *phi    = fmodf(atan2f(y, x)*180.f/pi+360.f, 360.f);
}

static inline void geodesica_esf2xyz(float r, float theta, float phi, float *x, float *y, float *z){  
  *x = r*sinf(theta*pi/180.f)*cosf(phi*pi/180.f);
  *y = r*sinf(theta*pi/180.f)*sinf(phi*pi/180.f);
  *z = r*cosf(theta*pi/180.f);
}

void geodesica_cambio_base(float posicion[4], float vector_xyz[3], float *vector_esf, unsigned int shift){
	unsigned int k;
   float base[3][3] = {{sinf(posicion[2])*cosf(posicion[3]),   sinf(posicion[2])*sinf(posicion[3]),    cosf(posicion[2])},

                       {cosf(posicion[2])*cosf(posicion[3]),   cosf(posicion[2])*sinf(posicion[3]),   -sinf(posicion[2])},

                       {-sinf(posicion[3]),                                      cosf(posicion[3]),                  0.f}};

	vector_esf[3*shift + 0]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 0] += base[0][k]*vector_xyz[k];

	vector_esf[3*shift + 1]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 1] += base[1][k]*vector_xyz[k];

	vector_esf[3*shift + 2]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 2] += base[2][k]*vector_xyz[k];

}

float rho1, rho2, phi2;

void geodesica_solution_set(float posicion0[4], float posicion1[4], float versor0[3], float versor_perp[3], float posicion0_original[4],
							float phi, float alfa, unsigned int j, unsigned int tipo_I, float *versores, float *D, float *solution_type){
	float versor_xyz[3], P;
 	versor_xyz[0] = -cosf(alfa)*versor0[0] + sinf(alfa)*versor_perp[0];
	versor_xyz[1] = -cosf(alfa)*versor0[1] + sinf(alfa)*versor_perp[1];
 	versor_xyz[2] = -cosf(alfa)*versor0[2] + sinf(alfa)*versor_perp[2];
 	geodesica_cambio_base(posicion0_original, versor_xyz, versores, j);
	P = fabsf(sinf(alfa))*metrica_r(posicion0[0]);
	if(tipo_I == 1){
		D[j] = geodesica_I_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 1.f;
	}
	else{
		D[j] = geodesica_G_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 2.f;
	}
}

void geodesica_solution_set2(float posicion0[4], float posicion1[4], float alfa, unsigned int j, 
							 unsigned int tipo_I, float *D, float *solution_type){
	float P;
	P = fabsf(sinf(alfa))*metrica_r(posicion0[0]);
	if(tipo_I == 1){
		D[j] = geodesica_I_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 1.f;
	}
	else{
		D[j] = geodesica_G_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 2.f;
	}
}

void geodesica_geodesica4(float posicion0_original[4], float posicion1[3], float *D, float *alfas, float *solution_type,
							float *versores){
	float versor0[3], versor1[3], versor_perp[3], versor_xyz[3], dot, alfa, modulo, posicion0[3];
	posicion0[0] = posicion0_original[1];
	posicion0[1] = posicion0_original[2];
	posicion0[2] = posicion0_original[3];

	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2];

	versor_perp[0]  = versor1[0] - coseno*versor0[0];
	versor_perp[1]  = versor1[1] - coseno*versor0[1];
	versor_perp[2]  = versor1[2] - coseno*versor0[2];
	modulo          = sqrtf(versor_perp[0]*versor_perp[0] + versor_perp[1]*versor_perp[1] + versor_perp[2]*versor_perp[2]);
	versor_perp[0] *= 1.f/modulo;
	versor_perp[1] *= 1.f/modulo;
	versor_perp[2] *= 1.f/modulo;

	unsigned int exito, exito_sum = 0, exito_I;


	float phi = acosf(coseno);


	
	D[0]     = acosf(cosf(posicion0[0]/metrica_R)*cosf(posicion1[0]/metrica_R) + sinf(posicion0[0]/metrica_R)*sinf(posicion1[0]/metrica_R)*coseno)*metrica_R;
	alfas[0] = acosf((cosf(posicion1[0]/metrica_R) - cosf(posicion0[0]/metrica_R)*cosf(D[0]/metrica_R))/(sinf(posicion0[0]/metrica_R)*sinf(D[0]/metrica_R)));

	

	alfa = alfas[0];

	if(D[0] <= pi*metrica_R){
		alfas[1] = fmodf(alfa + pi, 2*pi);
		D[1]     = 2*pi*metrica_R - D[0];
	}
	else{
		alfas[1] = alfa;
		D[1] = D[0];
		alfas[0] = fmodf(alfa + pi, 2*pi);
		D[0]     = 2*pi*metrica_R - D[1];
	}

	geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0_original, 0*pi + phi, alfas[0], 0, exito_I, versores, D, solution_type);
	geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0_original, 2*pi - phi, alfas[1], 1, exito_I, versores, D, solution_type);
}

void geodesica_geodesica5(float posicion0_original[4], float posicion1[3], float *D, float *alfas, float *solution_type){
	float versor0[3], versor1[3], versor_perp[3], versor_xyz[3], dot, alfa, modulo, posicion0[3];
	posicion0[0] = posicion0_original[1];
	posicion0[1] = posicion0_original[2];
	posicion0[2] = posicion0_original[3];

	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2];

	unsigned int exito, exito_sum, exito_I;


    float phi = acosf(coseno);

	
	
	/*
	exito_I = 1;
	exito_sum = 0;
	geodesica_raiz_I(posicion0[0], posicion1[0], phi, &alfa, &exito);
	exito_sum += exito;
	if(exito_sum == 0){
		geodesica_raiz_G(posicion0[0], posicion1[0], phi, &alfa, &exito);
		exito_sum += exito;
		exito_I = 0;
	}
  	if(exito_sum != 1)
 		printf("exito total = %d, de I = %d, alfa = %f, D = %f, rho1 = %f, rho2 = %f, phi = %f\n", exito_sum, exito_I, alfa, D[0], posicion0[0], posicion1[0], phi);
	alfas[0] = alfa;
	geodesica_solution_set2(posicion0, posicion1, alfa, 0, exito_I, D, solution_type);

	*/

 	D[0]     = acoshf(coshf(posicion0[0]/metrica_R)*coshf(posicion1[0]/metrica_R) - sinhf(posicion0[0]/metrica_R)*sinhf(posicion1[0]/metrica_R)*coseno)*metrica_R;
 	alfas[0] = acosf(-(coshf(posicion1[0]/metrica_R) - coshf(posicion0[0]/metrica_R)*coshf(D[0]/metrica_R))/(sinhf(posicion0[0]/metrica_R)*sinhf(D[0]/metrica_R)));
	
	alfa = 0.f;
// 	alfa = alfas[0];
// 	if(D[0] <= pi*metrica_R){
// 		alfas[1] = fmodf(alfa + pi, 2*pi);
// 		D[1]     = 2*pi*metrica_R - D[0];
// 	}
// 	else{
// 		alfas[1] = alfa;
// 		D[1] = D[0];
// 		alfas[0] = fmodf(alfa + pi, 2*pi);
// 		D[0]     = 2*pi*metrica_R - D[1];
// 	}
	
//  	printf("alfa 1 = %f, alfa2 = %f, D1 = %f, D2 = %f\n", alfas[0], alfas[1], D[0], D[1]);
	
/*
	exito_I = 1;
	exito_sum = 0;
	geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
	exito_sum += exito;
	if(exito_sum == 0){
		geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		exito_sum += exito;
		exito_I = 0;
	}
// 	printf("exito total 2 = %d, de I = %d, alfa = %f\n", exito_sum, exito_I, alfa);
	alfas[1] = alfa;
	geodesica_solution_set2(posicion0, posicion1, alfa, 1, exito_I, D, solution_type);
*/
}


void geodesica_ray_tracer(float posicion[4], float *versor_u, unsigned int shift, float vectores[4][4],
				float *cuadrivector_U, unsigned char *visible){
  float cuadrivector_u[4], numero, vectores2[4][4], alfa, D1, aux1, aux2, intensidad;
//   float cuadrivector_U[4];  //cambio de coordenadas

  cuadrivector_u[0] = -1.f;
  cuadrivector_u[1] = versor_u[3*shift + 0];
  cuadrivector_u[2] = versor_u[3*shift + 1];
  cuadrivector_u[3] = versor_u[3*shift + 2];

  for(unsigned int k = 0; k < 4; k++){
    vectores2[k][0]   = -vectores[k][0];
    vectores2[k][1]   =  vectores[k][1];
    vectores2[k][2]   =  vectores[k][2]*metrica_r(posicion[1]);
    vectores2[k][3]   =  vectores[k][3]*metrica_r(posicion[1])*sinf(posicion[2]);
  }
  for(unsigned int k=0; k<4; k++)
    vectores2[0][k] *= -1.f;

  cuadrivector_U[0] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[0] += vectores2[0][k]*cuadrivector_u[k];

  cuadrivector_U[1] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[1] += vectores2[1][k]*cuadrivector_u[k];

  cuadrivector_U[2] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[2] += vectores2[2][k]*cuadrivector_u[k];
  
  cuadrivector_U[3] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[3] += vectores2[3][k]*cuadrivector_u[k];


  //numero = fabsf(cuadrivector_U[1]);
  numero = 1.f;
}




unsigned int interactivo;

const unsigned int n_atrib = 7, v_info2 = 8, v_info = 2;//, n_atrib_shading = 18;


unsigned int asdfasdf = 0;
void geodesica_dibujador_objeto3(float posicion[4], float vectores[4][4], 
								 struct object_data objeto, float *vertx_data, unsigned char init){

//	unsigned int T[3];
	unsigned char visible;
	
#pragma omp parallel num_threads(4)
{
	float *distancia_fuente, vertice[3], *alfas, *solution_type;
	distancia_fuente = (float *) malloc(2*sizeof(float));
	alfas            = (float *) malloc(2*sizeof(float));
	solution_type    = (float *) malloc(2*sizeof(float));
	
	#pragma omp for schedule(dynamic, 20)
	for(unsigned int i = 0; i < objeto.n_vert; i++){
        vertice[0] = objeto.vertices[3*i + 0];
        vertice[1] = objeto.vertices[3*i + 1];
        vertice[2] = objeto.vertices[3*i + 2];

		geodesica_geodesica5(posicion, vertice, distancia_fuente, alfas, solution_type);
//  		printf("alfas = (%f, %f), distancias = (%f, %f)\n", alfas[0], alfas[1], distancia_fuente[0], distancia_fuente[1]);

		
		vertx_data[2*v_info*i + v_info*0 + 0] = alfas[0];
		vertx_data[2*v_info*i + v_info*0 + 1] = distancia_fuente[0];

		vertx_data[2*v_info*i + v_info*1 + 0] = alfas[1];
		vertx_data[2*v_info*i + v_info*1 + 1] = distancia_fuente[1];
	}
	free(alfas);
	free(distancia_fuente);
	free(solution_type);
}
}


void geodesica_dibujador_puntos(float posicion[4], float vectores[4][4], 
								 struct particula_data objeto, float *vertx_data){
	
#pragma omp parallel num_threads(4)
{
	float valor, *distancia_fuente, vertice[3], color[3], *alfas, *solution_type;
	distancia_fuente = (float *) malloc(2*sizeof(float));
	alfas            = (float *) malloc(2*sizeof(float));
	solution_type    = (float *) malloc(2*sizeof(float));
	
	#pragma omp for schedule(dynamic, 20)
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertice[0] = objeto.vertices[3*i + 0];
		vertice[1] = objeto.vertices[3*i + 1];
		vertice[2] = objeto.vertices[3*i + 2];

		color[0]   = objeto.color[3*i + 0];
		color[1]   = objeto.color[3*i + 1];
		color[2]   = objeto.color[3*i + 2];
		
		geodesica_geodesica5(posicion, vertice, distancia_fuente, alfas, solution_type);
		vertx_data[2*v_info2*i + v_info2*0 + 0] = color[0];
		vertx_data[2*v_info2*i + v_info2*0 + 1] = color[1];
		vertx_data[2*v_info2*i + v_info2*0 + 2] = color[2];
		vertx_data[2*v_info2*i + v_info2*0 + 3] = alfas[0];
		vertx_data[2*v_info2*i + v_info2*0 + 4] = vertice[0];
		vertx_data[2*v_info2*i + v_info2*0 + 5] = vertice[1];
		vertx_data[2*v_info2*i + v_info2*0 + 6] = vertice[2];
		vertx_data[2*v_info2*i + v_info2*0 + 7] = distancia_fuente[0];
		
		vertx_data[2*v_info2*i + v_info2*1 + 0] = color[0];
		vertx_data[2*v_info2*i + v_info2*1 + 1] = color[1];
		vertx_data[2*v_info2*i + v_info2*1 + 2] = color[2];
		vertx_data[2*v_info2*i + v_info2*1 + 3] = alfas[1];
		vertx_data[2*v_info2*i + v_info2*1 + 4] = vertice[0];
		vertx_data[2*v_info2*i + v_info2*1 + 5] = vertice[1];
		vertx_data[2*v_info2*i + v_info2*1 + 6] = vertice[2];
		vertx_data[2*v_info2*i + v_info2*1 + 7] = distancia_fuente[1];
	}
	free(alfas);
	free(distancia_fuente);
	free(solution_type);
}		
}
float model_acceleration(unsigned int, float[3], float[3]);

void geodesica_evolucionador(struct particula_data particulas){
	unsigned int numero = particulas.n_vert;
	if(particulas.moving == 1){
		float X[3], V[3], A[3], dtau = 0.01f;
		for(unsigned int i = 0; i < numero; i++){
			X[0] = particulas.vertices[3*i + 0];
			X[1] = particulas.vertices[3*i + 1];
			X[2] = particulas.vertices[3*i + 2];

			V[0] = particulas.velocity[3*i + 0];
			V[1] = particulas.velocity[3*i + 1];
			V[2] = particulas.velocity[3*i + 2];
			
			A[0] = model_acceleration(0, X, V);
			A[1] = model_acceleration(1, X, V);
			A[2] = model_acceleration(2, X, V);

			particulas.vertices[3*i + 0] += V[0]*dtau;
			particulas.vertices[3*i + 1] += V[1]*dtau;
			particulas.vertices[3*i + 2] += V[2]*dtau;
			
			particulas.velocity[3*i + 0] += A[0]*dtau;
			particulas.velocity[3*i + 1] += A[1]*dtau;
			particulas.velocity[3*i + 2] += A[2]*dtau;
		}
	}
}

static inline float min3(float x1, float x2, float x3){
	if((x1 <= x2)&&(x1 <= x3))
		return x1;
	else if ((x2 <= x1)&&(x2 <= x3))
		return x2;
	else
		return x3;
}

static inline float max3(float x1, float x2, float x3){
	if((x1 >= x2)&&(x1 >= x3))
		return x1;
	else if ((x2 >= x1)&&(x2 >= x3))
		return x2;
	else
		return x3;
}

void geodesica_triangulador(struct object_data objeto, float *vertx_data, float *triangulos){
	unsigned int stride1 = objeto.n_triang*3*n_atrib, stride2 = 3*n_atrib, stride3 = n_atrib, T[3];
	float vertices[3];
	for(unsigned int j = 0; j < 2; j++){
		for(unsigned int i = 0; i < objeto.n_triang; i++){
			T[0] = objeto.triangulos[3*i + 0];
			T[1] = objeto.triangulos[3*i + 1];
			T[2] = objeto.triangulos[3*i + 2];

			
			for(unsigned int k = 0; k < 3; k++){
				vertices[0] = objeto.vertices[3*T[k] + 0];
				vertices[1] = objeto.vertices[3*T[k] + 1];
				vertices[2] = objeto.vertices[3*T[k] + 2];
			
				triangulos[stride1*j + stride2*i + stride3*k + 0] = vertices[0];
				triangulos[stride1*j + stride2*i + stride3*k + 1] = vertices[1];
				triangulos[stride1*j + stride2*i + stride3*k + 2] = vertices[2];
				
				triangulos[stride1*j + stride2*i + stride3*k + 3] = vertx_data[2*v_info*T[k] + v_info*j + 0];    //alfa
				triangulos[stride1*j + stride2*i + stride3*k + 4] = vertx_data[2*v_info*T[k] + v_info*j + 1];    //distance
				
				triangulos[stride1*j + stride2*i + stride3*k + 5] = objeto.u[T[k]];
				triangulos[stride1*j + stride2*i + stride3*k + 6] = objeto.v[T[k]];
			}
		}
	}
}

/*
void geodesica_triangulador_shaders(struct object_data objeto, float *vertx_data, float *triangulos){
	unsigned int stride1 = objeto.n_triang*3*n_atrib_shading, stride2 = 3*n_atrib_shading, stride3 = n_atrib_shading, T[3];
	float P1, P2, P3, P_min, P_max, sol_type;
	
	for(unsigned int j = 0; j < 4; j++){
		for(unsigned int i = 0; i < objeto.n_triang; i++){
			T[0] = objeto.triangulos[3*i + 0];
			T[1] = objeto.triangulos[3*i + 1];
			T[2] = objeto.triangulos[3*i + 2];
// 			printf("j = %u, i = %u, indice = %u, n_triang = %u\n", j, i, stride1*j + stride2*i + stride3*2 + 5, objeto.n_triang);
			P1 = vertx_data[4*v_info*T[0] + v_info*j + 3];
			P2 = vertx_data[4*v_info*T[1] + v_info*j + 3];
			P3 = vertx_data[4*v_info*T[2] + v_info*j + 3];
// 			if(j == 0)
// 				printf("Ps = (%f, %f, %f)\n", P1, P2, P3);
			P_max = vertx_data[4*v_info*0 + v_info*j + 3];
// 			P_max = max3(P1, P2, P3);
			P_min = min3(P1, P2, P3);
			for(unsigned int k = 0; k < 3; k++){
				triangulos[stride1*j + stride2*i + stride3*k + 0] = vertx_data[4*v_info*T[k] + v_info*j + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 1] = vertx_data[4*v_info*T[k] + v_info*j + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 2] = vertx_data[4*v_info*T[k] + v_info*j + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 3] = objeto.u[T[k]];
				triangulos[stride1*j + stride2*i + stride3*k + 4] = objeto.v[T[k]];
				triangulos[stride1*j + stride2*i + stride3*k + 5] = vertx_data[4*v_info*T[k] + v_info*j + 3];
				triangulos[stride1*j + stride2*i + stride3*k + 6] = vertx_data[4*v_info*T[k] + v_info*j + 4];
				triangulos[stride1*j + stride2*i + stride3*k + 7] = vertx_data[4*v_info*T[k] + v_info*j + 5];
				triangulos[stride1*j + stride2*i + stride3*k + 8] = vertx_data[4*v_info*T[k] + v_info*j + 6];
				triangulos[stride1*j + stride2*i + stride3*k + 9] = vertx_data[4*v_info*T[k] + v_info*j + 7];
				triangulos[stride1*j + stride2*i + stride3*k +10] = vertx_data[4*v_info*T[k] + v_info*j + 8];
				triangulos[stride1*j + stride2*i + stride3*k +11] = objeto.light_reflex[3*T[k] + 0];
				triangulos[stride1*j + stride2*i + stride3*k +12] = objeto.light_reflex[3*T[k] + 1];
				triangulos[stride1*j + stride2*i + stride3*k +13] = objeto.light_reflex[3*T[k] + 2];
				triangulos[stride1*j + stride2*i + stride3*k +14] = objeto.normales[3*T[k] + 0];
				triangulos[stride1*j + stride2*i + stride3*k +15] = objeto.normales[3*T[k] + 1];
				triangulos[stride1*j + stride2*i + stride3*k +16] = objeto.normales[3*T[k] + 2];
				triangulos[stride1*j + stride2*i + stride3*k +17] = objeto.light_intensity[T[k]];

				// 				triangulos[stride1*j + stride2*i + stride3*k +11] = vertx_data[4*v_info*T[k] + v_info*j + 9];
				
			}
//			printf("P_min = %f\n", P_min);
		}
	}
// 	printf("arg max = %u\n", stride1*3 + stride2*(objeto.n_triang - 1) + stride3*2 + 5);
}
*/





void draw_vertex(float posicion[4], float vectores[4][4], float aspect_ratio, unsigned int shaderProgram2, unsigned int texture, GLuint VBO, unsigned int numero_triang, float *vertices, float *triangulos){
	float *base;
	unsigned int i;
	
	base    = (float *) malloc(9*sizeof(float));
	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[1] =  sinf(posicion[2])*sinf(posicion[3]);
	base[2] =  cosf(posicion[2]);

	base[3] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[5] = -sinf(posicion[2]);

	base[6] = -sinf(posicion[3]);
	base[7] =  cosf(posicion[3]);
	base[8] =  0.f;

	
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*N_GEO*n_atrib*numero_triang*sizeof(float), triangulos);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)0);      // coordenadas fijas
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)12);     // alfa
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)16);     // distance
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)20);     // coordenadas (u, v)
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glUseProgram(shaderProgram2);
	glBindTexture(GL_TEXTURE_2D, texture);
	

	i = glGetUniformLocation(shaderProgram2, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram2, "vectores");
	glUniformMatrix4fv(i, 1, GL_FALSE, &vectores[0][0]);

	i = glGetUniformLocation(shaderProgram2, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	i = glGetUniformLocation(shaderProgram2, "base");
	glUniformMatrix3fv(i, 1, GL_FALSE, &base[0]);

	glDrawArrays(GL_TRIANGLES, 0, N_GEO*3*numero_triang);

	free(base);
}


void draw_points(float posicion[4], float vectores[4][4], float aspect_ratio, unsigned int shaderProgram, GLuint VBO,  struct particula_data particulas, float *buffer_particulas){
	float *base;
	
	base = (float *) malloc(9*sizeof(float));
	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[1] =  sinf(posicion[2])*sinf(posicion[3]);
	base[2] =  cosf(posicion[2]);

	base[3] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[5] = -sinf(posicion[2]);

	base[6] = -sinf(posicion[3]);
	base[7] =  cosf(posicion[3]);
	base[8] =  0.f;

	

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, N_GEO*v_info2*particulas.n_vert*sizeof(float), buffer_particulas);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)0);      // color (r,g,b)
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)12);     // alfa
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)16);     // coordenadas fijas
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)28);     // distancia
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glUseProgram(shaderProgram);

	GLuint i  = glGetUniformLocation(shaderProgram, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram, "vectores");
	glUniformMatrix4fv(i, 1, GL_FALSE, &vectores[0][0]);

	i = glGetUniformLocation(shaderProgram, "base");
	glUniformMatrix3fv(i, 1, GL_FALSE, &base[0]);

	i = glGetUniformLocation(shaderProgram, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	glDrawArrays(GL_POINTS, 0, N_GEO*particulas.n_vert);

	free(base);
}

GLchar *load_shader_source(char *filename) {
	FILE *file = fopen(filename, "r");             // open 
	fseek(file, 0L, SEEK_END);                     // find the end
	size_t size = ftell(file);                     // get the size in bytes
	GLchar *shaderSource = calloc(1, size);        // allocate enough bytes
	rewind(file);                                  // go back to file beginning
	fread(shaderSource, size, sizeof(char), file); // read each char into ourblock
	fclose(file);                                  // close the stream
	return shaderSource;
}

void load_and_compile(unsigned int *shaderProgram1, unsigned int *shaderProgram2){
	int  success;
	char infoLog[512];
	GLchar *vertexSource2, *fragmentSource2, *vertexSource1, *fragmentSource1;// *geometrySource1;

	printf("Por compilar shaders\n");

	vertexSource1   = load_shader_source("shaders/vertex_source_hyperb.glsl");
	fragmentSource1 = load_shader_source("shaders/fragment_source_hyperb.glsl");
//	geometrySource1 = load_shader_source("shaders/geometry_source.glsl");
	
	vertexSource2   = load_shader_source("shaders/vertex_source_hyperb_points.glsl");
	fragmentSource2 = load_shader_source("shaders/fragment_source_hyperb_points.glsl");

// 	vertexSource3   = load_shader_source("shaders/vertex_source_shading.glsl");
// 	fragmentSource3 = load_shader_source("shaders/fragment_source_shading.glsl");
// 	geometrySource3 = load_shader_source("shaders/geometry_source_shading.glsl");

	//compilando shaders y todo eso 2


    unsigned int vertexShader1;
    vertexShader1 = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader1, 1, (const GLchar**)&vertexSource1, NULL);
    glCompileShader(vertexShader1);
    glGetShaderiv(vertexShader1, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(vertexShader1, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int fragmentShader1;
    fragmentShader1 = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader1, 1, (const GLchar**)&fragmentSource1, NULL);
    glCompileShader(fragmentShader1);
    glGetShaderiv(fragmentShader1, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(fragmentShader1, 512, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED %s\n", infoLog);
    }



	//uniendo shader 1
	*shaderProgram1 = glCreateProgram();
	glAttachShader(*shaderProgram1, vertexShader1);
// 	glAttachShader(*shaderProgram1, geometryShader1);
	glAttachShader(*shaderProgram1, fragmentShader1);
	glLinkProgram(*shaderProgram1);


	
	
	unsigned int vertexShader2;
	vertexShader2 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader2, 1, (const GLchar**)&vertexSource2, NULL);
	glCompileShader(vertexShader2);
	glGetShaderiv(vertexShader2, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(vertexShader2, 512, NULL, infoLog);
		printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED %s\n", infoLog);
	}

	unsigned int fragmentShader2;
	fragmentShader2 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader2, 1, (const GLchar**)&fragmentSource2, NULL);
	glCompileShader(fragmentShader2);
	glGetShaderiv(fragmentShader2, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(fragmentShader2, 512, NULL, infoLog);
		printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED %s\n", infoLog);
	}

	*shaderProgram2 = glCreateProgram();
	glAttachShader(*shaderProgram2, vertexShader2);
	glAttachShader(*shaderProgram2, fragmentShader2);
	glLinkProgram(*shaderProgram2);

}	


void volcado40(){
	FILE *pfout;
	float rho1, rho2;
	unsigned int N;
	pfout = fopen("volcado.txt", "w");
	rho1 = 0.065737f;
	rho2 = 0.418379f;
	float P_max = fminf(metrica_r(rho1), metrica_r(rho2)), P, valor;
	N = 200;
	fprintf(pfout, "P, rho1, rho2, G(P, rho1, rho2), F(rho1, P), F(rho2, P)\n");
	for(unsigned int i = 0; i < N; i++){
		P = (P_max)/N*i;
		valor = geodesica_G(P, rho1, rho2);
		fprintf(pfout, "%f %f %f %f %f %f\n", P, rho1, rho2, valor, geodesica_F(rho1, P), geodesica_F(rho2, P));
	}
	fclose(pfout);
	
	unsigned int resx = 1300, resy = 1100;
	float drho, dP, rho;
	drho = pi*metrica_R/(resy);
	dP   = 5.f/resx;
	pfout = fopen("imagen.dat", "w");
	for(unsigned int i = 0; i < resy; i++){
		rho = drho*i;
		for(unsigned int j = 0; j < resx; j++){
			P = dP*j;
			fprintf(pfout, "%f ", geodesica_F(rho, P));
		}
		fprintf(pfout, "\n");
	}
	fclose(pfout);
	pfout = fopen("imagen2.dat", "w");
	for(unsigned int i = 0; i < resy; i++){
		rho = drho*i;
		for(unsigned int j = 0; j < resx; j++){
			P = dP*j;
			fprintf(pfout, "%f ", geodesica_T(rho, P));
		}
		fprintf(pfout, "\n");
	}
	fclose(pfout);

	
}
