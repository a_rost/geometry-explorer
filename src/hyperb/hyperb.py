import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
plt.ion()


R = 5.


def r(x):
	return R*np.sinh(x/R)

def r_p(x):
	return np.cosh(x/R)

def r_p_p(x):
	return np.sinh(x/R)/R

def U(x, P):
    return -1./P**2 + 1./r(x)**2

def U_p(x, P):
    return -2*r_p(x)*r(x)**(-3)

def U_p_p(x, P):
	return 6*r(x)**(-4)*r_p(x)**2 - 2*r(x)**(-3)*r_p_p(x)

def raiz(P):
	return np.arcsinh(P/R)*R

def integrando_phi(x, P):
	if P == 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/r(x)**2

def integrando_t(x, P):
	if P == 0.:
		return 1.
	else:
		return 1./np.sqrt(1. - (P/r(x))**2)

def F(x, P):
	rho_min = raiz(P)
	if P == 0.:
		return np.pi/2.
	elif x < rho_min:
		print("fuera de rango")
		return 0.
	else:
		return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]

def t(x, P):
	rho_min = raiz(P)
	if P == 0.:
		return x
	elif x < rho_min:
		print("fuera de rango")
		return 0.
	else:
		return integrate.quad(integrando_t, rho_min, x, args=(P))[0]


def I(P, rho1, rho2):
	if P == 0.:
		return 0.
	else:
		return abs(F(rho2, P) - F(rho1, P))

def G(P, rho1, rho2):
	if P == 0.:
		return np.pi
	else:
		return F(rho1, P) + F(rho2, P)


def show_I(rho1, rho2):
	P_max = min(r(rho1), r(rho2))
	P_space = np.linspace(0., P_max, 200)
	y_space = np.zeros(200)
	for i in range(200):
		y_space[i] = I(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)

def show_G(rho1, rho2):
	P_max = min(r(rho1), r(rho2))
	P_space = np.linspace(0., P_max, 200)
	y_space = np.zeros(200)
	for i in range(200):
		y_space[i] = G(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)


rho_res, P_res = 1200, 1200
rho_min, rho_max = 0., 10.
P_min, P_max     = 0., r(rho_max)
"""
phi_array = np.zeros([rho_res, P_res])
for i in range(P_res):
	print(" i = ", i)
	P = P_min + (P_max - P_min)/(P_res - 1)*i
	for j in range(rho_res):
		rho = (rho_max - rho_min)/(rho_res - 1)*j + rho_min
		phi_array[j][i] = F(rho, P)
plt.imshow(phi_array)


archivo = open("data/hyperb/phi_array.dat", "w")
for i in range(rho_res):
	for j in range(P_res):
		archivo.write(str(phi_array[i][j]) + "\n")
archivo.close()
"""

"""
t_array = np.zeros([rho_res, P_res])
for i in range(P_res):
	print(" i = ", i)
	P = P_min + (P_max - P_min)/(P_res - 1)*i
	for j in range(rho_res):
		rho = (rho_max - rho_min)/(rho_res - 1)*j + rho_min
		t_array[j][i] = t(rho, P)
plt.imshow(t_array)


archivo = open("../../data/hyperb/t_array.dat", "w")
for i in range(rho_res):
	for j in range(P_res):
		archivo.write(str(t_array[i][j]) + "\n")
archivo.close()
"""

def interpolador(array, d_rho, d_P, rho0, P0, x, P):
	i = int(np.floor((x - rho0)/d_rho))
	j = int(np.floor((P - P0)/d_P))
	print("i = ", i, "j = ", j)
	#i = min(i, N2 - 2)
	#j = min(j, N - 2)
	
	valores = np.zeros(4)
	valores[0] = array[i + 0][j + 0]
	valores[1] = array[i + 0][j + 1]
	valores[2] = array[i + 1][j + 0]
	valores[3] = array[i + 1][j + 1]

	#if j < 0:
	#		valores[0] = np.pi*0.5
	#		valores[2] = np.pi*0.5

	print(valores)
	valores2 = np.zeros(2)
	valores2 = (valores[2:4] - valores[0:2])*(x - i*d_rho - rho0)/d_rho + valores[0:2]
	#print(valores2)
	valor = (valores2[1] - valores2[0])*(P - j*d_P - P0)/d_P + valores2[0]
	return valor

def integral_arcsin(u):
	return 0.5*u*np.sqrt(1. - u**2) + 0.5*np.arcsin(u)

def integral_arccosh(u):
	return -(0.5*(-u)*np.sqrt(u**2 - 1.) - 0.5*np.arccosh(-u))

def aproximacion6(h, P):
	rho_0 = raiz(P)
	beta, gamma = U_p(rho_0, P), U_p_p(rho_0, P)*0.5
	print(beta, gamma)
	x1, x2 = rho_0 + h, rho_0
	print("x1 = ", x1, "x2 = ", x2, "rho_min = ", rho_0, "h = ", h)
	if gamma >= 0.:
		u1, u2 = 2*gamma*(x1 - rho_0)/beta + 1., 2*gamma*(x2 - rho_0)/beta + 1.
		return (1./P**2*np.arcsin(u2)/np.sqrt(gamma) - beta**2/4./(gamma**1.5)*integral_arcsin(u2)) - (1./P**2*np.arcsin(u1)/np.sqrt(gamma) - beta**2/4./(gamma**1.5)*integral_arcsin(u1))
	else:
		u1, u2 = 2*(-gamma)*(x1 - rho_0)/beta - 1., 2*(-gamma)*(x2 - rho_0)/beta - 1.
		print("u1 = ", u1, "u2 = ", u2)
		return (-1./P**2/np.sqrt(-gamma)*np.arccosh(-u2) - beta**2/4./(-gamma)**1.5*integral_arccosh(u2)) - (-1./P**2/np.sqrt(-gamma)*np.arccosh(-u1) - beta**2/4./(-gamma)**1.5*integral_arccosh(u1))

def aproximacion6_T(h, P):
	rho_0 = raiz(P)
	beta, gamma = U_p(rho_0, P), U_p_p(rho_0, P)*0.5
	print(beta, gamma)
	x1, x2 = rho_0 + h, rho_0
	print("x1 = ", x1, "x2 = ", x2, "rho_min = ", rho_0, "h = ", h)
	if gamma >= 0.:
		u1, u2 = 2*gamma*(x1 - rho_0)/beta + 1., 2*gamma*(x2 - rho_0)/beta + 1.
		return (1./P*np.arcsin(u2)/np.sqrt(gamma)) - (1./P*np.arcsin(u1)/np.sqrt(gamma))
	else:
		u1, u2 = 2*(-gamma)*(x1 - rho_0)/beta - 1., 2*(-gamma)*(x2 - rho_0)/beta - 1.
		print("u1 = ", u1, "u2 = ", u2)
		return (-1./P/np.sqrt(-gamma)*np.arccosh(-u2)) - (-1./P/np.sqrt(-gamma)*np.arccosh(-u1))

def F3(rho, P):
	rho_min = raiz(P)
	if P == 0.:
		return np.pi/2.
	elif rho < rho_min:
		print("fuera de rango")
		return 0.
	elif rho - rho_min <= 0.2:
		return aproximacion6(rho - rho_min, P)
	else:
		return interpolador(phi_array, (rho_max - 0.)/(rho_res - 1), (P_max - P_min)/(P_res - 1), 0., P_min, rho, P)

def geodesica(rho1, rho2, phi):
	coseno = np.cos(phi)
	D     = np.arccosh(np.cosh(rho1/R)*np.cosh(rho2/R) - np.sinh(rho1/R)*np.sinh(rho2/R)*coseno)*R
	alfa  = np.arcsinh((np.cosh(rho2/R) - np.cosh(rho1/R)*np.cos(D/R))/(np.sinh(rho1/R)*np.sin(D/R)))
	P     = r(rho1)*abs(np.sin(alfa))
	show_G(rho1, rho2)
	show_I(rho1, rho2)
	plt.plot([P], [phi], ".")

def geodesica(rho1, rho2, phi):
	coseno = np.cos(phi)
	R2 = R*(0. + 1.j)
	D     = np.arccos(np.cos(rho1/R2)*np.cos(rho2/R2) + np.sin(rho1/R2)*np.sin(rho2/R2)*coseno)*R2;
	alfa  = np.arccos((np.cos(rho2/R2) - np.cos(rho1/R2)*np.cos(D/R2))/(np.sin(rho1/R2)*np.sin(D/R2)));
	P     = r(rho1)*abs(np.sin(alfa))
	print("alfa = ", alfa, " P = ", P, " D = ", D)
	show_G(rho1, rho2)
	show_I(rho1, rho2)
	plt.plot([P], [phi], ".")


def geodesica_final(rho1, rho2, phi):
	coseno = np.cos(phi)
	R2 = R*(0. + 1.j)
	D     = np.arccosh(np.cosh(rho1/R)*np.cosh(rho2/R) - np.sinh(rho1/R)*np.sinh(rho2/R)*coseno)*R;
	alfa  = np.arccos(-(np.cosh(rho2/R) - np.cosh(rho1/R)*np.cosh(D/R))/(np.sinh(rho1/R)*np.sinh(D/R)));
	P     = r(rho1)*abs(np.sin(alfa))
	print("alfa = ", alfa, " P = ", P, " D = ", D)
	show_G(rho1, rho2)
	show_I(rho1, rho2)
	plt.plot([P], [phi], ".")


