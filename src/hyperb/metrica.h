float metrica_B(float x){
  return powf(100.f, 2) ;
}

float metrica_B_d(float x){
  return 0.f;
}


float metrica_r(float x){
	return metrica_R*sinhf(x/metrica_R);
}

//Primera derivada de r
float metrica_r_d(float x){
	return coshf(x/metrica_R);
}

//Segunda derivada de r
float metrica_r_d_d(float x){
	return sinhf(x/metrica_R)/metrica_R;
}



float metrica_U(float x, float P){                 
	return  powf(metrica_r(x), -2) - powf(P, -2);
}

float metrica_U_d(float x, float P){
	return -2*powf(metrica_r(x), -3)*metrica_r_d(x);
}

float metrica_U_d_d(float x, float P){
	return 6.f*powf(metrica_r(x), -4)*powf(metrica_r_d(x), 2) - 2*powf(metrica_r(x), -3)*metrica_r_d_d(x);
}

float metrica_raiz_U(float P){
	return metrica_R*asinhf(P/metrica_R);
}
