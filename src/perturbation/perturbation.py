import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
plt.ion()


h = 1.
a, b = 0.5, 0.5
A = -h
C = np.sqrt(h/a/(a/b + 1.)) - h
B = C + (C + h)*a/b


print(2*b*(C - B))


def r(x):
	if x >= B:
		return x
	elif x >= C:
		return x + b*(x - B)**2
	elif x >= A:
		return x + h - a*(x - A)**2
	else:
		print("fuera de rango")
		return 0.


def r_p(x):
	if x >= B:
		return 1.
	elif x >= C:
		return 1. + 2*b*(x - B)
	elif x >= A:
		return 1. -2*a*(x - A)
	else:
		print("fuera de rango")
		return 0.

def r_p_p(x):
	if x >= B:
		return 0.
	elif x >= C:
		return 2*b
	elif x >= A:
		return -2*a
	else:
		print("fuera de rango")
		return 0.

def U(x, P):
    return -1./P**2 + 1./r(x)**2

def U_p(x, P):
    return -2*r_p(x)*r(x)**(-3)

def U_p_p(x, P):
	return 6*r(x)**(-4)*r_p(x)**2 - 2*r(x)**(-3)*r_p_p(x)

#r, r_p, r_p_p = np.vectorize(r), np.vectorize(r_p), np.vectorize(r_p_p)
"""
x_space = np.linspace(A, B + 3., 100)
y_space = r(x_space)

plt.plot(x_space, r(x_space))

plt.plot(raiz(np.linspace(0.1, 10., 60)), np.linspace(0.1, 10., 60), ".")

plt.plot(x_space, r_p(x_space))
plt.plot(x_space, r_p_p(x_space))
"""

y0, y1 = r(C), r(B)
def raiz(P):
	if P >= y1:
		return P
	elif P >= y0:
		return (-1 + np.sqrt(1 + 4*b*(P - B)))/2./b + B
	else:
		return (1 - np.sqrt(1 - 4*a*P))/2./a - h
raiz = np.vectorize(raiz)

def integrando_phi(x, P):
	if P == 0. or r(x) == 0.:
		return 1.
	elif 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/r(x)**2

def F(x, P):
	rho_min = raiz(P)
	if x < rho_min:
		print("fuera de rango")
		return 0.
	else:
		return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]

def I(P, rho1, rho2):
	if P == 0.:
		return 0.
	else:
		return abs(F(rho2, P) - F(rho1, P))

def G(P, rho1, rho2):
	if P == 0.:
		return np.pi
	else:
		return F(rho1, P) + F(rho2, P)

def show_both(rho1, rho2):
	show_I(rho1, rho2)
	show_G(rho1, rho2)
	
def show_I(rho1, rho2):
	P_max = min(r(rho1), r(rho2))
	P_space = np.linspace(0., P_max, 200)
	y_space = np.zeros(200)
	for i in range(200):
		y_space[i] = I(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)

def show_G(rho1, rho2):
	P_max = min(r(rho1), r(rho2))
	P_space = np.linspace(0., P_max, 200)
	y_space = np.zeros(200)
	for i in range(200):
		y_space[i] = G(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)


rho_res, P_res = 300, 300
rho_min, rho_max = A, 5.
P_min, P_max     = 0., 5.
phi_array = np.zeros([rho_res, P_res])
for i in range(P_res):
	print(" i = ", i)
	P = P_min + (P_max - P_min)/(P_res - 1)*i
	for j in range(rho_res):
		rho = (rho_max - rho_min)/(rho_res - 1)*j + rho_min
		phi_array[j][i] = F(rho, P)
plt.imshow(phi_array)

