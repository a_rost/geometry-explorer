import numpy as np
import matplotlib.pyplot as plt
from scipy.special import ellipj
plt.ion()

c, rs = 1.5, 2.
P = 10.

def sn(x, k2):
	k = k2**2
	print("k = ", k)
	result = ellipj(x, k)[0]
	return result

def sn_d(x, k2):
	k = k2**2
	print("k = ", k)
	result = ellipj(x, k)
	return result[1]*result[2]

def sn_inv(x, k):
	m = k**2
	print("sn_inv args:", x, k)
	return ellip(np.arcsin(x + 0.j), k)


x_space = np.linspace(0., 10., 200)
plt.plot(x_space, sn(x_space, 0.5))
plt.plot(x_space, sn_d(x_space, 0.5))

	
def f(phi, P, delta = 0.):
	array = np.roots([rs, -1, 0., c**2/P**2])[0:3]
	array.sort()
	u1, u2, u3 = array[0], array[1], array[2]
	k = np.sqrt((u2 - u1)/(u3 - u1))
	argumento = phi*np.sqrt(rs*(u3 - u1))/2. + delta
	u = u1 + (u2 - u1)*sn(argumento, k)**2
	return 1./u

def g(phi, P, delta = 0.):
	array = np.roots([rs, -1, 0., c**2/P**2])[0:3]
	array.sort()
	u1, u2, u3 = array[0], array[1], array[2]
	k = np.sqrt((u2 - u1)/(u3 - u1))
	argumento = phi*np.sqrt(rs*(u3 - u1))/2. + delta
	u = u1 + (u2 - u1)*sn(argumento, k)**2
	return u

def g_d(phi, P, delta = 0.):
	array = np.roots([rs, -1, 0., c**2/P**2])[0:3]
	array.sort()
	u1, u2, u3 = array[0], array[1], array[2]
	k = np.sqrt((u2 - u1)/(u3 - u1))
	argumento = phi*np.sqrt(rs*(u3 - u1))/2. + delta
	u = (u2 - u1)*sn(argumento, k)*2*sn_d(argumento, k)*np.sqrt(rs*(u3 - u1))/2.
	return u

def other_side(phi, P, delta= 0.):
	#array = np.roots([c**2/P**2, 0, -1, rs])[0:3]
	array = np.roots([rs, -1, 0., c**2/P**2])[0:3]
	array.sort()
	u1, u2, u3 = array[0], array[1], array[2]
	k = np.sqrt((u2 - u1)/(u3 - u1))
	return rs*(g(phi, P, delta) - u1)*(g(phi, P, delta) - u2)*(g(phi, P, delta) - u3)

def other_side2(phi, P, delta= 0.):
	return rs*g(phi, P, delta)**3 - g(phi, P, delta)**2 + c**2/P**2


def pw(x, e1, e2, e3):
	k = np.sqrt((e2 - e3)/(e1 - e3))
	w = x*np.sqrt(e1 - e3)
	return e3 + (e1 - e3)/sn(w, k)**2

def pw_inv(u, e1, e2, e3):
	print("pw_inv args:", u, e1, e2, e3)
	k = np.sqrt((e2 - e3)/(e1 - e3))
	w = sn_inv(np.sqrt((e1 - e3)/(u - e3)), k)
	x = w/np.sqrt(e1 - e3 + 0.j)
	print(k, w, x, e1 - e3)
	return x


def phi(r, P):
	array = np.roots([rs, -1, 0., c**2/P**2])[0:3]
	array.sort()
	e3, e2, e1 = array[0], array[1], array[2]
	return np.sqrt(rs)/2.*pw_inv(1./r, e1, e2, e3)
	#print(u1, u2, u3)
	
	
def pw_d(x, e1, e2, e3):
	k = np.sqrt((e2 - e3)/(e1 - e3))
	w = x*np.sqrt(e1 - e3)
	return (e1 - e3)*(-2)*sn(w, k)**(-3)*sn_d(w, k)*np.sqrt(e1-e3)

def other_side(x, e1, e2, e3):
	v = pw(x, e1, e2, e3)
	return 4*(v - e1)*(v - e2)*(v - e3)

e1, e2, e3 = 3., 2., -1.

x_space = np.linspace(0., 10., 200)
plt.plot(x_space, pw_d(x_space, e1, e2, e3)**2)
plt.plot(x_space, other_side(x_space, e1, e2, e3))
plt.ylim([0., 10.])



def diff_sn(x, k):
	v = sn(x, k)
	return (1 - v**2)*(1 - k**2*v**2)

k = 0.5
x_space = np.linspace(0., 10., 1000)
plt.plot(x_space, diff_sn(x_space, k))
plt.plot(x_space, sn_d(x_space, k)**2)



y_space1 = np.zeros(1000)
y_space2 = np.zeros(1000)
for i in range(1000):
	y_space1[i] = sn_d(x_space[i], k)**2
	y_space2[i] = diff_sn(x_space[i], k)
plt.plot(x_space, y_space1)
plt.plot(x_space, y_space2)


P = 20.
x_space = np.linspace(0., 2., 1000)
#plt.plot(x_space, other_side(x_space, P))
plt.plot(x_space, other_side2(x_space, P))
plt.plot(x_space, g_d(x_space, P)**2)


phi_space = np.linspace(0., 2*np.pi, 1000)
r_space = f(phi_space, P, -1.)



X, Y = np.cos(phi_space)*r_space, np.sin(phi_space)*r_space
mask = r_space >= 0.
plt.plot(X[mask], Y[mask])



def RF(x, y, z):
	print("This is RF(", x, y, z,")")
	x += 0.j
	y += 0.j
	z += 0.j
	r = 3*10**-4
	A0 = (x + y + z)/3.
	Q  = (3*r)**(-1./6)*max(abs(A0 - x), abs(A0 - y), abs(A0 - z))
	x_m, y_m, z_m = x, y, z
	A_m = A0
	for m in range(10):
		lambda_m = np.sqrt(x_m)*np.sqrt(y_m) + np.sqrt(z_m)*np.sqrt(y_m) + np.sqrt(x_m)*np.sqrt(z_m)
		A_m = (A_m + lambda_m)/4.
		x_m, y_m, z_m = (x_m + lambda_m)/4., (y_m + lambda_m)/4., (z_m + lambda_m)/4.
		if 4**(-m)*Q < abs(A_m):
			break
	m += 1
	X, Y = (A0 - x)/4**m/A_m,  (A0 - y)/4**m/A_m
	Z    = -X - Y
	E2   = X*Y - Z**2 
	E3   = X*Y*Z
	return A_m**(-0.5)*(1- 0.1*E2 + 1./14*E3 + 1./24*E2**2 - 3./44*E2*E3)


def ellip(phi, k):
	print("This is ellip(", phi, k,")")
	c = 1./np.sin(phi)**2
	return RF(c - 1., c - k**2, c)


def U(x, P):
	return -(c**2/P**2 - 1./x**2 + rs/x**3)

def f(r, P):
	array = np.roots([rs, -1, 0., c**2/P**2])[0:3]
	array.sort()
	u1, u2, u3 = array[0], array[1], array[2]
	k = np.sqrt((u2 - u1)/(u3 - u1))
	print(1./u1, 1./u2, 1./u3)
	return 2*pw_inv(1./r, u1, u2, u3)/np.sqrt(rs)

def F(x, P)

x_space = np.linspace(0., 2, 1000)
y_space1 = np.zeros(1000)
y_space2 = np.zeros(1000)

k = 0.25
y_space1 = ellipkinc(x_space, k**2)
for i in range(1000):
	y_space2[i] = F(x_space[i], k)
#plt.plot(x_space, y_space1)
plt.plot(x_space, y_space2 - y_space1)


mink = np.identity(4)
mink[0][0] *= -c**2

g = np.identity(4)
g[0,0] = -c**2*0.5
g[1,1] = 1.4
g[2,2] = 3.
g[3,3] = 3.2


matriz = np.zeros([4,4])
v1 = np.zeros(4)
v2 = np.zeros(4)
v3 = np.zeros(4)
v4 = np.zeros(4)
v1[0] = 1.
v1[3] = 0.5
v2[1] = 1.
v3[2] = 1.
v4[3] = 1.

modelo = dot(v1, v1)
v1 *= 1./np.sqrt(-modelo)
modelo = dot(v2, v1)

v2 += v1*modelo
modelo = dot(v2, v2)
v2 *= 1./np.sqrt(modelo)

modelo = dot(v2, v3)
v3 += -modelo*v2
modelo = dot(v3, v3)
v3 *= 1./np.sqrt(modelo)

modelo = dot(v2, v3)
v3 += -modelo*v2
modelo = dot(v3, v3)
v3 *= 1./np.sqrt(modelo)



def dot(v1, v2):
	inter = np.matmul(g, v2)
	return np.matmul(v1, inter)
