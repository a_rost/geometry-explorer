float *root_array1, *root_array2;

float metrica_B(float x){
	return (1.f - RS/x);
}
float metrica_B_d(float x){
	return RS/(x*x);
}

float metrica_B_d_d(float x){
	return -2*RS/(x*x*x);
}

float metrica_A(float x){
	return 1.f/(1.f - RS/x);
}

float metrica_A_d(float x){
	return -powf(1.f - RS/x, -2)*RS/powf(x, 2);
}

float metrica_U(float x, float P){                 
	return  -powf(c/P, 2) + powf(x, -2) - RS*powf(x, -3);
}

float metrica_U_d(float x, float P){
	return -2.f*powf(x, -3) + 3*RS*powf(x, -4);
}

float metrica_U_d_d(float x, float P){
	return 6.f*powf(x, -4) - 12.f*RS*powf(x, -5);
}


static inline float fn(float x, float P){
	return powf(c/P, 2)*powf(x, 3) - x + RS;
}

static inline float gn(float x, float P){
	return 3*powf(c/P, 2)*powf(x, 2) - 1.f;
}

float metrica_raiz_U2(float P, float *root1, float *root2){
	float a, medio, x0, x1;
	a = c/P;
	medio = 1.f/(sqrtf(3.f)*a);
	x0 = medio - 0.1f;
	for(unsigned int i = 0; i < 15; i++){
		x0 = x0 - fn(x0, P)/gn(x0, P);
		if(x0 < RS)
			x0 = RS;
	}
	x1 = medio + 0.1f;
	for(unsigned int i = 0; i < 15; i++){
		x1 = x1 - fn(x1, P)/gn(x1, P);
		if(x1 < medio)
			x1 = medio;
	}
// 	printf("raices = %f, %f\n", x0, x1);
	*root1 = x0;
	*root2 = x1;
}

float metrica_raiz_U(float P, float *root1, float *root2){
	float a = c/P, da = c/P_MAX/99999.f;
	int i;
	
	i = (int) (a/da);

	// 	if((i >= 1000)||(i < 0))
// 		printf("problema, i = %d, P = %f\n", i, P);
	*root1 = root_array1[i] + (a - i*da)*(root_array1[i + 1] - root_array1[i])/da;
	if(a <= 0.005f)
		*root2 = (2.f - 4*RS*a)/a/(2. - 3.*RS*a);
	else
		*root2 = root_array2[i] + (a - i*da)*(root_array2[i + 1] - root_array2[i])/da;
}


float metrica_metric(float x, float theta, int indice){
	if(indice == 0)
		return -powf(c, 2)*metrica_B(x);
	else if(indice == 1)
		return metrica_A(x);
	else if(indice == 2)
		return powf(x, 2);
	else if(indice == 3)
		return powf(x*sinf(theta), 2);
}

//REMOVE THIS AS SOON AS POSSIBLE

float metrica_r(float x){
	return x;
}
float metrica_r_d(float x){
	return 1.f;
}
