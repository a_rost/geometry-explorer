#define RS    (2.f)     //Schwarszchild radius
#define R_MAX (3.*RS)   //Maximum distance so far
#define deg2rad (pi/180.f)
#define rad2deg (180.f/pi)

#define N_Q1   (1000)   //phi_array  Q resolution
#define N_r1   (1000)   //phi_array  r resolution
#define N_P2   (1500)   //phi_array2 P resolution
#define N_r2 (1500)     //phi_array2 r resolution
#define N_Q3   (1500)   //phi_array3 Q resolution
#define N_r3 (1500)     //phi_array3 r resolution

#define array1_r0 (RS)  //phi_array minimum r
#define array1_Q0 (0.f) //phi_array minimum Q

#define array2_r0 (1.5*RS + 0.15f)  //phi_array2 minimum r
#define array2_P0 (0.f)             //phi_array2 minimum P

#define array3_r0 (1.5*RS + 0.15f)  //phi_array3 minimum r
#define array3_Q0 (0.f)             //phi_array3 minimum Q


#define d_r1 ((1.5f*RS - RS)/(N_r1 - 1))
#define d_r2 ((R_MAX - 1.5*RS - 0.15)/(N_r2 - 1))
#define d_r3 ((R_MAX - 1.5*RS - 0.15)/(N_r2 - 1))

#define d_Q1   (1./(N_Q1 - 1))
#define d_P2   (7.794228634059947f/(N_P2 - 1))
#define d_Q3   (1./(N_Q3 - 1))


const unsigned int N_GEO = 4;  //number of shown geodesics
const float c = 1.5f;         //Real velocity of light 

float P_MAX;
static inline float metrica_maxP(float);

void metrica_inicializador(void){
	P_MAX = metrica_maxP(1.5*RS);
}
