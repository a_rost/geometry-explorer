import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
plt.ion()


rs, c = 2., 1.5

def P_alpha(alfa):
	return np.sin(alfa)*c*r/np.sqrt(B(x))
def B(x):
	return (1. - rs/x)

def B_p(x):
	return rs/x**2

def B_p_p(x):
	return -2*rs/x**3
	
def A(x):
	return 1./(1. - rs/x)

def U(x, P):
    return -c**2./P**2 + 1./x**2 - rs/x**3

def U_p(x, P):
    return -2/x**3 + 3*rs/x**4

def U_p_p(x, P):
	return 6/x**4 - 12*rs/x**5



#def f(x, P):
	#return 1./A(x)*(c**2/P**2/B(x) - 1./x**2)
	

def integrando_phi(x, P):
	if P == 0.:
		return np.pi/2.
	else:
		return 1./np.sqrt(c**2/P**2 - 1./x**2 + rs/x**3)/x**2

#def simple(x):
	#raiz = np.sqrt(1 - rs/x)
	#return (x*raiz + rs/2.*np.log(2*x*(raiz + 1) - rs))/c

	
def integrando_T(x, P):
	if P == 0.:
		return 1./c/np.sqrt(B(x))
	else:
		return 1./np.sqrt(c**2/P**2 - 1./x**2 + rs/x**3)/np.sqrt(B(x))/P


#P_raiz = np.sqrt(27.*rs**2/4.*c**2)


def raiz(P):
	if P == max_P(1.5*rs):
		return np.array([1.5*rs, 1.5*rs])
	elif c**2/P**2 - 4/27./rs**2 < 0.:
		array = np.roots([c**2/P**2, 0, -1, rs])[1:3]
		array.sort()
		return array
	else:
		print("no hay raices")

#def fn(x, P):
	#return c**2/P**2*x**3 - x + rs


#def gn(x, P):
	#return c**2/P**2*3*x**2 - 1

#def raiz(P):
	#a = c/P
	#medio = 1./(np.sqrt(3.)*a)
	#x0 = medio - 0.1
	#for i in range(15):
		#print(x0, fn(x0, P), gn(x0, P))
		#x1 = x0 - fn(x0, P)/gn(x0, P)
		#if x1 < rs:
			#x1 = rs
		#x0 = x1

	#x0 = medio + 0.1
	#for i in range(15):
		#print(x0, fn(x0, P), gn(x0, P))
		#x2 = x0 - fn(x0, P)/gn(x0, P)
		#if x2 < 1.5*rs:
			#x2 = 1.5*rs
		#x0 = x2
	#return np.array([x1, x2])

def integrando_t(x, P):
	if P == 0.:
		return 1.
	else:
		return 1./np.sqrt(1. - (P/r(x))**2)


def F(x, P):
	if P == 0.:
		return 0.
	elif x == rs:
		return 0.
	elif P > max_P(x):
		print("P fuera de rango")
		return 0.
	elif P < max_P(rs*1.5):
			return integrate.quad(integrando_phi, rs, x, args=(P))[0]
	else:
		roots = raiz(P)
		root1, root2 = roots.min(), roots.max()
		if x <= root1:
			return integrate.quad(integrando_phi, rs, x, args=(P))[0]
		elif root2 <= x:
			return integrate.quad(integrando_phi, root2, x, args=(P))[0]
		else:
			print("X fuera de rango")
			return 0.
 
 
 
def T(x, P):
	if P == 0.:
		return simple(x) - simple(rs)
	elif x == rs:
		return 0.
	elif P > max_P(x):
		print("P fuera de rango")
		return 0.
	elif P < max_P(rs*1.5):
			return integrate.quad(integrando_T, rs, x, args=(P))[0]
	else:
		roots = raiz(P)
		root1, root2 = roots.min(), roots.max()
		if x <= root1:
			return integrate.quad(integrando_T, rs, x, args=(P))[0]
		elif root2 <= x:
			return integrate.quad(integrando_T, root2, x, args=(P))[0]
		else:
			print("X fuera de rango")
			return 0.


def F2(x0, x, P):
	if P == 0.:
		return 0.
	elif P > max_P(x):
		print("P fuera de rango")
		return 0.
	elif P <= max_P(rs*1.5):
			return integrate.quad(integrando_phi, x0, x, args=(P))[0]
	else:
		roots = raiz(P)
		root1, root2 = roots.min(), roots.max()
		if x <= root1:
			return integrate.quad(integrando_phi, rs, x, args=(P))[0]
		elif root2 <= x:
			return integrate.quad(integrando_phi, x0, x, args=(P))[0]
		else:
			print("X fuera de rango")
			return 0.

def T2(x0, x, P):
	if P == 0.:
		return simple(x) - simple(x0)
	elif P > max_P(x):
		print("P fuera de rango")
		return 0.
	elif P <= max_P(rs*1.5):
			return integrate.quad(integrando_T, x0, x, args=(P))[0]
	else:
		roots = raiz(P)
		root1, root2 = roots.min(), roots.max()
		if x <= root1:
			return integrate.quad(integrando_T, rs, x, args=(P))[0]
		elif root2 <= x:
			return integrate.quad(integrando_T, x0, x, args=(P))[0]
		else:
			print("X fuera de rango")
			return 0.

def max_P(r):
	if r == rs:
		return 100.
	else:
		return c/np.sqrt(1./r**2 - rs/r**3)













########## Approximation





def cargador(N_P = 1000, N_rho = 500):
	global phi_array, phi_array2, phi_array3

	asdf = np.genfromtxt("../../data/schwarzschild/phi_array.dat")
	phi_array = np.zeros([N_rho, N_P])
	for i in range(N_rho):
		for j in range(N_P):
			phi_array[i][j] = asdf[i*N_P + j]

	asdf = np.genfromtxt("../../data/schwarzschild/phi_array2.dat")
	phi_array2 = np.zeros([N_rho, N_P])
	for i in range(N_rho):
		for j in range(N_P):
			phi_array2[i][j] = asdf[i*N_P + j]

	asdf = np.genfromtxt("../../data/schwarzschild/phi_array3.dat")
	phi_array3 = np.zeros([N_rho, N_P])
	for i in range(N_rho):
		for j in range(N_P):
			phi_array3[i][j] = asdf[i*N_P + j]



def interpolador(array, d_rho, d_P, rho0, P0, x, P):
	i = int(np.floor((x - rho0)/d_rho))
	j = int(np.floor((P - P0)/d_P))
	print("i = ", i, "j = ", j)
	#i = min(i, N2 - 2)
	#j = min(j, N - 2)
	
	valores = np.zeros(4)
	valores[0] = array[i + 0][j + 0]
	valores[1] = array[i + 0][j + 1]
	valores[2] = array[i + 1][j + 0]
	valores[3] = array[i + 1][j + 1]

	#if j < 0:
	#		valores[0] = np.pi*0.5
	#		valores[2] = np.pi*0.5

	print(valores)
	valores2 = np.zeros(2)
	valores2 = (valores[2:4] - valores[0:2])*(x - i*d_rho - rho0)/d_rho + valores[0:2]
	#print(valores2)
	valor = (valores2[1] - valores2[0])*(P - j*d_P - P0)/d_P + valores2[0]
	return valor


