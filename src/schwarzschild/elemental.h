float *phi_array1, *phi_array2, *phi_array3, *T_array1, *T_array2, *T_array3;
int DEBUG = 0;
float pepe, pepes[4], fi;


void geodesica_inicializador2(){
	float valor;
	unsigned int boludez;
	FILE *pfout;

	phi_array1 = (float *) malloc(N_Q1*N_r1*sizeof(float));
	phi_array2 = (float *) malloc(N_P2*N_r2*sizeof(float));
	phi_array3 = (float *) malloc(N_Q3*N_r3*sizeof(float));
	T_array1 = (float *) malloc(N_Q1*N_r1*sizeof(float));
	T_array2 = (float *) malloc(N_P2*N_r2*sizeof(float));
	T_array3 = (float *) malloc(N_Q3*N_r3*sizeof(float));

	root_array1 = (float *) malloc(100000*sizeof(float));
	root_array2 = (float *) malloc(100000*sizeof(float));
	
	
	pfout = fopen("data/schwarzschild/phi_array.dat", "r");
	for(unsigned int i = 0; i < N_r1; i++){
		for(unsigned int j = 0; j < N_Q1; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			phi_array1[i*N_Q1 + j] = valor;
		}
	}
	fclose(pfout);

	pfout = fopen("data/schwarzschild/phi_array2.dat", "r");
	for(unsigned int i = 0; i < N_r2; i++){
		for(unsigned int j = 0; j < N_P2; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			phi_array2[i*N_P2 + j] = valor;
		}
	}
	fclose(pfout);

	pfout = fopen("data/schwarzschild/phi_array3.dat", "r");
	for(unsigned int i = 0; i < N_r3; i++){
		for(unsigned int j = 0; j < N_Q3; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			phi_array3[i*N_Q3 + j] = valor;
		}
	}
	fclose(pfout);

	pfout = fopen("data/schwarzschild/T_array.dat", "r");
	for(unsigned int i = 0; i < N_r1; i++){
		for(unsigned int j = 0; j < N_Q1; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			T_array1[i*N_Q1 + j] = valor;
		}
	}
	fclose(pfout);

	pfout = fopen("data/schwarzschild/T_array2.dat", "r");
	for(unsigned int i = 0; i < N_r2; i++){
		for(unsigned int j = 0; j < N_P2; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			T_array2[i*N_P2 + j] = valor;
		}
	}
	fclose(pfout);

	pfout = fopen("data/schwarzschild/T_array3.dat", "r");
	for(unsigned int i = 0; i < N_r3; i++){
		for(unsigned int j = 0; j < N_Q3; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			T_array3[i*N_Q3 + j] = valor;
		}
	}
	fclose(pfout);
	
	pfout = fopen("data/schwarzschild/root_array1.txt", "r");
	for(unsigned int i = 0; i < 100000; i++){
		boludez = fscanf(pfout, "%f\n", &valor);
		root_array1[i] = valor;
	}
	fclose(pfout);

	pfout = fopen("data/schwarzschild/root_array2.txt", "r");
	for(unsigned int i = 0; i < 100000; i++){
		boludez = fscanf(pfout, "%f\n", &valor);
		root_array2[i] = valor;
	}
	fclose(pfout);
}

void geodesica_exit(){
	free(phi_array1);
	free(phi_array2);
	free(phi_array3);
	free(T_array1);
	free(T_array2);
	free(T_array3);
	free(root_array1);
	free(root_array2);
	
	printf("Memoria dealocatada\n");
}

static inline float geodesica_interpolador(float *phi_array, float d_rho, float d_P, float rho0, float P0, unsigned int N_P, float x, float P){
	unsigned int i, j;
	float valores[4], valores2[2], valor;

	i = (unsigned int) ((x - rho0)/d_rho);
	j = (unsigned int) ((P - P0)/d_P);

	valores[0] = phi_array[N_P*(i + 0) + j + 0];
	valores[1] = phi_array[N_P*(i + 0) + j + 1];
	valores[2] = phi_array[N_P*(i + 1) + j + 0];
	valores[3] = phi_array[N_P*(i + 1) + j + 1];
	if(DEBUG == 1)
		printf("values around = (%f, %f, %f, %f), (%d, %d)\n", valores[0], valores[1], valores[2], valores[3], i, j);
	valores2[0] = (valores[2] - valores[0])*(x - i*d_rho - rho0)/d_rho + valores[0];
	valores2[1] = (valores[3] - valores[1])*(x - i*d_rho - rho0)/d_rho + valores[1];

	valor = (valores2[1] - valores2[0])*(P - j*d_P - P0)/d_P + valores2[0];
	return valor;
}




float geodesica_integral(float a, float b, float h){
	//calculates the integral of 1/sqrt(a*x + b*x**2) from 0 to h
	float u1, u2;
	u1 = 1.f;
	u2 = 1.f + 2*h*b/a;
	
	if(b >= 0.)
		return (acoshf(u2) - acoshf(u1))/sqrtf(b);
	else
		return -(asinf(u2) - asinf(u1))/sqrtf(-b);
}


float geodesica_integral2(float A, float B, float C, float x){
	//calculates the integral of 1/sqrt(A + B*x + C*x**2) from 0 to h, assuming C > 0
	return logf(2*sqrtf(C)*sqrtf(A + x*(B + C*x)) + B + 2*C*x)/sqrtf(C);
}

float geodesica_aproximacion6(float h, float P, unsigned int tipo){
	float root1, root2, x, a, b;
	metrica_raiz_U(P, &root1, &root2);

	if(tipo == 1)
		x = root2;
	else{
		x = root1;
		h *= -1.f;
	}
	
	a = -(powf(x, 4)*metrica_U_d(x, P) + 4*powf(x, 3)*metrica_U(x, P));
	b = -(4*powf(x, 3)*metrica_U_d(x, P) + powf(x, 4)*metrica_U_d_d(x, P) + 12*powf(x, 2)*metrica_U(x, P) + 4*powf(x, 3)*metrica_U_d(x, P))/2.f;
	return geodesica_integral(a, b, h);
}

float geodesica_aproximacion6_t(float h, float P){
	float x, A, B, C;
	x = 1.5*RS;
	A = -powf(x, 4)*metrica_U(x, P);
	B = -(powf(x, 4)*metrica_U_d(x, P) + 4*powf(x, 3)*metrica_U(x, P));
	C = -(4*powf(x, 3)*metrica_U_d(x, P) + powf(x, 4)*metrica_U_d_d(x, P) + 12*powf(x, 2)*metrica_U(x, P) + 4*powf(x, 3)*metrica_U_d(x, P))/2.f;
	return geodesica_integral2(A, B, C, h) - geodesica_integral2(A, B, C, 0.f);
}

float geodesica_aproximacion7(float h, float P, unsigned int tipo){
	float root1, root2, x, a, b;
	metrica_raiz_U(P, &root1, &root2);

	if(tipo == 1)
		x = root2;
	else{
		x = root1;
		h *= -1.f;
	}
	
	a = -(metrica_B(x)*metrica_U_d(x, P) + metrica_B_d(x)*metrica_U(x, P));
	b = -(metrica_B_d(x)*metrica_U_d(x, P) + metrica_B(x)*metrica_U_d_d(x, P) + metrica_B_d_d(x)*metrica_U(x, P) + metrica_B_d(x)*metrica_U_d(x, P))/2.f;
	return geodesica_integral(a, b, h)/P;
}

float geodesica_aproximacion8(float h, float P){
	float x, A, B, C;
	x = 1.5*RS;
	A = -metrica_B(x)*metrica_U(x, P);
	B = -(metrica_B(x)*metrica_U_d(x, P) + metrica_B_d(x)*metrica_U(x, P));
	return (2*sqrtf(A + B*h)/B - 2*sqrtf(A)/B)/P;
}


static inline float metrica_maxP(float r){
	return c/sqrtf(powf(r, -2) - RS*powf(r, -3));
}
static inline float geodesica_F(float, float);

static inline float geodesica_T(float, float);

static inline float geodesica_F_backup(float x, float P){
	float P1, P2, valor, r1, r2, Q, root1, root2;
	P1 = 0.f;
	P2 = metrica_maxP(x);
	P_MAX = metrica_maxP(1.5*RS);

	if((P == P_MAX)&&(x >= 1.5f*RS))
		return 50.f;
	else if((P2 == P)&&(x >= 1.5*RS))
		return 0.f;
	else{
		
		if((P == 0.f)||(x == RS))
			return 0.f;
		else if((P <= P_MAX)&&(x >= 1.5f*RS - 0.15f)&&(x <= 1.5f*RS + 0.15f)){
			// Type 1
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d\n", DEBUG, 1);
			valor =  geodesica_aproximacion6_t(x - 1.5f*RS, P) - geodesica_aproximacion6_t(-0.15f, P);

			r1 = RS;
			r2 = 1.5*RS;
			P1 = 0.f;
			P2 = metrica_maxP(1.5*RS - 0.15f);
			Q = P/P2;
			return geodesica_interpolador(phi_array1, d_r1, d_Q1, r1, 0.f, N_Q1, 1.5f*RS - 0.15f, Q) + valor;
		}

		else if((x >= 1.5f*RS + 0.15f) && (P <= P_MAX)){
            // Type 2
			r1 = 1.5f*RS + 0.15f;
			r2 = 3.f*RS;
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d\n", DEBUG, 2);
			if(DEBUG == 1)
				printf(" blablabla por printear%f, %f, %f, %f\n", geodesica_F(1.5f*RS - 0.15f, P), geodesica_aproximacion6_t(0.15f, P), - geodesica_aproximacion6_t(-0.15f, P), geodesica_interpolador(phi_array2, d_r2, d_P2, r1, 0.f, N_P2, x, P));
			return geodesica_F(1.5f*RS - 0.15f, P) + geodesica_aproximacion6_t(0.15f, P) - geodesica_aproximacion6_t(-0.15f, P) + 
							geodesica_interpolador(phi_array2, d_r2, d_P2, r1, 0.f, N_P2, x, P);
		}
		else if(P >= P_MAX){
			metrica_raiz_U(P, &root1, &root2);

			if((x <= 1.5f*RS)&&(fabsf(x - root1) < 0.1f)){
				// Type 3
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 3);

				if(root1 - 0.1f < RS)
					return geodesica_aproximacion6(root1 - RS, P, 0) - geodesica_aproximacion6(fabsf(x - root1), P, 0);
				else{
					valor = geodesica_aproximacion6(0.1f, P, 0) - geodesica_aproximacion6(fabsf(x - root1), P, 0);
					x = root1 - 0.1f;

					r1 = RS;
					r2 = 1.5f*RS;
					P1 = 0.f;
					P2 = metrica_maxP(x);

					Q = P/P2;
					return geodesica_interpolador(phi_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q) + valor;
				}
			}

			else if((x >= 1.5f*RS)&(fabsf(x - root2) < 0.15f)){
				// Type 4
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 4);
				return geodesica_aproximacion6(fabsf(x - root2), P, 1);
			}
			else if((x >= 1.5f*RS)&&(fabsf(x - root2) >= 0.15f)){
				// Type 5
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 5);

				r1 = RS*1.5f + 0.15f;
				r2 = RS*3.f;
				P1 = P_MAX;
				P2 = metrica_maxP(x - 0.15f);

				Q = (P - P1)/(P2 - P1);
				return geodesica_interpolador(phi_array3, d_r3, d_Q3, r1, 0.f, N_Q3, x, Q) + geodesica_aproximacion6(0.15f, P, 1);
			}

			else{
				// Type 0
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 0);
				r1 = RS;
				r2 = 1.5f*RS;
				P1 = 0.f;
				P2 = metrica_maxP(x);

				Q = P/P2;
				return geodesica_interpolador(phi_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q);
			}
		}
		else{
			// Type 0
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d, rho = %f, P = %f, P2 = %f, P_MAX = %f\n", DEBUG, 6, x, P, P2, P_MAX);
			r1 = RS;
			r2 = 1.5f*RS;

			P1 = 0.f;
			P2 = metrica_maxP(x);

			Q = P/P2;
			return geodesica_interpolador(phi_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q);
		}
	}
}


static inline float geodesica_F(float x, float P){
	if(DEBUG == 1) printf("valores llamados F(%f, %f) \n", x, P);
	float P1, P2, valor, r1, r2, Q, root1, root2;
// 	DEBUG = 1;
	P1 = 0.f;
	P2 = metrica_maxP(x);
	P_MAX = metrica_maxP(1.5*RS);

	if((P == P_MAX)&&(x >= 1.5f*RS))
		return 50.f;

	else if((P2 == P)&&(x >= 1.5*RS))
		return 0.f;

	else{
		if((P == 0.f)||(x == RS))
			return 0.f;
		
		else if((P < P_MAX)&&(x > 1.5f*RS - 0.15f)&&(x < 1.5f*RS + 0.15f)){
			// Type 1
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d\n", DEBUG, 1);
			valor =  geodesica_aproximacion6_t(x - 1.5f*RS, P) - geodesica_aproximacion6_t(-0.15f, P);
			return geodesica_F(1.5f*RS - 0.15f, P) + valor;
		}

		else if((x >= 1.5f*RS + 0.15f) && (P < P_MAX)){
            // Type 2
			r1 = 1.5f*RS + 0.15f;
			r2 = 3.f*RS;
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d\n", DEBUG, 2);
			return geodesica_F(1.5f*RS - 0.15f, P) + geodesica_aproximacion6_t(0.15f, P) - geodesica_aproximacion6_t(-0.15f, P) + 
							geodesica_interpolador(phi_array2, d_r2, d_P2, r1, 0.f, N_P2, x, P);
		}
		else if(P >= P_MAX){
			metrica_raiz_U(P, &root1, &root2);

			if((x <= 1.5f*RS)&&(fabsf(x - root1) < 0.1f)){
				// Type 3
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d, |x - root1| = %f, %d, %d, root1 = %f, x = %f, |root1 - x | < 0.1 = %d\n", DEBUG, 3, fabsf(x - root1), fabsf(x - root1) == 0.1f, fabsf((root1 - 0.1) - root1) == 0.1f, root1, x, fabsf(root1 - x) < 0.1f);

				if(root1 - 0.1f < RS)
					return geodesica_aproximacion6(root1 - RS, P, 0) - geodesica_aproximacion6(fabsf(x - root1), P, 0);
				else{
					if(DEBUG == 1)
						printf("antes de evaluar, root1 = %f para P = %f\n", root1, P);
					valor = geodesica_aproximacion6(0.1f, P, 0) - geodesica_aproximacion6(fabsf(x - root1), P, 0);
					return geodesica_F(root1 - 0.1000001f, P) + valor;
				}
			}

			else if((x > 1.5f*RS)&(fabsf(x - root2) < 0.15f)){
				// Type 4
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 4);
				return geodesica_aproximacion6(fabsf(x - root2), P, 1);
			}
			else if((x >= 1.5f*RS)&&(fabsf(x - root2) >= 0.15f)){
				// Type 5
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 5);

				r1 = RS*1.5f + 0.15f;
				r2 = RS*3.f;
				P1 = P_MAX;
				P2 = metrica_maxP(x - 0.15f);

				Q = (P - P1)/(P2 - P1);
				return geodesica_interpolador(phi_array3, d_r3, d_Q3, r1, 0.f, N_Q3, x, Q) + geodesica_aproximacion6(0.15f, P, 1);
			}

			else{
				// Type 0
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 0);
				r1 = RS;
				r2 = 1.5f*RS;
				P1 = 0.f;
				P2 = metrica_maxP(x);

				Q = P/P2;
				return geodesica_interpolador(phi_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q);
			}
		}
		else{
			// Type 0
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d, rho = %f, P = %f, P2 = %f, P_MAX = %f\n", DEBUG, 6, x, P, P2, P_MAX);
			r1 = RS;
			r2 = 1.5f*RS;

			P1 = 0.f;
			P2 = metrica_maxP(x);

			Q = P/P2;
			return geodesica_interpolador(phi_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q);
		}
	}
}



static inline float geodesica_T(float x, float P){
	float P1, P2, valor, r1, r2, Q, root1, root2;
// 	DEBUG = 1;
	P1 = 0.f;
	P2 = metrica_maxP(x);
	P_MAX = metrica_maxP(1.5*RS);

	if((P == P_MAX)&&(x >= 1.5f*RS))
		return 50.f;

	else if((P2 == P)&&(x >= 1.5*RS))
		return 0.f;

	else{
		if((P == 0.f)||(x == RS))
			return 0.f;
		
		else if((P < P_MAX)&&(x > 1.5f*RS - 0.15f)&&(x < 1.5f*RS + 0.15f)){
			// Type 1
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d\n", DEBUG, 1);
			valor =  geodesica_aproximacion8(x - 1.5f*RS, P) - geodesica_aproximacion8(-0.15f, P);
			return geodesica_T(1.5f*RS - 0.15f, P) + valor;
		}

		else if((x >= 1.5f*RS + 0.15f) && (P < P_MAX)){
            // Type 2
			r1 = 1.5f*RS + 0.15f;
			r2 = 3.f*RS;
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d\n", DEBUG, 2);
			return geodesica_T(1.5f*RS - 0.15f, P) + geodesica_aproximacion8(0.15f, P) - geodesica_aproximacion8(-0.15f, P) + 
							geodesica_interpolador(T_array2, d_r2, d_P2, r1, 0.f, N_P2, x, P);
		}
		else if(P >= P_MAX){
			metrica_raiz_U(P, &root1, &root2);

			if((x <= 1.5f*RS)&&(fabsf(x - root1) < 0.1f)){
				// Type 3
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d, |x - root1| = %f, %d, %d, root1 = %f, x = %f, |root1 - x | < 0.1 = %d\n", DEBUG, 3, fabsf(x - root1), fabsf(x - root1) == 0.1f, fabsf((root1 - 0.1) - root1) == 0.1f, root1, x, fabsf(root1 - x) < 0.1f);

				if(root1 - 0.1f < RS)
					return geodesica_aproximacion7(root1 - RS, P, 0) - geodesica_aproximacion7(fabsf(x - root1), P, 0);
				else{
					valor = geodesica_aproximacion7(0.1f, P, 0) - geodesica_aproximacion7(fabsf(x - root1), P, 0);
					return geodesica_T(root1 - 0.1000001f, P) + valor;
				}
			}

			else if((x > 1.5f*RS)&(fabsf(x - root2) < 0.15f)){
				// Type 4
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 4);
				return geodesica_aproximacion7(fabsf(x - root2), P, 1);
			}
			else if((x >= 1.5f*RS)&&(fabsf(x - root2) >= 0.15f)){
				// Type 5
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 5);

				r1 = RS*1.5f + 0.15f;
				r2 = RS*3.f;
				P1 = P_MAX;
				P2 = metrica_maxP(x - 0.15f);

				Q = (P - P1)/(P2 - P1);
				return geodesica_interpolador(T_array3, d_r3, d_Q3, r1, 0.f, N_Q3, x, Q) + geodesica_aproximacion7(0.15f, P, 1);
			}

			else{
				// Type 0
				if(DEBUG == 1)
					printf("debug = %d, tipo = %d\n", DEBUG, 0);
				r1 = RS;
				r2 = 1.5f*RS;
				P1 = 0.f;
				P2 = metrica_maxP(x);

				Q = P/P2;
				return geodesica_interpolador(T_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q);
			}
		}
		else{
			// Type 0
			if(DEBUG == 1)
				printf("debug = %d, tipo = %d, rho = %f, P = %f, P2 = %f, P_MAX = %f\n", DEBUG, 6, x, P, P2, P_MAX);
			r1 = RS;
			r2 = 1.5f*RS;

			P1 = 0.f;
			P2 = metrica_maxP(x);

			Q = P/P2;
			return geodesica_interpolador(T_array1, d_r1, d_Q1, r1, 0.f, N_Q1, x, Q);
		}
	}
}


static inline float geodesica_G(float P, float rho1, float rho2){
	if((rho1 >= 1.5*RS)&&(rho2 >= 1.5*RS)&&(P >= P_MAX)){
		return geodesica_F(rho1, P) + geodesica_F(rho2, P);
	}
	else
		return 0.f;
}

static inline float geodesica_G_T(float P, float rho1, float rho2){
	if((rho1 >= 1.5*RS)&&(rho2 >= 1.5*RS)&&(P >= P_MAX)){
		return geodesica_T(rho1, P) + geodesica_T(rho2, P);
	}
	else
		return 0.f;
}

static inline float geodesica_H(float P, float rho1, float rho2){
	float root1, root2;
	metrica_raiz_U(P, &root1, &root2);
	if( (P == metrica_maxP(rho1))||(P == metrica_maxP(rho2)))
		return fabsf(geodesica_F(rho1, P) - geodesica_F(rho2, P));
	else if(P == P_MAX)
		return 50.f;
	else if((rho1 <= 1.5*RS)&&(rho2 <= 1.5*RS)&&(P > P_MAX)){
		return 2*geodesica_F(root1, P) - geodesica_F(rho1, P) - geodesica_F(rho2, P);
	}
	else
		return 0.f;
}

static inline float geodesica_H_T(float P, float rho1, float rho2){
	float root1, root2;
	metrica_raiz_U(P, &root1, &root2);
	if( (P == metrica_maxP(rho1))||(P == metrica_maxP(rho2)))
		return fabsf(geodesica_T(rho1, P) - geodesica_T(rho2, P));
	else if(P == P_MAX)
		return 50.f;
	else if((rho1 <= 1.5*RS)&&(rho2 <= 1.5*RS)&&(P > P_MAX)){
		return 2*geodesica_T(root1, P) - geodesica_T(rho1, P) - geodesica_T(rho2, P);
	}
	else
		return 0.f;
}

static inline float geodesica_I(float P, float rho1, float rho2){
	if((P == P_MAX)&&((rho1 - 1.5f*RS)*(rho2 - 1.5f*RS) <= 0.f))
		return 20.f;
	else if((P == P_MAX)&&(rho1 >= 1.5f*RS)&&(rho2 >= 1.5f*RS))
		return fabsf(geodesica_F(rho1, P_MAX) - geodesica_F(rho2, P_MAX));
	else
		return fabsf(geodesica_F(rho1, P) - geodesica_F(rho2, P));
}

static inline float geodesica_I_T(float P, float rho1, float rho2){
	if((P == P_MAX)&&((rho1 - 1.5f*RS)*(rho2 - 1.5f*RS) <= 0.f))
		return 20.f;
	else if((P == P_MAX)&&(rho1 >= 1.5f*RS)&&(rho2 >= 1.5f*RS))
		return fabsf(geodesica_T(rho1, P_MAX) - geodesica_T(rho2, P_MAX));
	else
		return fabsf(geodesica_T(rho1, P) - geodesica_T(rho2, P));
}

static inline float geodesica_exp_I(float P, float rho1, float rho2){
	return expf(-geodesica_I(P, rho1, rho2));
}

static inline float geodesica_exp_G(float P, float rho1, float rho2){
	return expf(-geodesica_G(P, rho1, rho2));
}
static inline float geodesica_exp_H(float P, float rho1, float rho2){
	return expf(-geodesica_H(P, rho1, rho2));
}

float asinf2(float x){
	if(x >= 1.f)
		return pi*0.5f;
	else if(x <= -1.f)
		return -pi*0.5f;
	else
		return asinf(x);
}
void geodesica_secante(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, float error, float *result){
	float A = P1, B = P2, fa, fb, C;
	unsigned int i, j;

    fa = (*G)(A, rho1, rho2) - phi; 
	fb = (*G)(B, rho1, rho2) - phi;
// 	printf("fa = %f, fb = %f\n", fa, fb);
// 	if(error < 0.001f)
// 		error = error*phi;
    if(fa*fb > 0.f){
//  		printf("no existe solucion, phi fuera de rango, a = %f,b = %f, fa = %f, fb = %f, phi = %f\n", a, b, fa, fb, phi);
        result[0] = 0.f;
		result[1] = 0.f;
	}
    else{
        for(i = 0; i < 40; i++){
            C = B - (B - A)*fb/(fb - fa);
            if(C < P1)
				C = P1;
            else if(C >= P2)
				C = P2;
            A = B;
            B = C;
            fa = fb;
            fb = (*G)(B, rho1, rho2) - phi;
			if(DEBUG == 1)
				printf("sec: rho1 = %f, rho2 = %f, P1 = %f, P2 = %f, phi = %f, A, B, C = (%f, %f, %f), fb = %f\n", rho1, rho2, P1, P2, phi, A, B, C, fb);
//          printf("diferencia = %f, a = %f, b = %f\n", fabsf(fb), a, b);
            if(A == B){
				A = (P1 + P2)*0.5f; 
				B = P2;
				fa = (*G)(A, rho1, rho2) - phi;
				fb = (*G)(B, rho1, rho2) - phi;
			}

            if((fabsf(fb) < error)||(fabsf(A - B) <= 0.0001f))
                break;
		}
		if((fabsf(fb) < error)){
			if(DEBUG == 1)
				printf("secante: argum = %f, funcion = %f\n", B/rho1/c*sqrtf(metrica_B(rho1)), asinf(B/rho1/c*sqrtf(metrica_B(rho1))));
            result[0] = asinf2(B/rho1/c*sqrtf(metrica_B(rho1)));
            result[1] = 1.f;
        }
        else{
			if(DEBUG == 1)
				printf("secante: argum = %f, funcion = %f\n", B/rho1/c*sqrtf(metrica_B(rho1)), asinf(B/rho1/c*sqrtf(metrica_B(rho1))));
            result[0] = asinf2(B/rho1/c*sqrtf(metrica_B(rho1)));
            result[1] = 2.f;
        }
        if(DEBUG == 1)
			printf("sec: dice que termino\n");
	}
	pepe = B;
}

void zriddr(float (*func)(float, float, float), float rho1, float rho2, float phi, float x1, float x2, float xacc, float *result){
	const int MAXIT = 30;
	const float UNUSED = (-1.11e7);
	int j;

	float ans,fh,fl,fm,fnew,s,xh,xl,xm,xnew;
	
	fl=(*func)(x1, rho1, rho2) - phi;
	fh=(*func)(x2, rho1, rho2) - phi;
	if ((fl > 0.0 && fh < 0.0) || (fl < 0.0 && fh > 0.0)) {
		xl=x1;
		xh=x2;
		ans=UNUSED;
		for (j=1;j<=MAXIT;j++) {
			xm=0.5*(xl+xh);
			fm=(*func)(xm, rho1, rho2) - phi;
			s=sqrt(fm*fm-fl*fh);
			if (s == 0.0){
				result[0] = asinf2(ans/rho1/c*sqrtf(metrica_B(rho1)));
				result[1] = 1.f;
			}
			xnew=xm+(xm-xl)*((fl >= fh ? 1.0 : -1.0)*fm/s);
			if (fabs(xnew-ans) <= xacc){
				result[0] = asinf2(ans/rho1/c*sqrtf(metrica_B(rho1)));
				result[1] = 1.f;
			}
			ans=xnew;
			fnew=(*func)(ans, rho1, rho2) - phi;
			if (fnew == 0.0){
				result[0] = asinf2(ans/rho1/c*sqrtf(metrica_B(rho1)));
				result[1] = 1.f;
			}
			if (SIGN(fm,fnew) != fm) {
				xl=xm;
				fl=fm;
				xh=ans;
				fh=fnew;
			}
			else if (SIGN(fl,fnew) != fl) {
				xh=ans;
				fh=fnew;
			}
			else if (SIGN(fh,fnew) != fh) {
				xl=ans;
				fl=fnew;
				
			} 
			else 
				printf("never get here.\n");
			if (fabs(xh-xl) <= xacc){
				result[0] = asinf2(ans/rho1/c*sqrtf(metrica_B(rho1)));
				result[1] = 1.f;
			}
		}
		printf("zriddr exceed maximum iterations\n");
	}
	else {
		if (fl == 0.0){
			result[0] = asinf2(x1/rho1/c*sqrtf(metrica_B(rho1)));
			result[1] = 1.f;
		}
		if (fh == 0.0){
			result[0] = asinf2(x2/rho1/c*sqrtf(metrica_B(rho1)));
			result[1] = 1.f;
		}
		printf("root must be bracketed in zriddr.\n");
	}
}


void geodesica_biseccion2(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, unsigned int iteraciones, float error, float *result){
	float A = P1, B = P2, fa, fb, fc, C;
	unsigned int i, j;

	fa = (*G)(A, rho1, rho2) - phi; 
	fb = (*G)(B, rho1, rho2) - phi;
	if(fa*fb > 0.f){
		if(DEBUG == 1)
			printf("fuera de rango, rho1 = %f, rho2 = %f, phi = %f, fa = %f, fb = %f, A = %f, B = %f\n", rho1, rho2, phi, fa, fb, A, B);
//		printf("Fuera de rango, fa = %f, fb = %f, rho1 = %f, rho2 = %f, phi = %f, P1 = %f, P2 = %f\n", fa, fb, rho1, rho2, phi, P1, P2);
		if(DEBUG == 1)
			printf("Como puede ser, fa = %f, fb = %f, H(%f, %f, %f) = %f; H(%f, %f, %f) = %f\n", fa, fb, A, rho1, rho2, (*G)(A, rho1, rho2), B, rho1, rho2, (*G)(B, rho1, rho2));
		result[0] = 0.f;
		result[1] = 0.f;
// 		exit(0);s
	}
	else{
		for(i = 0; i < iteraciones; i++){
			C  = (A + B)*0.5f;
			fc = (*G)(C, rho1, rho2) - phi;
			if(DEBUG == 1)
				printf("bis: rho1 = %f, rho2 = %f, P1 = %f, P2 = %f, phi = %f, A, B, C = (%f, %f, %f)\n", rho1, rho2, P1, P2, phi, A, B, C);
			
			if(fa*fc < 0.f){
				B  = C;
				fb = fc;
			}
			else if(fc*fb < 0.f){
				A  = C;
				fa = fc;
			}
			if(fabsf(fc) < error)
				break;
		}
		result[0] = A;
		result[1] = B;
		if(DEBUG == 1)
			printf("bis: dice que termino\n");
	}
	pepe = B;
}

void geodesica_mixto(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, 
					 float (*X)(float, float, float), float *result){
	if(phi <= pi*0){
		geodesica_biseccion2((*G), rho1, rho2, phi, P1, P2, 10, 0.1f, result);

		if(result[1] != 0.f){
			geodesica_secante((*G), rho1, rho2, phi, result[0], result[1], 0.0001f, result);
		}
	}
	else{
		float error;
		if(phi >= 2*pi)
			error = 0.0009f;
		else
			error = 0.0009f;
		geodesica_biseccion2((*X), rho1, rho2, expf(-phi), P1, P2, 10, error*1.f, result);
		//geodesica_biseccion2((*G), rho1, rho2, phi, P1, P2, 10, 0.001f, result);

		if(result[1] != 0.f){
			
			geodesica_secante((*X), rho1, rho2, expf(-phi), result[0], result[1], error, result);
		}
	}
}

void geodesica_raiz_H(float rho1, float rho2, float phi, unsigned int tipo, float *alfa, unsigned int *exito){
	float P1 = metrica_maxP(rho1), P2 = metrica_maxP(rho2), P3;
	float P_max = fmin(P1, P2), *result, alfa1;


	result = (float *) malloc(2*sizeof(float));
	geodesica_mixto(&geodesica_H, rho1, rho2, phi, P_MAX, P_max, &geodesica_exp_H, result);
	alfa1  = result[0];
	*exito = (unsigned int) (result[1]);

	//printf("Raiz G, solucion, alfa = %f, exito = %u\n", alfa1, *exito);
	if(tipo == 0){
		if(fmodf(phi, 2*pi) < pi)
			*alfa = pi - alfa1;
		else
			*alfa = pi + alfa1;
	}
	free(result);
}

void geodesica_raiz_G(float rho1, float rho2, float phi, unsigned int tipo, float *alfa, unsigned int *exito){
	float P1 = metrica_maxP(rho1), P2 = metrica_maxP(rho2), P3;
	float P_max = fmin(P1, P2), *result, alfa1;

	result = (float *) malloc(2*sizeof(float));
	geodesica_mixto(&geodesica_G, rho1, rho2, phi, P_MAX, P_max, &geodesica_exp_G, result);
	alfa1  = result[0];
	*exito = (unsigned int) (result[1]);

	//printf("Raiz G, solucion, alfa = %f, exito = %u\n", alfa1, *exito);
	if(tipo == 0){
		if(fmodf(phi, 2*pi) < pi)
			*alfa = alfa1;
		else
			*alfa = 2*pi - alfa1;
	}
	else{
		if(fmodf(phi, 2*pi) < pi)
			*alfa = pi - alfa1;
		else
			*alfa = pi + alfa1;
	}
	free(result);
}


void geodesica_raiz_I(float rho1, float rho2, float phi, float *alfa, unsigned int *exito){
	float P1 = metrica_maxP(rho1), P2 = metrica_maxP(rho2), P3;
	float P_max, *result, alfa1;

	if((rho1 - 1.5f*RS)*(rho2 - 1.5f*RS) < 0.f)
		P_max = P_MAX;
	else
		P_max = fmin(P1, P2);
	
	result = (float *) malloc(2*sizeof(float));
// 	printf("I(%f, %f, %f) = %f\n", 0.f, rho1, rho2, geodesica_I(0.f, rho1, rho2));
// 	printf("I(%f, %f, %f) = %f\n", P_max, rho1, rho2, geodesica_I(P_max, rho1, rho2));
	geodesica_mixto(&geodesica_I, rho1, rho2, phi, 0.f, P_max, &geodesica_exp_I, result);
	alfa1 = result[0];
	*exito = (unsigned int) (result[1]);

	if(rho1 < rho2){
		if(fmodf(phi, 2*pi) <= pi)
			*alfa = pi - alfa1;
		else
			*alfa = pi + alfa1;
	}
	else{
		if(fmodf(phi, 2*pi) <= pi)
			*alfa = alfa1;
		else
			*alfa = 2*pi - alfa1;
	}
	free(result);

}
