




def H(P, rho1, rho2):
	raices = raiz(P)
	return F(raices[0], P) - F(rho1, P) - F(rho2, P)

def G(P, rho1, rho2):
	return F(rho1, P) + F(rho2, P)
	
def I(P, rho1, rho2):
	if P == 0.:
		return 0.
	else:
		return abs(F(rho2, P) - F(rho1, P))

def H3(P, rho1, rho2):
	raices = raiz(P)
	return 2*F3(raices[0], P) - F3(rho1, P) - F3(rho2, P)

def G3(P, rho1, rho2):
	return F3(rho1, P) + F3(rho2, P)


def I3(P, rho1, rho2):
	if P == 0.:
		return 0.
	else:
		return abs(F3(rho2, P) - F3(rho1, P))


def show_I(rho1, rho2, opt = False):
	N = 400
	if (rho1 - 1.5*rs)*(rho2 - 1.5*rs) < 0.:
		P_maxi = max_P(1.5*rs)
	else:
		P_maxi = min(max_P(rho1), max_P(rho2))
		
	P_space = np.linspace(0., P_maxi, N)
	y_space = np.zeros(N)
	for i in range(N):
		print(P_space[i], rho1, rho2)
		if opt == False:
			y_space[i] = I(P_space[i], rho1, rho2)
		else:
			y_space[i] = I3(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)

def show_exp_I(rho1, rho2, opt = False):
	N = 400
	if (rho1 - 1.5*rs)*(rho2 - 1.5*rs) < 0.:
		P_maxi = max_P(1.5*rs)
	else:
		P_maxi = min(max_P(rho1), max_P(rho2))
		
	P_space = np.linspace(0., P_maxi, N)
	y_space = np.zeros(N)
	for i in range(N):
		print(P_space[i], rho1, rho2)
		if opt == False:
			y_space[i] = np.exp(-I(P_space[i], rho1, rho2))
		else:
			y_space[i] = np.exp(-I3(P_space[i], rho1, rho2))
	plt.plot(P_space, y_space)

def show_H(rho1, rho2, opt = False):
	N = 400
	P_maxi = min(max_P(rho1), max_P(rho2))
	P_space = np.linspace(max_P(1.5*rs), P_maxi, N)
	y_space = np.zeros(N)
	for i in range(N):
		print(P_space[i], rho1, rho2)
		if opt == False:
			y_space[i] = H(P_space[i], rho1, rho2)
		else:
			y_space[i] = H3(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)

def show_G(rho1, rho2, opt = False):
	N = 400
	P_maxi = min(max_P(rho1), max_P(rho2))
	P_space = np.linspace(max_P(1.5*rs), P_maxi, N)
	y_space = np.zeros(N)
	for i in range(N):
		print(P_space[i], rho1, rho2)
		if opt == False:
			y_space[i] = G(P_space[i], rho1, rho2)
		else:
			y_space[i] = G3(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)



"""

archivo = open("../../data/schwarzschild/phi_array.dat", "w")
for i in range(len(phi_array)):
	for j in range(len(phi_array[0])):
		archivo.write(str(phi_array[i][j]) + "\n")
archivo.close()

archivo = open("../../data/schwarzschild/phi_array2.dat", "w")
for i in range(len(phi_array2)):
	for j in range(len(phi_array2[0])):
		archivo.write(str(phi_array2[i][j]) + "\n")
archivo.close()

archivo = open("../../data/schwarzschild/phi_array3.dat", "w")
for i in range(len(phi_array3)):
	for j in range(len(phi_array3[0])):
		archivo.write(str(phi_array3[i][j]) + "\n")
archivo.close()



archivo = open("../../data/schwarzschild/T_array.dat", "w")
for i in range(len(T_array)):
	for j in range(len(T_array[0])):
		archivo.write(str(T_array[i][j]) + "\n")
archivo.close()

archivo = open("../../data/schwarzschild/T_array2.dat", "w")
for i in range(len(T_array2)):
	for j in range(len(T_array2[0])):
		archivo.write(str(T_array2[i][j]) + "\n")
archivo.close()

archivo = open("../../data/schwarzschild/T_array3.dat", "w")
for i in range(len(T_array3)):
	for j in range(len(T_array3[0])):
		archivo.write(str(T_array3[i][j]) + "\n")
archivo.close()

"""



asdf = np.genfromtxt("../../data/schwarzschild/T_array.dat")
T_array = np.zeros([1000, 1000])
for i in range(1000):
	for j in range(1000):
		T_array[i][j] = asdf[i*1000 + j]

asdf = np.genfromtxt("../../data/schwarzschild/T_array2.dat")
T_array2 = np.zeros([1500, 1500])
for i in range(1500):
	for j in range(1500):
		T_array2[i][j] = asdf[i*1500 + j]

asdf = np.genfromtxt("../../data/schwarzschild/T_array3.dat")
T_array3 = np.zeros([1500, 1500])
for i in range(1500):
	for j in range(1500):
		T_array3[i][j] = asdf[i*1500 + j]


def integral(a, b, h):
	#calculates the integral of 1/sqrt(a*x + b*x**2) from 0 to h
	u1, u2 = 1., 1. + 2*h*b/a
	if b >= 0.:
		return (np.arccosh(u2) - np.arccosh(u1))/np.sqrt(b)
	else:
		return -(np.arcsin(u2) - np.arcsin(u1))/np.sqrt(-b)

def integral2(a, b, c, x):
	#calculates the integral of 1/sqrt(a + b*x + c*x**2) from 0 to h, assuming c > 0
	if c == 0.:
		return 2./b*np.sqrt(a + b*x)
	else:
		return np.log(2*np.sqrt(c)*np.sqrt(a + x*(b + c*x)) + b + 2*c*x)/np.sqrt(c)

def aproximacion6(h, P, tipo):
	raices = raiz(P)
	if tipo == 1:
		x = raices[1]
	else:
		x = raices[0]
		h *= -1.
	a = -(x**4*U_p(x, P) + 4*x**3*U(x, P))
	b = -(4*x**3*U_p(x, P) + x**4*U_p_p(x, P) + 12*x**2*U(x, P) + 4*x**3*U_p(x, P))/2.
	return integral(a, b, h)

def aproximacion6_t(h, P):
	x = 1.5*rs
	a = -x**4*U(x, P)
	b = -(x**4*U_p(x, P) + 4*x**3*U(x, P))
	c = -(4*x**3*U_p(x, P) + x**4*U_p_p(x, P) + 12*x**2*U(x, P) + 4*x**3*U_p(x, P))/2.
	return integral2(a, b, c, h) - integral2(a, b, c, 0.)




def aproximacion7(h, P, tipo):
	raices = raiz(P)
	if tipo == 1:
		x = raices[1]
	else:
		x = raices[0]
		h *= -1.
	a = -(B(x)*U_p(x, P) + B_p(x)*U(x, P))
	b = -(B_p(x)*U_p(x, P) + B(x)*U_p_p(x, P) + B_p_p(x)*U(x, P) + B_p(x)*U_p(x, P))/2.
	return integral(a, b, h)/P

def aproximacion8(h, P):
	x = 1.5*rs
	a = -B(x)*U(x, P)
	b = -(B(x)*U_p(x, P) + B_p(x)*U(x, P))
	c = -(B_p(x)*U_p(x, P) + B(x)*U_p_p(x, P) + B_p_p(x)*U(x, P) + B_p(x)*U_p(x, P))/2.*0.
	print(a, b, c)
	return (integral2(a, b, c, h) - integral2(a, b, c, 0.))/P


def F3(x, P):
	print("valores llamados :", x, P)
	P1, P2 = 0., max_P(x)
	if P2 < P - 0.0001:
		print("P fuera de rango")
		return 0.
	else:
		if P == 0. or x == 0.:
			return 0.
		elif P == P_max and x >= 1.5*rs:
			return 50.
		elif P <= P_max and x >= 1.5*rs - 0.15 and x <= 1.5*rs + 0.15:
			print("Tipo 1")
			valor =  aproximacion6_t(x - 1.5*rs, P) - aproximacion6_t(- 0.15, P)
			x = 1.5*rs - 0.15
			print("Tipo 0", x, P)
			r1, r2 = rs, 1.5*rs
			P1, P2 = 0., max_P(x)
			Q = P/P2
			return interpolador(phi_array, (r2 - r1)/999., 1./999., r1, 0., x, Q) + valor
		elif x >= 1.5*rs + 0.15 and P <= P_max:
			print("Tipo 2")
			Q = P/P_max
			r1, r2 = 1.5*rs + 0.15, 3*rs
			return F3(1.5*rs - 0.15, P) + aproximacion6_t(0.15, P) - aproximacion6_t(-0.15, P) + interpolador(phi_array2, (r2 - r1)/1499., 1./1499., r1, 0., x, Q)
		elif P >= P_max:
			raices = raiz(P)
			if x < 1.5*rs and abs(x - raices[0]) < 0.1:
				print("Tipo 3")
				if raices[0] - 0.1 < rs:
					return aproximacion6(raices[0] - rs, P, 0) - aproximacion6(abs(x - raices[0]), P, 0)
				else:
					valor = aproximacion6(0.1, P, 0) - aproximacion6(abs(x - raices[0]), P, 0)
					x = raices[0] - 0.1
					print("Tipo 0", x, P)
					r1, r2 = rs, 1.5*rs
					P1, P2 = 0., max_P(x)
					Q = P/P2
					print("'valores llamados' :", x, P)
					return interpolador(phi_array, (r2 - r1)/999., 1./999., r1, 0., x, Q) + valor			
			elif x >= 1.5*rs and abs(x - raices[1]) < 0.15:
				print("Tipo 4")
				return aproximacion6(abs(x - raices[1]), P, 1)
			elif x >= 1.5*rs and abs(x - raices[1]) >= 0.15:
				print("Tipo 5")
				r1, r2 = rs*1.5 + 0.15, rs*3
				P1, P2 = P_max, max_P(x - 0.15)
				Q = (P - P1)/(P2 - P1)
				return interpolador(phi_array3, (r2 - r1)/1499., 1./1499., r1, 0., x, Q) + aproximacion6(0.15, P, 1)
			else:
				print("Tipo 0", x, P)
				r1, r2 = rs, 1.5*rs
				P1, P2 = 0., max_P(x)
				Q = P/P2
				return interpolador(phi_array, (r2 - r1)/999., 1./999., r1, 0., x, Q)
		else:
			print("Tipo 0", x, P)
			r1, r2 = rs, 1.5*rs
			P1, P2 = 0., max_P(x)
			Q = P/P2
			return interpolador(phi_array, (r2 - r1)/999., 1./999., r1, 0., x, Q)



def T3(x, P):
	P1, P2 = 0., max_P(x)
	if P2 < P - 0.0001:
		print("P fuera de rango")
		return 0.
	else:
		if P == 0. or x == rs:
			return 0.
		elif P == P_max and x >= 1.5*rs:
			return 50.
		elif P <= P_max and x >= 1.5*rs - 0.15 and x <= 1.5*rs + 0.15:
			print("Tipo 1")
			valor =  aproximacion8(x - 1.5*rs, P) - aproximacion8(- 0.15, P)
			x = 1.5*rs - 0.15
			print("Tipo 0", x, P)
			r1, r2 = rs, 1.5*rs
			P1, P2 = 0., max_P(x)
			Q = P/P2
			return interpolador(T_array, (r2 - r1)/999., 1./999., r1, 0., x, Q) + valor
		elif x >= 1.5*rs + 0.15 and P <= P_max:
			print("Tipo 2")
			Q = P/P_max
			r1, r2 = 1.5*rs + 0.15, 3*rs
			return T3(1.5*rs - 0.15, P) + aproximacion8(0.15, P) - aproximacion8(-0.15, P) + interpolador(T_array2, (r2 - r1)/1499., 1./1499., r1, 0., x, Q)
		elif P >= P_max:
			raices = raiz(P)
			if x < 1.5*rs and abs(x - raices[0]) < 0.1:
				print("Tipo 3")
				if raices[0] - 0.1 < rs:
					return aproximacion7(raices[0] - rs, P, 0) - aproximacion7(abs(x - raices[0]), P, 0)
				else:
					valor = aproximacion7(0.1, P, 0) - aproximacion7(abs(x - raices[0]), P, 0)
					x = raices[0] - 0.1
					print("Tipo 0", x, P)
					r1, r2 = rs, 1.5*rs
					P1, P2 = 0., max_P(x)
					Q = P/P2
					return interpolador(T_array, (r2 - r1)/999., 1./999., r1, 0., x, Q) + valor			
			elif x >= 1.5*rs and abs(x - raices[1]) < 0.15:
				print("Tipo 4")
				return aproximacion7(abs(x - raices[1]), P, 1)
			elif x >= 1.5*rs and abs(x - raices[1]) >= 0.15:
				print("Tipo 5")
				r1, r2 = rs*1.5 + 0.15, rs*3
				P1, P2 = P_max, max_P(x - 0.15)
				Q = (P - P1)/(P2 - P1)
				return interpolador(T_array3, (r2 - r1)/1499., 1./1499., r1, 0., x, Q) + aproximacion7(0.15, P, 1)
			else:
				print("Tipo 0", x, P)
				r1, r2 = rs, 1.5*rs
				P1, P2 = 0., max_P(x)
				Q = P/P2
				return interpolador(T_array, (r2 - r1)/999., 1./999., r1, 0., x, Q)
		else:
			print("Tipo 0", x, P)
			r1, r2 = rs, 1.5*rs
			P1, P2 = 0., max_P(x)
			Q = P/P2
			return interpolador(T_array, (r2 - r1)/999., 1./999., r1, 0., x, Q)




r1, r2 = 1.5*rs + 0.5, 1.5*3

h = 0.15
T_array2 = np.zeros([1500, 1500])
r1, r2 = rs*1.5 + h, 3*rs
n, m = len(T_array2) - 1., len(T_array2[0]) - 1
for i in range(len(T_array2)):
	r = r1 + (r2 - r1)/n*i
	print("i = ", i, "r = ", r)
	for j in range(len(T_array2[0])):
		P1, P2 = 0., P_max
		P = P1 + (P2 - P1)/m*j
		T_array2[i, j] = T2(r1, r, P)

T_array3 = np.zeros([1500, 1500])
n, m = len(T_array3) - 1., len(T_array3[0]) - 1.
r1, r2 = rs*1.5 + h, 3*rs
for i in range(len(T_array3)):
	r = r1 + (r2 - r1)/n*i
	print("i = ", i, "r = ", r)
	for j in range(len(T_array3[0])):
		P1, P2 = max_P(1.5*rs), max_P(r - h)
		P = P1 + (P2 - P1)/m*j
		if P <= max_P(1.5*rs):
			x0 = rs*1.5 + h
		else:
			raices = raiz(P)
			x0 = raices[1] + h
		T_array3[i, j] = T2(x0, r, P)





N = 20000
r_space = np.random.random(N)*(3*rs - rs) + rs
P_space = np.random.random(N)
for i in range(N):
	P_space[i] *= max_P(r_space[i])

y_space1, y_space2 = np.zeros(N), np.zeros(N)
for i in range(N):
	y_space1[i] = F(r_space[i], P_space[i])	
	y_space2[i] = F3(r_space[i], P_space[i])	

#plt.plot(y_space1, y_space2, ".")


errors = abs(y_space2 - y_space1)
errors2 = abs(y_space2 - y_space1)/y_space1
mask = errors < 0.002
#mask = errors2 < np.percentile(errors2, 10.)
#mask = errors < 0.04
plt.plot(r_space[mask], P_space[mask],   ".")
plt.plot(r_space[~mask], P_space[~mask], ".")
plt.ylim([0., 20.])



P_space = np.linspace(P_max, min(max_P(rho1), max_P(rho2)), 1400)
y_space= np.zeros(1400)
for i in range(1400):
	y_space[i] = F(rho2, P_space[i])
plt.plot(P_space, y_space)
plt.plot(P_space, asdf1[2])






r1, r2 = rs, 3*rs - 0.01
arreglo = np.zeros([1000, 1000])
for i in range(1000):
	rho = r1 + i*(r2 - r1)/999.
	P1, P2 = 0., max_P(rho)
	for j in range(1000):
		P = P1 + (P2 - P1)*j/999.
		arreglo[i, j] = F3(rho, P)


asdf = np.genfromtxt("../../soluciones.txt", unpack  = True)


def ploteador(i):
	rho1, rho2, phi, P1, P2, P3, P4 = asdf[0][i], asdf[1][i], asdf[2][i], asdf[3][i], asdf[4][i], asdf[5][i], asdf[6][i]
	show_I(rho1, rho2, True)
	if((rho1 - 1.5*rs)*(rho2 - 1.5*rs)) <= 0.:
		pass
	elif (rho1 <= 1.5*rs) and (rho2 <= 1.5*rs):
		show_H(rho1, rho2, True)
	else:
		show_G(rho1, rho2, True)
	plt.plot([P1], [2*0     + phi], "o")
	plt.plot([P2], [2*np.pi - phi], "o")
	plt.plot([P3], [2*np.pi + phi], "o")
	plt.plot([P4], [4*np.pi - phi], "o")
	
		

valores = np.zeros(len(qwer[0]))
for i in range(len(valores)):
	valores[i] = F3(qwer[0][i], qwer[1][i])
	
	
	
def soluciones(a):
	if c/a == max_P(1.5*rs):
		return np.array([1.5*rs, 1.5*rs])
	elif a**2 - 4/27./rs**2 < 0.:
		array = np.roots([a**2, 0, -1, rs])[1:3]
		array.sort()
		return array
	else:
		print("no hay raices")


a_space = np.linspace(0., c/P_max, 1000)
root1_space = np.zeros(1000)
root2_space = np.zeros(1000)
root3_space = np.zeros(1000)

for i in range(1, 1000):
	roots = soluciones(a_space[i])
	root1_space[i] = roots[0]
	root2_space[i] = roots[1]

root3_space = (rs - 2*a_space**2*rs**3)/(1. - 3*a_space*rs**2)
root4_space = 1./a_space
root5_space = (2 - 4*rs*a_space)/a_space/(2 - 3*rs*a_space)

def raiz3(P):
	a = c/P
	da = c/P_max/999.
	if a >= c/P_max:
		print("fuera de rango")
	else:
		i = int(a/da)
		root1 = root1_space[i] + (a - i*da)*(root1_space[i + 1] - root1_space[i])/da
		if a <= 0.005:
			root2 = (2 - 4*rs*a)/a/(2 - 3.*rs*a)
		else:
			root2 = root2_space[i] + (a - i*da)*(root2_space[i + 1] - root2_space[i])/da
		return root1, root2




asdf = np.genfromtxt("../../volcado_vert.dat", unpack = True) 
P1, P2, P3, P4 = asdf[9], asdf[10], asdf[11], asdf[12]
rho2, theta2, phi2 = asdf[3], asdf[4], asdf[5]
rho1, theta1, phi1 = asdf[6], asdf[7], asdf[8]

x1, y1, z1 = np.cos(phi1)*np.sin(theta1), np.sin(phi1)*np.sin(theta1), np.cos(theta1)
x2, y2, z2 = np.cos(phi2)*np.sin(theta2), np.sin(phi2)*np.sin(theta2), np.cos(theta2)

cosenos = x1*x2 + y1*y2 + z1*z2
phis = np.arccos(cosenos)


def evidenciador(i, mask = np.ones(len(asdf[0]), dtype = np.bool)):
	show_I(rho1[mask][i], rho2[mask][i], True)
	if((rho1[mask][i] - 1.5*rs)*(rho2[mask][i] - 1.5*rs)) <= 0.:
		pass
	elif (rho1[mask][i] <= 1.5*rs) and (rho2[mask][i] <= 1.5*rs):
		show_H(rho1[mask][i], rho2[mask][i], True)
	else:
		show_G(rho1[mask][i], rho2[mask][i], True)
	plt.plot([P1[mask][i]], [2*0     + phis[mask][i]], "o")
	plt.plot([P2[mask][i]], [2*np.pi - phis[mask][i]], "o")
	plt.plot([P3[mask][i]], [2*np.pi + phis[mask][i]], "o")
	plt.plot([P4[mask][i]], [4*np.pi - phis[mask][i]], "o")


def show_exp(i):

	x1 = rho1[i]
	x2 = rho2[i]
	phi1 = phis[i]

	N = 400
	P_maxi = min(max_P(x1), max_P(x2))
	P_space = np.linspace(max_P(1.5*rs), P_maxi, N)
	y_space = np.zeros(N)
	for k in range(N):
		print(P_space[k], x1, x2)
		y_space[k] = H3(P_space[k], x1, x2)

	plt.plot(P_space, np.exp(-y_space))
	phi2 = 2*np.pi - phi1
	phi3 = 2*np.pi + phi1
	phi4 = 4*np.pi - phi1
	print(P1[i])
	plt.plot([P1[i], P2[i], P3[i], P4[i]], np.exp(-np.array([phi1, phi2, phi3, phi4])), ".")



asdf = np.genfromtxt("../../volcado_T2.dat") 

x_space = np.linspace(rs, 1.5*rs, 1000)
y_space1 = np.zeros(1000)
y_space2 = np.zeros(1000)

for i in range(1000):
	y_space1[i] = T3(x_space[i], max_P(x_space[i]))
	y_space2[i] = T(x_space[i] - 0.000001, max_P(x_space[i]))
	
plt.plot(x_space, y_space2)
plt.plot(x_space, y_space1)
plt.plot(x_space, asdf)



from mpl_toolkits.mplot3d import Axes3D

asdf = np.genfromtxt("../../volcado_vert.dat", unpack  = True)
rho2, theta2, phi2 = asdf[3], asdf[4], asdf[5]
rho1, theta1, phi1 = asdf[6], asdf[7], asdf[8]

x1, y1, z1 = np.cos(phi1)*np.sin(theta1), np.sin(phi1)*np.sin(theta1), np.cos(theta1)
x2, y2, z2 = np.cos(phi2)*np.sin(theta2), np.sin(phi2)*np.sin(theta2), np.cos(theta2)

cosenos = x1*x2 + y1*y2 + z1*z2
phis = np.arccos(cosenos)



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(asdf[3], phis, asdf[9], ".")
