import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
plt.ion()

a = 1.
c = 1.5
def r(x):
	return np.sqrt(a**2 + x**2)

def r_p(x):
	return (a**2 + x**2)**(-0.5)*x


def U(x, P):
    return -c**2/P**2 + 1./r(x)**2

def U_p(x, P):
    return -2*r_p(x)*r(x)**(-3)

def raiz(P):
	if P/c < a:
		print("no hay solucion, P < h")
		return -1.
	elif P/c == a:
		return 0.
	else:
		return np.sqrt(P**2/c**2 - a**2)

def integrando_phi(x, P):
	if c**2/P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(c**2/P**2 - 1./r(x)**2)/r(x)**2


def F(x, P):
	if P**2/c**2 >= a:
		rho_min = raiz(P)
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_phi, rho_max, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_phi, 0., x, args=(P))[0]


def inter(x, a, b):
	u = 1./r(x)
	m = a**2*b**2
	print("arg", u/b, "funcion = ", ellip(np.arcsin(u/b), m), "m = ", m)
	return -b*np.sqrt(1. - u**2/b**2)*ellip(np.arcsin(u/b), m)/np.sqrt(b**2 - u**2)


def F2(x, P):
	if x >= 0.:
		if P/c < a:
			return inter(x, a, c**2/P**2) - inter(0., a, c**2/P**2)
		else:
			root = raiz(P)
			return inter(x, a, c**2/P**2) - inter(root, a, c**2/P**2)
	else:
		return -F2(-x, P)

x_space = np.linspace(-10., 10., 200)
plt.plot(x_space, r(x_space))
