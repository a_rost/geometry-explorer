import numpy as np
import matplotlib.pyplot as plt
plt.ion()
import metrica


phi_array = "pepe"
T_array   = "pepa"

def calculate(N_P = 1000, N_rho = 500):

	global phi_array, T_array
	
	phi_array = np.zeros([N_rho, N_P])
	r1, r2 = metrica.rs, 1.5*metrica.rs

	for i in range(len(phi_array)):
		r = r1 + (r2 - r1)/(N_rho - 1.)*i
		print("i = ", i, "r = ", r)
		for j in range(len(phi_array)):
			P1, P2 = 0., metrica.max_P(r)
			P = P1 + (P2 - P1)/(N_P - 1.)*j
			phi_array[i, j] = metrica.F(r, P)

	T_array = np.zeros([N_rho, N_P])
	r1, r2 = metrica.rs, 1.5*metrica.rs

	for i in range(len(T_array)):
		r = r1 + (r2 - r1)/(N_rho - 1.)*i
		print("i = ", i, "r = ", r)
		for j in range(len(T_array)):
			P1, P2 = 0., metrica.max_P(r)
			P = P1 + (P2 - P1)/(N_P - 1.)*j
			T_array[i, j] = metrica.T(r, P)


	P_max = metrica.max_P(3.)
	phi_array2 = np.zeros([N_rho, N_P])
	r1, r2 = metrica.rs*1.5, 3*metrica.rs

	for i in range(len(phi_array2)):
		r = r1 + (r2 - r1)/n*i
		print("i = ", i, "r = ", r)
		for j in range(len(phi_array2[0])):
			P1, P2 = 0., P_max
			P = P1 + (P2 - P1)/m*j
			phi_array2[i, j] = metrica.F2(r2, r, P)



	phi_array3 = np.zeros([N_rho, N_P])
	r1, r2 = metrica.rs*1.5, 3*metrica.rs

	for i in range(len(phi_array2)):
		r = r1 + (r2 - r1)/(N_rho - 1.)*i
		print("i = ", i, "r = ", r)
		for j in range(len(phi_array2[0])):
			P1, P2 = P_max, metrica.max_P(r)
			P = P1 + (P2 - P1)/(N_P - 1.)*j
			phi_array3[i, j] = metrica.F2(r2, r, P)


def guardador():
	archivo = open("../../data/schwarzschild/phi_array.dat", "w")
	for i in range(len(phi_array)):
		for j in range(len(phi_array[0])):
			archivo.write(str(phi_array[i][j]) + "\n")
	archivo.close()

	archivo = open("../../data/schwarzschild/phi_array2.dat", "w")
	for i in range(len(phi_array2)):
		for j in range(len(phi_array2[0])):
			archivo.write(str(phi_array2[i][j]) + "\n")
	archivo.close()

	archivo = open("../../data/schwarzschild/phi_array3.dat", "w")
	for i in range(len(phi_array3)):
		for j in range(len(phi_array3[0])):
			archivo.write(str(phi_array3[i][j]) + "\n")
	archivo.close()



calculate()
guardador()
