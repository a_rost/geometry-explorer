#define pi (3.1415926535f)
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <GL/glew.h>
#include "parametros.h"
#include "metrica.h"
#include "elemental.h"
#include "structs.h"

unsigned int fracasos;
int ind_mode = 0, triangulo = 48;



static inline void geodesica_xyz2esf(float x, float y, float z, float *theta, float *phi){  
  const float r = sqrtf((powf(x,2) + powf(y,2) + powf(z,2)));
  *theta  = acosf(z/r)*180.f/pi;
  *phi    = fmodf(atan2f(y, x)*180.f/pi+360.f, 360.f);
}

static inline void geodesica_esf2xyz(float r, float theta, float phi, float *x, float *y, float *z){  
  *x = r*sinf(theta*pi/180.f)*cosf(phi*pi/180.f);
  *y = r*sinf(theta*pi/180.f)*sinf(phi*pi/180.f);
  *z = r*cosf(theta*pi/180.f);
}

void geodesica_cambio_base(float posicion[3], float vector_xyz[3], float *vector_esf, unsigned int shift){
	unsigned int k;
   float base[3][3] = {{sinf(posicion[1])*cosf(posicion[2]),   sinf(posicion[1])*sinf(posicion[2]),    cosf(posicion[1])},

                       {cosf(posicion[1])*cosf(posicion[2]),   cosf(posicion[1])*sinf(posicion[2]),   -sinf(posicion[1])},

                       {-sinf(posicion[2]),                                      cosf(posicion[2]),                  0.f}};

	vector_esf[3*shift + 0]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 0] += base[0][k]*vector_xyz[k];

	vector_esf[3*shift + 1]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 1] += base[1][k]*vector_xyz[k];

	vector_esf[3*shift + 2]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 2] += base[2][k]*vector_xyz[k];

}

float rho1, rho2, phi2;

void geodesica_solution_set(float posicion0[3], float posicion1[3], float versor0[3], float versor_perp[3], float posicion0_original[3],
							float phi, float alfa, unsigned int j, unsigned int tipo_I, float *versores, float *D, float *solution_type){
	float versor_xyz[3], P;
 	versor_xyz[0] = -cosf(alfa)*versor0[0] + sinf(alfa)*versor_perp[0];
	versor_xyz[1] = -cosf(alfa)*versor0[1] + sinf(alfa)*versor_perp[1];
 	versor_xyz[2] = -cosf(alfa)*versor0[2] + sinf(alfa)*versor_perp[2];
 	geodesica_cambio_base(posicion0_original, versor_xyz, versores, j);
	P = fabsf(sinf(alfa))*(posicion0[0])*c/sqrtf(metrica_B(posicion0[0]));
	if(tipo_I == 1){
		D[j] = geodesica_I_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 1.f;
	}
	else{
		if(posicion0[0] < 1.5f*RS)
			D[j] = geodesica_H_T(P, posicion0[0], posicion1[0]);
		else
			D[j] = geodesica_G_T(P, posicion0[0], posicion1[0]);

		solution_type[j] = 2.f;
	}
}

void geodesica_solution_set2(float posicion0[4], float posicion1[4], float alfa, unsigned int j, 
							 unsigned int tipo_I, float *D, float *solution_type){
	float P;
	P = fabsf(sinf(alfa))*(posicion0[0])*c/sqrtf(metrica_B(posicion0[0]));
	if(tipo_I == 1){
		D[j] = geodesica_I_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 1.f;
	}
	else{
		if(posicion0[0] < 1.5f*RS)
			D[j] = geodesica_H_T(P, posicion0[0], posicion1[0]);
		else
			D[j] = geodesica_G_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 1.f;
	}
}


void geodesica_geodesica5(float posicion0_original[4], float posicion1[3], float *D, float *alfas, float *solution_type){
	float versor0[3], versor1[3], versor_perp[3], versor_xyz[3], dot, alfa, modulo, posicion0[3];
	posicion0[0] = posicion0_original[1];
	posicion0[1] = posicion0_original[2];
	posicion0[2] = posicion0_original[3];

	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2];

	unsigned int exito, exito_sum = 0, exito_I;


	float phi = acosf(coseno);
	int tipo = 0;
	fi = phi;
	
	if((posicion0[0] - 1.5f*RS)*(posicion1[0] - 1.5f*RS) <= 0.f){
		geodesica_raiz_I(posicion0[0], posicion1[0], 0*pi + phi, &alfa, &exito);
		alfas[0] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 0, 1, D, solution_type);
		pepes[0] = pepe;

		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		alfas[1] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 1, 1, D, solution_type);
		pepes[1] = pepe;

		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		alfas[2] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 2, 1, D, solution_type);
		pepes[2] = pepe;

		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		alfas[3] = alfa;
		geodesica_solution_set2(posicion0, posicion1,  alfa, 3, 1, D, solution_type);
		pepes[3] = pepe;
// 		printf("alfas = (%f %f %f %f), (r1, r2, phi) = (%f %f %f)\n", alfas[0], alfas[1], alfas[2], alfas[3], posicion0[0], posicion1[0], phi);
	}
	else if((posicion0[0] <= 1.5f*RS)&&(posicion1[0] <= 1.5f*RS)){
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_H(posicion0[0], posicion1[0], phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[0] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 0, exito_I, D, solution_type);
		pepes[0] = pepe;
 
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_H(posicion0[0], posicion1[0], 2*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[1] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 1, exito_I, D, solution_type);
		pepes[1] = pepe;
 
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_H(posicion0[0], posicion1[0], 2*pi + phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[2] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 2, exito_I, D, solution_type);
		pepes[2] = pepe;

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_H(posicion0[0], posicion1[0], 4*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[3] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 3, exito_I, D, solution_type);
		pepes[3] = pepe;
	}
	else{
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[0] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 0, exito_I, D, solution_type);
		pepes[0] = pepe;
 
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[1] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 1, exito_I, D, solution_type);
		pepes[1] = pepe;
 
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi + phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[2] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 2, exito_I, D, solution_type);
		pepes[2] = pepe;

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 4*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[3] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 3, exito_I, D, solution_type);
		pepes[3] = pepe;
	}
	if((fabsf(posicion0[0] - 1.5f*RS) <= 2.5f)&&(fabsf(posicion1[0] - 1.5f*RS) <= 2.5f))
		D[0] = phi*posicion0[0]/sqrtf(metrica_B(posicion0[0]))/c;
	if((alfas[0] == 0.f)||(alfas[1] == 0.f)||(alfas[2] == 0.f)||(alfas[3] == 0.f))
		printf("rho1, rho2, phi = (%f, %f, %f), alfas = (%f, %f, %f, %f)\n", posicion0[0], posicion1[0], phi, alfas[0], alfas[1], alfas[2], alfas[3]);
}


void geodesica_geodesica4(float posicion0[3], float posicion1[3], float *D, float *alfas, float *solution_type,
							float *versores){
	
	float posicion4[4] = {0.f, posicion0[0], posicion0[1], posicion0[2]}, versor0[3], versor1[3], versor_perp[3];
	
	geodesica_geodesica5(posicion4, posicion1, D, alfas, solution_type);

	
	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2], modulo;

	versor_perp[0]  = versor1[0] - coseno*versor0[0];
	versor_perp[1]  = versor1[1] - coseno*versor0[1];
	versor_perp[2]  = versor1[2] - coseno*versor0[2];
	modulo          = sqrtf(versor_perp[0]*versor_perp[0] + versor_perp[1]*versor_perp[1] + versor_perp[2]*versor_perp[2]);
	versor_perp[0] *= 1.f/modulo;
	versor_perp[1] *= 1.f/modulo;
	versor_perp[2] *= 1.f/modulo;
    float phi = acosf(coseno);

	geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 0*pi + phi, alfas[0], 0, 1, versores, D, solution_type);
	geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 2*pi - phi, alfas[1], 1, 1, versores, D, solution_type);
	geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 2*pi + phi, alfas[2], 2, 1, versores, D, solution_type);
	geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 4*pi - phi, alfas[3], 3, 1, versores, D, solution_type);
}


void geodesica_ray_tracer(float posicion[4], float *versor_u, unsigned int shift, float vectores[4][4],
				float *cuadrivector_U, unsigned char *visible){
  float cuadrivector_u[4], numero, vectores2[4][4], alfa, D1, aux1, aux2, intensidad;
//   float cuadrivector_U[4];  //cambio de coordenadas

  cuadrivector_u[0] = -1.f;
  cuadrivector_u[1] = versor_u[3*shift + 0];
  cuadrivector_u[2] = versor_u[3*shift + 1];
  cuadrivector_u[3] = versor_u[3*shift + 2];

  for(unsigned int k = 0; k<4; k++){
    vectores2[k][0]   = -vectores[k][0]/sqrtf(metrica_B(posicion[1]));
    vectores2[k][1]   =  vectores[k][1]/sqrtf(metrica_A(posicion[1]));
    vectores2[k][2]   =  vectores[k][2]*(posicion[1]);
    vectores2[k][3]   =  vectores[k][3]*(posicion[1])*sinf(posicion[2]);
  }
  for(unsigned int k=0; k<4; k++)
    vectores2[0][k] *= -1.f;

  cuadrivector_U[0] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[0] += vectores2[0][k]*cuadrivector_u[k];

  cuadrivector_U[1] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[1] += vectores2[1][k]*cuadrivector_u[k];

  cuadrivector_U[2] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[2] += vectores2[2][k]*cuadrivector_u[k];
  
  cuadrivector_U[3] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[3] += vectores2[3][k]*cuadrivector_u[k];


  //numero = fabsf(cuadrivector_U[1]);
  numero = 1.f;
}




unsigned int interactivo;

const unsigned int n_atrib = 8, v_info = 3, n_atrib_shading = 9, v_info2 = 9;



unsigned int asdfasdf = 0;
void geodesica_dibujador_objeto3(float posicion[4], float vectores[4][4], 
								 struct object_data objeto, float *vertx_data, unsigned char init){

	float valor, P_min[4];
	unsigned int T[3];
	unsigned char visible;
	
#pragma omp parallel num_threads(4)
{
	float valor, *distancia_fuente, vertice[3], *alfas, *solution_type, sol_type;
	distancia_fuente = (float *) malloc(4*sizeof(float));
	alfas            = (float *) malloc(4*sizeof(float));
	solution_type    = (float *) malloc(4*sizeof(float));
	
	#pragma omp for schedule(dynamic, 20)
	for(unsigned int i = 0; i < objeto.n_vert; i++){
        vertice[0] = objeto.vertices[3*i + 0];
        vertice[1] = objeto.vertices[3*i + 1];
        vertice[2] = objeto.vertices[3*i + 2];

		geodesica_geodesica5(posicion, vertice, distancia_fuente, alfas, solution_type);
		if(vertice[0]*posicion[1] < 0.f)
			valor = -1.f;
		else
			valor = 0.f;
		for(unsigned int j = 0; j < 4; j++){
			if(valor == -1.f)
				sol_type = 0.f;
			else
				sol_type = solution_type[j];
			vertx_data[4*v_info*i + v_info*j + 0] = alfas[j];
			vertx_data[4*v_info*i + v_info*j + 1] = distancia_fuente[j];
			vertx_data[4*v_info*i + v_info*j + 2] = (float) j + sol_type/3.;
		}
	}
	free(alfas);
	free(distancia_fuente);
	free(solution_type);
}
}


void geodesica_dibujador_puntos(float posicion[4], float vectores[4][4], 
								 struct particula_data objeto, float *vertx_data){
	
#pragma omp parallel num_threads(4)
{
	float valor, *distancia_fuente, vertice[3], color[3], *alfas, *solution_type, sol_type;
	distancia_fuente = (float *) malloc(4*sizeof(float));
	alfas            = (float *) malloc(4*sizeof(float));
	solution_type    = (float *) malloc(4*sizeof(float));
	
	#pragma omp for schedule(dynamic, 20)
	for(unsigned int i = 0; i < objeto.n_vert; i++){
		vertice[0] = objeto.vertices[3*i + 0];
		vertice[1] = objeto.vertices[3*i + 1];
		vertice[2] = objeto.vertices[3*i + 2];

		color[0]   = objeto.color[3*i + 0];
		color[1]   = objeto.color[3*i + 1];
		color[2]   = objeto.color[3*i + 2];
		
		geodesica_geodesica5(posicion, vertice, distancia_fuente, alfas, solution_type);
		if(vertice[0]*posicion[1] < 0.f)
			valor = -1.f;
		else
			valor = 0.f;
		for(unsigned int j = 0; j < 4; j++){
			if(valor == -1.f)
				sol_type = 0.f;
			else
				sol_type = solution_type[j];
			vertx_data[4*v_info2*i + v_info2*j + 0] = color[0];
			vertx_data[4*v_info2*i + v_info2*j + 1] = color[1];
			vertx_data[4*v_info2*i + v_info2*j + 2] = color[2];
			vertx_data[4*v_info2*i + v_info2*j + 3] = (float) j + sol_type/3.;
			vertx_data[4*v_info2*i + v_info2*j + 4] = alfas[j];
			vertx_data[4*v_info2*i + v_info2*j + 5] = vertice[0];
			vertx_data[4*v_info2*i + v_info2*j + 6] = vertice[1];
			vertx_data[4*v_info2*i + v_info2*j + 7] = vertice[2];
			vertx_data[4*v_info2*i + v_info2*j + 8] = distancia_fuente[j];
		}
	}
	free(alfas);
	free(distancia_fuente);
	free(solution_type);
}		
}
float model_acceleration(unsigned int, float[3], float[3]);

void geodesica_evolucionador(struct particula_data particulas){
	unsigned int numero = particulas.n_vert;
	if(particulas.moving == 1){
		float X[3], V[3], A[3], dtau = 0.01f;
		for(unsigned int i = 0; i < numero; i++){
			X[0] = particulas.vertices[3*i + 0];
			X[1] = particulas.vertices[3*i + 1];
			X[2] = particulas.vertices[3*i + 2];

			V[0] = particulas.velocity[3*i + 0];
			V[1] = particulas.velocity[3*i + 1];
			V[2] = particulas.velocity[3*i + 2];
			
			A[0] = model_acceleration(0, X, V);
			A[1] = model_acceleration(1, X, V);
			A[2] = model_acceleration(2, X, V);

			particulas.vertices[3*i + 0] += V[0]*dtau;
			particulas.vertices[3*i + 1] += V[1]*dtau;
			particulas.vertices[3*i + 2] += V[2]*dtau;
			
			particulas.velocity[3*i + 0] += A[0]*dtau;
			particulas.velocity[3*i + 1] += A[1]*dtau;
			particulas.velocity[3*i + 2] += A[2]*dtau;
		}
	}
}

float min3(float x1, float x2, float x3){
	if((x1 <= x2)&&(x1 <= x3))
		return x1;
	else if ((x2 <= x1)&&(x2 <= x3))
		return x2;
	else
		return x3;
}

float max3(float x1, float x2, float x3){
	if((x1 >= x2)&&(x1 >= x3))
		return x1;
	else if ((x2 >= x1)&&(x2 >= x3))
		return x2;
	else
		return x3;
}

void geodesica_triangulador(struct object_data objeto, float *vertx_data, float *triangulos){
	unsigned int stride1 = objeto.n_triang*3*n_atrib, stride2 = 3*n_atrib, stride3 = n_atrib, T[3];
	float P1, P2, P3, P_min, P_max, sol_type;
	
	for(unsigned int j = 0; j < 4; j++){
		for(unsigned int i = 0; i < objeto.n_triang; i++){
			T[0] = objeto.triangulos[3*i + 0];
			T[1] = objeto.triangulos[3*i + 1];
			T[2] = objeto.triangulos[3*i + 2];

			for(unsigned int k = 0; k < 3; k++){
				triangulos[stride1*j + stride2*i + stride3*k + 0] = objeto.vertices[3*T[k] + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 1] = objeto.vertices[3*T[k] + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 2] = objeto.vertices[3*T[k] + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 3] = vertx_data[4*v_info*T[k] + v_info*j + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 4] = vertx_data[4*v_info*T[k] + v_info*j + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 5] = vertx_data[4*v_info*T[k] + v_info*j + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 6] = objeto.u[T[k]];
				triangulos[stride1*j + stride2*i + stride3*k + 7] = objeto.v[T[k]];
			}
		}
	}
}


void geodesica_triangulador_shaders(struct object_data objeto, float *vertx_data, float *triangulos){
	unsigned int stride1 = objeto.n_triang*3*n_atrib_shading, stride2 = 3*n_atrib_shading, stride3 = n_atrib_shading, T[3];
	float P1, P2, P3, P_min, P_max, sol_type;
	
	for(unsigned int j = 0; j < 4; j++){
		for(unsigned int i = 0; i < objeto.n_triang; i++){
			T[0] = objeto.triangulos[3*i + 0];
			T[1] = objeto.triangulos[3*i + 1];
			T[2] = objeto.triangulos[3*i + 2];

			for(unsigned int k = 0; k < 3; k++){
				triangulos[stride1*j + stride2*i + stride3*k + 0] = objeto.vertices[3*T[k] + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 1] = objeto.vertices[3*T[k] + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 2] = objeto.vertices[3*T[k] + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 3] = vertx_data[4*v_info*T[k] + v_info*j + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 4] = vertx_data[4*v_info*T[k] + v_info*j + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 5] = vertx_data[4*v_info*T[k] + v_info*j + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 6] = (float )T[k];
				
				triangulos[stride1*j + stride2*i + stride3*k + 7] = objeto.u[T[k]];
				triangulos[stride1*j + stride2*i + stride3*k + 8] = objeto.v[T[k]];
			}
		}
	}
}



void draw_vertex(float posicion[4], float vectores[4][4], float aspect_ratio, unsigned int shaderProgram2, unsigned int texture, GLuint VBO, unsigned int numero_triang, float *vertices, float *triangulos){
	float versor0[3], *worm_position, worm_max_angle, *cuadrivector_U, norma, *base;
	unsigned char visible;

	float rho0 = posicion[1];
	worm_max_angle = asinf(P_MAX*sqrtf(metrica_B(rho0))/(rho0*c));

	versor0[0] = sinf(posicion[2])*cosf(posicion[3]);
	versor0[1] = sinf(posicion[2])*sinf(posicion[3]);
	versor0[2] = cosf(posicion[2]);
	worm_position  = (float *) malloc(3*sizeof(float));
	cuadrivector_U = (float *) malloc(4*sizeof(float));

	if(posicion[1] >= 1.5f*RS){
		versor0[0] *= -1.f;
		versor0[1] *= -1.f;
		versor0[2] *= -1.f;
	}

	float aux[3];
	aux[0] = posicion[1];
	aux[1] = posicion[2];
	aux[2] = posicion[3];
	geodesica_cambio_base(aux, versor0, worm_position, 0);
	geodesica_ray_tracer(posicion, worm_position, 0, vectores, cuadrivector_U, &visible);
	norma = sqrtf(powf(cuadrivector_U[1], 2) +  powf(cuadrivector_U[2], 2) +  powf(cuadrivector_U[3], 2))*24;
	worm_position[0] = -cuadrivector_U[2]/norma;
	worm_position[1] = -cuadrivector_U[3]/norma;
	worm_position[2] = -cuadrivector_U[1]/norma;

	
	base = (float *) malloc(9*sizeof(float));
	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[1] =  sinf(posicion[2])*sinf(posicion[3]);
	base[2] =  cosf(posicion[2]);

	base[3] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[5] = -sinf(posicion[2]);

	base[6] = -sinf(posicion[3]);
	base[7] =  cosf(posicion[3]);
	base[8] =  0.f;


	
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*N_GEO*n_atrib*numero_triang*sizeof(float), triangulos);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)0);      // coordenadas
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)12);     // alpha
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)16);     // distance
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)20);     // type
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)24);     // uv coordinates
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glUseProgram(shaderProgram2);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	
	GLuint i = glGetUniformLocation(shaderProgram2, "worm_position");
	glUniform4f(i, worm_position[0], worm_position[1], worm_position[2], worm_max_angle);

	i = glGetUniformLocation(shaderProgram2, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram2, "vectores");
	glUniformMatrix4fv(i, 1, GL_FALSE, &vectores[0][0]);

	i = glGetUniformLocation(shaderProgram2, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	i = glGetUniformLocation(shaderProgram2, "base");
	glUniformMatrix3fv(i, 1, GL_FALSE, &base[0]);

	glDrawArrays(GL_TRIANGLES, 0, N_GEO*3*numero_triang);

	free(base);
	free(worm_position);
	free(cuadrivector_U);
}


void draw_vertex_shaders(float posicion[4], float vectores[4][4], float aspect_ratio, unsigned int shaderProgram2, unsigned int texture, GLuint VBO, unsigned int numero_triang, float *vertices, float *triangulos, unsigned int stride, float contrast){
	float versor0[3], *worm_position, worm_max_angle, *cuadrivector_U, norma, *base;
	unsigned char visible;

	worm_max_angle = asinf(P_MAX/(posicion[1]));

	versor0[0] = sinf(posicion[2])*cosf(posicion[3]);
	versor0[1] = sinf(posicion[2])*sinf(posicion[3]);
	versor0[2] = cosf(posicion[2]);
	worm_position  = (float *) malloc(3*sizeof(float));
	cuadrivector_U = (float *) malloc(4*sizeof(float));
	
	if(posicion[1] >= 0.f){
		versor0[0] *= -1.f;
		versor0[1] *= -1.f;
		versor0[2] *= -1.f;
	}

	float aux[3];
	aux[0] = posicion[1];
	aux[1] = posicion[2];
	aux[2] = posicion[3];
	geodesica_cambio_base(aux, versor0, worm_position, 0);
	geodesica_ray_tracer(posicion, worm_position, 0, vectores, cuadrivector_U, &visible);
	norma = sqrtf(powf(cuadrivector_U[1], 2) +  powf(cuadrivector_U[2], 2) +  powf(cuadrivector_U[3], 2))*24;
	worm_position[0] = -cuadrivector_U[2]/norma;
	worm_position[1] = -cuadrivector_U[3]/norma;
	worm_position[2] = -cuadrivector_U[1]/norma;

	
	base = (float *) malloc(9*sizeof(float));
	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[1] =  sinf(posicion[2])*sinf(posicion[3]);
	base[2] =  cosf(posicion[2]);

	base[3] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[5] = -sinf(posicion[2]);

	base[6] = -sinf(posicion[3]);
	base[7] =  cosf(posicion[3]);
	base[8] =  0.f;


	
// 	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*N_GEO*n_atrib_shading*numero_triang*sizeof(float), triangulos);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, n_atrib_shading * sizeof(float), (void*)0);      // coordenadas
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, n_atrib_shading * sizeof(float), (void*)12);     // alpha
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, n_atrib_shading * sizeof(float), (void*)16);     // distance
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, n_atrib_shading * sizeof(float), (void*)20);     // type
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, n_atrib_shading * sizeof(float), (void*)24);     // index
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, n_atrib_shading * sizeof(float), (void*)28);     // uv coordinates
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glUseProgram(shaderProgram2);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	
	GLuint i = glGetUniformLocation(shaderProgram2, "worm_position");
	glUniform4f(i, worm_position[0], worm_position[1], worm_position[2], worm_max_angle);

	i = glGetUniformLocation(shaderProgram2, "stride");
	glUniform1i(i, stride);

	i = glGetUniformLocation(shaderProgram2, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram2, "vectores");
	glUniformMatrix4fv(i, 1, GL_FALSE, &vectores[0][0]);

	i = glGetUniformLocation(shaderProgram2, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	i = glGetUniformLocation(shaderProgram2, "contrast");
	glUniform1f(i, contrast);

	i = glGetUniformLocation(shaderProgram2, "base");
	glUniformMatrix3fv(i, 1, GL_FALSE, &base[0]);

	glDrawArrays(GL_TRIANGLES, 0, N_GEO*3*numero_triang);

	free(base);
	free(worm_position);
	free(cuadrivector_U);
}


void draw_points(float posicion[4], float vectores[4][4], float aspect_ratio, unsigned int shaderProgram, GLuint VBO,  struct particula_data particulas, float *buffer_particulas){
	float *base;
	
	base = (float *) malloc(9*sizeof(float));
	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[1] =  sinf(posicion[2])*sinf(posicion[3]);
	base[2] =  cosf(posicion[2]);

	base[3] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[5] = -sinf(posicion[2]);

	base[6] = -sinf(posicion[3]);
	base[7] =  cosf(posicion[3]);
	base[8] =  0.f;

	

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, N_GEO*v_info2*particulas.n_vert*sizeof(float), buffer_particulas);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)0);      // color (r,g,b)
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)12);     // tipo
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)16);     // alfa
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)20);     // coordenadas fijas
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, v_info2 * sizeof(float), (void*)32);     // distancia
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glUseProgram(shaderProgram);

	GLuint i  = glGetUniformLocation(shaderProgram, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram, "vectores");
	glUniformMatrix4fv(i, 1, GL_FALSE, &vectores[0][0]);

	i = glGetUniformLocation(shaderProgram, "base");
	glUniformMatrix3fv(i, 1, GL_FALSE, &base[0]);

	i = glGetUniformLocation(shaderProgram, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	glDrawArrays(GL_POINTS, 0, N_GEO*particulas.n_vert);

	free(base);
}

GLchar *load_shader_source(char *filename) {
	FILE *file = fopen(filename, "r");             // open 
	fseek(file, 0L, SEEK_END);                     // find the end
	size_t size = ftell(file);                     // get the size in bytes
	GLchar *shaderSource = calloc(1, size);        // allocate enough bytes
	rewind(file);                                  // go back to file beginning
	fread(shaderSource, size, sizeof(char), file); // read each char into ourblock
	fclose(file);                                  // close the stream
	return shaderSource;
}

void load_and_compile(unsigned int *shaderProgram1, unsigned int *shaderProgram2, unsigned int *shaderProgram3){
	int  success;
	char infoLog[512];
	GLchar *vertexSource1, *fragmentSource1, *vertexSource2, *fragmentSource2, *geometrySource1, *vertexSource3, *fragmentSource3, *geometrySource3;


	vertexSource1   = load_shader_source("shaders/schwarzschild/vertex_source.glsl");
	fragmentSource1 = load_shader_source("shaders/schwarzschild/fragment_source.glsl");
	geometrySource1 = load_shader_source("shaders/schwarzschild/geometry_source.glsl");
	
	vertexSource2   = load_shader_source("shaders/worm/vertex_source_points.glsl");
	fragmentSource2 = load_shader_source("shaders/worm/fragment_source_points.glsl");

 	vertexSource3   = load_shader_source("shaders/worm/vertex_source_shading.glsl");
 	fragmentSource3 = load_shader_source("shaders/worm/fragment_source_shading.glsl");
 	geometrySource3 = load_shader_source("shaders/worm/geometry_source_shading.glsl");

	//compilando shaders y todo eso 2


    unsigned int vertexShader1;
    vertexShader1 = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader1, 1, (const GLchar**)&vertexSource1, NULL);
    glCompileShader(vertexShader1);
    glGetShaderiv(vertexShader1, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(vertexShader1, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX1::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int fragmentShader1;
    fragmentShader1 = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader1, 1, (const GLchar**)&fragmentSource1, NULL);
    glCompileShader(fragmentShader1);
    glGetShaderiv(fragmentShader1, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(fragmentShader1, 512, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT1::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int geometryShader1;
	geometryShader1 = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(geometryShader1, 1, (const GLchar**)&geometrySource1, NULL);
	glCompileShader(geometryShader1);
	glGetShaderiv(geometryShader1, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(geometryShader1, 512, NULL, infoLog);
		printf("ERROR::SHADER::GEOMETRY1::COMPILATION_FAILED %s\n", infoLog);
	}





	//uniendo shader 1
	*shaderProgram1 = glCreateProgram();
	glAttachShader(*shaderProgram1, vertexShader1);
 	glAttachShader(*shaderProgram1, geometryShader1);
	glAttachShader(*shaderProgram1, fragmentShader1);
	glLinkProgram(*shaderProgram1);


	
	
	unsigned int vertexShader2;
	vertexShader2 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader2, 1, (const GLchar**)&vertexSource2, NULL);
	glCompileShader(vertexShader2);
	glGetShaderiv(vertexShader2, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(vertexShader2, 512, NULL, infoLog);
		printf("ERROR::SHADER::VERTEX2::COMPILATION_FAILED %s\n", infoLog);
	}

	unsigned int fragmentShader2;
	fragmentShader2 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader2, 1, (const GLchar**)&fragmentSource2, NULL);
	glCompileShader(fragmentShader2);
	glGetShaderiv(fragmentShader2, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(fragmentShader2, 512, NULL, infoLog);
		printf("ERROR::SHADER::FRAGMENT2::COMPILATION_FAILED %s\n", infoLog);
	}

	*shaderProgram2 = glCreateProgram();
	glAttachShader(*shaderProgram2, vertexShader2);
	glAttachShader(*shaderProgram2, fragmentShader2);
	glLinkProgram(*shaderProgram2);

	
	
	
	
	
	
	
	
	unsigned int vertexShader3;
	vertexShader3 = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader3, 1, (const GLchar**)&vertexSource3, NULL);
	glCompileShader(vertexShader3);
	glGetShaderiv(vertexShader3, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(vertexShader3, 512, NULL, infoLog);
		printf("ERROR::SHADER::VERTEX3::COMPILATION_FAILED %s\n", infoLog);
	}

	unsigned int fragmentShader3;
	fragmentShader3 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader3, 1, (const GLchar**)&fragmentSource3, NULL);
	glCompileShader(fragmentShader3);
	glGetShaderiv(fragmentShader3, GL_COMPILE_STATUS, &success);
	if(!success){
		glGetShaderInfoLog(fragmentShader3, 512, NULL, infoLog);
		printf("ERROR::SHADER::FRAGMENT3::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int geometryShader3;
	geometryShader3 = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometryShader3, 1, (const GLchar**)&geometrySource3, NULL);
    glCompileShader(geometryShader3);
    glGetShaderiv(geometryShader3, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(geometryShader3, 512, NULL, infoLog);
        printf("ERROR::SHADER::GEOMETRY3::COMPILATION_FAILED %s\n", infoLog);
    }





	//uniendo shader 3
	*shaderProgram3 = glCreateProgram();
	glAttachShader(*shaderProgram3, vertexShader3);
	glAttachShader(*shaderProgram3, geometryShader3);
	glAttachShader(*shaderProgram3, fragmentShader3);
	glLinkProgram(*shaderProgram3);



	printf("shaders compilados\n");
}



void geodesica_benchmark_F(unsigned int N){
	float rho, P, *rho_space, *P_space, *F_space;
	double tiempo, tiempo2, delta;

	rho_space = (float *) malloc(N*sizeof(float));
	P_space   = (float *) malloc(N*sizeof(float));
	F_space   = (float *) malloc(N*sizeof(float));
	
	
	for(unsigned int i = 0; i < N; i++){
		rho = RS + (3.f*RS - RS)*drand48();
		P   = 0.f + metrica_maxP(rho)*drand48();
		rho_space[i] = rho;
		P_space[i]   = P;
	}
	tiempo = omp_get_wtime();
	for(unsigned int i = 0; i < N; i++){
		F_space[i] = geodesica_F(rho_space[i], P_space[i]);
	}
	tiempo2 = omp_get_wtime();
	delta = tiempo2 - tiempo;
	printf("Seconds = %lf, Time per F evaluation = %f us\n", delta, delta/N*1000000.);
	free(rho_space);
	free(P_space);
	free(F_space);
}


