import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
plt.ion()

R, h, B= 5., 0.5, 1.

dT = 2*(R*np.sin(B/R) - h)/np.cos(B/R)
a  = np.cos(B/R)/2./dT
A = B - dT
def r(x):
	if x > np.pi*R/2.:
		return r(np.pi*R - x)
	elif x >= B:
		return R*np.sin(x/R)
	elif x >= B - dT:
		return h + a*(x - (B - dT))**2
	else:
		print("fuera de rango")
		return 0.

"""
r = np.vectorize(r)

x_space = np.linspace(B - dT, dT + np.pi*R - B, 1000)
plt.plot(x_space, r(x_space))
"""

y0 = r(B)
def raiz(P):
	if P < h:
		print("no existe solucion")
		return 0.
	elif P <= y0:
		return np.sqrt((P - h)/a) + B - dT
	else:
		return np.arcsin(P/R)*R


def U(x, P):
    return -1./P**2 + 1./r(x)**2

def U_p(x, P):
    return -2*r_p(x)*r(x)**(-3)

def U_p_p(x, P):
	return 6*r(x)**(-4)*r_p(x)**2 - 2*r(x)**(-3)*r_p_p(x)



def integrando_phi(x, P):
	if P == 0. or r(x) == 0.:
		return 1.
	elif 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/r(x)**2

def F(x, P):
	rho_min = raiz(P)
	if x < rho_min:
		print("fuera de rango")
		return 0.
	else:
		return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]

def I(P, rho1, rho2):
	if P == 0.:
		return 0.
	else:
		return abs(F(rho2, P) - F(rho1, P))

def G(P, rho1, rho2):
	if P == 0.:
		return np.pi
	else:
		return F(rho1, P) + F(rho2, P)

def show_I(rho1, rho2):
	P_max = min(r(rho1), r(rho2))
	P_space = np.linspace(0., P_max, 200)
	y_space = np.zeros(200)
	for i in range(200):
		y_space[i] = I(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)

def show_G(rho1, rho2):
	P_max = min(r(rho1), r(rho2))
	P_space = np.linspace(h, P_max, 200)
	y_space = np.zeros(200)
	for i in range(200):
		y_space[i] = G(P_space[i], rho1, rho2)
	plt.plot(P_space, y_space)

def show_both(rho1, rho2):
	show_I(rho1, rho2)
	show_G(rho1, rho2)



rho_res, P_res = 300, 300
rho_min, rho_max = A, np.pi*R/2.
P_min, P_max     = 0., 5.
phi_array = np.zeros([rho_res, P_res])
for i in range(P_res):
	print(" i = ", i)
	P = P_min + (P_max - P_min)/(P_res - 1)*i
	for j in range(rho_res):
		rho = (rho_max - rho_min)/(rho_res - 1)*j + rho_min
		phi_array[j][i] = F(rho, P)
plt.imshow(phi_array)

