float *phi_array, *T_array;

void geodesica_inicializador2(){
	unsigned int boludez;

	phi_array = (float *) malloc(N_P*N_rho*sizeof(float));
	T_array   = (float *) malloc(N_P*N_rho*sizeof(float));
	float valor;

	FILE *pfout;
	
	pfout = fopen("data/closed/phi_array.dat", "r");
	for(unsigned int i = 0; i < N_rho; i++){
		for(unsigned int j = 0; j < N_P; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			phi_array[i*N_P + j] = valor;
		}
	}
	fclose(pfout);

	pfout = fopen("data/closed/t_array.dat", "r");
	for(unsigned int i = 0; i < N_rho; i++){
		for(unsigned int j = 0; j < N_P; j++){
			boludez = fscanf(pfout, "%f\n", &valor);
			T_array[i*N_P + j] = valor;
		}
	}
	fclose(pfout);
}

void geodesica_exit(){
	free(phi_array);
	free(T_array);
	printf("Memoria dealocatada\n");
}

static inline float geodesica_interpolador(float *phi_array, float d_rho2, float d_P2, float rho0, float P0, unsigned int N_P2, float x, float P){
	unsigned int i, j;
	float valores[4], valores2[2], valor;

	i = (unsigned int) ((x - rho0)/d_rho2);
	j = (unsigned int) ((P - P0)/d_P2);

	valores[0] = phi_array[N_P2*(i + 0) + j + 0];
	valores[1] = phi_array[N_P2*(i + 0) + j + 1];
	valores[2] = phi_array[N_P2*(i + 1) + j + 0];
	valores[3] = phi_array[N_P2*(i + 1) + j + 1];

	valores2[0] = (valores[2] - valores[0])*(x - i*d_rho2 - rho0)/d_rho2 + valores[0];
	valores2[1] = (valores[3] - valores[1])*(x - i*d_rho2 - rho0)/d_rho2 + valores[1];

	valor = (valores2[1] - valores2[0])*(P - j*d_P2 - P0)/d_P2 + valores2[0];

	return valor;
}


static inline float geodesica_integral_arcsin(float u){
	if(fabsf(u) > 1.f){
		return 0.f;
	}
	else
		return 0.5f*u*sqrtf(1.f - u*u) + 0.5f*asinf(u);
}

static inline float geodesica_integral_arccosh(float u){
	if(fabsf(u) < 1.f){
		return 0.f;
	}
	else
		return -(0.5f*(-u)*sqrtf(u*u - 1.f) - 0.5f*acoshf(-u));
}

static inline float geodesica_integral_sin(float h, float P){
	float rho_min = metrica_raiz_U(P), a, b, valor, arg1, arg2;
	a = metrica_r(rho_min);
	b = metrica_r_d(rho_min);
	arg1 = P/a;
	arg2 = P/(a + b*h);
	if(arg2 > 1.f)
		arg2 = 1.f;
	if(arg1 > 1.f)
		arg1 = 1.f;
	valor = (asinf(arg1) - asinf(arg2))/b;
	return valor;
}


static inline float geodesica_integral_sqrt(float h, float P){
	float rho_min = metrica_raiz_U(P), a, b, valor, u1, u2;
	a = metrica_r(rho_min);
	b = metrica_r_d(rho_min);

	u1 = a;
	u2 = a + b*h;
	return (sqrtf(u2*u2 - P*P) - sqrtf(u1*u1 - P*P))/b;
}

static inline float geodesica_aproximacion6(float h, float P){
	float rho_0, beta, gamma, x1, x2, u1, u2;
	rho_0 = metrica_raiz_U(P);
	beta  = metrica_U_d(rho_0, P);
	gamma = metrica_U_d_d(rho_0, P)*0.5f;

	if(P <= 0.5f)
		return geodesica_integral_sin(h, P);
	else{
		x1 = rho_0 + h;
		x2 = rho_0;
		if(gamma >= 0.f){
			u1 = 2*gamma*(x1 - rho_0)/beta + 1.f;
			u2 = 2*gamma*(x2 - rho_0)/beta + 1.f;
			return  (1.f/(P*P)*asinf(u2)/sqrtf(gamma) - beta*beta/4.f/powf(gamma, 1.5f)*geodesica_integral_arcsin(u2)) - 
					(1.f/(P*P)*asinf(u1)/sqrtf(gamma) - beta*beta/4.f/powf(gamma, 1.5f)*geodesica_integral_arcsin(u1));
		}
		else{
			u1 = 2*(-gamma)*(x1 - rho_0)/beta - 1.f; 
			u2 = 2*(-gamma)*(x2 - rho_0)/beta - 1.f;
			return  (-1.f/(P*P)/sqrtf(-gamma)*acoshf(-u2) - beta*beta/4.f/powf(-gamma, 1.5)*geodesica_integral_arccosh(u2)) -
					(-1.f/(P*P)/sqrtf(-gamma)*acoshf(-u1) - beta*beta/4.f/powf(-gamma, 1.5)*geodesica_integral_arccosh(u1));
		}
	}
}


static inline float geodesica_aproximacion6_T(float h, float P){
	float rho_0, beta, gamma, x1, x2, u1, u2;
	h = fabsf(h);
	rho_0 = metrica_raiz_U(P);
	beta  = metrica_U_d(rho_0, P);
	gamma = metrica_U_d_d(rho_0, P);

	if(P <= 0.5f)
		return geodesica_integral_sqrt(h, P);
	else{
		x1 = rho_0 + h;
		x2 = rho_0;
		if(gamma >= 0.f){
			u1 = 2*gamma*(x1 - rho_0)/beta + 1.f;
			u2 = 2*gamma*(x2 - rho_0)/beta + 1.f;
			return  1.f/P*asinf(u2)/sqrtf(gamma) - 1.f/P*asinf(u1)/sqrtf(gamma);
		}
		else{
			u1 = 2*(-gamma)*(x1 - rho_0)/beta - 1.f; 
			u2 = 2*(-gamma)*(x2 - rho_0)/beta - 1.f;
			return  -1.f/P/sqrtf(-gamma)*acoshf(-u2) + 1.f/P/sqrtf(-gamma)*acoshf(-u1);
		}
	}
}
static inline float geodesica_F(float, float);    //leave it here, it is needed

static inline float geodesica_F(float rho, float P){
	float rho_min = metrica_raiz_U(P);;
	if((metrica_r(rho) == P)&(rho <= pi*metrica_R/2.f))
		return 0.f;
	else if(P == 0.)
		return pi/2.f;
	else if(rho < rho_min)
		return 0.f;
	else if(rho - rho_min <= 0.1f)
		return geodesica_aproximacion6(rho - rho_min, P);
	else if(rho <= pi*metrica_R/2.f)
		return geodesica_interpolador(phi_array, d_rho, d_P, 0.f, 0.f, N_P, rho, P);
	else
		return pi - geodesica_F(pi*metrica_R - rho, P);
}

static inline float geodesica_T(float, float);    //leave it here, it is needed

static inline float geodesica_T(float rho, float P){
	float rho_min = metrica_raiz_U(P);;
	if((metrica_r(rho) == P)&(rho <= pi*metrica_R/2.f))
		return 0.f;
	else if(P == 0.)
		return rho;
	else if(rho < rho_min)
		return 0.f;
	else if(rho - rho_min <= 0.1f)
		return geodesica_aproximacion6_T(rho - rho_min, P);
	else if(rho <= pi*metrica_R/2.f)
		return geodesica_interpolador(T_array, d_rho, d_P, 0.f, 0.f, N_P, rho, P);
	else
		return pi*metrica_R - geodesica_T(pi*metrica_R - rho, P);
}


static inline float geodesica_G(float P, float rho1, float rho2){
	float valor = geodesica_F(rho1, P) + geodesica_F(rho2, P);
	if(P == 0.f)
		return pi;
	else if(valor >= pi)
		return 2*pi - valor;
	else
		return valor;
}

static inline float geodesica_I(float P, float rho1, float rho2){
	return fabsf(geodesica_F(rho1, P) - geodesica_F(rho2, P));
}

static inline float geodesica_G_T(float P, float rho1, float rho2){
	float valor = fabsf(geodesica_T(rho1, P) + geodesica_T(rho2, P));
	if(valor >= metrica_R*pi)
		return 2*pi*metrica_R - valor;
	else
		return valor;
}

static inline float geodesica_I_T(float P, float rho1, float rho2){
	return fabsf(geodesica_T(rho1, P) - geodesica_T(rho2, P));
}

/*
static inline float geodesica_exp_I(float P, float rho1, float rho2){
	if(P == metrica_h)
		return 0.f;
	else
		return expf(-fabsf(geodesica_F(rho1, P) - geodesica_F(rho2, P)));
}

static inline float geodesica_exp_G(float P, float rho1, float rho2){
	if((rho1 >= 0.f)&&(rho2 >= 0.f))
		return expf(-(geodesica_F(rho1, P) + geodesica_F(rho2, P)));
	else if((rho1 < 0.f)&&(rho2 < 0.f))
		return expf((geodesica_F(rho1, P) + geodesica_F(rho2, P)));
}

*/
void geodesica_secante(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, float error, float *result){
	float a = P1, b = P2, fa, fb, c;
	unsigned int i, j;

    fa = (*G)(a, rho1, rho2) - phi; 
	fb = (*G)(b, rho1, rho2) - phi;
//  	printf("fa = %f, fb = %f\n", fa, fb);
	if(error < 0.001f)
		error = error*phi;
    if(fa*fb > 0.f){
//   		printf("no existe solucion, phi fuera de rango, a = %f,b = %f, fa = %f, fb = %f, phi = %f\n", a, b, fa, fb, phi);
        result[0] = 0.f;
		result[1] = 0.f;
	}
    else{
        for(i = 0; i < 40; i++){
            c = b - (b - a)*fb/(fb - fa);
            if(c < P1)
				c = P1;
            else if(c >= P2)
				c = P2;
            a = b;
            b = c;
            fa = fb;
            fb = (*G)(b, rho1, rho2) - phi;
			//printf("diferencia = %f, a = %f, b = %f\n", fabsf(fb), a, b);
            if(a == b){
                a = (P1 + P2)*0.5f; 
				b = P2;
                fa = (*G)(a, rho1, rho2) - phi;
				fb = (*G)(b, rho1, rho2) - phi;
			}

            if(fabsf(fb) < error)
                break;
		}
		if((fabsf(fb) < error)){
            result[0] = asinf(b/metrica_r(rho1));
            result[1] = 1.f;
        }
        else{
            result[0] = asinf(0.5f/metrica_r(rho1))*0.f + asinf(b/metrica_r(rho1));
            result[1] = 2.f;
        }
	}
}



void geodesica_biseccion2(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, unsigned int iteraciones, float error, float *result){
	float a = P1, b = P2, fa, fb, fc, c;
	unsigned int i, j;

	fa = (*G)(a, rho1, rho2) - phi; 
	fb = (*G)(b, rho1, rho2) - phi;
//   	printf("por calcular raiz, Ia = %f, Ib = %f, rho1 = %f, rho2 = %f, phi = %f, P1 = %f, P2 = %f\n", fa + phi, fb + phi, rho1, rho2, phi, P1, P2);
	if(fa*fb > 0.f){
		result[0] = 0.f;
		result[1] = 0.f;
	}
	else{
		for(i = 0; i < iteraciones; i++){
			c  = (a + b)*0.5f;
			fc = (*G)(c, rho1, rho2) - phi;
// 			if((fabsf(15.407555 - rho1) <= 0.001f)&&(fabsf(0.621303 - rho2) <= 0.001f)&&(fabsf(1.608252 - phi) <= 0.001f))
// 				printf("iteracion %d, a, b = (%f, %f), fa, fb = (%f, %f), c = %f, fc = %f\n", i, a, b, fa, fb, c, fc);
			if(fabsf(fc) < error)
				break;
			if(fa*fc < 0.f){
				b  = c;
				fb = fc;
			}
			else if(fc*fb < 0.f){
				a  = c;
				fa = fc;
			}
		}
		result[0] = a;
		result[1] = b;
// 		printf("a,b = (%f, %f), i = %d, error = %f, fc = %f\n", a, b, i, error, fc);
	}
}

void geodesica_mixto(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, float *result){
	geodesica_biseccion2((*G), rho1, rho2, phi, P1, P2, 10, 0.01f, result);

	if(result[1] != 0.f){
		geodesica_secante((*G), rho1, rho2, phi, result[0], result[1], 0.001f, result);
	}
}

void geodesica_raiz_G(float rho1, float rho2, float phi, float *alfa, unsigned int *exito){
	float P1 = metrica_r(rho1), P2 = metrica_r(rho2), P3;
	float P_max = fmin(P1, P2), *result, alfa1;

	result = (float *) malloc(2*sizeof(float));
	geodesica_mixto(&geodesica_G, rho1, rho2, phi, 0.f, P_max, result);
	alfa1  = result[0];
	*exito = (unsigned int) (result[1]);

	//printf("Raiz G, solucion, alfa = %f, exito = %u\n", alfa1, *exito);
	if(rho2 <= rho1){
		if(fmodf(phi, 2*pi) < pi)
			*alfa = alfa1;
		else
			*alfa = 2*pi - alfa1;
	}
	else{
		if(fmodf(phi, 2*pi) < pi)
			*alfa = pi - alfa1;
		else
			*alfa = pi + alfa1;
	}
	free(result);

}


void geodesica_raiz_I(float rho1, float rho2, float phi, float *alfa, unsigned int *exito){
	float P1 = metrica_r(rho1), P2 = metrica_r(rho2), P3;
	float P_max, *result, alfa1;

	P_max = fmin(P1, P2);
	
	result = (float *) malloc(2*sizeof(float));
	geodesica_mixto(&geodesica_I, rho1, rho2, phi, 0.f, P_max, result);
	alfa1 = result[0];
	*exito = (unsigned int) (result[1]);
//	printf("alfa1 = %f, P = %f\n", alfa1, metrica_r(rho1)*sinf(alfa1));
	if(rho1 < rho2){
		if(fmodf(phi, 2*pi) <= pi)
			*alfa = pi - alfa1;
		else
			*alfa = pi + alfa1;
	}
	else{
		if(fmodf(phi, 2*pi) <= pi)
			*alfa = alfa1;
		else
			*alfa = 2*pi - alfa1;
	}
	free(result);
}
