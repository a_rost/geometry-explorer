struct object_data{
	unsigned int n_vert;
	float *vertices;
	unsigned int n_triang;
	unsigned int *triangulos;
	float *xyz_coord;
	float *xyz_normales;
	float *normales;
	float *u;
	float *v;
	float *lights_info;
// 	float *light_intensity;

	//posicion de luz usada para el objeto
	float light_position[3];
    unsigned int texture;
};

struct particula_data {
	unsigned char moving;
	unsigned int n_vert;
	float *color;
	float *velocity;
	float *vertices;
};

struct particula_movil{
	unsigned int n_vert;
	float *color;
	float *velocity;
	float *vertices;
};

