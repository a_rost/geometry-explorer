#define RHO_MAX (5.f)
#define deg2rad (pi/180.f)
#define rad2deg (180.f/pi)

#define N_P1   (1600)
#define N_rho1 (400)
#define N_P2   (100)
#define N_rho2 (500)
#define N_P3   (50)
#define N_rho3 (500)
#define d_rho1 (RHO_MAX/(N_rho1 - 1))
#define d_P1   (5.f/(N_P1 - 1))
#define d_rho2 ((RHO_MAX - 0.1f)/(N_rho2 - 1))
#define d_P2   ((0.0025f + 0.01f)/(N_P2 - 1))
#define d_rho3 ((RHO_MAX - 0.2f)/(N_rho3 - 1))
#define d_P3   ((0.01f - 0.002f)/(N_P3 - 1))

const float metrica_T = 2.f, light_speed = 1.f;
const unsigned int N_GEO = 4;

float metrica_b, metrica_h, metrica_R;

void metrica_inicializador(void){
	metrica_b = 0.5f/metrica_T;
	metrica_h = metrica_T - metrica_b*metrica_T*metrica_T;
	metrica_R = metrica_T;
}
