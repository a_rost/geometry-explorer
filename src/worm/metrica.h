float metrica_B(float x){
	//Checked
	return 1.f;
}

float metrica_B_d(float x){
	//Checked
	return 0.f;
}


float metrica_r(float x){
	//checked
	if(x >= metrica_T)
		return x;
	else if(x >= -metrica_T)
		return metrica_h + metrica_b*x*x;
	else 
		return -x;
}

//Primera derivada de r
float metrica_r_d(float x){
	//checked
	if(x >= metrica_T)
		return 1.f;
	else if(x >= -metrica_T)
		return 2*metrica_b*x;
	else 
		return -1.f;
}

//Segunda derivada de r
float metrica_r_d_d(float x){
	//checked
	if(x >= metrica_T)
		return 0.f;
	else if(x >= -metrica_T)
		return 2*metrica_b;
	else 
		return 0.f;
}



float metrica_U(float x, float P){
	//checked
	return  powf(metrica_r(x), -2) - powf(P, -2);
}

float metrica_U_d(float x, float P){
	//checked
	return -2*powf(metrica_r(x), -3)*metrica_r_d(x);
}

float metrica_U_d_d(float x, float P){
	return 6.f*powf(metrica_r(x), -4)*powf(metrica_r_d(x), 2) - 2*powf(metrica_r(x), -3)*metrica_r_d_d(x);
}

float metrica_raiz_U(float beta){
	if(beta < metrica_h)
		return -1.f;
	else if (beta >= metrica_R)
		return beta;
	else
		return sqrtf((beta - metrica_h)/metrica_b);
}

float metrica_metric(float x, float theta, int indice){
	//checked
	if(indice == 0)
		return -powf(light_speed, 2);
	else if(indice == 1)
		return 1.f;
	else if(indice == 2)
		return powf(metrica_r(x), 2);
	else if(indice == 3)
		return powf(metrica_r(x)*sinf(theta), 2);
}

float metrica_A(float x){
	//checked
	return 1.f;
}

float metrica_A_d(float x){
	//checked
	return 0.f;
}
