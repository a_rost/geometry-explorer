float *array1, *array2, *array3, *array4, *array1_t, *array2_t, *array3_t, *array4_t;
const int P_res = 200, rho_res = 200;
const float delta_alpha = 0.03f*3.141592/180.f*0.6;
float dw = 1.f/(P_res - 1);

void geodesica_inicializador2(){
	unsigned int boludez;
	printf("Por inizializar\n");
	array1 = (float *) malloc(P_res*rho_res*sizeof(float));
	array2 = (float *) malloc(P_res*sizeof(float));
	array3 = (float *) malloc(P_res*rho_res*sizeof(float));
	array4 = (float *) malloc(P_res*sizeof(float));

	array1_t = (float *) malloc(P_res*rho_res*sizeof(float));
	array2_t = (float *) malloc(P_res*sizeof(float));
	array3_t = (float *) malloc(P_res*rho_res*sizeof(float));
	array4_t = (float *) malloc(P_res*sizeof(float));


	float valor;

	FILE *pfout1,*pfout2, *pfout3, *pfout4, *pfout;

	pfout1 = fopen("data/worm/matrix_array1", "r");
	pfout2 = fopen("data/worm/vector_array2", "r");
	pfout3 = fopen("data/worm/matriz_array_bis", "r");
	pfout4 = fopen("data/worm/vector_array_bis", "r");
	for(unsigned int i = 0; i < rho_res; i++){
		for(unsigned int j = 0; j < P_res; j++){
			boludez = fscanf(pfout1, "%f ", &valor);
			array1[i*P_res + j] = valor;

			boludez = fscanf(pfout3, "%f ", &valor);
			array3[i*P_res + j] = valor;
			
		}
		boludez = fscanf(pfout1, "\n");
		boludez = fscanf(pfout3, "\n");
	}
	for(unsigned int i = 0; i < P_res; i++){
		boludez = fscanf(pfout2, "%f\n", &valor);
		array2[i] = valor;

		boludez = fscanf(pfout4, "%f\n", &valor);
		array4[i] = valor;
	}
	
	fclose(pfout1);
	fclose(pfout2);
	fclose(pfout3);
	fclose(pfout4);

	printf("Fase uno completa\n");
	pfout1 = fopen("data/worm/matrix_array5", "r");
	pfout2 = fopen("data/worm/vector_array6", "r");
	pfout3 = fopen("data/worm/matrix_array7", "r");
	pfout4 = fopen("data/worm/vector_array8", "r");
	for(unsigned int i = 0; i < rho_res; i++){

		for(unsigned int j = 0; j < P_res; j++){
			boludez = fscanf(pfout1, "%f ", &valor);
			array1_t[i*P_res + j] = valor;

			boludez = fscanf(pfout3, "%f ", &valor);
			array3_t[i*P_res + j] = valor;
		}
		boludez = fscanf(pfout1, "\n");
		boludez = fscanf(pfout3, "\n");
	}
	for(unsigned int i = 0; i < P_res; i++){
		boludez = fscanf(pfout2, "%f\n", &valor);
		array2_t[i] = valor;
		boludez = fscanf(pfout4, "%f\n", &valor);
		array4_t[i] = valor;
	}
	fclose(pfout1);
	fclose(pfout2);
	fclose(pfout3);
	fclose(pfout4);
	
	printf("Todo cargado\n");

}

void geodesica_exit(){
	free(array1);
	free(array2);
	free(array3);
	free(array4);
	free(array1_t);
	free(array2_t);
	free(array3_t);
	free(array4_t);

	printf("Memoria dealocatada\n");
}

static inline float geodesica_interpolador(float *phi_array, float d_rho, float d_P, float rho0, float P0, unsigned int N_P, float x, float P){
	unsigned int i, j;
	float valores[4], valores2[2], valor;

	i = (unsigned int) ((x - rho0)/d_rho);
	j = (unsigned int) ((P - P0)/d_P);

	valores[0] = phi_array[N_P*(i + 0) + j + 0];
	valores[1] = phi_array[N_P*(i + 0) + j + 1];
	valores[2] = phi_array[N_P*(i + 1) + j + 0];
	valores[3] = phi_array[N_P*(i + 1) + j + 1];

	valores2[0] = (valores[2] - valores[0])*(x - i*d_rho - rho0)/d_rho + valores[0];
	valores2[1] = (valores[3] - valores[1])*(x - i*d_rho - rho0)/d_rho + valores[1];

	valor = (valores2[1] - valores2[0])*(P - j*d_P - P0)/d_P + valores2[0];

	
	
// 	valores2[0] = (valores[1] - valores[0])*(P - j*d_P - P0)/d_P + valores[0];
// 	valores2[1] = (valores[3] - valores[2])*(P - j*d_P - P0)/d_P + valores[2];
// 
// 	valor = (valores2[1] - valores2[0])*(x - i*d_rho - rho0)/d_rho + valores2[0];

	return valor;
}



/*
static inline float geodesica_aproximacion(float P, float dr){
	float alfa, beta;
	alfa = 2*metrica_b*powf(metrica_h, -3);
	beta = powf(metrica_h, -2);
	return 1.f/sqrtf(alfa)/(metrica_h*metrica_h)*asinhf(dr*sqrtf(alfa)/sqrtf(1.f/(P*P) - beta));
}


static inline float geodesica_integral_arcsin(float u){
	if(fabsf(u) > 1.f){
		return 0.f;
	}
	else
		return 0.5f*u*sqrtf(1.f - u*u) + 0.5f*asinf(u);
}

static inline float geodesica_integral_arccosh(float u){
	if(fabsf(u) < 1.f){
		return 0.f;
	}
	else
		return -(0.5f*(-u)*sqrtf(u*u - 1.f) - 0.5f*acoshf(-u));
}


static inline float geodesica_aproximacion6(float h, float P){
	float rho_0, beta, gamma, x1, x2, u1, u2;
	rho_0 = metrica_raiz_U(P);
	beta  = metrica_U_d(rho_0, P);
	gamma = metrica_U_d_d(rho_0, P)*0.5f;

	x1 = rho_0 + h;
	x2 = rho_0;
	if(gamma >= 0.f){
		u1 = 2*gamma*(x1 - rho_0)/beta + 1.f;
		u2 = 2*gamma*(x2 - rho_0)/beta + 1.f;
		return  (1.f/(P*P)*asinf(u2)/sqrtf(gamma) - beta*beta/4.f/powf(gamma, 1.5f)*geodesica_integral_arcsin(u2)) - 
				(1.f/(P*P)*asinf(u1)/sqrtf(gamma) - beta*beta/4.f/powf(gamma, 1.5f)*geodesica_integral_arcsin(u1));
	}
	else{
		u1 = 2*(-gamma)*(x1 - rho_0)/beta - 1.f; 
		u2 = 2*(-gamma)*(x2 - rho_0)/beta - 1.f;
		return  (-1.f/(P*P)/sqrtf(-gamma)*acoshf(-u2) - beta*beta/4.f/powf(-gamma, 1.5)*geodesica_integral_arccosh(u2)) -
				(-1.f/(P*P)/sqrtf(-gamma)*acoshf(-u1) - beta*beta/4.f/powf(-gamma, 1.5)*geodesica_integral_arccosh(u1));
	}
}


static inline float geodesica_aproximacion6_T(float h, float P){
	float rho_0, beta, gamma, x1, x2, u1, u2;
	h = fabsf(h);
	rho_0 = metrica_raiz_U(P);
	beta  = metrica_U_d(rho_0, P);
	gamma = metrica_U_d_d(rho_0, P);

	x1 = rho_0 + h;
	x2 = rho_0;
	if(gamma >= 0.f){
		u1 = 2*gamma*(x1 - rho_0)/beta + 1.f;
		u2 = 2*gamma*(x2 - rho_0)/beta + 1.f;
		return  1.f/P*asinf(u2)/sqrtf(gamma) - 1.f/P*asinf(u1)/sqrtf(gamma);
	}
	else{
		u1 = 2*(-gamma)*(x1 - rho_0)/beta - 1.f; 
		u2 = 2*(-gamma)*(x2 - rho_0)/beta - 1.f;
		return  -1.f/P/sqrtf(-gamma)*acoshf(-u2) + 1.f/P/sqrtf(-gamma)*acoshf(-u1);
	}
}

static inline float geodesica_aproximacion_T(float P, float h){
	float beta, alfa;
	h = fabsf(h);
	beta = sqrtf(-metrica_U(0.f, P));
	alfa = sqrtf(-metrica_U_d_d(0.f, P)*0.5f);
	return 1.f/(P*alfa)*asinhf(alfa*h/beta);
}
*/


float interpolador_lineal2(float *array, float x1, float x2, unsigned int N, float x){
	float dx, f1, f2;
	int i;
	dx = (x2 - x1)/(N - 1);
	i = (int)((x - x1)/dx);
	
	if(i >= N)
		i = N - 1;
	else if(i < 0)
		i = 0;

	f1 = array[i];
	f2 = array[i + 1];
	return (f2 - f1)/dx*(x - x1 - i*dx) + f1;
}

float exact(float x, float P){
	float v = -P/x;
// 	printf("x = %f, P = %f, v = %f, valor = %f\n", x, P, v, asinf(v));
	return asinf(v);
}
int DEBUG = -1;
void toggle_debug(){
	DEBUG *= -1;
}





void coefficients_F(double x, double P, double *A, double *B, double *C){
	*A = pow(metrica_r(x), 4.)/pow(P, 2.) - pow(metrica_r(x), 2.);
	*B = (4.*pow(metrica_r(x), 3)/pow(P, 2.) - 2*metrica_r(x))*metrica_r_d(x);
	*C = ((4*pow(metrica_r(x), 3)/pow(P, 2) - 2*metrica_r(x))*metrica_r_d_d(x) + pow(metrica_r_d(x), 2)*(12*pow(metrica_r(x), 2)/pow(P, 2) - 2.))/2.;
}



void coefficients_T(double x, double P, double *A, double *B, double *C){
	*A = 1.f - powf(P/metrica_r(x), 2);
	*B = 2*powf(P, 2)*metrica_r_d(x)*powf(metrica_r(x), -3);
	*C = (-6*powf(P, 2)*powf(metrica_r(x),-4)*powf(metrica_r_d(x), 2) + 2*powf(P, 2)*powf(metrica_r(x), -3)*metrica_r_d_d(x))/2.f;
}



double juan2(double x){
	double arg = sqrt(fabs(x*x - 1.)) + x;
	return log(fabs(arg));
}



double integral(double a, double b, double c, double x){
	//integral of 1/sqrt(a + b*x + c*x**2)
	double eta, arg, sita;
	if(c == 0.)
		return 2/b*sqrt(a + b*x) - 2*sqrt(a)/b;
	if(b == 0.)
		return asinh(x*sqrt(c/a))/sqrt(c);
	if(c < 0.){
		c = fabs(c);
		sita = -b/2./sqrt(c);
		eta  = sqrt(a + sita*sita);
		
		arg = sqrt(c)*x/eta + sita/eta;
		if(arg > 1.)
			arg = 1.;
		return (asin(arg) - asin(sita/eta))/sqrt(c);
	}
	else{
		if(a - b*b/(4.*c) >= 0.){
			eta = sqrt(a - b*b/(4.*c));
			return (asinh(sqrt(c)*x/eta + b/2./sqrt(c)/eta) - asinh(b/2./sqrt(c)/eta))/sqrt(c);
		}
		else{
			eta = sqrt(-a + b*b/(4.*c));
// 			#print("Eta: ", eta)
			return (juan2(sqrt(c)*x/eta + b/2/sqrt(c)/eta) - juan2(b/2/sqrt(c)/eta))/sqrt(c);
		}
	}
}

double approx_F(double x, double P){
	double rho_min, A, B, C, eta, u1, u2;
	if(P >= 1.){
		rho_min = metrica_raiz_U(P);
		coefficients_F(rho_min, P, &A, &B, &C);
		
		return integral(0., B, C, fabs(x - rho_min));
	}
	else{
		coefficients_F(0., P, &A, &B, &C);
		
		return integral(A, B, C, fabs(x));
	}
}

double approx_F2(double rho1, double rho2, double P){
	double A, B, C;
	
	coefficients_F(rho1, P, &A, &B, &C);
		
	return integral(A, B, C, fabs(rho2 - rho1));
}
double approx_T2(double rho1, double rho2, double P){
	double A, B, C;
	
	coefficients_T(rho1, P, &A, &B, &C);
		
	return integral(fabs(A), B, C, fabs(rho2 - rho1));
}

double approx_T(double x, double P){
	double rho_min, A, B, C, eta, u1, u2;
	if(P >= 1.){
		rho_min = metrica_raiz_U(P);
		coefficients_T(rho_min, P, &A, &B, &C);
		
		return integral(0., B, C, fabs(x - rho_min));
	}
	else{
		coefficients_T(0., P, &A, &B, &C);
		
		return integral(A, B, C, fabs(x));
	}
}

float geodesica_F(float x, float P);

float blend(float w, float dw){
	if(w <= 1.f - dw*2)
		return 0.f;
	else if(w <= 1.f - dw)
		return (w - (1.f - 2*dw))/dw;
	else
		return 1.f;
}
float top(float x){
	if(x > 1.f)
		return 1.f;
	else
		return x;
}
float geodesica_F(float x, float P){
	float P_max = metrica_r(x), M1, M2, rho_min, u, coeff1, coeff2, result1, result2, diff, param;
	rho_min = metrica_raiz_U(P);
	if(P == 0.f)
		return 0.f;

	else if(P >= P_max)
		return 0.f;

	else if(x == 0.f)
		return 0.f;

	else if(x < 0.f)
		return -geodesica_F(-x, P);
	
	else if(x < 2.){
		if(P <= 1.){
// 			u = 1.f - sqrtf(fabsf(1.f - P));
			u = P;
			M1 = geodesica_interpolador(array1, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, u);
			M2 = interpolador_lineal2(array2, 0.f, 1.f, P_res, u);
	
// 			result1 = M2*approx_F2(0.f, 2.f, P) - M1*approx_F2(x, 2.f, P);
			result1 = (M2 + approx_F2(0.f, 2.f, P)) - (M1 + approx_F2(x, 2.f, P));
			
			coeff2  = blend(1.f - x, 2.f/(rho_res - 1));
			coeff1  = 1.f - coeff2;
		}
		else{
			u = metrica_raiz_U(P)/x;
			if(u >= 1.f)
				u = 0.999999999f;
			param = u;
			
			M1 = geodesica_interpolador(array3, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, u);

			u = metrica_raiz_U(P)/2.f;
			if(u >= 1.f)
				u = 0.999999999f;
			M2 = interpolador_lineal2(array4, 0.f, 1.f, P_res, u);
			
// 			result1 = M2*approx_F2(rho_min, 2.f, P) - M1*approx_F2(x, 2.f, P);
			result1 = (M2 + approx_F2(rho_min, 2.f, P)) - (M1 + approx_F2(x, 2.f, P));

			coeff2 = top(blend(param, dw) + blend(1.f - x, 2.f/(rho_res - 1)));
			coeff1 = 1.f - coeff2;
		}
		result2 = approx_F(x, P);
		return coeff1*result1 + coeff2*result2;
		
	}
	else if(x == 2.f){
		if(P <= 1.){
// 			u = 1.f - sqrtf(fabsf(1.f - P));
			u = P;
			M2 = interpolador_lineal2(array2, 0.f, 1.f, P_res, u);
			return M2 + approx_F(2.f, P);
		}
		else{
			u = metrica_raiz_U(P)/2.f;
			M2 = interpolador_lineal2(array4, 0.f, 1.f, P_res, u);
			return M2 + approx_F(2.f, P);
		}
	}
	else if(x > 2.){
		float u1, u2;
		u2 = P/x;
		if(P <= 2.f){
			u1 = P/2.f;
			M1 = geodesica_F(2.f, P);
		}
		else{
			u1 = 1.f;
			M1 = 0.f;
		}
		return -asinf(u2) + asinf(u1) + M1;
	}
}

float juan(float x, float P){
	return x*sqrtf(1.f - P*P/(x*x));
}

float geodesica_T(float x, float P){
	float P_max = metrica_r(x), M1, M2, rho_min, u, coeff1, coeff2, result1, result2, diff, param;
	rho_min = metrica_raiz_U(P);
	if(P == 0.f)
		return x;

	else if(P >= P_max)
		return 0.f;

	else if(x == 0.f)
		return 0.f;

	else if(x < 0.f)
		return -geodesica_T(-x, P);
	
	else if(x < 2.){
		if(P <= 1.){
// 			u = 1.f - sqrtf(fabsf(1.f - P));
			u = P;
			M1 = geodesica_interpolador(array1_t, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, u);
			M2 = interpolador_lineal2(array2_t, 0.f, 1.f, P_res, u);

			result1 = M2 + approx_T2(0.f, 2.f, P) - (M1 + approx_T2(x, 2.f, P));
			coeff2  = blend(1.f - x, 2.f/(rho_res - 1));
			coeff1  = 1.f - coeff2;
		}
		else{
			u = metrica_raiz_U(P)/x;
			if(u >= 1.f)
				u = 0.999999999f;
			param = u;
			M1 = geodesica_interpolador(array3_t, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, u);

			u = metrica_raiz_U(P)/2.f;
			if(u >= 1.f)
				u = 0.999999999f;
			M2 = interpolador_lineal2(array4_t, 0.f, 1.f, P_res, u);

// 			result1 = M2*approx_T2(rho_min, 2.f, P) - M1*approx_T2(x, 2.f, P);
			result1 = M2 + approx_T2(rho_min, 2.f, P) - (M1 + approx_T2(x, 2.f, P));
			coeff2 = top(blend(param, dw) + blend(1.f - x, 2.f/(rho_res - 1)));
			coeff1 = 1.f - coeff2;
		}
		result2 = approx_T(x, P);

		return coeff1*result1 + coeff2*result2;
		
	}
	else if(x == 2.f){
		if(P <= 1.){
// 			u = 1.f - sqrtf(fabsf(1.f - P));
			u = P;
			M2 = interpolador_lineal2(array2_t, 0.f, 1.f, P_res, u);
			return M2 + approx_T(2.f, P);
		}
		else{
			u = metrica_raiz_U(P)/2.f;
			M2 = interpolador_lineal2(array4_t, 0.f, 1.f, P_res, u);
			return M2 + approx_T(2.f, P);
		}
	}
	else if(x > 2.){
		float u1, u2;
		u2 = x;
		
		if(P <= 2.f){
			u1 = 2.f;
			M1 = geodesica_T(2.f, P);
		}
		else{
			u1 = P;
			M1 = 0.f;
		}
		return juan(u2, P) - juan(u1, P) + M1;
	}
}

/*
float geodesica_F5(float x, float P){
	float P_max = metrica_r(x), M1, M2, rho_min, u, coeff1, coeff2, result1, result2, diff;
	rho_min = metrica_raiz_U(P);
	if(P == 0.f)
		return 0.f;

	else if(P >= P_max)
		return 0.f;

	else if(x == 0.f)
		return 0.f;

	else if(x < 0.f)
		return -geodesica_F(-x, P);
	
	else if(x < 2.){
		if(P <= 1.){
			M1 = geodesica_interpolador(array1, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, P);
			M2 = interpolador_lineal2(array2, 0.f, 1.f, P_res, P);
		}
		else{
			u = (P - 1.f)/(P_max - 1.f);
			M1 = geodesica_interpolador(array5, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, u);
			M2 = interpolador_lineal2(array6, 1.f, 2.f, P_res, P);

		}
		return M2*approx_F(2.f, P) - M1*(approx_F(2.f, P) - approx_F(x, P));
	}
	else if(x == 2.f){
		if(P <= 1.){
			M2 = interpolador_lineal2(array2, 0.f, 1.f, P_res, P);
			return M2*approx_F(2.f, P);
		}
		else{
			M2 = interpolador_lineal2(array6, 1.f, 2.f, P_res, P);
			return M2*approx_F(2.f, P);
		}
	}
	else if(x > 2.){
		float u1, u2;
		u2 = P/x;
		if(P <= 2.f){
			u1 = P/2.f;
			M1 = geodesica_F(2.f, P);
		}
		else{
			u1 = 1.f;
			M1 = 0.f;
		}
		return -asinf(u2) + asinf(u1) + M1;
	}
}*/


/*
float geodesica_T(float x, float P){
	float P_max = metrica_r(x), M1, M2, rho_min, u;

	if(P == 0.f)
		return x;

	else if(P == P_max)
		return 0.f;

	else if( P > P_max)
		return 0.f;
	
	else if(x == 0.f)
		return 0.f;

	else if(x < 0.f)
		return -geodesica_T(-x, P);
	
	else if(x < 2.){
		if(P <= 1.){
			M1 = geodesica_interpolador(array1_t, 2.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 0.f, P_res, x, P);
			M2 = interpolador_lineal2(array2_t, 0.f, 1.f, P_res, P);
		}
		else{
			rho_min = metrica_raiz_U(P);
			u = (x - rho_min)/(2.f - rho_min);
			M1 = geodesica_interpolador(array3_t, 1.f/(rho_res - 1), 1.f/(P_res - 1), 0.f, 1.f, P_res, u, P);
			M2 = interpolador_lineal2(array4_t, 1.f, 2.f, P_res, P);
		}
		return -logf(M2) + logf(M1);
	}
	else if(x == 2.f){
		if(P <= 1.)
			M2 = interpolador_lineal2(array2_t, 0.f, 1.f, P_res, P);
		else
			M2 = interpolador_lineal2(array4_t, 1.f, 2.f, P_res, P);
		return -logf(M2);
	}
	else if(x > 2.){
		float u1, u2;
		u2 = x;
		
		if(P <= 2.f){
			u1 = 2.f;
			M1 = geodesica_T(2.f, P);
		}
		else{
			u1 = P;
			M1 = 0.f;
		}
		return juan(u2, P) - juan(u1, P) + M1;
	}
}
*/
float interpolador_lineal(float P, float *array){
	float dP = 2.f/999, f1, f2;
	int i;
	i = (int)(P/dP);
	
	if(i >= 1000)
		i = 999;
	else if(i < 0)
		i = 0;
	
	f1 = array[i];
	f2 = array[i + 1];
	return (f2 - f1)/dP*(P - i*dP) + f1;
}



float geodesica_G(float P, float rho1, float rho2){
	if((rho1 >= 0.f)&&(rho2 >= 0.f)){
		if(P == metrica_h)
			return 100.f;
		else
			return geodesica_F(rho1, P) + geodesica_F(rho2, P);
	}
	else if((rho1 < 0.f)&&(rho2 < 0.f)){
		if(P == metrica_h)
			return -100.f;
		else
			return -geodesica_F(rho1, P) - geodesica_F(rho2, P);
	}
}

float geodesica_I(float P, float rho1, float rho2){
	if(P == metrica_h)
		return 200.f;
	else
		return fabsf(geodesica_F(rho1, P) - geodesica_F(rho2, P));
}

float geodesica_G_T(float P, float rho1, float rho2){
	return fabsf(geodesica_T(rho1, P) + geodesica_T(rho2, P));

}

float geodesica_I_T(float P, float rho1, float rho2){
//  	if(fabsf(rho2 - 0.362903f) <= 0.001f )
//  		printf("P = %f, rho1 = %f, rho2 = %f, resultado = %f\n", P, rho1, rho2, fabsf(geodesica_T_debug(rho1, P) - geodesica_T_debug(rho2, P)));
	if(P == metrica_h)
		return 20.f;
	else
		return fabsf(geodesica_T(rho1, P) - geodesica_T(rho2, P));
}

static inline float geodesica_exp_I(float P, float rho1, float rho2){
	if(P == metrica_h)
		return 0.f;
	else
		return expf(-fabsf(geodesica_F(rho1, P) - geodesica_F(rho2, P)));
}

static inline float geodesica_exp_G(float P, float rho1, float rho2){
	if( P == metrica_h)
		return 0.f;
	else if((rho1 >= 0.f)&&(rho2 >= 0.f))
		return expf(-(geodesica_F(rho1, P) + geodesica_F(rho2, P)));
	else if((rho1 < 0.f)&&(rho2 < 0.f))
		return expf((geodesica_F(rho1, P) + geodesica_F(rho2, P)));
}




void geodesica_secante(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, float error, float *result){
	float a = P1, b = P2, fa, fb, c, der;
	unsigned int i, j;

    fa = (*G)(a, rho1, rho2) - phi; 
	fb = (*G)(b, rho1, rho2) - phi;
	if(DEBUG == 1)
		printf("Starting secant method, rho1 = %f, rho2 = %f, P1 = %f, P2 = %f, fa = %f, fb = %f\n", rho1, rho2, P1, P2, fa, fb);
// 	printf("fa = %f, fb = %f\n", fa, fb);
	if(error < 0.001f)
		error = error*phi;
    if(fa*fb > 0.f){
//  		printf("no existe solucion, phi fuera de rango, a = %f,b = %f, fa = %f, fb = %f, phi = %f\n", a, b, fa, fb, phi);
        result[0] = 0.f;
		result[1] = 0.f;
	}
    else{
        for(i = 0; i < 40; i++){
			der = fabsf((fb - fa)/(b - a)) + 0.00000001f;
            c = b - (b - a)*fb/(fb - fa);
            if(c < P1)
				c = P1;
            else if(c >= P2)
				c = P2;
            a = b;
            b = c;
            fa = fb;
            fb = (*G)(b, rho1, rho2) - phi;
			if(DEBUG == 1)
				printf("sec diferencia = %f, a = %f, b = %f\n", fabsf(fb), a, b);
            if(a == b){
                a = (P1 + P2)*0.5f; 
				b = P2;
                fa = (*G)(a, rho1, rho2) - phi;
				fb = (*G)(b, rho1, rho2) - phi;
			}

            if(fabsf(fb/der) < 0.00001f)
                break;
		}
		float beta;
		if(fabsf(fa) < fabsf(fb))
			beta = a;
		else
			beta = b;
		float P = beta;
		if((fabsf(fb) < error)){
            result[0] = asinf(P/metrica_r(rho1));
            result[1] = 1.f;
        }
        else{
            result[0] = asinf(P/metrica_r(rho1));
            result[1] = 2.f;
        }
	}
}



void geodesica_biseccion2_old(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, unsigned int iteraciones, float error, float *result){
	float a = P1, b = P2, fa, fb, fc, c;
	unsigned int i, j;

	fa = (*G)(a, rho1, rho2) - phi; 
	fb = (*G)(b, rho1, rho2) - phi;
	
	if(DEBUG == 1){
		printf("Bisection started. a = %f, b = %f, fa = %f, fb = %f\n", a, b, fa, fb);
	}
	if(fa*fb > 0.f){
//		printf("Fuera de rango, fa = %f, fb = %f, rho1 = %f, rho2 = %f, phi = %f, P1 = %f, P2 = %f\n", fa, fb, rho1, rho2, phi, P1, P2);
		result[0] = 0.f;
		result[1] = 0.f;
	}
	else{
		for(i = 0; i < iteraciones; i++){
			if(DEBUG == 1)
				printf("bis diferencia = %f, a = %f, b = %f, fa = %f, fb = %f,   phi = %f, Ib = %f\n", fabsf(fb), a, b, fa, fb, phi, fb + phi);
			c  = (a + b)*0.5f;
			fc = (*G)(c, rho1, rho2) - phi;
			if(fa*fc < 0.f){
				b  = c;
				fb = fc;
			}
			else if(fc*fb < 0.f){
				a  = c;
				fa = fc;
			}

			if(fabsf(fc) < error)
				break;
		}
		result[0] = a;
		result[1] = b;
	}
}
void geodesica_biseccion2_old2(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, unsigned int iteraciones, float error, float *result){
	float a = P1, b = P2, fa, fb, fc, c, current_error;
	unsigned int i, j;

	fa = (*G)(a, rho1, rho2) - phi; 
	fb = (*G)(b, rho1, rho2) - phi;
	
	if(DEBUG == 1){
		printf("Bisection started. a = %f, b = %f, fa = %f, fb = %f\n", a, b, fa, fb);
	}
	if(fa*fb > 0.f){
//		printf("Fuera de rango, fa = %f, fb = %f, rho1 = %f, rho2 = %f, phi = %f, P1 = %f, P2 = %f\n", fa, fb, rho1, rho2, phi, P1, P2);
		result[0] = 0.f;
		result[1] = 0.f;
	}
	else{
		error = fabsf(P2 - P1)/10000.f;
		current_error = fabsf(P2 - P1);
		for(i = 0; i < 100; i++){
			if(DEBUG == 1)
				printf("bis diferencia = %f, a = %f, b = %f, fa = %f, fb = %f,   phi = %f, Ib = %f\n", fabsf(fb), a, b, fa, fb, phi, fb + phi);
			
			if(i >= 6)
				error = delta_alpha*sqrtf(fabsf(metrica_r(rho1)*metrica_r(rho1) - c*c));
			c  = (a + b)*0.5f;
			current_error *= 0.5f;
			fc = (*G)(c, rho1, rho2) - phi;
			if(fa*fc < 0.f){
				b  = c;
				fb = fc;
			}
			else if(fc*fb < 0.f){
				a  = c;
				fa = fc;
			}

			if(current_error < error)
				break;
		}
		result[0] = asinf(c/metrica_r(rho1));;
		result[1] = 1.f;
	}
}
void geodesica_biseccion2(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, unsigned int iteraciones, float error, float *result){
	float a = P1, b = P2, fa, fb, fc, c, current_error, a_alpha, b_alpha;
	unsigned int i, j;
	a_alpha = asinf(a/metrica_r(rho1));
	b_alpha = asinf(b/metrica_r(rho1));

	fa = (*G)(a, rho1, rho2) - phi;
	fb = (*G)(b, rho1, rho2) - phi;
	
	if(DEBUG == 1){
		printf("Bisection started. a = %f, b = %f, fa = %f, fb = %f\n", a, b, fa, fb);
	}
	if(fa*fb > 0.f){
//		printf("Fuera de rango, fa = %f, fb = %f, rho1 = %f, rho2 = %f, phi = %f, P1 = %f, P2 = %f\n", fa, fb, rho1, rho2, phi, P1, P2);
		result[0] = 0.f;
		result[1] = 0.f;
	}
	else if(fa == 0.f){
		result[0] = a_alpha;
		result[1] = 1.f;
	}
	else if(fb == 0.f){
		result[0] = b_alpha;
		result[1] = 1.f;
	}
	else{
		error = delta_alpha;
		current_error = fabsf(b_alpha - a_alpha);

		for(i = 0; i < 100; i++){
			if(DEBUG == 1)
				printf("P_a = %f, P_b = %f, a = %f, b = %f, fa = %f, fb = %f, phi = %f, Ib = %f, current_error = %f, criteria = %f\n", sinf(a_alpha)*metrica_r(rho1), sinf(b_alpha)*metrica_r(rho1), a_alpha, b_alpha, fa, fb, phi, fb + phi, current_error, error);
			
			c  = (a_alpha + b_alpha)*0.5f;
			current_error *= 0.5f;
			fc = (*G)(sinf(c)*metrica_r(rho1), rho1, rho2) - phi;

			if((current_error < error)||(fc == 0.f))
				break;

			if(fa*fc < 0.f){
				b_alpha  = c;
				fb = fc;
			}
			else if(fc*fb < 0.f){
				a_alpha  = c;
				fa = fc;
			}

		}

		if((fb - fa != 0.)&&(fc != 0.f)){
			c = a_alpha - fa*(b_alpha - a_alpha)/(b - a);
			if(c < a_alpha)
				c = a_alpha;
			else if(c > b_alpha)
				c = b_alpha;
		}
		result[0] = c;
		result[1] = 1.f;
	}
}

void geodesica_mixto(float (*G)(float, float, float), float rho1, float rho2, float phi, float P1, float P2, 
					 float (*X)(float, float, float), float *result){

		geodesica_biseccion2((*X), rho1, rho2, expf(-phi), P1, P2, 40, 1000.f, result);

}

void geodesica_raiz_G(float rho1, float rho2, float phi, unsigned int tipo, float *alfa, unsigned int *exito){
	float P1 = metrica_r(rho1), P2 = metrica_r(rho2), P3;
	float P_max = fmin(P1, P2), *result, alfa1;

	if(rho1*rho2 < 0.f){
		printf("No tiene solucion\n");
	}
	else{
		result = (float *) malloc(2*sizeof(float));
		geodesica_mixto(&geodesica_G, rho1, rho2, phi, metrica_h, P_max, &geodesica_exp_G, result);
		alfa1  = result[0];
		*exito = (unsigned int) (result[1]);

		//printf("Raiz G, solucion, alfa = %f, exito = %u\n", alfa1, *exito);
		if(tipo == 0){
			if(fmodf(phi, 2*pi) < pi)
				*alfa = alfa1;
			else
				*alfa = 2*pi - alfa1;
		}
		else{
			if(fmodf(phi, 2*pi) < pi)
				*alfa = pi - alfa1;
			else
				*alfa = pi + alfa1;
		}
		free(result);
	}
}


void geodesica_raiz_I(float rho1, float rho2, float phi, float *alfa, unsigned int *exito){
	float P1 = metrica_r(rho1), P2 = metrica_r(rho2), P3;
	float P_max, *result, alfa1;

	if(rho1*rho2 < 0.f)
		P_max = metrica_h;
	else
		P_max = fmin(P1, P2);
	
	result = (float *) malloc(2*sizeof(float));
	geodesica_mixto(&geodesica_I, rho1, rho2, phi, 0.f, P_max, &geodesica_exp_I, result);
	alfa1 = result[0];
	*exito = (unsigned int) (result[1]);
//	printf("alfa1 = %f, P = %f\n", alfa1, metrica_r(rho1)*sinf(alfa1));
	if(rho1 < rho2){
		if(fmodf(phi, 2*pi) <= pi)
			*alfa = pi - alfa1;
		else
			*alfa = pi + alfa1;
	}
	else{
		if(fmodf(phi, 2*pi) <= pi)
			*alfa = alfa1;
		else
			*alfa = 2*pi - alfa1;
	}
	free(result);
}
