import numpy as np
import modulo
import time
from scipy import integrate

import matplotlib.pyplot as plt
plt.ion()
DEBUG = False

T = 2.
b = 1./(2*T)
h = T - b*T**2
R = h + b*T**2

RHO_MAX, RHO_MIN = 5., -5.
condicion = 1

def r(x):
	if x >= T:
		return x
	elif x >= -T:
		return h + b*x**2
	else:
		return -x

def r_p(x):
	if x >= T:
		return 1.
	elif x >= -T:
		return 2*b*x
	else:
		return -1.

def r_p_p(x):
	if x >= T:
		return 0.
	elif x >= -T:
		return 2*b
	else:
		return 0.

def U(x, P):
	return -1./P**2 + 1./r(x)**2

def U_p(x, P):
	return -2*r_p(x)*r(x)**(-3)

def U_p_p(x, P):
	return 6*r(x)**(-4)*r_p(x)**2 - 2*r(x)**(-3)*r_p_p(x)

def raiz(P):
	if P < h:
		#print("no hay solucion, P < h")
		return 0.
	elif P >= R:
		return P
	else:
		return np.sqrt((P - h)/b)



def metrica(x, theta, indice):
	if indice == 0:
		return -c**2
	elif indice == 1:
		return 1.
	elif indice == 2:
		return r(x)**2
	elif indice == 3:
		return (r(x)*np.sin(theta))**2





def integrando_phi(x, P):
	if 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/r(x)**2



def F(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_phi, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_phi, rho_max, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_phi, 0., x, args=(P))[0]


def F2(x0, x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_phi, x0, x, args=(P))[0]
		else:
			return integrate.quad(integrando_phi, x0, x, args=(P))[0]
	elif P == 0.:
		return 0.
	else:
		return integrate.quad(integrando_phi, x0, x, args=(P))[0]

def F6(x, P):
	return modulo.geodesica_F([x], [P])[0]

def T6(x, P):
	return modulo.geodesica_T([x], [P])[0]



def integrando_T(x, P):
	if 1./P**2 - 1./r(x)**2 <= 0.:
		return 1.
	else:
		return 1./np.sqrt(1./P**2 - 1./r(x)**2)/P



def t(x, P):
	if P > h:
		rho_min = raiz(P)
		rho_max = -rho_min
		if (x < rho_min) and (x > rho_max):
			print("Error, fuera de rango")
			return 0.
		if x >= 0.:
			return integrate.quad(integrando_T, rho_min, x, args=(P))[0]
		else:
			return integrate.quad(integrando_T, rho_max, x, args=(P))[0]
	elif P == 0.:
		return x
	else:
		return integrate.quad(integrando_T, 0., x, args=(P))[0]
 



def generate_background_arrays():
	N, M = 200, 200
	rho1, rho2 = 0., 10.
	
	array_alphas1    = np.zeros([N, M])
	array_distances1 = np.zeros([N, M])

	array_alphas2    = np.zeros([N, M])
	array_distances2 = np.zeros([N, M])
	
	drho = (rho2 - rho1)/(N - 1)
	dw   = 1./(M - 1)
	for i in range(N):
		rho = rho1 + drho*i
		for j in range(M):
			w = j*dw
			alpha_min = np.arcsin(1/r(rho))
			
			alpha1 = w*alpha_min
			alpha2 = alpha_min + (np.pi - alpha_min)*w
			
			P1 = r(rho)*np.sin(alpha1)
			P2 = r(rho)*np.sin(alpha2)
			
			array_alphas1[i, j]    = F(-100., P1) - F(rho, P1)
			array_distances1[i, j] = t(-100., P1) - t(rho, P1)
			
			if alpha2 <= np.pi/2.:
				array_alphas2[i, j]    = F(100., P2) + F(rho, P2)
				array_distances2[i, j] = t(100., P2) + t(rho, P2)
			else:
				array_alphas2[i, j]    = F(100., P2) - F(rho, P2)
				array_distances2[i, j] = t(100., P2) - t(rho, P2)
	
	np.savetxt("data/worm/background_alpha1.txt", array_alphas1)
	np.savetxt("data/worm/background_alpha2.txt", array_alphas2)
	np.savetxt("data/worm/background_distances1.txt", array_distances1)
	np.savetxt("data/worm/background_distances2.txt", array_distances2)
	return array_alphas1, array_distances1, array_alphas2, array_distances2

array_alphas1, array_distances1, array_alphas2, array_distances2 = generate_background_arrays()

