#define pi (3.1415926535f)
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <GL/glew.h>
#include "parametros.h"
#include "metrica.h"
#include "elemental.h"
#include "structs.h"

float *concatenated;
GLuint ssbo_background, vbo_background;



static inline void geodesica_xyz2esf(float x, float y, float z, float *theta, float *phi){  
  const float r = sqrtf((powf(x,2) + powf(y,2) + powf(z,2)));
  *theta  = acosf(z/r)*180.f/pi;
  *phi    = fmodf(atan2f(y, x)*180.f/pi+360.f, 360.f);
}

static inline void geodesica_esf2xyz(float r, float theta, float phi, float *x, float *y, float *z){  
  *x = r*sinf(theta*pi/180.f)*cosf(phi*pi/180.f);
  *y = r*sinf(theta*pi/180.f)*sinf(phi*pi/180.f);
  *z = r*cosf(theta*pi/180.f);
}

void geodesica_cambio_base(float posicion[3], float vector_xyz[3], float *vector_esf, unsigned int shift){
	unsigned int k;
   float base[3][3] = {{sinf(posicion[1])*cosf(posicion[2]),   sinf(posicion[1])*sinf(posicion[2]),    cosf(posicion[1])},

                       {cosf(posicion[1])*cosf(posicion[2]),   cosf(posicion[1])*sinf(posicion[2]),   -sinf(posicion[1])},

                       {-sinf(posicion[2]),                                      cosf(posicion[2]),                  0.f}};

	vector_esf[3*shift + 0]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 0] += base[0][k]*vector_xyz[k];

	vector_esf[3*shift + 1]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 1] += base[1][k]*vector_xyz[k];

	vector_esf[3*shift + 2]   = 0.f;
	for(k = 0; k < 3; k++) vector_esf[3*shift + 2] += base[2][k]*vector_xyz[k];

}

float rho1, rho2, phi2;

void geodesica_solution_set(float posicion0[3], float posicion1[3], float versor0[3], float versor_perp[3], float posicion0_original[3],
							float phi, float alfa, unsigned int j, unsigned int tipo_I, float *versores, float *D, float *solution_type){
	float versor_xyz[3], P;
 	versor_xyz[0] = -cosf(alfa)*versor0[0] + sinf(alfa)*versor_perp[0];
	versor_xyz[1] = -cosf(alfa)*versor0[1] + sinf(alfa)*versor_perp[1];
 	versor_xyz[2] = -cosf(alfa)*versor0[2] + sinf(alfa)*versor_perp[2];
 	geodesica_cambio_base(posicion0_original, versor_xyz, versores, j);
	P = fabsf(sinf(alfa))*metrica_r(posicion0[0]);
	if(tipo_I == 1){
		D[j] = geodesica_I_T(P, posicion0[0], posicion1[0]);
		
		solution_type[j] = 1.f;
	}
	else{
		D[j] = geodesica_G_T(P, posicion0[0], posicion1[0]);
		solution_type[j] = 2.f;
	}
}

void geodesica_solution_set2(float posicion0[4], float posicion1[4], float alfa, unsigned int j, 
							 unsigned int tipo_I, float *D, float *solution_type, float phi){
	float P, S, dx;
	P = fabsf(sinf(alfa))*metrica_r(posicion0[0]);
	if(tipo_I == 1){
		D[j] = geodesica_I_T(P, posicion0[0], posicion1[0]);

		dx = (phi - geodesica_I(P, posicion0[0], posicion1[0]))*metrica_r(posicion1[0]);
		

		solution_type[j] = 1.f;
	}
	else{
		D[j] = geodesica_G_T(P, posicion0[0], posicion1[0]);
		dx = (phi - geodesica_G(P, posicion0[0], posicion1[0]))*metrica_r(posicion1[0]);

		solution_type[j] = 2.f;
	}
	float dt = dx*P/metrica_r(posicion1[0]);

	D[j] += dt;

	// 	printf("pitocorto\n");
//  	printf("S = %f, D = %f, phi = %f\n", S, D[j], phi);
// 	D[j] = 1.f;
// 	printf("D = %f\n", D[j]);
}

void geodesica_geodesica4(float posicion0[3], float posicion1[3], float *D, float *alfas, float *solution_type,
							float *versores){
	float versor0[3], versor1[3], versor_perp[3], versor_xyz[3], dot, alfa, modulo;

	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2];

	versor_perp[0]  = versor1[0] - coseno*versor0[0];
	versor_perp[1]  = versor1[1] - coseno*versor0[1];
	versor_perp[2]  = versor1[2] - coseno*versor0[2];
	modulo          = sqrtf(versor_perp[0]*versor_perp[0] + versor_perp[1]*versor_perp[1] + versor_perp[2]*versor_perp[2]);
	versor_perp[0] *= 1.f/modulo;
	versor_perp[1] *= 1.f/modulo;
	versor_perp[2] *= 1.f/modulo;


//	*D = 0.5f;
// 	float alfa;
	unsigned int exito, exito_sum = 0, exito_I;


    float phi = acosf(coseno);

	if(posicion0[0]*posicion1[0] <= 0.f){
		geodesica_raiz_I(posicion0[0], posicion1[0], 0*pi + phi, &alfa, &exito);
		alfas[0] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 0*pi + phi, alfa, 0, 1, versores, D, solution_type);

		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		alfas[1] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 2*pi - phi, alfa, 1, 1, versores, D, solution_type);

		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		alfas[2] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 2*pi + phi, alfa, 2, 1, versores, D, solution_type);

		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		alfas[3] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 4*pi - phi, alfa, 3, 1, versores, D, solution_type);

	}
	else{
		unsigned int tipo;
		if((posicion0[0] >= 0.f)&&(posicion1[0] >= 0.f))
			tipo = 0;
		else if((posicion0[0] < 0.f)&&(posicion1[0] < 0.f))
			tipo = 1;
			
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[0] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 0*pi + phi, alfa, 0, exito_I, versores, D, solution_type);

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[1] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 2*pi - phi, alfa, 1, exito_I, versores, D, solution_type);

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi + phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[2] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 2*pi + phi, alfa, 2, exito_I, versores, D, solution_type);

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 4*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[3] = alfa;
		geodesica_solution_set(posicion0, posicion1, versor0, versor_perp, posicion0, 4*pi - phi, alfa, 3, exito_I, versores, D, solution_type);
	}
}

void geodesica_geodesica5(float posicion0_original[4], float posicion1[3], float *D, float *alfas, float *solution_type){
	float versor0[3], versor1[3], versor_perp[3], versor_xyz[3], dot, alfa, modulo, posicion0[3];
	posicion0[0] = posicion0_original[1];
	posicion0[1] = posicion0_original[2];
	posicion0[2] = posicion0_original[3];

	versor0[0] = sinf(posicion0[1])*cosf(posicion0[2]);
	versor0[1] = sinf(posicion0[1])*sinf(posicion0[2]);
	versor0[2] = cosf(posicion0[1]);

	versor1[0] = sinf(posicion1[1])*cosf(posicion1[2]);
	versor1[1] = sinf(posicion1[1])*sinf(posicion1[2]);
	versor1[2] = cosf(posicion1[1]);

	float coseno = versor0[0]*versor1[0] + versor0[1]*versor1[1] + versor0[2]*versor1[2];

	unsigned int exito, exito_sum = 0, exito_I;


    float phi = acosf(coseno);

	if(posicion0[0]*posicion1[0] <= 0.f){
		geodesica_raiz_I(posicion0[0], posicion1[0], 0*pi + phi, &alfa, &exito);
		alfas[0] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 0, 1, D, solution_type, 0*pi + phi);

		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		alfas[1] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 1, 1, D, solution_type, 2*pi - phi);

		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		alfas[2] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 2, 1, D, solution_type, 2*pi + phi);

		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		alfas[3] = alfa;
		geodesica_solution_set2(posicion0, posicion1,  alfa, 3, 1, D, solution_type, 4*pi - phi);

	}
	else{
		unsigned int tipo;
		if((posicion0[0] >= 0.f)&&(posicion1[0] >= 0.f))
			tipo = 0;
		else if((posicion0[0] < 0.f)&&(posicion1[0] < 0.f))
			tipo = 1;
			
		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[0] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 0, exito_I, D, solution_type, 0*pi + phi);

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[1] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 1, exito_I, D, solution_type, 2*pi - phi);

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 2*pi + phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 2*pi + phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[2] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 2, exito_I, D, solution_type, 2*pi + phi);

		exito_I = 1;
		exito_sum = 0;
		geodesica_raiz_I(posicion0[0], posicion1[0], 4*pi - phi, &alfa, &exito);
		exito_sum += exito;
		if(exito_sum == 0){
			geodesica_raiz_G(posicion0[0], posicion1[0], 4*pi - phi, tipo, &alfa, &exito);
			exito_sum += exito;
			exito_I = 0;
		}
		alfas[3] = alfa;
		geodesica_solution_set2(posicion0, posicion1, alfa, 3, exito_I, D, solution_type, 4*pi - phi);
	}
	solution_type[0] = 1.f;
	solution_type[1] = 2.f;
	solution_type[2] = 3.f;
	solution_type[3] = 4.f;	
	/*
	if((fabsf(posicion0[0]) <= 0.05f)&&(fabsf(posicion1[0]) <= 0.05f)){
		float factor = metrica_r((posicion0[0] + posicion1[0])/2.f);
		D[0] = (0*pi + phi)*factor;
		D[1] = (2*pi - phi)*factor;
		D[2] = (2*pi + phi)*factor;
		D[3] = (4*pi - phi)*factor;
	}
	else if((fabsf(posicion0[0] - posicion1[0]) <= 0.05f)&&(phi*metrica_r((posicion0[0] + posicion1[0])/2.f) <= 0.05f)){
		float dx, dy;
		dx = posicion0[0] - posicion1[0];
		dy = phi*metrica_r((posicion0[0] + posicion1[0])/2.f);
		D[0]     = sqrtf(dx*dx + dy*dy);
// 		alfas[0] = atan2f(dy, dx);
	}*/
}


void geodesica_ray_tracer(float posicion[4], float *versor_u, unsigned int shift, float vectores[4][4],
				float *cuadrivector_U, unsigned char *visible){
  float cuadrivector_u[4], numero, vectores2[4][4], alfa, D1, aux1, aux2, intensidad;
//   float cuadrivector_U[4];  //cambio de coordenadas

  cuadrivector_u[0] = -1.f;
  cuadrivector_u[1] = versor_u[3*shift + 0];
  cuadrivector_u[2] = versor_u[3*shift + 1];
  cuadrivector_u[3] = versor_u[3*shift + 2];

  for(unsigned int k = 0; k<4; k++){
    vectores2[k][0]   = -vectores[k][0];
    vectores2[k][1]   =  vectores[k][1];
    vectores2[k][2]   =  vectores[k][2]*metrica_r(posicion[1]);
    vectores2[k][3]   =  vectores[k][3]*metrica_r(posicion[1])*sinf(posicion[2]);
  }
  for(unsigned int k=0; k<4; k++)
    vectores2[0][k] *= -1.f;

  cuadrivector_U[0] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[0] += vectores2[0][k]*cuadrivector_u[k];

  cuadrivector_U[1] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[1] += vectores2[1][k]*cuadrivector_u[k];

  cuadrivector_U[2] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[2] += vectores2[2][k]*cuadrivector_u[k];
  
  cuadrivector_U[3] = 0.f;
  for(unsigned int k = 0; k<4 ;k++) cuadrivector_U[3] += vectores2[3][k]*cuadrivector_u[k];


  //numero = fabsf(cuadrivector_U[1]);
  numero = 1.f;
}




const unsigned int n_atrib = 8, v_info = 3, v_info2 = 9;


void geodesica_dibujador_objeto3(float posicion[4], float vectores[4][4], 
								 struct object_data objeto, float *vertx_data, unsigned char init){

	float valor, P_min[4];
	unsigned int T[3];
	unsigned char visible;
	
#pragma omp parallel num_threads(8)
{
	float valor, *distancia_fuente, vertice[3], *alfas, *solution_type, sol_type;
	distancia_fuente = (float *) malloc(4*sizeof(float));
	alfas            = (float *) malloc(4*sizeof(float));
	solution_type    = (float *) malloc(4*sizeof(float));
	
	#pragma omp for schedule(dynamic, 20)
	for(unsigned int i = 0; i < objeto.n_vert; i++){
        vertice[0] = objeto.vertices[3*i + 0];
        vertice[1] = objeto.vertices[3*i + 1];
        vertice[2] = objeto.vertices[3*i + 2];

		geodesica_geodesica5(posicion, vertice, distancia_fuente, alfas, solution_type);
		if(vertice[0]*posicion[1] < 0.f)
			valor = -1.f;
		else
			valor = 0.f;
		for(unsigned int j = 0; j < 4; j++){
			sol_type = solution_type[j];
			vertx_data[4*v_info*i + v_info*j + 0] = alfas[j];
			vertx_data[4*v_info*i + v_info*j + 1] = distancia_fuente[j];
			vertx_data[4*v_info*i + v_info*j + 2] = sol_type;
		}
	}
	free(alfas);
	free(distancia_fuente);
	free(solution_type);
}
}



float model_acceleration(unsigned int, float[3], float[3]);

void geodesica_evolucionador(struct particula_data particulas){
	unsigned int numero = particulas.n_vert;
	if(particulas.moving == 1){
		float X[3], V[3], A[3], dtau = 0.01f;
		for(unsigned int i = 0; i < numero; i++){
			X[0] = particulas.vertices[3*i + 0];
			X[1] = particulas.vertices[3*i + 1];
			X[2] = particulas.vertices[3*i + 2];

			V[0] = particulas.velocity[3*i + 0];
			V[1] = particulas.velocity[3*i + 1];
			V[2] = particulas.velocity[3*i + 2];
			
			A[0] = model_acceleration(0, X, V);
			A[1] = model_acceleration(1, X, V);
			A[2] = model_acceleration(2, X, V);

			particulas.vertices[3*i + 0] += V[0]*dtau;
			particulas.vertices[3*i + 1] += V[1]*dtau;
			particulas.vertices[3*i + 2] += V[2]*dtau;
			
			particulas.velocity[3*i + 0] += A[0]*dtau;
			particulas.velocity[3*i + 1] += A[1]*dtau;
			particulas.velocity[3*i + 2] += A[2]*dtau;
		}
	}
}

static inline float min3(float x1, float x2, float x3){
	if((x1 <= x2)&&(x1 <= x3))
		return x1;
	else if ((x2 <= x1)&&(x2 <= x3))
		return x2;
	else
		return x3;
}

static inline float max3(float x1, float x2, float x3){
	if((x1 >= x2)&&(x1 >= x3))
		return x1;
	else if ((x2 >= x1)&&(x2 >= x3))
		return x2;
	else
		return x3;
}

void geodesica_triangulador(struct object_data objeto, float *vertx_data, float *triangulos){
	unsigned int stride1 = objeto.n_triang*3*n_atrib, stride2 = 3*n_atrib, stride3 = n_atrib, T[3];
	float P1, P2, P3, P_min, P_max, sol_type;
	
	for(unsigned int j = 0; j < 4; j++){
		for(unsigned int i = 0; i < objeto.n_triang; i++){
			T[0] = objeto.triangulos[3*i + 0];
			T[1] = objeto.triangulos[3*i + 1];
			T[2] = objeto.triangulos[3*i + 2];

			for(unsigned int k = 0; k < 3; k++){
				triangulos[stride1*j + stride2*i + stride3*k + 0] = objeto.vertices[3*T[k] + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 1] = objeto.vertices[3*T[k] + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 2] = objeto.vertices[3*T[k] + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 3] = vertx_data[4*v_info*T[k] + v_info*j + 0];
				triangulos[stride1*j + stride2*i + stride3*k + 4] = vertx_data[4*v_info*T[k] + v_info*j + 1];
				triangulos[stride1*j + stride2*i + stride3*k + 5] = vertx_data[4*v_info*T[k] + v_info*j + 2];
				triangulos[stride1*j + stride2*i + stride3*k + 6] = objeto.u[T[k]];
				triangulos[stride1*j + stride2*i + stride3*k + 7] = objeto.v[T[k]];
			}
		}
	}
}


void load_background_arrays(){
	float *background_alphas1, *background_alphas2, *background_distances1, *background_distances2;
	unsigned int N = 200, M = 200;

	background_alphas1 = (float *) malloc(N*M*sizeof(float));
	background_alphas2 = (float *) malloc(N*M*sizeof(float));
	background_distances1 = (float *) malloc(N*M*sizeof(float));
	background_distances2 = (float *) malloc(N*M*sizeof(float));
	concatenated = (float *) malloc(4*N*M*sizeof(float));
	
	FILE *pfin1, *pfin2, *pfin3, *pfin4;
	
	pfin1 = fopen("data/worm/background_alpha1.txt", "r");
	pfin2 = fopen("data/worm/background_alpha2.txt", "r");
	pfin3 = fopen("data/worm/background_distance1.txt", "r");
	pfin4 = fopen("data/worm/background_distance2.txt", "r");

	for(unsigned int i = 0; i < N; i++){
		for(unsigned int j = 0; j < M; j++){
			fscanf(pfin1, "%f ", &background_alphas1[i*M + j]);
			fscanf(pfin2, "%f ", &background_alphas2[i*M + j]);
			fscanf(pfin3, "%f ", &background_distances1[i*M + j]);
			fscanf(pfin4, "%f ", &background_distances2[i*M + j]);
			
			
			concatenated[0*N*M + i*M + j] = background_alphas1[i*M + j];
			concatenated[1*N*M + i*M + j] = background_alphas2[i*M + j];
			concatenated[2*N*M + i*M + j] = background_distances1[i*M + j];
			concatenated[3*N*M + i*M + j] = background_distances2[i*M + j];
// 			printf("value %f\n", concatenated[0*N*M + i*M + j]);
		}
		fscanf(pfin1, "\n");
		fscanf(pfin2, "\n");
		fscanf(pfin3, "\n");
		fscanf(pfin4, "\n");
	}
	fclose(pfin1);
	fclose(pfin2);
	fclose(pfin3);
	fclose(pfin4);

	
	float *buffer    = (float *) malloc(2*3*2*sizeof(float));
	buffer[0] = -1.f; 
	buffer[1] = -1.f;
	buffer[2] = 1.f;
	buffer[3] = -1.f;
	buffer[4] = 1.f;
	buffer[5] = 1.f;
	
	buffer[6] = 1.f; 
	buffer[7] = 1.f;
	buffer[8] = -1.f;
	buffer[9] = 1.f;
	buffer[10] = -1.f;
	buffer[11] = -1.f;
	
	
	glGenBuffers(1, &vbo_background);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_background);
	glBufferData(GL_ARRAY_BUFFER, 2*3*2*sizeof(float), buffer, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &ssbo_background);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_background);
	glBufferData(GL_SHADER_STORAGE_BUFFER, 4*N*M*sizeof(float), concatenated, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4*N*M*sizeof(float), concatenated);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	
	free(concatenated);
	free(background_alphas1);
	free(background_alphas2);
	free(background_distances1);
	free(background_distances2);
}

void draw_vertex(float posicion[4], float *cam_matrix, float *cam_matrix_inv, float aspect_ratio, float pix_size, unsigned int resolutionx, unsigned int resolutiony, unsigned int shaderProgram2, unsigned int *textures, unsigned int tex_idx, GLuint VBO, unsigned int numero_triang, float *vertices, float *triangulos){
	float *base;
	int i;



	
	base = (float *) malloc(9*sizeof(float));
	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[3] =  sinf(posicion[2])*sinf(posicion[3]);
	base[6] =  cosf(posicion[2]);

	base[1] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[7] = -sinf(posicion[2]);

	base[2] = -sinf(posicion[3]);
	base[5] =  cosf(posicion[3]);
	base[8] =  0.f;


	
	glUseProgram(shaderProgram2);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3*N_GEO*n_atrib*numero_triang*sizeof(float), triangulos);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)0);      // coordenadas
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)12);     // alpha
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)16);     // distance
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)20);     // type
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, n_atrib * sizeof(float), (void*)24);     // uv coordinates
	glEnableVertexAttribArray(0); 
	glEnableVertexAttribArray(1); 
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	i = glGetUniformLocation(shaderProgram2, "ourTexture");
	glUniform1i(i, 0);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, textures[tex_idx]);
	
// 	printf("boludeces, i = %d, tex_idx = %d, tex = %d\n", i, tex_idx, textures[tex_idx]);
	
	i = glGetUniformLocation(shaderProgram2, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram2, "cam_matrix");
	glUniformMatrix4fv(i, 1, GL_TRUE, cam_matrix);

	i = glGetUniformLocation(shaderProgram2, "cam_matrix_inv");
	glUniformMatrix4fv(i, 1, GL_TRUE, cam_matrix_inv);

	i = glGetUniformLocation(shaderProgram2, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	i = glGetUniformLocation(shaderProgram2, "pix_size");
	glUniform1f(i, pix_size);

	i = glGetUniformLocation(shaderProgram2, "light_speed");
	glUniform1f(i, light_speed);

	i = glGetUniformLocation(shaderProgram2, "resolution");
	glUniform2f(i, resolutionx*1.f, resolutiony*1.f);

	i = glGetUniformLocation(shaderProgram2, "base");
	glUniformMatrix3fv(i, 1, GL_TRUE, &base[0]);


	glDrawArrays(GL_TRIANGLES, 0, N_GEO*3*numero_triang);


	glBindTexture(GL_TEXTURE_2D, 0);
	free(base);
}






void render_background(float posicion[4],  float *cam_matrix, float *cam_matrix_inv, unsigned int shaderProgram, unsigned int resolutionx, unsigned int resolutiony, float aspect_ratio, float pix_size, unsigned int *textures, unsigned int tex_idx1, unsigned int tex_idx2){
	int i;
	float *base;
	
	base = (float *) malloc(9*sizeof(float));

	base[0] =  sinf(posicion[2])*cosf(posicion[3]);
	base[3] =  sinf(posicion[2])*sinf(posicion[3]);
	base[6] =  cosf(posicion[2]);

	base[1] =  cosf(posicion[2])*cosf(posicion[3]);
	base[4] =  cosf(posicion[2])*sinf(posicion[3]);
	base[7] = -sinf(posicion[2]);

	base[2] = -sinf(posicion[3]);
	base[5] =  cosf(posicion[3]);
	base[8] =  0.f;

	glUseProgram(shaderProgram);
	glClearColor(1.f, 1.f, 0.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_background);


	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2* sizeof(float), (void*)0);      // coordenadas
	glEnableVertexAttribArray(0); 

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, ssbo_background);

	i = glGetUniformLocation(shaderProgram, "texture1");
	glUniform1i(i, 0);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, textures[tex_idx1]);

	i = glGetUniformLocation(shaderProgram, "texture2");
	glUniform1i(i, 1);
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, textures[tex_idx2]);

	i = glGetUniformLocation(shaderProgram, "light_speed");
	glUniform1f(i, light_speed);
	
	i = glGetUniformLocation(shaderProgram, "aspect_ratio");
	glUniform1f(i, aspect_ratio);

	i = glGetUniformLocation(shaderProgram, "pix_size");
	glUniform1f(i, pix_size);
	
	i = glGetUniformLocation(shaderProgram, "resolution");
	glUniform2f(i, resolutionx*1.f, resolutiony*1.f);

	i = glGetUniformLocation(shaderProgram, "cam_matrix");
	glUniformMatrix4fv(i, 1, GL_TRUE, cam_matrix);

	i = glGetUniformLocation(shaderProgram, "cam_matrix_inv");
	glUniformMatrix4fv(i, 1, GL_TRUE, cam_matrix_inv);
	
	i = glGetUniformLocation(shaderProgram, "posicion");
	glUniform4f(i, posicion[0], posicion[1], posicion[2], posicion[3]);

	i = glGetUniformLocation(shaderProgram, "base");
	glUniformMatrix3fv(i, 1, GL_TRUE, &base[0]);

	glDrawArrays(GL_TRIANGLES, 0, 2*3);
}
GLchar *load_shader_source(char *filename) {
	FILE *file = fopen(filename, "r");             // open 
	fseek(file, 0L, SEEK_END);                     // find the end
	size_t size = ftell(file);                     // get the size in bytes
	GLchar *shaderSource = calloc(1, size);        // allocate enough bytes
	rewind(file);                                  // go back to file beginning
	fread(shaderSource, size, sizeof(char), file); // read each char into ourblock
	fclose(file);                                  // close the stream
	return shaderSource;
}

void load_and_compile(unsigned int *shaderProgram1, unsigned int *shaderProgram2, unsigned int *shaderProgram3){
	int  success;
	char infoLog[15120];
	GLchar *vertexSource1, *fragmentSource1, *vertexSource2, *fragmentSource2, *geometrySource1, *vertexSource3, *fragmentSource3, *geometrySource3;


	vertexSource1   = load_shader_source("shaders/worm/vertex_source.glsl");
	fragmentSource1 = load_shader_source("shaders/worm/fragment_source.glsl");
	geometrySource1 = load_shader_source("shaders/worm/geometry_source.glsl");
	
	vertexSource2   = load_shader_source("shaders/worm/vertex_background.glsl");
	fragmentSource2 = load_shader_source("shaders/worm/fragment_background.glsl");

	//compilando shaders y todo eso 2
    unsigned int vertexShader1;
    vertexShader1 = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader1, 1, (const GLchar**)&vertexSource1, NULL);
    glCompileShader(vertexShader1);
    glGetShaderiv(vertexShader1, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(vertexShader1, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX1::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int fragmentShader1;
    fragmentShader1 = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader1, 1, (const GLchar**)&fragmentSource1, NULL);
    glCompileShader(fragmentShader1);
    glGetShaderiv(fragmentShader1, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(fragmentShader1, 512, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT1::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int geometryShader1;
	geometryShader1 = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometryShader1, 1, (const GLchar**)&geometrySource1, NULL);
    glCompileShader(geometryShader1);
    glGetShaderiv(geometryShader1, GL_COMPILE_STATUS, &success);

	glGetShaderInfoLog(geometryShader1, 15120, NULL, infoLog);
	printf("SHADER::GEOMETRY1 IMPORTANT::COMPILATION_LOG %s\n", infoLog);




	//uniendo shader 1
	*shaderProgram1 = glCreateProgram();
	glAttachShader(*shaderProgram1, vertexShader1);
	glAttachShader(*shaderProgram1, geometryShader1);
	glAttachShader(*shaderProgram1, fragmentShader1);
	glLinkProgram(*shaderProgram1);





    unsigned int vertexShader2;
    vertexShader2 = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader2, 1, (const GLchar**)&vertexSource2, NULL);
    glCompileShader(vertexShader2);
    glGetShaderiv(vertexShader2, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(vertexShader2, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX2::COMPILATION_FAILED %s\n", infoLog);
    }

	unsigned int fragmentShader2;
    fragmentShader2 = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader2, 1, (const GLchar**)&fragmentSource2, NULL);
    glCompileShader(fragmentShader2);
    glGetShaderiv(fragmentShader2, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(fragmentShader2, 512, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT2::COMPILATION_FAILED %s\n", infoLog);
    }
	*shaderProgram2 = glCreateProgram();
	glAttachShader(*shaderProgram2, vertexShader2);
	glAttachShader(*shaderProgram2, fragmentShader2);
	glLinkProgram(*shaderProgram2);

	printf("shaders compilados\n");
}	


void geodesica_benchmark_F(unsigned int N){
	float rho, P, *rho_space, *P_space, *F_space;
	double tiempo, tiempo2, delta;

	rho_space = (float *) malloc(N*sizeof(float));
	P_space   = (float *) malloc(N*sizeof(float));
	F_space   = (float *) malloc(N*sizeof(float));
	
	
	for(unsigned int i = 0; i < N; i++){
		rho = 20.f*drand48() - 10.f;
		P   = metrica_r(rho)*drand48()*0.99f;
		rho_space[i] = rho;
		P_space[i]   = P;
	}
	tiempo = omp_get_wtime();
	for(unsigned int i = 0; i < N; i++){
// 		printf("rho = %f, P = %f\n", rho_space[i], P_space[i]);
		F_space[i] = geodesica_F(rho_space[i], P_space[i]);
	}
	tiempo2 = omp_get_wtime();
	delta = tiempo2 - tiempo;
	printf("Seconds = %lf, Time per F evaluation = %f us\n", delta, delta/N*1000000.);
	free(rho_space);
	free(P_space);
	free(F_space);
}
