#version 330 core

#define pi (3.1415926535)

layout(triangles) in;
layout(triangle_strip, max_vertices = 200) out;

in vec3 cons_pos[];
in float alfas[];
in float T[];
in float tipo2[];
in vec2 TexCoord[];
// in vec3 aPos2[];

out vec2 tex_coord;
out float P_min;
out vec3 aPos3;
 out vec4 color;
out float eliminar;
out float render;
uniform vec4 posicion;
uniform vec4 worm_position;
uniform mat4 vectores;
uniform float modo;
uniform mat3 base;
uniform float aspect_ratio;
uniform float indice;
uniform float cantidad;
const int bines  = 5;
const int bines2 = 6;
const float c = 2.;
vec3 var_pos1, var_pos2, var_pos3, dir, centro,versor0;
vec3 punto1, punto2;
vec2 tex1, tex2;
float dtheta = 10.0*pi/180., phi_medio, alpha1, alpha2, T1, T2, P_minimo, phi_minimo, phi_maximo;
int exito;
float renderizado = 1.;

float metrica_r(float x){
	if(x >= 2.)
		return x;
	else if(x >= -2.)
		return 1. + 0.25*x*x;
	else
		return -x;
}

float funciones(vec2 vert1, vec2 vert2, vec2 vert3, int i, vec2 posicion){
	float x, modulo;
	vec2 vector_0, vector_jk, vector_i, versor_jk, vector_perp, versor_perp;
	
	if(i == 0){
		vector_0  = vert2;
		vector_jk = vert3 - vector_0;
		vector_i  = vert1 - vector_0;
	}
	else if(i == 1){
		vector_0  = vert1;
		vector_jk = vert3 - vector_0;
		vector_i  = vert2 - vector_0;
	}
	else{
		vector_0  = vert1;
		vector_jk = vert2 - vector_0;
		vector_i  = vert3 - vector_0;
	}

	modulo = sqrt(vector_jk.x*vector_jk.x + vector_jk.y*vector_jk.y);
	versor_jk = vector_jk/modulo;


	x = versor_jk.x*vector_i.x + versor_jk.y*vector_i.y;
	vector_perp = vector_i - x*versor_jk;

	modulo = sqrt(vector_perp.x*vector_perp.x + vector_perp.y*vector_perp.y);
	versor_perp = vector_perp/modulo;

	return dot(posicion - vector_0, versor_perp);
}




float interpolador(vec2 vert1, vec2 vert2, vec2 vert3, float valor1, float valor2, float valor3, vec2 posicion){
	return  valor1*funciones(vert1, vert2, vert3, 0, posicion) + 
			valor2*funciones(vert1, vert2, vert3, 1, posicion) + 
			valor3*funciones(vert1, vert2, vert3, 2, posicion);
}

vec3 geodesica4(vec3 pos, float alfa){
	vec3 versor1, versor_perp, versor_xyz, vector_esf;
	float modulo, coseno;
	int k;
// 	mat3 base;

	versor1[0] = sin(pos[1])*cos(pos[2]);
	versor1[1] = sin(pos[1])*sin(pos[2]);
	versor1[2] = cos(pos[1]);

	coseno = dot(versor0, versor1);
	
	versor_perp  = versor1 - coseno*versor0;
	
	modulo = sqrt(dot(versor_perp, versor_perp));

	versor_perp *= 1./modulo;

	versor_xyz = -cos(alfa)*versor0 + sin(alfa)*versor_perp;

	vector_esf = versor_xyz*base;
	return vector_esf;
}

vec4 raytracer(vec3 versor_u){
	vec4 cuadrivector_u, cuadrivector_U;
	int k;

	cuadrivector_u[0] = -1./c;
	cuadrivector_u[1] = versor_u[0];
	cuadrivector_u[2] = versor_u[1]/metrica_r(posicion[1]);
	cuadrivector_u[3] = versor_u[2]/metrica_r(posicion[1])/sin(posicion[2]);

	cuadrivector_U = cuadrivector_u*vectores;
	return cuadrivector_U;
}



vec3 transformacion(vec3 pos, float alfa, float distancia){
	vec3 vector_esf, vector;
	vec4 cuadrivector;
	float norma;

	vector_esf = geodesica4(pos, alfa);
	cuadrivector = raytracer(vector_esf);

	norma = sqrt(dot(cuadrivector.yzw, cuadrivector.yzw));
	
	vector.xyz = distancia*cuadrivector.zwy/norma;
	return vector;
}

float signo (vec2 p1, vec2 p2, vec2 p3){
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

int in_triangle (vec2 pt, vec2 v1, vec2 v2, vec2 v3){
    int b1, b2, b3;
	if(signo(pt, v1, v2) < 0.)
	    b1 = 1;
	else
		b1 = 0;
	if(signo(pt, v2, v3) < 0.)
	    b2 = 1;
	else
		b2 = 0;
	if(signo(pt, v3, v1) < 0.)
	    b3 = 1;
	else
		b3 = 0;

	if((b1 == b2) && (b2 == b3))
	    return 1;
	else
		return 0;
}

float simple(float v1, float v2, float v3, int bines, int i, int j){
	float a, b;
	if(i > 0){
		a = i*(v2 - v1)/bines + v1;
		b = i*(v3 - v1)/bines + v1;
		return j*(b - a)/i + a;
	}
	else
		return v1;
}

vec2 R(float t, vec2 r0, vec2 da){
	return r0 + t*da;
}
float angulo(vec2 vector, int tipo){
	float numero;
	if(tipo == 0){
		numero = atan(vector.y, vector.x);
		if(numero >= 0.)
			return numero;
		else
			return 2*pi + numero;
	}
	else
		return pi + atan(vector.y, vector.x);
}

vec2 solucion(int n, vec2 r0, vec2 da, int tipo){
	vec2 retorno;
	float theta, tangente, t;
	if(tipo == 0)
		theta = n*dtheta;
	else
		theta = n*dtheta - pi;
	tangente = tan(theta);
// 	if(abs(da.y - da.x*tangente) >= 0.001)
		t = (r0.x*tangente - r0.y)/(da.y - da.x*tangente);
// 	else
// 		t = 0.;
	if(abs(cos(theta)) <= 0.001)
		t = -r0.x/da.x;
	if(abs(cos(theta)) >= 0.4)
		retorno.y = (da.x*t + r0.x)/cos(theta);
	else
		retorno.y = (da.y*t + r0.y)/sin(theta);
	
	retorno.x = t;
	return retorno;
}

vec2 solucion2(float n, vec2 r0, vec2 da, int tipo){
	vec2 retorno;
	float theta, tangente, t;
	if(tipo == 0)
		theta = n*dtheta;
	else
		theta = n*dtheta - pi;
	tangente = tan(theta);
	t = (r0.x*tangente - r0.y)/(da.y - da.x*tangente);
	if(abs(cos(theta)) <= 0.001)
		t = -r0.x/da.x;
	if(t < 0.)
		t = 0.;
	else if(t > 1.)
		t = 1.;
	retorno.x = t;
	if(abs(cos(theta)) >= 0.1)
		retorno.y = (da.x*t + r0.x)/cos(theta);
	else
		retorno.y = (da.y*t + r0.y)/sin(theta);
	return retorno;
}
// vec2 punto1, punto2;

void interseccion(vec2 A, vec2 B, vec2 C, int i, int tipo){
	int soluciones = 0;
	vec2 retorno, aux1, aux2, aux3, tex3;
	vec3 punto3;
	float t, sigma, r1, r2, alpha3, T3;
	
	retorno = solucion(i, A, B - A, tipo);
 	t     = retorno.x;
 	sigma = retorno.y;

	if((t >= -0.000)&&(t <= 1.00)&&(sigma >= 0.)){
		soluciones += 1;
		aux1   = R(t, A, B - A);
		punto1 = cons_pos[0] + t*(cons_pos[1] - cons_pos[0]);
		alpha1 = alfas[0]    + t*(alfas[1]    - alfas[0]);
		tex1   = TexCoord[0] + t*(TexCoord[1] - TexCoord[0]);
		T1     = T[0]        + t*(T[1]        - T[0]);
	}
	
	retorno = solucion(i, B, C - B, tipo);
 	t     = retorno.x;
 	sigma = retorno.y;

	if((t >= -0.00)&&(t <= 1.00)&&(sigma >= 0.)){
		soluciones += 1;
		if(soluciones == 2){
			aux2   = R(t, B, C - B);
			punto2 = cons_pos[1] + t*(cons_pos[2] - cons_pos[1]);
			alpha2 = alfas[1]    + t*(alfas[2]    - alfas[1]);
			tex2   = TexCoord[1] + t*(TexCoord[2] - TexCoord[1]);
			T2     = T[1]        + t*(T[2]        - T[1]);

		}

		else{
			aux1   = R(t, B, C - B);
			punto1 = cons_pos[1] + t*(cons_pos[2] - cons_pos[1]);
			alpha1 = alfas[1]    + t*(alfas[2]    - alfas[1]);
			tex1   = TexCoord[1] + t*(TexCoord[2] - TexCoord[1]);
			T1     = T[1]        + t*(T[2]        - T[1]);
		}
	}
	
	retorno = solucion(i, C, A - C, tipo);
 	t     = retorno.x;
 	sigma = retorno.y;
	
	if((t >= -0.000)&&(t <= 1.00)&&(sigma >= 0.)&&(soluciones < 2)){
		soluciones += 1;
		aux2   = R(t, C, A - C);
		punto2 = cons_pos[2] + t*(cons_pos[0] - cons_pos[2]);
		alpha2 = alfas[2]    + t*(alfas[0]    - alfas[2]);
		tex2   = TexCoord[2] + t*(TexCoord[0] - TexCoord[2]);
		T2     = T[2]        + t*(T[0]        - T[2]);

	}
	if(soluciones == 2){
		r1 = dot(aux1, aux1);
		r2 = dot(aux2, aux2);
		
		if(r1 > r2){
			punto3 = punto1;
			punto1 = punto2;
			punto2 = punto3;
			
			aux3 = aux1;
			aux1 = aux2;
			aux2 = aux3;
			
			alpha3 = alpha1;
			alpha1 = alpha2;
			alpha2 = alpha3;
			
			tex3 = tex1;
			tex1 = tex2;
			tex2 = tex3;
			
			T3 = T1;
			T1 = T2;
			T2 = T3;
		}
	}
 	else if (soluciones == 1){
		punto2 = cons_pos[0];
 		alpha2 = alfas[0];
 	}
 	else{
		punto1 = cons_pos[1];
		alpha1 = alfas[1];
		punto2 = cons_pos[0];
		alpha2 = alfas[0];
	}
}


void interseccion5(vec2 A, vec2 B, vec2 C, int i){
	vec2 retorno;
	float t, sigma;
	retorno = solucion(i, A, B - A, 0);
	t     = retorno.x;
	sigma = retorno.y;
	if((t >= 0.)&&(t <= 1.)&&(sigma >= 0.)){
		alpha1 = alfas[0]    + t*(alfas[1]    - alfas[0]);
		tex1   = TexCoord[0] + t*(TexCoord[1] - TexCoord[0]);
		T1     = T[0]        + t*(T[1]        - T[0]);
		punto1 = cons_pos[0] + t*(cons_pos[1] - cons_pos[0]);
	}
	else{
		retorno = solucion(i, B, C - B, 0);
		t     = retorno.x;
		sigma = retorno.y;
		if((t >= 0.)&&(t <= 1.)&&(sigma >= 0.)){
			alpha1 = alfas[1]    + t*(alfas[2]    - alfas[1]);
			tex1   = TexCoord[1] + t*(TexCoord[2] - TexCoord[1]);
			T1     = T[1]        + t*(T[2]        - T[1]);
			punto1 = cons_pos[1] + t*(cons_pos[2] - cons_pos[1]);
		}
		else{
			retorno = solucion(i, C, A - C, 0);
			t     = retorno.x;
			sigma = retorno.y;
			if((t >= 0.)&&(t <= 1.)&&(sigma >= 0.)){
				alpha1 = alfas[2]    + t*(alfas[0]    - alfas[2]);
				tex1   = TexCoord[2] + t*(TexCoord[0] - TexCoord[2]);
				T1     = T[2]        + t*(T[0]        - T[2]);
				punto1 = cons_pos[2] + t*(cons_pos[0] - cons_pos[2]);
			}
		}
	}
}

void interseccion3(vec2 A, vec2 B, vec2 C, int i, float phi_medio, int tipo){
	int soluciones = 0;
	vec2 retorno, aux1, aux2, aux3, tex3;
	vec3 punto3;
	float t, sigma, r1, r2, alpha3, T3;
	exito = 0;
	if(angulo(A, tipo) == phi_medio){
		soluciones += 1;
		retorno = solucion(i, B, C - B, tipo);
		t     = retorno.x;
		sigma = retorno.y;

		aux1   = R(t, B, C - B);
		punto1 = cons_pos[1] + t*(cons_pos[2] - cons_pos[1]);
		alpha1 = alfas[1]    + t*(alfas[2]    - alfas[1]);
		tex1   = TexCoord[1] + t*(TexCoord[2] - TexCoord[1]);
		T1     = T[1]        + t*(T[2]        - T[1]);


		retorno = solucion(i, A, B - A, tipo);
		t     = retorno.x;
		sigma = retorno.y;
		if((t >= -0.001)&&(t <= 1.001)&&(sigma >= 0.)){
			soluciones += 1;
			aux2   = R(t, A, B - A);
			punto2 = cons_pos[0] + t*(cons_pos[1] - cons_pos[0]);
			alpha2 = alfas[0]    + t*(alfas[1]    - alfas[0]);
			tex2   = TexCoord[0] + t*(TexCoord[1] - TexCoord[0]);
			T2     = T[0]        + t*(T[1]        - T[0]);
		}
		else{
			soluciones += 1;
			retorno = solucion(i, A, C - A, tipo);
			t     = retorno.x;
			sigma = retorno.y;
		
			aux2   = R(t, A, C - A);
			punto2 = cons_pos[0] + t*(cons_pos[2] - cons_pos[0]);
			alpha2 = alfas[0]    + t*(alfas[2]    - alfas[0]);
			tex2   = TexCoord[0] + t*(TexCoord[2] - TexCoord[0]);
			T2     = T[0]        + t*(T[2]        - T[0]);
		}
	}
	else if(angulo(B, tipo) == phi_medio){
		soluciones += 1;
		retorno = solucion(i, A, C - A, tipo);
		t     = retorno.x;
		sigma = retorno.y;

		aux1   = R(t, A, C - A);
		punto1 = cons_pos[0] + t*(cons_pos[2] - cons_pos[0]);
		alpha1 = alfas[0]    + t*(alfas[2]    - alfas[0]);
		tex1   = TexCoord[0] + t*(TexCoord[2] - TexCoord[0]);
		T1     = T[0]        + t*(T[2]        - T[0]);


		retorno = solucion(i, B, A - B, tipo);
		t     = retorno.x;
		sigma = retorno.y;
		if((t >= -0.001)&&(t <= 1.001)&&(sigma >= 0.)){
			soluciones += 1;
			aux2   = R(t, B, A - B);
			punto2 = cons_pos[1] + t*(cons_pos[0] - cons_pos[1]);
			alpha2 = alfas[1]    + t*(alfas[0]    - alfas[1]);
			tex2   = TexCoord[1] + t*(TexCoord[0] - TexCoord[1]);
			T2     = T[1]        + t*(T[0]        - T[1]);
		}
		else{
			soluciones += 1;
			retorno = solucion(i, B, C - B, tipo);
			t     = retorno.x;
			sigma = retorno.y;
		
			aux2   = R(t, B, C - B);
			punto2 = cons_pos[1] + t*(cons_pos[2] - cons_pos[1]);
			alpha2 = alfas[1]    + t*(alfas[2]    - alfas[1]);
			tex2   = TexCoord[1] + t*(TexCoord[2] - TexCoord[1]);
			T2     = T[1]        + t*(T[2]        - T[1]);
		}
	}
	else{
		soluciones += 1;
		retorno = solucion(i, A, B - A, tipo);
		t     = retorno.x;
		sigma = retorno.y;

		aux1   = R(t, A, B - A);
		punto1 = cons_pos[0] + t*(cons_pos[1] - cons_pos[0]);
		alpha1 = alfas[0]    + t*(alfas[1]    - alfas[0]);
		tex1   = TexCoord[0] + t*(TexCoord[1] - TexCoord[0]);
		T1     = T[0]        + t*(T[1]        - T[0]);


		retorno = solucion(i, C, B - C, tipo);
		t     = retorno.x;
		sigma = retorno.y;
		if((t >= -0.001)&&(t <= 1.001)&&(sigma >= 0.)){
			soluciones += 1;
			aux2   = R(t, C, B - C);
			punto2 = cons_pos[2] + t*(cons_pos[1] - cons_pos[2]);
			alpha2 = alfas[2]    + t*(alfas[1]    - alfas[2]);
			tex2   = TexCoord[2] + t*(TexCoord[1] - TexCoord[2]);
			T2     = T[2]        + t*(T[1]        - T[2]);
		}
		else{
			soluciones += 1;
			retorno = solucion(i, C, A - C, tipo);
			t     = retorno.x;
			sigma = retorno.y;
		
			aux2   = R(t, C, A - C);
			punto2 = cons_pos[2] + t*(cons_pos[0] - cons_pos[2]);
			alpha2 = alfas[2]    + t*(alfas[0]    - alfas[2]);
			tex2   = TexCoord[2] + t*(TexCoord[0] - TexCoord[2]);
			T2     = T[2]        + t*(T[0]        - T[2]);
		}
	}
	r1 = dot(aux1, aux1);
	r2 = dot(aux2, aux2);
		
	if(r1 > r2){
		punto3 = punto1;
		punto1 = punto2;
		punto2 = punto3;
		
		aux3 = aux1;
		aux1 = aux2;
		aux2 = aux3;
		
		alpha3 = alpha1;
		alpha1 = alpha2;
		alpha2 = alpha3;
			
		tex3 = tex1;
		tex1 = tex2;
		tex2 = tex3;
			
		T3 = T1;
		T1 = T2;
		T2 = T3;
	}
	if(soluciones == 2)
		exito = 1;
}

void interseccion2(vec2 A, vec2 B, vec2 C, float phi_medio, int tipo){
	float r1, r2, t, sigma, alpha3, i, T3;
	vec2 aux1, aux2, aux3, retorno, tex3;
	vec3 punto3;
	
	i = (phi_medio/dtheta);
	if(angulo(A, tipo) == phi_medio){
		retorno = solucion2(i, B, C - B, tipo);
		t     = retorno.x;
		sigma = retorno.y;
		
		aux1 = R(t, B, C - B);
		aux2 = A;
		
		punto1 = cons_pos[1] + t*(cons_pos[2] - cons_pos[1]);
		alpha1 = alfas[1]    + t*(alfas[2] - alfas[1]);
		tex1   = TexCoord[1] + t*(TexCoord[2] - TexCoord[1]);
		T1     = T[1]        + t*(T[2] - T[1]);
		
		punto2 = cons_pos[0];
		alpha2 = alfas[0];
		tex2   = TexCoord[0];
		T2     = T[0];
	}
	else if(angulo(B, tipo) == phi_medio){
		retorno = solucion2(i, C, A - C, tipo);
		t     = retorno.x;
		sigma = retorno.y;
		
		aux1 = R(t, C, A - C);
		aux2 = B;

		punto1 = cons_pos[2] + t*(cons_pos[0] - cons_pos[2]);
		alpha1 = alfas[2]    + t*(alfas[0] - alfas[2]);
		tex1   = TexCoord[2] + t*(TexCoord[0] - TexCoord[2]);
		T1     = T[2]        + t*(T[0] - T[2]);

		punto2 = cons_pos[1];
		alpha2 = alfas[1];
		tex2   = TexCoord[1];
		T2     = T[1];
	}
	else{
		retorno = solucion2(i, B, A - B, tipo);
		t     = retorno.x;
		sigma = retorno.y;
		
		aux1 = R(t, B, A - B);
		aux2 = C;

		punto1 = cons_pos[1] + t*(cons_pos[0] - cons_pos[1]);
		alpha1 = alfas[1]    + t*(alfas[0] - alfas[1]);
		tex1   = TexCoord[1] + t*(TexCoord[0] - TexCoord[1]);
		T1     = T[1]        + t*(T[0] - T[1]);

		punto2 = cons_pos[2];
		alpha2 = alfas[2];
		tex2   = TexCoord[2];
		T2     = T[2];


	}
	r1 = dot(aux1, aux1);
	r2 = dot(aux2, aux2);

	if(r1 > r2){
		punto3 = punto1;
		punto1 = punto2;
		punto2 = punto3;
			
		aux3 = aux1;
		aux1 = aux2;
		aux2 = aux3;
			
		alpha3 = alpha1;
		alpha1 = alpha2;
		alpha2 = alpha3;
		
		tex3 = tex1;
		tex1 = tex2;
		tex2 = tex3;
		
		T3 = T1;
		T1 = T2;
		T2 = T3;
	}
}

void interseccion4(vec2 A, vec2 B, vec2 C, float phi_medio, int tipo){
	if(angulo(A, tipo) == phi_medio){
		punto1 = cons_pos[0];
		alpha1 = alfas[0];
		T1     = T[0];
		tex1   = TexCoord[0];
	}
	else if(angulo(B, tipo) == phi_medio){
		punto1 = cons_pos[1];
		alpha1 = alfas[1];
		T1     = T[1];
		tex1   = TexCoord[1];
	}
	else{
		punto1 = cons_pos[2];
		alpha1 = alfas[2];
		T1     = T[2];
		tex1   = TexCoord[2];
	}
}

vec3 esf2xyz(vec3 vertice){
	//Be careful, this returns a vector of lenght 1.
	vec3 retorno;
	retorno = vec3(sin(vertice.y)*cos(vertice.z), sin(vertice.y)*sin(vertice.z), cos(vertice.y));
	return retorno;
}

int det_tipo(vec2 A, vec2 B, vec2 C){
	int tipo = 0;
	vec2 retorno;
	float t, sigma;

	retorno =  solucion(0, A, B - A, 0);
	t     = retorno.x;
	sigma = retorno.y;
	if((t >= 0.)&&(t <= 1.)&&(sigma >= 0.))
		tipo = 1;

	retorno =  solucion(0, B, C - B, 0);
	t     = retorno.x;
	sigma = retorno.y;

	if((t >= 0.)&&(t <= 1.)&&(sigma >= 0.))
		tipo = 1;

	retorno =  solucion(0, C, A - C, 0);
	t     = retorno.x;
	sigma = retorno.y;
	if((t >= 0.)&&(t <= 1.)&&(sigma >= 0.))
		tipo = 1;
	return tipo;
}

vec3 min_med_max(vec2 A, vec2 B, vec2 C, int tipo){
	vec3 retorno;
	float phi_a, phi_b, phi_c, phi_minimo, phi_medio, phi_maximo;

	phi_a = angulo(A, tipo);
	phi_b = angulo(B, tipo);
	phi_c = angulo(C, tipo);

	phi_minimo = min(phi_a, min(phi_b, phi_c));
	phi_maximo = max(phi_a, max(phi_b, phi_c));

	if((phi_a != phi_minimo)&&(phi_a != phi_maximo)) 
		phi_medio = phi_a;
	else if ((phi_b != phi_minimo)&&(phi_b != phi_maximo)) 
		phi_medio = phi_b;
	else
		phi_medio = phi_c;
	retorno = vec3(phi_minimo, phi_medio, phi_maximo);
	return retorno;
}
/*
float estimador(){
	float angulo_max, angulo_min;
	
	angulo_max = acos(dot(posicion.))

}*/
void emisor(vec3 vert1, vec3 vert2, vec3 vert3, vec2 tex1, vec2 tex2, vec2 tex3){
 	render = renderizado;
 	tex_coord = tex1;
	P_min = P_minimo;
	aPos3 = vert1;
  	color = vec4(phi_medio/6.28, phi_medio/6.28, .1, 1.);
	gl_Position    = vec4(aPos3, -aPos3.z);
	gl_Position.z *= aPos3.z;
	gl_Position.y *= aspect_ratio;
	EmitVertex();

 	tex_coord = tex2;
	aPos3 = vert2;
  	color = vec4(phi_medio/6.28, phi_medio/6.28, .1, 1.);
	gl_Position    = vec4(aPos3, -aPos3.z);
	gl_Position.z *= aPos3.z;
	gl_Position.y *= aspect_ratio;
	EmitVertex();

 	tex_coord = tex3;
	aPos3 = vert3;
  	color = vec4(phi_medio/6.28, phi_medio/6.28, 0.1, 1.);
	gl_Position    = vec4(aPos3, -aPos3.z);
	gl_Position.z *= aPos3.z;
	gl_Position.y *= aspect_ratio;
	EmitVertex();
	EndPrimitive();
}

void main() {
	int i, adentro, ronda, j, k;
	vec3 vert1, vert2, vert3, vert4, var_pos1, var_pos2, var_pos3, var_pos4;
	float alphas[4], modulo2, modulo1, modulo3, distancias[3], P[3];
	vec3 vector_esf1, vector_esf2, vector_esf3;
	vec2 tex_coord1, tex_coord2, tex_coord3, tex_coord4;

	//Cantidades para el procesamiento
	versor0[0] = sin(posicion[2])*cos(posicion[3]);
	versor0[1] = sin(posicion[2])*sin(posicion[3]);
	versor0[2] = cos(posicion[2]);
	

	//Fin cantidades para el procesamiento
	
	alphas[0] = alfas[0];
	alphas[1] = alfas[1];
	alphas[2] = alfas[2];

	P[0] = abs(sin(alfas[0]))*metrica_r(posicion[1]);
	P[1] = abs(sin(alfas[1]))*metrica_r(posicion[1]);
	P[2] = abs(sin(alfas[2]))*metrica_r(posicion[1]);
	
	P_minimo = min(P[0], min(P[1], P[2]));
	P_min = P_minimo;

	modulo1 = T[0]/24.;
	modulo2 = T[1]/24.;
	modulo3 = T[2]/24.;

	vert1 = transformacion(cons_pos[0], alphas[0], T[0]/24.);
	vert2 = transformacion(cons_pos[1], alphas[1], T[1]/24.);
	vert3 = transformacion(cons_pos[2], alphas[2], T[2]/24.);

// 	adentro = in_triangle(normalize(-posicion.yzw).xy, normalize(vert1).xy, normalize(vert2).xy, normalize(vert3).xy);
	adentro = in_triangle(normalize(worm_position.xyz).xy, normalize(vert1).xy, normalize(vert2).xy, normalize(vert3).xy);
	centro.x = (cons_pos[0].x + cons_pos[1].x + cons_pos[2].x)/3.;

	vec3 pos_aux = transformacion(cons_pos[0], alphas[0], T[0]/24.);
 	if(pos_aux.x >= 0.){
		centro.y = posicion[2];
		centro.z = posicion[3];
 	}
	else{
 		centro.y = pi - posicion[2];
 		centro.z = posicion[3];
 	}
	
	float P1, P2, P3, P4;

	int criterio;
	vec3 auxiliar = vec3(sin(cons_pos[0].y)*cos(cons_pos[0].z), sin(cons_pos[0].y)*sin(cons_pos[0].z), cos(cons_pos[0].y));
	float coseno_aux = dot(versor0, auxiliar);

	int tipo = int(tipo2[0]);
	if(tipo2[0] - tipo == 2./3.)
		criterio = 1;
	else if((tipo2[0] - tipo == 1./3.)&&(tipo == 0)&&(coseno_aux >= 0.2))
		criterio = 0;
	else if((tipo2[0] - tipo == 0.)&&(tipo == 0)&&(coseno_aux >= 0.2))
		criterio = 0;
	else
		criterio = 1;

	eliminar = 1.*criterio;

	vec3 posicion_3d, vector_base1, vector_base2, retorno, cons_min, cons_max, a_vec, b_vec, c_vec, d_vec, vec_min, vec_max, nuevo, punto3;
	vec2 A, B, C, tex_a, tex_b, tex_c, tex_d, tex_final, tex_nuevo, tex_max, tex_min, tex_medio;
	float phi_a, phi_b, phi_c;
	int n1, n2, n3;
//   	criterio = 1;
//  	eliminar = 0.;

// 	punto1 = vec3(1., 1., 1.);
// 	punto2 = vec3(1., 1., 1.);
// 	alpha1 = 0.;
// 	alpha2 = 0.;
// 	if(adentro == 1)
// 		criterio = 0;
	if(criterio == 1){
   			eliminar = 0.;

		posicion_3d = esf2xyz(posicion.yzw);
		vector_base1 = vec3(0., 0., 1.);
		float coseno = dot(vector_base1, posicion_3d);
 		if(abs(coseno - 1.) <= 0.01){
  			vector_base1 = vec3(1., 0., 0.);
  			coseno = dot(vector_base1, posicion_3d);
 		}
		vector_base1 = vector_base1 - coseno*posicion_3d;
		coseno = dot(vector_base1, vector_base1);
		vector_base1 *= 1./sqrt(coseno);

		vector_base2 = cross(vector_base1, posicion_3d);

		var_pos1 = esf2xyz(cons_pos[0]);
		var_pos2 = esf2xyz(cons_pos[1]);
		var_pos3 = esf2xyz(cons_pos[2]);

		A = vec2(dot(var_pos1, vector_base2), dot(var_pos1, vector_base1));
		B = vec2(dot(var_pos2, vector_base2), dot(var_pos2, vector_base1));
		C = vec2(dot(var_pos3, vector_base2), dot(var_pos3, vector_base1));

		adentro = in_triangle(vec2(0., 0.), A, B, C);

		tipo    = det_tipo(A, B, C);
		if(adentro == 1){
			tipo = 0;
// 			eliminar = 2.;
		}
		retorno = min_med_max(A, B, C, tipo);

		phi_minimo = retorno.x;
		phi_medio  = retorno.y;
		phi_maximo = retorno.z;

		phi_a = angulo(A, tipo);
		phi_b = angulo(B, tipo);
		phi_c = angulo(C, tipo);

		n1 = int(phi_minimo/dtheta) + 1;
		n2 = int(phi_medio/dtheta ) + 1;
		n3 = int(phi_maximo/dtheta) + 1;

		if(phi_minimo == phi_a){
			cons_min = cons_pos[0];
			vec_min  = transformacion(cons_min, alphas[0], T[0]/24.);
			tex_a = TexCoord[0];
			tex_b = TexCoord[0];
			tex_min = TexCoord[0];
		}
		else if(phi_minimo == phi_b){
			cons_min = cons_pos[1];
			vec_min  = transformacion(cons_min, alphas[1], T[1]/24.);
			tex_a = TexCoord[1];
			tex_b = TexCoord[1];
			tex_min = TexCoord[1];
		}
		else{
			cons_min = cons_pos[2];
			vec_min  = transformacion(cons_min, alphas[2], T[2]/24.);
			tex_a = TexCoord[2];
			tex_b = TexCoord[2];
			tex_min = TexCoord[2];
		}
		
		if(phi_maximo == phi_a){
			cons_max = cons_pos[0];
			vec_max  = transformacion(cons_max, alphas[0], T[0]/24.);
			tex_final = TexCoord[0];
			tex_max = TexCoord[0];
		}
		else if(phi_maximo == phi_b){
			cons_max = cons_pos[1];
			vec_max  = transformacion(cons_max, alphas[1], T[1]/24.);
			tex_final = TexCoord[1];
			tex_max = TexCoord[1];
		}
		else{
			cons_max = cons_pos[2];
			vec_max  = transformacion(cons_max, alphas[2], T[2]/24.);
			tex_final = TexCoord[2];
			tex_max = TexCoord[2];
		}
		a_vec = vec_min;
		b_vec = vec_min;
		if(adentro == 0){
			for(i = n1; i <= n3; i++){
				if(i == n2){
					interseccion4(A, B, C, phi_medio, tipo);
					punto3 = punto1;
					nuevo = transformacion(punto1, alpha1, T1/24.);
					tex_nuevo = tex1;
					interseccion2(A, B, C, phi_medio, tipo);
					c_vec = transformacion(punto2, alpha2, T1/24.);
					d_vec = transformacion(punto2, alpha2, T2/24.);
					if(dot(nuevo - d_vec, nuevo - d_vec) == 0.){
						d_vec = nuevo;
						tex_d = tex_nuevo;
						c_vec = a_vec;
						tex_c = tex_a;
						emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_d);
					}
					else{
	// 					d_vec = nuevo;
	// 					tex_d = tex_nuevo;
	// 					c_vec = a_vec;
	// 					tex_c = tex_a;
	// 					emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_d);
						d_vec = b_vec;
						tex_d = tex_b;
						c_vec = nuevo;
						tex_c = tex_nuevo;
						emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_c);
					}
					a_vec = c_vec;
					b_vec = d_vec;
					tex_a = tex_c;
					tex_b = tex_d;
				}
				if(i == n3){
	// 				emisor(vec_min, vec_max, vert1);
					c_vec = vec_max;
					d_vec = vec_max;
					tex_c = tex_final;
					tex_d = tex_final;
					
					emisor(a_vec, b_vec, c_vec, tex_a, tex_b, tex_c);
					emisor(b_vec, c_vec, d_vec, tex_b, tex_c, tex_d);
				}
				else{
					interseccion(A, B, C, i, tipo);
	// 				interseccion3(A, B, C, i, phi_medio, tipo);
	// 				if(exito == 1){
						c_vec = transformacion(punto2, alpha2, T1/24.);
						d_vec = transformacion(punto2, alpha1, T2/24.);
						tex_c = tex1;
						tex_d = tex2;
					
	// 				emisor(vert1, vert2, vert3);
						emisor(a_vec, b_vec, c_vec, tex_a, tex_b, tex_c);
						emisor(b_vec, c_vec, d_vec, tex_b, tex_c, tex_d);
	// 				}
					a_vec = c_vec;
					b_vec = d_vec;
					tex_a = tex_c;
					tex_b = tex_d;
				}
			}
		}
		else{
			eliminar = 2.;
			renderizado = 0.;
			vec3 centro, medio, cons_med;
			float alfa_min = min(alfas[0], min(alfas[1], alfas[2]));
			centro.x = (cons_pos[0].x + cons_pos[1].x + cons_pos[2].x)/3.;
			centro.y = pi - posicion.z;
			centro.z = pi + posicion.w;
			if(centro.z >= 2*pi)
				centro.z += -2*pi;
   			centro.yz = posicion.wz;
			
			if(phi_medio == phi_a){
				tex_medio = TexCoord[0];
				cons_med  = cons_pos[0];
				medio = transformacion(cons_pos[0], alfas[0], T[0]/24.);
			}
			else if(phi_medio == phi_b){
				tex_medio = TexCoord[1];
				cons_med  = cons_pos[1];
				medio = transformacion(cons_pos[1], alfas[1], T[1]/24.);
			}
			else{
				tex_medio = TexCoord[2];
				cons_med  = cons_pos[2];
				medio = transformacion(cons_pos[2], alfas[2], T[2]/24.);
			}

			interseccion5(A, B, C, 0);
			b_vec = transformacion(punto1, alpha1, T1/24.);
			a_vec = transformacion(punto1, alfa_min, (T[0] + T[1] + T[2])/3./24.);
			for(i = 0; i <= min(int(2*pi/dtheta), int(cantidad)); i++){
				if(i == n1){
					d_vec = vec_min;
					tex_d = tex_min;
					
					c_vec = transformacion(cons_min, alfa_min, (T[0] + T[1] + T[2])/3./24.);
					tex_c = (TexCoord[0] + TexCoord[1] + TexCoord[2])/3.;
					
					emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_d);
					emisor(c_vec, d_vec, a_vec, tex_c, tex_d, tex_a);

					a_vec = c_vec;
					b_vec = d_vec;
					tex_a = tex_c;
					tex_b = tex_d;
				}
				if(i == n2){
					d_vec = medio;
					tex_d = tex_medio;

					c_vec = transformacion(cons_med, alfa_min, (T[0] + T[1] + T[2])/3./24.);
					tex_c = (TexCoord[0] + TexCoord[1] + TexCoord[2])/3.;
					
					emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_d);
					emisor(c_vec, d_vec, a_vec, tex_c, tex_d, tex_a);

					a_vec = c_vec;
					b_vec = d_vec;
					tex_a = tex_c;
					tex_b = tex_d;
				}
				if(i == n3){
					d_vec = vec_max;
					tex_d = tex_max;
					
					c_vec = transformacion(cons_max, alfa_min, (T[0] + T[1] + T[2])/3./24.);
					tex_c = (TexCoord[0] + TexCoord[1] + TexCoord[2])/3.;
					
					emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_d);
					emisor(c_vec, d_vec, a_vec, tex_c, tex_d, tex_a);

					a_vec = c_vec;
					b_vec = d_vec;
					tex_a = tex_c;
					tex_b = tex_d;

				}


				interseccion5(A, B, C, i);
				d_vec = transformacion(punto1, alpha1, T1/24.);
				tex_d = tex_max;
				c_vec = transformacion(punto1,  alfa_min, (T[0] + T[1] + T[2])/3./24.);
				tex_c = (TexCoord[0] + TexCoord[1] + TexCoord[2])/3.;
  				emisor(a_vec, b_vec, d_vec, tex_a, tex_b, tex_d);
				emisor(c_vec, d_vec, a_vec, tex_c, tex_d, tex_a);

				a_vec = c_vec;
				b_vec = d_vec;
				tex_a = tex_c;
				tex_b = tex_d;
			}
		}
	}
	else if(criterio == 0){
		var_pos1 = cons_pos[0];
		var_pos2 = cons_pos[1];
		var_pos3 = cons_pos[2];

		vert1 = transformacion(var_pos1, alphas[0], T[0]/24.);
		vert2 = transformacion(var_pos2, alphas[1], T[1]/24.);
		vert3 = transformacion(var_pos3, alphas[2], T[2]/24.);

		tex_coord = TexCoord[0];
		aPos3 = vert1;
		P_min = P[0];
		gl_Position    = vec4(vert1, -vert1.z);
		gl_Position.z *= vert1.z;
		gl_Position.y *= aspect_ratio;
		EmitVertex();

		tex_coord = TexCoord[1];
		aPos3 = vert2;
		P_min = P[1];
		gl_Position    = vec4(vert2, -vert2.z);
		gl_Position.z *= vert2.z;
		gl_Position.y *= aspect_ratio;
		EmitVertex();

		tex_coord = TexCoord[2];
		aPos3 = vert3;
		P_min = P[2];
		gl_Position    = vec4(vert3, -vert3.z);
		gl_Position.z *= vert3.z;
		gl_Position.y *= aspect_ratio;
		EmitVertex();
		EndPrimitive();
	}
	else if(criterio == 579){
		vec3 lim1, lim2, lim1_up, lim2_up;
		lim1 = cons_pos[0];
		lim2 = cons_pos[0];
		float asdf1, asdf2, asdf3, asdf4;
		
		for(i = 1; i <= bines2; i++){
			lim1_up = lim1;
			lim2_up = lim2;
			lim1 = cons_pos[0] + (cons_pos[1] - cons_pos[0])/bines2*i;
			lim2 = cons_pos[0] + (cons_pos[2] - cons_pos[0])/bines2*i;
			
			for(j = 0; j < i; j++){
				var_pos1 = lim1 + (lim2 - lim1)/i*j;
				var_pos2 = lim1 + (lim2 - lim1)/i*(j + 1);
				if(i != 1)
					var_pos3 = lim1_up + (lim2_up - lim1_up)/(i - 1)*j;
				else
					var_pos3 = cons_pos[0];

				distancias[0] = simple(modulo1, modulo2, modulo3, bines2, i, j);
				distancias[1] = simple(modulo1, modulo2, modulo3, bines2, i, j + 1);
				distancias[2] = simple(modulo1, modulo2, modulo3, bines2, i - 1, j);
				
				asdf1 = simple(alphas[0], alphas[1], alphas[2], bines2, i, j);
				asdf2 = simple(alphas[0], alphas[1], alphas[2], bines2, i, j + 1);
				asdf3 = simple(alphas[0], alphas[1], alphas[2], bines2, i - 1, j);
				
				tex_coord1.x = simple(TexCoord[0].x, TexCoord[1].x, TexCoord[2].x, bines2, i, j);
				tex_coord2.x = simple(TexCoord[0].x, TexCoord[1].x, TexCoord[2].x, bines2, i, j + 1);
				tex_coord3.x = simple(TexCoord[0].x, TexCoord[1].x, TexCoord[2].x, bines2, i - 1, j);

				tex_coord1.y = simple(TexCoord[0].y, TexCoord[1].y, TexCoord[2].y, bines2, i, j);
				tex_coord2.y = simple(TexCoord[0].y, TexCoord[1].y, TexCoord[2].y, bines2, i, j + 1);
				tex_coord3.y = simple(TexCoord[0].y, TexCoord[1].y, TexCoord[2].y, bines2, i - 1, j);

				vert1 = transformacion(var_pos1, asdf1, distancias[0]);
 				vert2 = transformacion(var_pos2, asdf2, distancias[1]);
 				vert3 = transformacion(var_pos3, asdf3, distancias[2]);

				modulo1 = sqrt(dot(vert1, vert1));
				modulo2 = sqrt(dot(vert2, vert2));
				modulo3 = sqrt(dot(vert3, vert3));

				P1 = simple(P[0], P[1], P[2], bines2, i, j);
				P2 = simple(P[0], P[1], P[2], bines2, i, j + 1);
				P3 = simple(P[0], P[1], P[2], bines2, i - 1, j);
// 				P_min = min(P1, min(P2, P3));

				tex_coord = tex_coord1;
				aPos3 = vert1;
// 				P_min = P1;
				gl_Position    = vec4(vert1, -vert1.z);
				gl_Position.z *= vert1.z;
				gl_Position.y *= aspect_ratio;
				EmitVertex();

				tex_coord = tex_coord2;
				aPos3 = vert2;
// 				P_min = P2;
				gl_Position    = vec4(vert2, -vert2.z);
				gl_Position.z *= vert2.z;
				gl_Position.y *= aspect_ratio;
				EmitVertex();

				tex_coord = tex_coord3;
				aPos3 = vert3;
// 				P_min = P3;
				gl_Position    = vec4(vert3, -vert3.z);
				gl_Position.z *= vert3.z;
				gl_Position.y *= aspect_ratio;
				EmitVertex();
			    EndPrimitive();
			    
 				if((i > 1)&&(j < i - 1)){
					var_pos4 = lim1_up + (lim2_up - lim1_up)/(i - 1)*(j + 1);
					distancias[0] = simple(modulo1, modulo2, modulo3, bines2, i - 1, j + 1);
					asdf4 = simple(alphas[0], alphas[1], alphas[2], bines2, i - 1, j + 1);
					tex_coord4.x = simple(TexCoord[0].x, TexCoord[1].x, TexCoord[2].x, bines2, i - 1, j + 1);
					tex_coord4.y = simple(TexCoord[0].y, TexCoord[1].y, TexCoord[2].y, bines2, i - 1, j + 1);

					vert4 = transformacion(var_pos4, asdf4,     distancias[0]);
					modulo3 = sqrt(dot(vert4, vert4));

					P4 = simple(P[0], P[1], P[2], bines2, i - 1, j + 1);
// 					P_min = min(P2, min(P3, P4));
// 					P_min = P4;
					tex_coord = tex_coord4;
					aPos3 = vert4;
					gl_Position    = vec4(vert4, -vert4.z);
					gl_Position.z *= vert4.z;
					gl_Position.y *= aspect_ratio;
					EmitVertex();

					tex_coord = tex_coord2;
					aPos3 = vert2;
					gl_Position    = vec4(vert2, -vert2.z);
					gl_Position.z *= vert2.z;
					gl_Position.y *= aspect_ratio;
					EmitVertex();

					tex_coord = tex_coord3;
					aPos3 = vert3;
					gl_Position    = vec4(vert3, -vert3.z);
					gl_Position.z *= vert3.z;
					gl_Position.y *= aspect_ratio;
					EmitVertex();
					EndPrimitive();
 				}
			}
		}
	}
}
