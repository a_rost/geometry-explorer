#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "src/worm/parametros.h"
#include "src/worm/metrica.h"

// extern float metrica_r(float), c;
// 
// extern float metrica_r_d(float);
// 
// extern float metrica_B_d(float);
// 
// extern float metrica_A(float);
// 
// extern float metrica_A_d(float);
// 
// extern float metrica_B(float);
// 
// extern float metrica_metric(float, float, int);


float christoffel(float posicion[4], unsigned int gamma, unsigned int alpha, unsigned int beta){
	if(gamma == 0){
		if(((alpha == 0)&(beta == 1))||((alpha == 1)&(beta == 0)))
			return metrica_B_d(posicion[1])*0.5f/metrica_B(posicion[1]);
	else
		return 0.f;
	}
	else if(gamma == 1){
	if((alpha == 0)&(beta == 0))
		return powf(c, 2)*metrica_B_d(posicion[1])*0.5f/metrica_A(posicion[1]);

	else if((alpha == 1)&(beta == 1))
		return metrica_A_d(posicion[1])/2.f/metrica_A(posicion[1]);
	
	else if((alpha == 2)&(beta == 2))
		return -metrica_r(posicion[1])*metrica_r_d(posicion[1])/metrica_A(posicion[1]);

	else if((alpha == 3)&(beta== 3))
		return -metrica_r(posicion[1])*metrica_r_d(posicion[1])*powf(sinf(posicion[2]), 2)/metrica_A(posicion[1]);

	else
		return 0.f;
	}
  
	else if(gamma == 2){
		if(((alpha == 2)&(beta == 1))||((alpha == 1)&(beta == 2)))
			return metrica_r_d(posicion[1])/metrica_r(posicion[1]);

	else if((alpha == 3)&(beta == 3))
		return -sinf(posicion[2])*cosf(posicion[2]);

	else
		return 0.f;
	}
	else{
		if(((alpha == 3)&(beta == 1))||((alpha == 1)&(beta == 3)))
			return metrica_r_d(posicion[1])/metrica_r(posicion[1]);

	else if(((alpha == 3)&(beta == 2))||((alpha == 2)&(beta == 3)))
		return cosf(posicion[2])/sinf(posicion[2]);

	else
		return 0.f;
	}
}



void evolve(float position0[4], float quadvel0[4], float T, float dsigma, float *result){
	float X[4], V[4], A[4];
	int N;

	X[0] = position0[0];
	X[1] = position0[1];
	X[2] = position0[2];
	X[3] = position0[3];
	
	V[0] = quadvel0[0];
	V[1] = quadvel0[1];
	V[2] = quadvel0[2];
	V[3] = quadvel0[3];
	
	float sigma = 0.f;

	N = (int) (T/dsigma);

	for(unsigned int i = 0; i < N; i++){

		//Calculating acceleration
		for (int gamma = 0; gamma < 4; gamma++){
			A[gamma] = 0.f;
			for(int j = 0; j < 4; j++){
				for(int k = 0; k < 4; k++)
					A[gamma] += -christoffel(X, gamma, j, k)*V[j]*V[k];
			}
		}
// 		printf("X = (%f, %f, %f, %f), V = (%f, %f, %f, %f), A = (%f, %f, %f, %f)\n", X[0], X[1], X[2], X[3], V[0], V[1], V[2], V[3], A[0], A[1], A[2], A[3]);

		//Calculating displacement
  		X[0] += dsigma*V[0];
  		X[1] += dsigma*V[1];
  		X[2] += dsigma*V[2];
  		X[3] += dsigma*V[3];
  		
		//Calculating impulse
		V[0] += dsigma*A[0];
		V[1] += dsigma*A[1];
		V[2] += dsigma*A[2];
		V[3] += dsigma*A[3];

		sigma += dsigma;
	}
	result[0] = X[0];
	result[1] = X[1];
	result[2] = X[2];
	result[3] = X[3];
} 

void evolve_array(float *pos_array, float *vel_array, float *distances, unsigned int N, float dsigma, float *result_array){
	for(unsigned int i = 0; i < N; i++)
		evolve(&pos_array[4*i], &vel_array[4*i], distances[i], dsigma, &result_array[4*i]);
}
