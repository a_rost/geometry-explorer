#version 440 core

in vec3  fixed_pos2;
in float alfa_in;
in float T2;
in float tipo;
in float indice2;
in vec2  tex_coord;

out vec2  TexCoord;
out float tipo2;
out float alfas;
out vec3  cons_pos;
out float T;
out int indice;

void main(){
	cons_pos = fixed_pos2;
	T = T2;
	alfas   = alfa_in;
	tipo2   = tipo;
	TexCoord.xy = tex_coord.xy;
	indice = int(indice2);
}
