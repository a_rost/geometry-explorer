import numpy as np
import numpy.ctypeslib as npct
from ctypes import c_int, c_float
#from _ctypes import dlclose

# input type for the cos_doubles function
# must be a double array, with single dimension that is contiguous
array_1d_float = npct.ndpointer(dtype=np.float32, ndim=1, flags='CONTIGUOUS')

# load the library, using numpy mechanisms
libcd = npct.load_library("lib/solve_ode.so", ".")


# setup the return types and argument types
libcd.evolve_array.restype = None
libcd.evolve_array.argtypes = [array_1d_float, array_1d_float, array_1d_float, c_int, c_float, array_1d_float]

libcd.metrica_inicializador.restype = None
libcd.metrica_inicializador.argtypes = []

libcd.metrica_inicializador()

def solve_ode(pos_init, vel_init, distances, dsigma = 0.01):
	N = len(pos_init)
	pos_array = np.reshape(np.float32(pos_init), [N*4])
	vel_array = np.reshape(np.float32(vel_init), [N*4])
	
	result_array = np.zeros(4*N, dtype = np.float32)
	libcd.evolve_array(pos_array, vel_array, np.float32(distances), N, np.float32(dsigma), result_array)
	return np.reshape(result_array, [N, 4])
