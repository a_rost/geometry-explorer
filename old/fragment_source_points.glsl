#version 330 core

in vec3 color2;
out vec4 FragColor;


void main(){
	vec2 coord = gl_PointCoord - vec2(0.5);
	FragColor.xyz = color2*exp(-gl_PointCoord.s);
	FragColor.w   = 1.;
	if(length(coord) > 0.5)                  //outside of circle radius?
		discard;
}
