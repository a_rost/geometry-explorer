#version 330 core

in vec2 tex_coord;
in float P_min;
in vec3 aPos3;
in float eliminar;
in vec4 color;
in float render;
in float intensity;
out vec4 FragColor;

uniform sampler2D ourTexture;
uniform vec4 worm_position;
uniform vec4 posicion;
uniform float contrast;

float tau = 0.2;
float metrica_r(float x){
	if(x >= 2.)
		return x;
	else if(x >= -2.)
		return 1. + 0.25*x*x;
	else
		return -x;
}

void main(){
	float coseno, angulo, modulo, P, pi = 3.14159265;
	vec3 aPos;

	FragColor = texture(ourTexture, tex_coord)*exp(-sqrt(dot(aPos3, aPos3))/tau)*intensity*contrast;

//  	FragColor.xyz = vec3(1.0)*intensity*contrast;
	FragColor.w = 1.;
	modulo = sqrt(dot(aPos3, aPos3));
	aPos = aPos3/modulo;

	modulo = sqrt(dot(worm_position.xyz, worm_position.xyz));


	coseno = dot(worm_position.xyz/modulo, aPos);
	angulo = acos(coseno);
	P = abs(sin(angulo))*metrica_r(posicion[1]);

	if((P < P_min - 0.005)&&(eliminar == 1.))
		discard;
	if(eliminar == 2.)
		discard;
}
